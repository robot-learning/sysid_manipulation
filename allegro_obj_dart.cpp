cmake_minimum_required(VERSION 2.8.3)
project(sysid_manipulation)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  roscpp
  rospy
  sensor_msgs
  tf
  visualization_msgs
  message_generation
)

# gtsam stuff:
find_package(GTSAM REQUIRED) # Uses installed package
include_directories(${GTSAM_INCLUDE_DIR})
set(GTSAM_LIBRARIES gtsam)   # TODO: automatic search libs

## System dependencies are found with CMake's conventions
find_package(Eigen3 3.3.7 REQUIRED)

#find_package(Eigen3 3.3.7 REQUIRED)

find_package(Boost 1.58 REQUIRED COMPONENTS serialization system filesystem thread program_options date_time timer chrono regex)
find_package(DART 6.7 REQUIRED  COMPONENTS gui utils utils-urdf)

add_message_files(
  FILES
  MeasurementData.msg
  )
generate_messages(
  DEPENDENCIES
  geometry_msgs
  )
#find_package(GTSAM)
#include_directories(${GTSAM_INCLUDE_DIR}   ${Boost_INCLUDE_DIRS})
#set(GTSAM_LIBRARIES gtsam)
catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES parameter_estimation
#  CATKIN_DEPENDS geometry_msgs roscpp rospy sensor_msgs
CATKIN_DEPENDS message_runtime
#  DEPENDS system_lib
)
include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/include
${catkin_INCLUDE_DIRS}
${DART_INCLUDE_DIRS}
${EIGEN3_INCLUDE_DIR}

  )
add_definitions(${DART_DEFINITIONS})

#set(GTSAM_BOOST_LIBRARIES  Boost::serialization  Boost::system  Boost::filesystem  Boost::thread  Boost::date_time  Boost::regex)
#add_executable(gtsam_example src/gtsam_example.cpp)
#target_link_libraries(gtsam_example gtsam ${GTSAM_BOOST_LIBRARIES} ${catkin_LIBRARIES})

add_executable(sim_obj_dart src/sim_obj_dart.cpp)
target_link_libraries(sim_obj_dart ${DART_LIBRARIES} ${catkin_LIBRARIES} )

add_executable(sim_dart_headless src/sim_dart_headless.cpp)
target_link_libraries(sim_dart_headless ${DART_LIBRARIES} ${catkin_LIBRARIES} )

add_executable(gtsam_demo src/gtsam_demo.cpp)
target_link_libraries(gtsam_demo ${GTSAM_LIBRARIES} ${catkin_LIBRARIES} )

add_library(inertial_problem src/inertial_problem.cpp)
target_link_libraries(inertial_problem ${GTSAM_LIBRARIES})

add_library(inertial_problem_isam src/inertial_problem_isam.cpp)
target_link_libraries(inertial_problem_isam ${GTSAM_LIBRARIES})

add_executable(isam_inertial_est src/isam_inertial_est.cpp)
target_link_libraries(isam_inertial_est  inertial_problem_isam ${GTSAM_LIBRARIES} ${catkin_LIBRARIES})
add_dependencies(isam_inertial_est ${PROJECT_NAME}_generate_messages_cpp)


add_executable(online_inertial_est src/online_inertial_est.cpp)
target_link_libraries(online_inertial_est  inertial_problem ${GTSAM_LIBRARIES} ${catkin_LIBRARIES})
add_dependencies(online_inertial_est ${PROJECT_NAME}_generate_messages_cpp)
