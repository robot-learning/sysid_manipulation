# read tf and publish as pose stamped
import rospy
import tf
from geometry_msgs.msg import PoseStamped
import copy

import numpy as np
import sys
from rospkg import RosPack
rp=RosPack()
rp.list()
path=rp.get_path('ll4ma_opt_utils')+'/scripts'
sys.path.insert(0,path)

from tf_helper import *

if __name__=='__main__':
    rospy.init_node('tf_pose_pub')
    tf_h=tfHelper()
    rate=rospy.Rate(100)
    # tf listener:
    tf_l=tf.TransformListener()
    pub=rospy.Publisher('/object/pose',PoseStamped,queue_size=1)
    while(not rospy.is_shutdown()):

        # get pose from tf:
        pose=tf_h.get_tf_pose(tf_l,'lbr4_base_link','optoforce_model_link')
        # publish as pose
        pose_msg=PoseStamped()
        pose_msg.header.stamp=rospy.Time.now()
        pose_msg.header.frame_id='lbr4_base_link'
        
        pose_msg.pose=pose
        pub.publish(pose_msg)
        # invert pose:
        rate.sleep()
