import numpy as np
import matplotlib as mpl
mpl.rcParams.update({'font.size': 20})
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from ast import literal_eval

import seaborn as sns
import copy
import pandas as pd
from lie_group_inertia import *
mpl.rcParams['axes.unicode_minus'] = False

safeuiall=['#7F534B','#DDDDDD','#7bccc4','#2b8cbe',"#CC0000"]
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42


def rename_objects(labels):
    ycb_names={"Sim (0.25)":"Sim\n($\sigma^2$=0.25)",
               "Sim": "Sim \n($\sigma^2$=0)",
               "Sim (0.1)":"Sim\n($\sigma^2$=0.1)",
               "Sim (0.5)":"Sim\n($\sigma^2$=0.5)",
               "Sim (1)":"Sim\n($\sigma^2$=1)",
               "All Objects":"All Objects"}

    for i in range(len(labels)):
        labels[i].set_text(ycb_names[labels[i].get_text()])
    return labels

def plot_log(f_name_,obj_names):
    # read data from csv:
    data = pd.read_csv(f_name_) 
    data['Geodesic error']=0.0
    data['w'] = data['w'].apply(literal_eval)

    #obj_names = ['box_0','box_1','box_2','box_3','box_4','Sim (0.25)','Sim (no noise)']
    # for each object:
    for i in range(len(obj_names)):
        print obj_names[i]
        temp_obj=data[data['Object']==obj_names[i]]
        #print temp_obj
        gt_w = np.array((temp_obj[temp_obj['Method']=='Ground truth']['w']).iloc[0])
        ###gt_w=np.array(map(float, gt_w.split()))
        for j in range(len(temp_obj)):
            est_w = np.array(temp_obj.iloc[j]['w'])
            err = compute_error(gt_w,est_w)
            data.loc[(data['Object']==obj_names[i]) & (data['Method']==temp_obj.iloc[j]['Method']),'Geodesic error'] = err

    # add data for all objects:
    d_max=len(data)
    for j in range(d_max):
        for i in range(len(obj_names)):
            if(data.iloc[j]['Object']==obj_names[i]):
                new_row = data.iloc[j]
                new_row.loc['Object'] = 'All Objects'
                data = data.append(new_row)
    #print len(data)
    # rename method:
    data.loc[data['Method']=='No constraint, No Geodesic', 'Method'] = 'No Constraint, No Geodesic'

    plt.figure(figsize=(10,4.5))
    
    sns.set(font_scale=1.15)

    #sns.set(font_scale=1.15)
    sns.set_style("whitegrid")
    obj_order = copy.deepcopy(obj_names)#['box_0','box_1','box_2','box_3','box_4','All Real Objects']
    obj_order.append('All Objects')
    data = data[data['Method']!='Ground truth']

    # get median error error:
    print np.median(data.loc[(data['Method']=='Constraint+Geodesic') &
                   (data['Object']=='All Objects'),'Geodesic error'])
    #print data_frame
    ax=sns.barplot(y="Geodesic error", x="Object",hue = "Method",
                   order = obj_order,data=data,orient="v",linewidth=1.25,
                   dodge=True, palette = safeuiall, ci = None)

    # fix object labels:
    labels = ax.get_xticklabels()

    labels = rename_objects(labels)

    ax.set_xticklabels(labels)

    plt.setp(ax.lines, color="black")

    for i,box in enumerate(ax.artists):
        box.set_edgecolor('black')

    #ax.set(yscale="log")
    ax.set(xlabel='Object',ylabel='Inertial Error')
    handles, labels = ax.get_legend_handles_labels()
    ax.legend_.remove()
    
    
    plt.figlegend(handles,labels,loc='upper center',ncol=3,bbox_to_anchor=(0.45, 1.17))
    plt.savefig('/home/bala/bt_inertial_error.eps', bbox_inches='tight')

    #plt.show()
def plot_baseline(f_name_,obj_names):
    # read data from csv:
    data = pd.read_csv(f_name_) 
    data['Geodesic error']=0.0
    data['w'] = data['w'].apply(literal_eval)

    #obj_names = ['box_0','box_1','box_2','box_3','box_4','Sim (0.25)','Sim (no noise)']
    # for each object:
    for i in range(len(obj_names)):
        print obj_names[i]
        temp_obj=data[data['Object']==obj_names[i]]
        #print temp_obj
        gt_w = np.array((temp_obj[temp_obj['Method']=='Ground truth']['w']).iloc[0])
        ###gt_w=np.array(map(float, gt_w.split()))
        for j in range(len(temp_obj)):
            est_w = np.array(temp_obj.iloc[j]['w'])
            err = compute_error(gt_w,est_w)
            data.loc[(data['Object']==obj_names[i]) & (data['Method']==temp_obj.iloc[j]['Method']),'Geodesic error'] = err

    # add data for all objects:
    d_max=len(data)
    for j in range(d_max):
        for i in range(len(obj_names)):
            if(data.iloc[j]['Object']==obj_names[i]):
                new_row = data.iloc[j]
                new_row.loc['Object'] = 'All Objects'
                data = data.append(new_row)
    #print len(data)
    # rename method:
    data.loc[data['Method']=='No constraint, No Geodesic', 'Method'] = 'No Constraint, No Geodesic'

    plt.figure(figsize=(10,4.5))
    
    sns.set(font_scale=1.15)

    #sns.set(font_scale=1.15)
    sns.set_style("whitegrid")
    obj_order = copy.deepcopy(obj_names)#['box_0','box_1','box_2','box_3','box_4','All Real Objects']
    obj_order.append('All Objects')
    data = data[data['Method']!='Ground truth']

    
    # get median error error:
    print np.median(data.loc[(data['Method']=='Constraint, No Geodesic') &
                   (data['Object']=='All Objects'),'Geodesic error'])

    data = data[(data['Method']!='Ground truth') & (data['Method']!='No Constraint, No Geodesic') &
                (data['Method']!='Linear') & (data['Method']!='Constraint+Geodesic')]

    #print data_frame
    ax=sns.barplot(y="Geodesic error", x="Object",hue = "Method",
                   order = obj_order,data=data,orient="v",linewidth=1.25,
                   dodge=True, palette = safeuiall, ci = None)

    # fix object labels:
    labels = ax.get_xticklabels()

    #labels = rename_objects(labels)

    ax.set_xticklabels(labels)

    plt.setp(ax.lines, color="black")

    for i,box in enumerate(ax.artists):
        box.set_edgecolor('black')

    #ax.set(yscale="log")
    ax.set(xlabel='Object',ylabel='Inertial Error')
    handles, labels = ax.get_legend_handles_labels()
    ax.legend_.remove()
    
    
    plt.figlegend(handles,labels,loc='upper center',ncol=3,bbox_to_anchor=(0.5, 1.17))
    plt.savefig('/home/bala/bt_inertial_error.eps', bbox_inches='tight')

    #plt.show()

def plot(f_name_, obj_names):
        # read data from csv:
    data = pd.read_csv(f_name_) 
    data['Geodesic error']=0.0
    data['w'] = data['w'].apply(literal_eval)

    #obj_names = ['box_0','box_1','box_2','box_3','box_4','Sim (0.25)','Sim (no noise)']
    # for each object:
    for i in range(len(obj_names)):
        temp_obj=data[data['Object']==obj_names[i]]
        gt_w = np.array((temp_obj[temp_obj['Method']=='Ground truth']['w']).iloc[0])
        #print gt_w
        ###gt_w=np.array(map(float, gt_w.split()))
        for j in range(len(temp_obj)):
            est_w = np.array(temp_obj.iloc[j]['w'])
            err = compute_error(gt_w,est_w)
            data.loc[(data['Object']==obj_names[i]) & (data['Method']==temp_obj.iloc[j]['Method']),'Geodesic error'] = err
            
    # add data for all objects:
    d_max=len(data)
    for j in range(d_max):
        for i in range(len(obj_names)):
            if(data.iloc[j]['Object']==obj_names[i]):
                new_row = data.iloc[j]
                new_row.loc['Object'] = 'All Objects'
                data = data.append(new_row)
    #print len(data)

    # rename method:
    data.loc[data['Method']=='No constraint, No Geodesic', 'Method'] = 'No Constraint, No Geodesic'

    plt.figure(figsize=(10,4))
    
    sns.set(font_scale=1.15)

    #sns.set(font_scale=1.15)
    sns.set_style("whitegrid")
    obj_order = copy.deepcopy(obj_names)#['box_0','box_1','box_2','box_3','box_4','All Real Objects']
    obj_order.append('All Objects')

    #obj_order = ['box_0','box_1','box_2','box_3','box_4','All Real Objects']
    data = data[(data['Method']!='Ground truth') & (data['Method']!='No Constraint, No Geodesic') &
    (data['Method']!='Linear')]

    #data = data[(data['Method']!='Ground truth') & (data['Method']!='No Constraint, No Geodesic')]
    #data = data[(data['Method']!='Ground truth')]

    #print data_frame
    ax=sns.barplot(y="Geodesic error", x="Object",hue = "Method",
                   order = obj_order,data=data,orient="v",linewidth=1.25,
                   dodge=True, palette = safeuiall[-2:],ci=None)

    labels = ax.get_xticklabels()

    labels = rename_objects(labels)
    ax.set_xticklabels(labels)

    plt.setp(ax.lines, color="black")

    for i,box in enumerate(ax.artists):
        box.set_edgecolor('black')

    #ax.set(yscale="log")
    ax.set(xlabel='Object',ylabel='Inertial Error')
    handles, labels = ax.get_legend_handles_labels()
    ax.legend_.remove()

    
    #plt.figlegend(handles,labels,loc='upper center',ncol=3)
    plt.savefig('/home/bala/bt_inertial_zoom_error.eps', bbox_inches='tight')

    #plt.show()

if __name__=='__main__':
    f_name="../data/bt_dart_results.csv"
    f_name_2="../data/sysid_bt_results.csv"
    #obj_names = ['box_0','box_1','box_2','box_3','box_4']
    obj_names = ['Sim','Sim (0.1)','Sim (0.25)','Sim (0.5)','Sim (1)']

    #obj_names = ['box_0','box_1','box_2','box_3','box_4','Box_1_600','box_1_l900','Box_2_l600']
    #plot(f_name,obj_names)
    plot_log(f_name,obj_names)
    #plot(f_name,obj_names)
    #plot_baseline(f_name,obj_names)
