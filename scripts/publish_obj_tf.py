import rospy
import tf
from geometry_msgs.msg import PoseStamped
import copy
webcam_pose=None
xtion_pose=None
import numpy as np
def handle_pose_object(msg, object_name, cam_name):
    br = tf.TransformBroadcaster()
    br.sendTransform((msg.pose.position.x, msg.pose.position.y, msg.pose.position.z),
                     (msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w),
                     rospy.Time.now(),
                     object_name,
                     cam_name)
#def pose_cb(msg):
#    handle_pose_object(msg, "board", msg.header.frame_id)
def pose_cb1(msg):
    global xtion_pose
    xtion_pose=msg
    #handle_pose_object(msg, "board", msg.header.frame_id)
def pose_cb2(msg):
    global webcam_pose
    webcam_pose=msg
    #handle_pose_object(msg, "board", msg.header.frame_id)
   
if __name__=='__main__':
    rospy.init_node('pose_tf_pub')
    
    rospy.Subscriber('/xtion/objectdetection_pose',
                     PoseStamped,
                     pose_cb1)

    rospy.Subscriber('/usb_cam/objectdetection_pose',
                     PoseStamped,
                     pose_cb2)

    rate=rospy.Rate(100)
    br1 = tf.TransformBroadcaster() 
    br2 = tf.TransformBroadcaster()
   
    while(not rospy.is_shutdown()):
        #print webcam_pose
        if(webcam_pose!=None):
            msg=copy.deepcopy(webcam_pose)
        
            # invert pose:
            R=tf.transformations.quaternion_matrix([msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w])
            T=np.eye(4)
            T=R
            T[0:3,3]=np.array([msg.pose.position.x, msg.pose.position.y, msg.pose.position.z])
            T=np.linalg.inv(T)
            q=tf.transformations.quaternion_from_matrix(T)
            br1.sendTransform((T[0,3],T[1,3],T[2,3]),
                              (q[0],q[1],q[2],q[3]),
                             rospy.Time.now(),
                              msg.header.frame_id,'board')
            #print msg.header.frame_id
        if(xtion_pose!=None):
            msg=copy.deepcopy(xtion_pose)
            
            # invert pose:
            R=tf.transformations.quaternion_matrix([msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w])
            T=np.eye(4)
            T=R
            T[0:3,3]=np.array([msg.pose.position.x, msg.pose.position.y, msg.pose.position.z])
            #T=np.linalg.inv(T)
            q=tf.transformations.quaternion_from_matrix(T)
            br2.sendTransform((T[0,3],T[1,3],T[2,3]),
                              (q[0],q[1],q[2],q[3]),
                              rospy.Time.now(),
                              'board',msg.header.frame_id)
            '''
            br2.sendTransform((msg.pose.position.x, msg.pose.position.y, msg.pose.position.z),
                             (msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w),
                             rospy.Time.now(),
                              'board', msg.header.frame_id)
            '''
        rate.sleep()
