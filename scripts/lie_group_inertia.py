import numpy as np
import scipy
from scipy.linalg import logm, expm
#from transformations import *

def project_regressor():
    mat = np.zeros((4,4))
    return mat
def project_inertia(m,r_c,h):
    mat = np.zeros((4,4))

    mat[0:3,3] = r_c
    mat[3,0:3] = r_c
    mat[3,3] = m

    # trace:
    i_mat = np.matrix([[h[0],h[3],h[4]],
                       [h[3],h[1],h[5]],
                       [h[4],h[5],h[2]]])

    i_trace = 0.5 * np.trace(i_mat) * np.eye(3) - i_mat
    mat[0:3,0:3] = i_trace
    return mat

def retract_inertia(p_mat):
    m = p_mat[3,3]

    r_c = np.ravel(p_mat[0:3,3])

    i_mat = np.trace(p_mat[0:3,0:3]) * np.eye(3) - p_mat[0:3,0:3]

    h=np.array([i_mat[0,0],i_mat[1,1],i_mat[2,2],i_mat[0,1],i_mat[0,2],i_mat[1,2]])
    
    
    return m,r_c,h
def exp_inertia(V):
    P = np.zeros(4,4)
    return P

def log_inertia(P):
    S = logm(P)
    return S

def check(mat):
    return np.all(np.linalg.eigvals(mat) > 0)

def geodesic(p1,p2):
    
    eig_vals=np.linalg.eigvals(np.matrix(np.linalg.inv(p1)) * np.matrix(p2))
    
    #print eig_vals
    dist = np.sqrt(np.sum(np.log(eig_vals)**2))
    #dist = np.sqrt(np.sum(np.log(eig_vals)**2))
        
    return dist

def geodesic_trace(p1,p2):
    #print np.matrix(np.linalg.inv(p1)) * np.matrix(p2)
    #print np.matrix(np.linalg.inv(p1)) * np.matrix(p2)
    #print np.trace(np.matrix(np.linalg.inv(p1)) * np.matrix(p2))
    
    dist= abs(4.0-(np.trace(np.matrix(np.linalg.inv(p1)) * np.matrix(p2))))
    
    #dist = np.sqrt(np.sum(np.log(eig_vals)**2))
    #dist = np.sqrt(np.sum(np.log(eig_vals)**2))
        
    return dist

# how to add regressor to inertia matrix?
def map_product(w):
    # w = m,r_x,r_y,r_z,i_xx,i_yy, i_zz, i_xy, i_xz, i_yz
    mat = np.zeros((4,4))
    mat[0:3,0:3] = np.matrix([[w[5]+w[6],-0.5*w[7],-0.5*w[9]],
                              [-0.5*w[7],w[6]+w[4],-0.5*w[8]],
                              [-0.5*w[9],-0.5*w[8],w[4]+w[5]]])
    mat[3,3] = w[0]
    mat[3,0:3] = 0.5*np.ravel(w[1:4])
    mat[0:3,3] = 0.5*np.ravel(w[1:4])

    return mat
def compute_error(w_des,w_est):
    i_des = project_inertia(w_des[0],w_des[1:4],w_des[4:])
    i_est = project_inertia(w_est[0],w_est[1:4],w_est[4:])

    return geodesic_trace(i_des,i_est)

if __name__=='__main__':
    m = 0.05
    r_c = np.array([0.07,-0.03,0.03])
    h = np.array([0.0001,0.0001,0.0001,0,0,0])

    #print w_est

    #w_est = [0.170310640597,1.24547314932,0.901643362751,0.583802224411,0.466739533794,0.495819808622,0.712594496485,0.0146134123324,-0.039157735727,-0.0940664890143]

    #print w_des
    #print w_des[1:4]
    # get positive definite matrix:
    i_des = project_inertia(w_des[0],w_des[1:4],w_des[4:])
    i_est = project_inertia(w_est[0],w_est[1:4],w_est[4:])

    #print i_est#np.matrix(np.linalg.inv(i_des)) * np.matrix(i_est)
    #print i_est

    #exit()
    #print np.linalg.eigvals(np.matrix(np.linalg.inv(i_des))*np.matrix(i_est))
    #print i_est
    #print map_product(w)
    print 'g trace'
    print geodesic_trace(i_des,i_est)
    #print 'g'
    #print geodesic(i_des,i_est)
        
    exit()
    # check if positive definite
    #
    dist = log_inertia(i_2) - log_inertia(i_1)
    
    dist_1 =  np.linalg.inv(i_1) * i_2
    
    #print dist
    #print expm(dist)
    #print dist_1
    #print retract_inertia(dist_1)
    #print retract_inertia(expm(dist))
    #print check(expm(dist))
    
    #print 
    #print check(dist)

    '''
    # create two rotation matrices:
    R_1=np.matrix(euler_matrix(1.0,0,0.0)[0:3,0:3])
    R_2=np.matrix(euler_matrix(0,0,1.0)[0:3,0:3])
    dt=1.0 # seconds
    #print R_1,R_2
    #print R_1*R_1.T
    # find delta:
    r_1= np.real(logm(R_1))
    r_2= np.real(logm(R_2))
    #print R_1
    delta_r=(1.0/dt)*(r_2-r_1)
    #print np.linalg.det(R_1), np.linalg.det(delta_R)

    print 'Angular velocities: ',delta_r[2,1],delta_r[0,2],delta_r[1,0]

    R_3=np.matrix(euler_matrix(0,1.0,0.0)[0:3,0:3])
    r_3=np.real(logm(R_3))
    delta_r2=(1.0/dt)*(r_3-r_2)

    dd_r=(1.0/dt)*(delta_r2-delta_r)
    
    print 'Angular accelerations: ',dd_r[2,1],dd_r[0,2],dd_r[1,0]
    
    #print expm(delta_r)
    

    # get euler angles?

    #print expm(logm(delta_R))

    # for SE(3)
    T_1=np.matrix(euler_matrix(0.0,0,0.0))
    T_2=np.matrix(euler_matrix(0,0,1.0))

    #T_1[0:3,3]=np.matrix([0,1,0]).T
    #T_2[0:3,3]=np.matrix([1,1,0]).T


    
    t_1=np.matrix(np.real(logm(T_1)))
    t_2=np.matrix(np.real(logm(T_2)))

    t_1[0:3,3]=np.matrix([0,1,0]).T
    t_2[0:3,3]=np.matrix([1,1,0]).T
    
    '''
