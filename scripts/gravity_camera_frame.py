import rospy
import tf
import numpy as np

if __name__=='__main__':
    # get rotation from camera to gravity

    c_r_w = np.array([0.110, -0.069, -0.001, 0.992]) # qx,qy,qz,qw

    w_g = np.matrix([0,0.1,0])

    # build rotation matrix
    c_R_w=tf.transformations.quaternion_matrix(c_r_w)

    c_g = c_R_w[0:3,0:3] * w_g.transpose()
    c_g = np.ravel(c_g)
    print c_g[0],c_g[1],c_g[2]
    
