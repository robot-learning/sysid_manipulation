import rospy
import sys
import tf
from rospkg import RosPack

rp=RosPack()
rp.list()
path=rp.get_path('trajectory_smoothing')+'/scripts'
sys.path.insert(0,path)
from trajectory_pub import *
path=rp.get_path('in_hand_controllers')+'/scripts'
sys.path.insert(0,path)
from hand_client import *

path=rp.get_path('dope')+'/src'
sys.path.insert(0,path)
from dope_client_demo import dopeDetect

path=rp.get_path('grasp_planner')+'/scripts'
sys.path.insert(0,path)
from grasp_client import * 
path=rp.get_path('ll4ma_opt_utils')+'/scripts'
sys.path.insert(0,path)
from tf_helper import tfHelper
#from robot_class import robotInterface
if __name__=='__main__':
    pp_obj=Pose()
    
    pp_obj.position.x=0.08155324627862542
    pp_obj.position.y=-0.07422381999594901
    pp_obj.position.z=-0.06
    pp_obj.orientation.x= 0.2777115210971999
    pp_obj.orientation.y=0.013704288538276805
    pp_obj.orientation.z=0.9603956039827185
    pp_obj.orientation.w= 0.018132495035394276

    
    obj_name='bleach'
    env_obj=['small_table']#,'small_table']
    g_plan=graspPlanner()
    #hand_client=handClient()
    #lbr4_client=robotInterface(init_node=False)
    rate=rospy.Rate(10)
    tf_helper=tfHelper()
    tf_listener=tf.TransformListener()
    while(not g_plan.got_state):
        rate.sleep()
    #print "ready"
    robot_js=JointState()
    robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
    robot_js.position=g_plan.joint_state.position[0:7]

    
    hand_js=JointState()
    hand_js.name=['index_joint_0','index_joint_1','index_joint_2','index_joint_3',
                  'middle_joint_0','middle_joint_1', 'middle_joint_2','middle_joint_3',
                  'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                  'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2','thumb_joint_3']
    hand_js.position=g_plan.joint_state.position[7:]

    hand_preshape=copy.deepcopy(hand_js)
    hand_preshape.position=np.array([0.0,0.0,0.3,0.3, 0.0, 0.0, 0.3, 0.3,
                                     0.0,0.0,0.3,0.3, 1.4,0.0,0.0,0.0])
    arm_j0=robot_js
    hand_j0=hand_js

    #arm_initial=copy.deepcopy(arm_j0)
    #arm_initial.position=np.zeros(7)

    # read environment poses:
    env_poses=[]
    for i in range(len(env_obj)):
        e_pose=tf_helper.get_tf_pose(tf_listener,'lbr4_base_link',env_obj[i])
        env_poses.append(e_pose)

    env_poses[0].position.z+=0.02
    # read object pose from dope:
    dope_detect=dopeDetect(False,'/camera/rgb/image_raw')
    obj_pose=[]
    while(len(obj_pose)==0):
        obj_pose=dope_detect.detect_object(obj_name)
        
        
    obj_pose=obj_pose[0]
    #print obj_pose
    
    b_T_c=tf_helper.get_T(tf_listener,'lbr4_base_link',obj_pose.header.frame_id)
    # transform object pose
    c_T_obj=tf_helper.get_T_pose(obj_pose.pose)
    b_T_obj=b_T_c*c_T_obj
    obj_pose=tf_helper.get_pose(b_T_obj)
    # get palm pose in robot frame:
    obj_T_pp=tf_helper.get_T_pose(pp_obj)
    b_T_pp=b_T_obj*obj_T_pp
    des_pp=tf_helper.get_pose(b_T_pp)
    
    #print env_poses
    g_plan.update_plan_env(obj_pose,env_poses,srv_name='/grasp_preshape_planner/update_world')
    print 'updated preshape pose'
    g_plan.update_plan_env(obj_pose,env_poses,srv_name='/grasp_planner/update_world')

    g_plan.update_plan_env(Pose(),env_poses,srv_name='/ll4ma_planner/update_world')

    #exit()
    #raw_input('plan?')
    jtraj,p_pose=g_plan.get_preshape_plan(arm_j0,hand_j0,hand_preshape,des_pp)
    g_plan.viz_traj(jtraj,t_name='grasp_planner/preshape')
    #exit()
    raw_input('grasp plan?')
        
    jtraj,p_pose,_,_,_=g_plan.get_grasp_plan(arm_j0,hand_j0,joint_traj=jtraj)

    #raw_input("plan full trajectory?")
    jtraj,p_pose=g_plan.get_palm_traj(arm_j0,hand_j0,hand_preshape,p_pose,jtraj)


    g_plan.viz_traj(jtraj,t_name='grasp_planner/full_traj')

    # get smooth trajectory:

    
    raw_input("send to robot?")

    # move hand to preshape

    # send joint trajectory to arm

    # reset bt and close hand

    # increase stiffness

    
    # lift object?

    des_pose=copy.deepcopy(p_pose)
    des_pose.position.z+=0.3

    
    # get palm traj:
    jtraj,_=g_plan.get_palm_traj(arm_j0,hand_j0,hand_preshape,des_pose)
    
    g_plan.viz_traj(jtraj,t_name='grasp_planner/full_traj')

    # get smooth trajectory:

    
    # send to robot:
    raw_input('Lift object(send to robot)?')
    #

    ## TODO:
    # plan different trajectories:

    
    # place object:
    des_pose=copy.deepcopy(p_pose)
    des_pose.position.z+=0.05


    jtraj,_=g_plan.get_palm_traj(arm_j0,hand_j0,hand_preshape,des_pose)
    
    g_plan.viz_traj(jtraj,t_name='grasp_planner/full_traj')
    
    raw_input('Move object to table (send to robot)?')



    
    des_pose=copy.deepcopy(p_pose)
    
    jtraj,_=g_plan.get_palm_traj(arm_j0,hand_j0,hand_preshape,des_pose)
    
    g_plan.viz_traj(jtraj,t_name='grasp_planner/full_traj')
    
    raw_input('Place object?')
    
    # Enable placement detection service:


    
