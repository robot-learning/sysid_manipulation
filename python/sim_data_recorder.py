import rospy
from geometry_msgs.msg import WrenchStamped,PoseStamped
import tf_helper
import copy
import message_filters
import numpy as np
import tf.transformations
import tf
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle
import sys
from sysid_manipulation.msg import FullMeasurementData
from geometry_msgs.msg import PoseStamped,WrenchStamped

from biotac_sensors.msg import BioTacHand,BioTacTareHand


class recordData(object):
    def __init__(self):
        f_topics=['/sim/force1',
                  '/sim/force2',
                  '/sim/force3',
                  '/sim/force4']
        self.base_frame="world"
        self.palm="object"
        self.obj_frame="object"
        #self.ft_frame="optoforce_sensor_link"

        self.finger_frames=['object','object','object','object']
        self.cpt_frames=['f1','f2','f3','f4']

        self.tf=tf_helper.tfHelper()
        self.tf_l=tf.TransformListener()
        self.f1_sub=message_filters.Subscriber(f_topics[0],WrenchStamped)
        self.f2_sub=message_filters.Subscriber(f_topics[1],WrenchStamped)
        self.f3_sub=message_filters.Subscriber(f_topics[2],WrenchStamped)
        self.f4_sub=message_filters.Subscriber(f_topics[3],WrenchStamped)
        #self.ft_sub=message_filters.Subscriber('/ft_sensor/negated',WrenchStamped)

        # frames:
        
        self.palm_T_ftips=[PoseStamped() for i in range(len(self.finger_frames))]
        self.ftips_T_cpts=[PoseStamped() for i in range(len(self.cpt_frames))]
        self.gvec=WrenchStamped()
        self.gvec.wrench.force.x=0
        self.gvec.wrench.force.y=0
        self.gvec.wrench.force.z=-9.8
        self.base_T_palm=PoseStamped()
        self.palm_T_obj=PoseStamped()
        self.palm_T_ft=PoseStamped()        
        self.ft_force=WrenchStamped()
        self.cpt_forces=[WrenchStamped() for i in range(len(f_topics))]
        

        self.ts = message_filters.ApproximateTimeSynchronizer([self.f1_sub, self.f2_sub, self.f3_sub, self.f4_sub], 100,slop=0.1)

        self.ts.registerCallback(self.msg_cb)
        self.time=None
        self.got_data=False
        self.got_TF=False
    def bt_cb(self,msg):
        self.bt_data=msg
    def bt_tare_cb(self,msg):
        self.bt_tare_data=msg
    def msg_cb(self,f1_msg,f2_msg,f3_msg,f4_msg,ft_msg=None,bt_msg=None,bt_tare_msg=None):

        self.cpt_forces[0].wrench=f1_msg.wrench
        self.cpt_forces[1].wrench=f2_msg.wrench
        self.cpt_forces[2].wrench=f3_msg.wrench
        self.cpt_forces[3].wrench=f4_msg.wrench
        #self.ft_force.wrench=ft_msg.wrench
        
        self.time=f1_msg.header.stamp
        
        #print "got msg data"
        self.get_tf_data(self.time)
        #print "Got tf data"
        self.got_data=True
    def get_tf_data(self,time):
        # get tf dataset:
        time=rospy.Time(0)
        # lbr4_base_link to palm_link
        self.base_T_palm.pose=self.tf.get_tf_pose(self.tf_l,self.base_frame,self.palm,time)
        
        # palm to object
        self.palm_T_obj.pose=self.tf.get_tf_pose(self.tf_l,self.palm,self.obj_frame,time)
        
        # palm to fingertips, contact points
        for i in range(len(self.finger_frames)):
            self.palm_T_ftips[i].pose=self.tf.get_tf_pose(self.tf_l,self.palm,self.finger_frames[i])
            self.ftips_T_cpts[i].pose=self.tf.get_tf_pose(self.tf_l,self.finger_frames[i],self.cpt_frames[i])
 

        # get palm to force torque sensor:
        #self.palm_T_ft.pose=self.tf.get_tf_pose(self.tf_l,self.ft_frame,self.palm,time)
        '''
        # get gravity
        g_vec=np.matrix(self.g_vec).T
        T_mat=copy.deepcopy(self.tf.get_T(self.tf_l,self.obj_frame,self.g_frame,time))#[0:3,0:3]
        g=np.ravel(T_mat*g_vec)
        self.g=copy.deepcopy(g[0:3])
        # get fingertip contact point vectors:
        for i in range(len(self.f_frames)):
            f_pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,self.f_frames[i],time)
            f_arr=np.matrix([self.forces[i][0],self.forces[i][1],self.forces[i][2],0.0]).T
            f_mat=self.tf.get_T_pose(f_pose)
            self.f_loc[i]=np.array([f_pose.position.x,f_pose.position.y,f_pose.position.z,f_pose.orientation.x,f_pose.orientation.y,f_pose.orientation.z,f_pose.orientation.w])

            self.forces[i]=np.ravel(f_mat*f_arr)[0:3]
            
            #if(i==0):
                #pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,self.g_frame,time)
        # transform ft data:
        ft_pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,self.ft_frame,time)
        f_arr=np.matrix([self.ft_force[0],self.ft_force[1],self.ft_force[2],0.0]).T
        t_arr=np.matrix([self.ft_torque[0],self.ft_torque[1],self.ft_torque[2],0.0]).T
        f_mat=self.tf.get_T_pose(ft_pose)
        self.ft_loc=np.array([ft_pose.position.x, ft_pose.position.y, ft_pose.position.z, ft_pose.orientation.x, ft_pose.orientation.y, ft_pose.orientation.z, ft_pose.orientation.w])

        self.ft_force=np.ravel(f_mat*f_arr)[0:3]

        # transform torque:
        self.ft_torque=np.ravel(f_mat*t_arr)[0:3]
        
        o_pose=copy.deepcopy(self.tf.get_tf_pose(self.tf_l,self.g_frame,self.obj_frame,time))
        
        quaternion = (
            o_pose.orientation.x,
            o_pose.orientation.y,
            o_pose.orientation.z,
            o_pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)


        self.obj_pose=np.array([o_pose.position.x, o_pose.position.y,o_pose.position.z,euler[0],euler[1],euler[2]])
        '''
        self.got_TF=True
            
if __name__=='__main__':
    rospy.init_node('data_logger')
    rate=rospy.Rate(100)
    r_data=recordData()

    file_name=sys.argv[1]
    #data_dir='/hdd/data1/sysid_grasp/pickle/'+file_name
    data_dir='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/'+file_name
        
    print data_dir

    data=[]

    i=0
    pub = rospy.Publisher('/sysid/data', FullMeasurementData, queue_size=10)
    
    raw_input("start recording?")
    try:
        while (not rospy.is_shutdown()):# and i<2000:
            #print r_data.got_data
            if(r_data.got_data):
                # publish to ros
                d_msg=FullMeasurementData()
                d_msg.header.stamp=rospy.Time.now()
                
                # poses:
                d_msg.base_T_palm=r_data.base_T_palm
                d_msg.palm_T_obj=r_data.palm_T_obj
                d_msg.palm_T_ftips=r_data.palm_T_ftips
                d_msg.ftips_T_cpts=r_data.ftips_T_cpts
                d_msg.palm_T_ft=r_data.palm_T_ft
                
                # forces:
                d_msg.ft_force=r_data.ft_force
                d_msg.cpt_forces=r_data.cpt_forces
                d_msg.base_gvec=r_data.gvec
            
                pub.publish(d_msg)
                data.append(copy.deepcopy(d_msg))
                r_data.got_data=False
                print i
                i+=1
                #print data[-1].base_T_palm
            rate.sleep()
    except:
        pass
    print "writing data"
    #exit()
    #rospy.spin()
    # create a pickle file
    print len(data)
    pickle.dump( data, open( data_dir, "wb" ) )
