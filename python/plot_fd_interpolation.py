# This file implements pose interpolation and plots fd acc data
import numpy as np
import copy
import matplotlib.pyplot as plt

if __name__=='__main__':
    #
    x_init = 0.0
    T = 5000
    dt = 0.01
    x_dd = 0.1
    x_d = 0.0
    x_t = x_init
    x_t_arr = [x_t]
    t_arr= [0.0]
    a_arr = [0.3,-0.2]
    t_an_arr = [0.0]
    x_an_arr = [x_t]
    for i in range(T):
        #x_dd = np.random.randn()*20.0
            
        x_t = x_t + x_d*dt +  0.5 * x_dd * dt * dt
        x_d = x_d + x_dd * dt
        #print x_d
        if(i%100 ==0):
            x_t_arr.append(x_t)
            t_arr.append(dt*(i+1))
        x_an_arr.append(x_t)
        t_an_arr.append(dt*(i+1))

    #print x_t_arr
    # finite difference to get accel, vel
    # at a larger dt
    x_init = x_t_arr[0]
    x_arr = [x_t_arr[0],x_t_arr[1]]
    x_t = x_arr[-1]
    fd_t_arr = [0.0,t_arr[1]]
    print 'fd'
    for i in range(2,len(x_t_arr)):
        v_21 = (x_t_arr[i-1] - x_t_arr[i-2])/(t_arr[i-1] - t_arr[i-2])
        
        v_10 = (x_t_arr[i] - x_t_arr[i-1])/(t_arr[i] - t_arr[i-1])
        
        acc = (v_10 - v_21)/(t_arr[i]-t_arr[i-1])
        #print v_10 - acc * 0.5 * (t_arr[i]-t_arr[i-1])
        x_t = x_t + v_10 * (t_arr[i] - t_arr[i-1]) #+ 0.5 * acc *  (t_arr[i]-t_arr[i-1])**2
        #fd_t_arr
        fd_t_arr.append(t_arr[i])
        x_arr.append(x_t)
    #print (np.array(x_t_arr) - np.array(x_arr))
    print np.sum((np.array(x_t_arr) - np.array(x_arr))**2)

    plt.plot(t_an_arr,x_an_arr,'r',fd_t_arr,x_arr,'b')
    plt.show()
    exit()
    x_init = x_t_arr[0]
    x_arr = [x_t_arr[0],x_t_arr[1],x_t_arr[2]]
    x_t = x_arr[-1]
    fd_t_arr = [0.0,t_arr[1],t_arr[2]]
    print 'fd'
    for i in range(3,len(x_t_arr)):
        v_21 = (x_t_arr[i-2] - x_t_arr[i-3])/(t_arr[i-2] - t_arr[i-3])
        
        v_10 = (x_t_arr[i-1] - x_t_arr[i-2])/(t_arr[i-1] - t_arr[i-2])
        
        acc = (v_10 - v_21)/(t_arr[i-1]-t_arr[i-2])
        #print v_10 - acc * 0.5 * (t_arr[i]-t_arr[i-1])
        x_t = x_t + v_10 * (t_arr[i-1] - t_arr[i-2])+ 0.5 * acc *  (t_arr[i-1]-t_arr[i-2])**2
        #fd_t_arr
        fd_t_arr.append(t_arr[i])
        x_arr.append(x_t)

    
    print np.sum((np.array(x_t_arr) - np.array(x_arr))**2)
    plt.plot(t_arr,x_t_arr,'r',fd_t_arr,x_arr,'b*')
    plt.show()

