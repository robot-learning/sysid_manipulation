import numpy as np
import matplotlib as mpl
#mpl.use('TKAgg')
import matplotlib.pyplot as plt
import copy
from matplotlib import animation

from matplotlib.animation import FuncAnimation
plt.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'

class visualizeBox(object):
    def __init__(self,dim_x,dim_y):
        self.dims=[dim_x,dim_y]
        self.diag=np.sqrt((dim_x/2)**2+(dim_y/2)**2)
        #print self.diag
        self.fig = plt.figure(figsize=(10, 10))
        self.ax = plt.axes(xlim=(-10, 10), ylim=(-10, 10))

        #self.ax = self.fig.add_subplot(111)#plt.axes(xlim=(-1, 1), ylim=(-1, 1),autoscale_on=False)
    def rot(self,angle):
        r_mat=np.zeros((2,2))
        r_mat[0,0]=np.cos(angle)
        r_mat[0,1]=-np.sin(angle)
        r_mat[1,0]=np.sin(angle)
        r_mat[1,1]=np.cos(angle)
        return r_mat

    def get_patch(self,s0,color='r',label=None):

        rotate_offset=np.ravel(self.rot(s0[2])*
                               np.matrix([-self.dims[0]/2.0,-self.dims[1]/2.0]).T)

        low_edges=rotate_offset+s0[0:2]
        patch=plt.Rectangle((low_edges[0],low_edges[1]),
                              self.dims[0],self.dims[1],np.rad2deg(s0[2]),fc=color,label=label)

    
        return patch

    def init_animation(self):
        
        self.ln, = plt.plot([], [],animated=True)
               
 
        self.ax.set_xlim([-10.0,10.0])
        self.ax.set_ylim([-10.0,10.0])
        self.p1=self.get_patch([0.0,0.0,0.0],'g',label='oracle')
        self.ax.add_patch(self.p1)
        return self.p1,
    def update_animation(self,data):
        self.ax.clear()
        colors=['g','r','b']
        patch1=self.get_patch(data[0],colors[0],label='oracle')
        plt.gca().add_patch(patch1)

        self.p1=patch1
        #return self.p1,
        #self.ln.add_patch(patch1)
        
        for i in range(1,len(data)):
            patch1=self.get_patch(data[i],colors[i],label='est_'+str(i))
            plt.gca().add_patch(patch1)
        #patch2=self.get_patch(data[1],'r',label="est")
        #plt.gca().add_patch(patch2)
        plt.legend(prop={'size': 12}, loc='upper center', bbox_to_anchor=(0.55, 1.0))
        #self.ax.set_xlim([-5.0,5.0])
        #self.ax.set_ylim([-5.0,5.0])

        #plt.axis("scaled")
        #plt.axis.set_xlim([-5.0,5.0])
        #plt.axis.set_ylim([-5.0,5.0])

        return self.ln,self.ax
    def show_animation(self,data_):
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

        ani = FuncAnimation(self.fig, self.update_animation, frames=data_,
                            init_func=self.init_animation, blit=True, interval=30,repeat=True)

        plt.axis('scaled')
        #plt.grid(color='r', linestyle='-', linewidth=2)
        plt.show()
        plt.close()
        #ani.save('/home/bala/animation.gif', writer='imagemagick')

        #ani.save('/home/bala/trial_0.mp4')
        #plt.show()
        #plt.close()
