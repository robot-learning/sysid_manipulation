#!/bin/bash

rosbag record __name:=record_bag \
       -O $1 \
       /biotac_pub \
       /biotac_tare_pub \
       /biotac/sigmoid \
       /biotac/contact_state \
       /camera/depth_registered/image_raw \
       /camera/depth_registered/camera_info \
       /camera/rgb/image_rect_color \
       /camera/rgb/camera_info \
       /lbr4_allegro/joint_states \
       /ethdaq_data \
       /ft_sensor/negated 
#       /tf_static \
#       /tf

#  /tf \
#  /tf_static \
#  /tracker/tf \
#  /tracker/robot/tf \
#  /tracker/tf_static \
#  /camera/depth_registered/sw_registered/camera_info \
#  /tracker/associated_depth_image
