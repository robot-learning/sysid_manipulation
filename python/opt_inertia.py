import numpy as np
import pymc as mc
import pygmo as pg

class cholesky_inertia_function:
    def __init__(self):
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        self.m=0.0
        self.rc=0.0
        self.o_f=None
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec,m,rc,o_f):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        self.m=m
        self.rc=rc
        self.o_f=o_f
        print "Inertia SLSQP: Updated parameters"
        
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=self.m
                

        H_rc=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H_rc[0,:]=np.array([ixx,ixy,ixz])
        H_rc[1,:]=np.array([ixy,iyy,iyz])
        H_rc[2,:]=np.array([ixz,iyz,izz])
        #print H_rc
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]
            #rc=self.o_f[i]-self.rc
            rc_vec=np.matrix([self.rc[0],self.rc[1],self.rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.linalg.inv(o_T_fref)*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]
            H=H_rc+m*(np.linalg.norm(rc)**2)
            #print rc
            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)
            
            
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)

            g_v_mat=np.matrix([g_vec[0],g_vec[1],g_vec[2]]).T
            g_rc=self.skew_cross(rc)
            tau_g=m*np.ravel(g_rc*g_v_mat)
            #tau_g=m*np.array([-g_vec[2]*rc[1]+g_vec[1]*rc[2],
            #                  -g_vec[0]*rc[2]+g_vec[2]*rc[0],
            #                  -g_vec[1]*rc[0]+g_vec[0]*rc[1]])
            #print self.ang_acc[i]
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            cost+=1.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=1.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=1.0*(omega_d[2]-tau[2]-tau_g[2])**2

        # inequality constraints
        nic1=np.copysign((H_rc[0,0]-H_rc[1,1]-H_rc[2,2])**2,H_rc[0,0]-H_rc[1,1]-H_rc[2,2])
        nic2=np.copysign((H_rc[1,1]-H_rc[0,0]-H_rc[2,2])**2,H_rc[1,1]-H_rc[0,0]-H_rc[2,2])
        nic3=np.copysign((H_rc[2,2]-H_rc[0,0]-H_rc[1,1])**2,H_rc[2,2]-H_rc[0,0]-H_rc[1,1])
        # equality constraints:
        r11=x[6+0]
        r22=x[6+1]
        r33=x[6+2]
        r12=x[6+3]
        r13=x[6+4]
        r23=x[6+5]
        nec1=1.0*(ixx-r11*r11)**2
        nec2=1.0*(iyy-r12*r12-r22*r22)**2
        nec3=1.0*(izz-r13*r13-r23*r23-r33*r33)**2
        nec4=1.0*(ixy-r11*r12)**2
        nec5=1.0*(ixz-r11*r13)**2
        nec6=1.0*(iyz-r12*r13-r22*r23)**2
        nec1=1.0*(nec1+nec2+nec3+nec4+nec5+nec6)**2
        return [cost,nec1,nic1,nic2,nic3]

    def gradient(self, x):
        #print x
        grad=np.zeros(12+12+12*3)
        m=self.m
        
        #rc=self.rc

        H_rc=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H_rc[0,:]=np.array([ixx,ixy,ixz])
        H_rc[1,:]=np.array([ixy,iyy,iyz])
        H_rc[2,:]=np.array([ixz,iyz,izz])


        # nic1:
        nic1=np.copysign((H_rc[0,0]-H_rc[1,1]-H_rc[2,2])**2,H_rc[0,0]-H_rc[1,1]-H_rc[2,2])
        
        if(nic1>0):
            nic1=2.0*(H_rc[0,0]-H_rc[1,1]-H_rc[2,2])

            grad[12+12+0]=nic1
            grad[12+12+1]=-1.0*nic1
            grad[12+12+2]=-1.0*nic1

        nic2=np.copysign((H_rc[1,1]-H_rc[0,0]-H_rc[2,2])**2,H_rc[1,1]-H_rc[0,0]-H_rc[2,2])
        if(nic2>0):
            nic2=2.0*(H_rc[1,1]-H_rc[0,0]-H_rc[2,2])

            grad[12+12+12+1]=nic2
            grad[12+12+12+0]=-1.0*nic2
            grad[12+12+12+2]=-1.0*nic2

        nic3=np.copysign((H_rc[2,2]-H_rc[0,0]-H_rc[1,1])**2,H_rc[2,2]-H_rc[0,0]-H_rc[1,1])
        #print nic3
        if(nic3>0):
            nic3=2.0*(H_rc[2,2]-H_rc[0,0]-H_rc[1,1])    
            grad[12+12+2*12+2]=nic3
            grad[12+12+2*12+0]=-1.0*nic3
            grad[12+12+2*12+1]=-1.0*nic3

        # nec gradient:
        r11=x[6+0]
        r22=x[6+1]
        r33=x[6+2]
        r12=x[6+3]
        r13=x[6+4]
        r23=x[6+5]

        nec1=1.0*(ixx-r11*r11)**2
        nec2=1.0*(iyy-r12*r12-r22*r22)**2
        nec3=1.0*(izz-r13*r13-r23*r23-r33*r33)**2
        nec4=1.0*(ixy-r11*r12)**2
        nec5=1.0*(ixz-r11*r13)**2
        nec6=1.0*(iyz-r12*r13-r22*r23)**2
        nec=2.0*(nec1+nec2+nec3+nec4+nec5+nec6)

        
        nec1=2.0*(ixx-r11*r11)
        nec2=2.0*(iyy-r12*r12-r22*r22)       
        nec3=2.0*(izz-r13*r13-r23*r23-r33*r33)
        nec4=2.0*(ixy-r11*r12)
        nec5=2.0*(ixz-r11*r13)
        nec6=2.0*(iyz-r12*r13-r22*r23)
                
        grad[12+0]+=nec*nec1
        grad[12+6+0]+=-1.0*nec1*r11*nec

        
        grad[12+1]+=nec2*nec
        grad[12+6+3]+=-2.0*nec2*r12*nec
        grad[12+6+1]+=-2.0*nec2*r22*nec
        
        
        grad[12+2]+=nec3*nec
        grad[12+6+4]+=-2.0*nec3*r13*nec
        grad[12+6+5]+=-2.0*nec3*r23*nec
        grad[12+6+2]+=-2.0*nec2*r33*nec
        
        grad[12+3]+=nec4*nec
        grad[12+6+0]+=-nec4*r12*nec
        grad[12+6+3]+=-nec4*r11*nec
        
        grad[12+4]+=nec5*nec
        grad[12+6+0]+=-nec5*r13*nec
        grad[12+6+4]+=-nec5*r11*nec
        
        grad[12+5]+=nec6*nec
        grad[12+6+3]+=-nec6*r13*nec
        grad[12+6+4]+=-nec6*r12*nec
        
        grad[12+6+1]+=-nec6*r23*nec
        grad[12+6+5]+=-nec6*r22*nec
        for i in range(len(self.force)):
            #rc=self.rc-self.o_f[i]
            rc_vec=np.matrix([self.rc[0],self.rc[1],self.rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.linalg.inv(o_T_fref)*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]

            H=H_rc+m*(np.linalg.norm(rc)**2)

            #rc=self.o_f[i]-self.rc

            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec

            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            g_v_mat=np.matrix([g_vec[0],g_vec[1],g_vec[2]]).T
            g_rc=self.skew_cross(rc)
            tau_g=m*np.ravel(g_rc*g_v_mat)

            #tau_g=m*np.array([-g_vec[2]*rc[1]+g_vec[1]*rc[2],
            #                  -g_vec[0]*rc[2]+g_vec[2]*rc[0],
            #                  -g_vec[1]*rc[0]+g_vec[0]*rc[1]])
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
                
            # inertia gradients:
            grad[0]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]

            grad[1]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]


            grad[2]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]

            # ixy
            grad[3]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][1]
            grad[3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][0]

            # ixz
            grad[4]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][2]
            grad[4]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][0]

            # iyz
            grad[5]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][2]
            grad[5]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][1]
        #grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)
        #print grad_fd-grad
        #print 'fd'
        #print grad_fd[12:12*6]
        #print grad[12:12*6]
        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        #low_bounds=[0.01,-0.5,-0.5,-0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        #up_bounds =[1.0,0.5,0.5,0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        low_bounds=[-1.0,-1.0,-1.0,-1.0,-1.0,-1.0, -1.0,-1.0,-1.0,-1.0,-1.0,-1.0]
        up_bounds =[1.0,1.0,1.0, 1.0,1.0,1.0, 1.0,1.0,1.0,1.0,1.0,1.0]
        return (low_bounds,up_bounds)
    def get_nic(self):
        return 3+1
    def get_nec(self):
        return 0
    
class cholesky_inertial_function: # estimates all inertial parameters with no noise on force sensing
    def __init__(self):
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        #self.m=0.0
        #self.rc=0.0
        self.o_f=None
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec,o_f):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        #self.m=m
        #self.rc=rc
        self.o_f=o_f
        print "Inertia SLSQP: Updated parameters"

    def inv_function(self,m,rc,H,lin_acc,ang_acc,g):
        # returns the net force and net torque acting on the object
        y=np.zeros(6)
        y[0:3]=m*(lin_acc-g)
        y[3:6]=np.ravel((H+m*np.linalg.norm(rc)**2)*np.matrix(ang_acc).T-self.skew_cross(rc)*np.matrix(g).T)
        return y
    def covariance(self,m,rc,H,lin_acc,ang_acc,g):
        # differentiate function with respect to parameters:
        df_m=np.zeros((6,1))
        
        df_m[0:3,0]=lin_acc-g

        df_m[3:6,0]=(np.linalg.norm(rc)**2)*ang_acc-np.ravel(self.skew_cross(rc)*np.matrix(g).T)

        # center of mass:
        df_rc=np.zeros((6,3))
        df_rc[3,0]+=m*2*ang_acc[0]*rc[0]
        df_rc[4,1]+=m*2*ang_acc[1]*rc[1]
        df_rc[5,2]+=m*2*ang_acc[2]*rc[2]

        df_rc[3:6,0]+=np.ravel([0.0,m*g[2],-m*g[1]])
        df_rc[3:6,1]+=np.ravel([-m*g[2],0,m*g[0]])
        df_rc[3:6,2]+=np.ravel([m*g[1],-m*g[0],0.0])
        
        #df_rc[3:6,0]=

        # inertia

        df_H=np.zeros((6,6))
        df_H[3:6,0]=np.ravel([ang_acc[0],0,0])
        df_H[3:6,1]=np.ravel([0,ang_acc[1],0])
        df_H[3:6,2]=np.ravel([0,0,ang_acc[2]])
        df_H[3:6,3]=np.ravel([ang_acc[1],ang_acc[0],0])
        df_H[3:6,4]=np.ravel([ang_acc[2],0,ang_acc[0]])
        df_H[3:6,5]=np.ravel([0,ang_acc[2],ang_acc[1]])
        
        df_mat=np.zeros((10,6))
        df_mat[0,:]=np.ravel(df_m)
        df_mat[1:4,:]=df_rc.T
        df_mat[4:10,:]=df_H.T
        cv=np.matrix(df_mat)*np.matrix(df_mat).T
        return cv
    def variance(self,est_m,est_rc,est_H):
        sigma_2=0.0

        y_mean=np.zeros(6)
        f_mean=np.zeros(6)
        # compute mean y, f
        for i in range(len(self.force)):
            # compute net force and net torque
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:

            y_t=np.zeros(6)
            y_t[0:3]=np.sum(self.force[i],axis=0)

            # compute net torque:
            
            # get estimate from inv function
            # compute rc from finger
            rc_vec=np.matrix([est_rc[0],est_rc[1],est_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            est_frc=np.ravel(fref_T_rc)[0:3]

            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
    
            y_t[3:6]=tau
            y_mean+=y_t
            f_mean+=self.inv_function(est_m,est_frc,est_H, self.linear_acc[i],self.ang_acc[i],self.g[i])
        y_mean=y_mean/len(self.force)
        f_mean=f_mean/len(self.force)
        print y_mean,f_mean
        for i in range(len(self.force)):
            # compute net force and net torque
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:

            y_t=np.zeros(6)
            y_t[0:3]=np.sum(self.force[i],axis=0)

            # compute net torque:
            
            # get estimate from inv function
            # compute rc from finger
            rc_vec=np.matrix([est_rc[0],est_rc[1],est_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            est_frc=np.ravel(fref_T_rc)[0:3]

            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
    
            y_t[3:6]=tau
            sigma_2+=np.linalg.norm((y_t-y_mean)-(self.inv_function(est_m,est_frc,est_H,
                                                                    self.linear_acc[i],self.ang_acc[i],self.g[i])-f_mean))**2
            
        sigma_2=sigma_2/(len(self.force)*6-10)

        co_var=np.zeros((10,10))
        # compute covariance
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:

            y_t=np.zeros(6)
            y_t[0:3]=np.sum(self.force[i],axis=0)

            # compute net torque:
            
            # get estimate from inv function
            # compute rc from finger
            rc_vec=np.matrix([est_rc[0],est_rc[1],est_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            est_frc=np.ravel(fref_T_rc)[0:3]

            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            co_var+=self.covariance(est_m,est_frc,est_H,self.linear_acc[i],self.ang_acc[i],self.g[i])
            
        #
        #co_var=sigma_2*np.linalg.inv(co_var/len(self.force))
        precision=1/sigma_2*(co_var/len(self.force))
        return sigma_2,precision
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=x[12]
        o_rc=np.array([x[13],x[14],x[15]])
                
        H_rc=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H_rc[0,:]=np.array([ixx,ixy,ixz])
        H_rc[1,:]=np.array([ixy,iyy,iyz])
        H_rc[2,:]=np.array([ixz,iyz,izz])
        #print H_rc
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]
            #rc=self.o_f[i]-self.rc
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]
            H=H_rc+m*(np.linalg.norm(rc)**2)
            #print rc
            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            cost+=1.0*np.ravel(np.linalg.norm(m*(self.linear_acc[i]-g_vec)-f_sum)**2)[0]

            # compute net torque:
            #rc_vec=self.skew_cross(rc)
            
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)

            g_v_mat=np.matrix([g_vec[0],g_vec[1],g_vec[2]]).T
            g_rc=self.skew_cross(rc)
            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            
            #tau_g=m*np.ravel(g_rc*g_v_mat)
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #omega_d=np.zeros(3)

            #cost+=np.ravel(np.linalg.norm(omega_d-tau-tau_g)**2)[0]
            cost+=1.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=1.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=1.0*(omega_d[2]-tau[2]-tau_g[2])**2

        # inequality constraints
        nic1=np.copysign((H_rc[0,0]-H_rc[1,1]-H_rc[2,2])**2,H_rc[0,0]-H_rc[1,1]-H_rc[2,2])
        nic2=np.copysign((H_rc[1,1]-H_rc[0,0]-H_rc[2,2])**2,H_rc[1,1]-H_rc[0,0]-H_rc[2,2])
        nic3=np.copysign((H_rc[2,2]-H_rc[0,0]-H_rc[1,1])**2,H_rc[2,2]-H_rc[0,0]-H_rc[1,1])
        # equality constraints:
        r11=x[6+0]
        r22=x[6+1]
        r33=x[6+2]
        r12=x[6+3]
        r13=x[6+4]
        r23=x[6+5]
        nec1=1.0*(ixx-r11*r11)**2
        nec2=1.0*(iyy-r12*r12-r22*r22)**2
        nec3=1.0*(izz-r13*r13-r23*r23-r33*r33)**2
        nec4=1.0*(ixy-r11*r12)**2
        nec5=1.0*(ixz-r11*r13)**2
        nec6=1.0*(iyz-r12*r13-r22*r23)**2
        
        nec1=1.0*(nec1+nec2+nec3+nec4+nec5+nec6)**2
        cost=cost#/len(self.force)
        return [cost,nec1,nic1,nic2,nic3]

    def gradient(self, x):
        #grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)
        #return grad_fd
        #print x
        grad=np.zeros(16+16+16*3)
        m=x[12]
        o_rc=np.array([x[13],x[14],x[15]])
        

        H_rc=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H_rc[0,:]=np.array([ixx,ixy,ixz])
        H_rc[1,:]=np.array([ixy,iyy,iyz])
        H_rc[2,:]=np.array([ixz,iyz,izz])
        
        # nic1:
        nic1=np.copysign((H_rc[0,0]-H_rc[1,1]-H_rc[2,2])**2,H_rc[0,0]-H_rc[1,1]-H_rc[2,2])
        
        if(nic1>0):
            nic1=2.0*(H_rc[0,0]-H_rc[1,1]-H_rc[2,2])

            grad[16+16+0]=nic1
            grad[16+16+1]=-1.0*nic1
            grad[16+16+2]=-1.0*nic1

        nic2=np.copysign((H_rc[1,1]-H_rc[0,0]-H_rc[2,2])**2,H_rc[1,1]-H_rc[0,0]-H_rc[2,2])
        if(nic2>0):
            nic2=2.0*(H_rc[1,1]-H_rc[0,0]-H_rc[2,2])

            grad[16+16+16+1]=nic2
            grad[16+16+16+0]=-1.0*nic2
            grad[16+16+16+2]=-1.0*nic2

        nic3=np.copysign((H_rc[2,2]-H_rc[0,0]-H_rc[1,1])**2,H_rc[2,2]-H_rc[0,0]-H_rc[1,1])
        #print nic3
        if(nic3>0):
            nic3=2.0*(H_rc[2,2]-H_rc[0,0]-H_rc[1,1])    
            grad[16+16+2*16+2]=nic3
            grad[16+16+2*16+0]=-1.0*nic3
            grad[16+16+2*16+1]=-1.0*nic3

        # nec gradient:
        r11=x[6+0]
        r22=x[6+1]
        r33=x[6+2]
        r12=x[6+3]
        r13=x[6+4]
        r23=x[6+5]

        nec1=1.0*(ixx-r11*r11)**2
        nec2=1.0*(iyy-r12*r12-r22*r22)**2
        nec3=1.0*(izz-r13*r13-r23*r23-r33*r33)**2
        nec4=1.0*(ixy-r11*r12)**2
        nec5=1.0*(ixz-r11*r13)**2
        nec6=1.0*(iyz-r12*r13-r22*r23)**2
        nec=2.0*(nec1+nec2+nec3+nec4+nec5+nec6)

        
        nec1=2.0*(ixx-r11*r11)
        nec2=2.0*(iyy-r12*r12-r22*r22)       
        nec3=2.0*(izz-r13*r13-r23*r23-r33*r33)
        nec4=2.0*(ixy-r11*r12)
        nec5=2.0*(ixz-r11*r13)
        nec6=2.0*(iyz-r12*r13-r22*r23)
                
        grad[16+0]+=nec*nec1
        grad[16+6+0]+=-1.0*nec1*r11*nec

        
        grad[16+1]+=nec2*nec
        grad[16+6+3]+=-2.0*nec2*r12*nec
        grad[16+6+1]+=-2.0*nec2*r22*nec
        
        
        grad[16+2]+=nec3*nec
        grad[16+6+4]+=-2.0*nec3*r13*nec
        grad[16+6+5]+=-2.0*nec3*r23*nec
        grad[16+6+2]+=-2.0*nec2*r33*nec
        
        grad[16+3]+=nec4*nec
        grad[16+6+0]+=-nec4*r12*nec
        grad[16+6+3]+=-nec4*r11*nec
        
        grad[16+4]+=nec5*nec
        grad[16+6+0]+=-nec5*r13*nec
        grad[16+6+4]+=-nec5*r11*nec
        
        grad[16+5]+=nec6*nec
        grad[16+6+3]+=-nec6*r13*nec
        grad[16+6+4]+=-nec6*r12*nec
        
        grad[16+6+1]+=-nec6*r23*nec
        grad[16+6+5]+=-nec6*r22*nec
        for i in range(len(self.force)):
            #rc=self.rc-self.o_f[i]
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.linalg.inv(o_T_fref)*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]

            H=H_rc+m*(np.linalg.norm(rc)**2)

            #rc=self.o_f[i]-self.rc

            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)

            # gradient for linear acc
            #grad[12]+=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T
            grad[12]+=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T

            #continue
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec

            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            g_v_mat=np.matrix([g_vec[0],g_vec[1],g_vec[2]]).T
            g_rc=self.skew_cross(rc)
            #tau_g=m*np.ravel(g_rc*g_v_mat)

            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #omega_d=np.zeros(3)
            # gradient for mass in tau_g:
            grad[12]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*tau_g[0]/m
            grad[12]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*tau_g[1]/m
            grad[12]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*tau_g[2]/m

            # gradient for mass in H=H_rc+m*(||rc||)**2
            grad[12]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]*np.linalg.norm(rc)**2
            grad[12]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]*np.linalg.norm(rc)**2
            grad[12]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]*np.linalg.norm(rc)**2

            f_grad_rc=np.zeros(4)
            
            f_grad_rc[0:3]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*2.0*m*rc
            f_grad_rc[0:3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*2.0*m*rc
            f_grad_rc[0:3]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*2.0*m*rc

            # center of mass gradients:
            #f_grad_rc[3]=1.0
            f_grad_rc[0]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[2]
            f_grad_rc[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[1]
            
            
            f_grad_rc[1]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[2]
            f_grad_rc[1]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[0]

            f_grad_rc[2]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[1]
            f_grad_rc[2]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[0]
            
            grad[13:16]+=np.ravel(o_T_fref*np.matrix(f_grad_rc).T)[0:3]
            # inertia gradients:
            grad[0]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]

            grad[1]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]


            grad[2]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]

            # ixy
            grad[3]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][1]
            grad[3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][0]

            # ixz
            grad[4]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][2]
            grad[4]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][0]

            # iyz
            grad[5]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][2]
            grad[5]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][1]

        grad[0:5]=grad[0:5]#/len(self.force)
        grad[12:16]=grad[12:16]#/len(self.force)
        # gradient for mass:
        #print grad_fd-grad
        #print 'fd'
        #print grad_fd[12:12*6]
        #print grad[12:12*6]
        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        #low_bounds=[0.01,-0.5,-0.5,-0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        #up_bounds =[1.0,0.5,0.5,0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        low_bounds=[0.0,0.0,0.0, -1.0,-1.0,-1.0, -1.0,-1.0,-1.0,-1.0,-1.0,-1.0, 0.01, -0.5,-0.5,-0.5]
        up_bounds =[1.0,1.0,1.0, 1.0,1.0,1.0, 1.0,1.0,1.0,1.0,1.0,1.0, 2.0,0.5,0.5,0.5]
        return (low_bounds,up_bounds)
    def get_nic(self):
        return 1+3
    def get_nec(self):
        return 0
class tls_inertial_function: # estimates all inertial parameters with noise on force readings
    def __init__(self):
        self.t_steps=0
        self.m_dim=10+4*3
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        #self.m=0.0
        #self.rc=0.0
        self.o_f=None
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec,o_f):
        self.t_steps=len(force)
        self.m_dim=10+len(force[0])*3*self.t_steps
                
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        #self.m=m
        #self.rc=rc
        self.o_f=o_f
        print "Inertia SLSQP: Updated parameters"

    def inv_function(self,m,rc,H,lin_acc,ang_acc,g):
        # returns the net force and net torque acting on the object
        y=np.zeros(6)
        y[0:3]=m*(lin_acc-g)
        y[3:6]=np.ravel((H+m*np.linalg.norm(rc)**2)*np.matrix(ang_acc).T-self.skew_cross(rc)*np.matrix(g).T)
        return y
    def covariance(self,m,rc,H,lin_acc,ang_acc,g):
        # differentiate function with respect to parameters:
        df_m=np.zeros((6,1))
        
        df_m[0:3,0]=lin_acc-g

        df_m[3:6,0]=np.linalg.norm(rc)**2*ang_acc-np.ravel(self.skew_cross(rc)*np.matrix(g).T)

        # center of mass:
        df_rc=np.zeros((6,3))
        df_rc[3,0]+=m*2*ang_acc[0]*rc[0]
        df_rc[4,1]+=m*2*ang_acc[1]*rc[1]
        df_rc[5,2]+=m*2*ang_acc[2]*rc[2]

        df_rc[3:6,0]+=np.ravel([0.0,m*g[2],-m*g[1]])
        df_rc[3:6,1]+=np.ravel([-m*g[2],0,m*g[0]])
        df_rc[3:6,2]+=np.ravel([m*g[1],-m*g[0],0.0])
        
        #df_rc[3:6,0]=

        # inertia

        df_H=np.zeros((6,6))
        df_H[3:6,0]=np.ravel([ang_acc[0],0,0])
        df_H[3:6,1]=np.ravel([0,ang_acc[1],0])
        df_H[3:6,2]=np.ravel([0,0,ang_acc[2]])
        df_H[3:6,3]=np.ravel([ang_acc[1],ang_acc[0],0])
        df_H[3:6,4]=np.ravel([ang_acc[2],0,ang_acc[0]])
        df_H[3:6,5]=np.ravel([0,ang_acc[2],ang_acc[1]])
        
        df_mat=np.zeros((10,6))
        df_mat[0,:]=np.ravel(df_m)
        df_mat[1:4,:]=df_rc.T
        df_mat[4:10,:]=df_H.T
        cv=np.matrix(df_mat)*np.matrix(df_mat).T
        return cv
    def variance(self,est_m,est_rc,est_H):
        sigma_2=0.0
        for i in range(len(self.force)):
            # compute net force and net torque
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:

            y_t=np.zeros(6)
            y_t[0:3]=np.sum(self.force[i],axis=0)

            # compute net torque:
            
            # get estimate from inv function
            # compute rc from finger
            rc_vec=np.matrix([est_rc[0],est_rc[1],est_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            est_frc=np.ravel(fref_T_rc)[0:3]

            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
    
            y_t[3:6]=tau
            sigma_2+=np.linalg.norm(y_t-self.inv_function(est_m,est_frc,est_H,
                                           self.linear_acc[i],self.ang_acc[i],self.g[i]))**2
            
        sigma_2=sigma_2/(len(self.force)*6-10)

        co_var=np.zeros((10,10))
        # compute covariance
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:

            y_t=np.zeros(6)
            y_t[0:3]=np.sum(self.force[i],axis=0)

            # compute net torque:
            
            # get estimate from inv function
            # compute rc from finger
            rc_vec=np.matrix([est_rc[0],est_rc[1],est_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            est_frc=np.ravel(fref_T_rc)[0:3]

            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            co_var+=self.covariance(est_m,est_frc,est_H,self.linear_acc[i],self.ang_acc[i],self.g[i])
            
        #
        co_var=sigma_2*np.linalg.inv(co_var/len(self.force))
        return np.sqrt(sigma_2),co_var
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=x[12]
        o_rc=np.array([x[13],x[14],x[15]])
                
        H_rc=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H_rc[0,:]=np.array([ixx,ixy,ixz])
        H_rc[1,:]=np.array([ixy,iyy,iyz])
        H_rc[2,:]=np.array([ixz,iyz,izz])
        e=[np.zeros(3) for i in range(len(self.force[0]))]
            
        #print H_rc
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]

            for j in range(len(self.force[i])):
                '''
                print (16+i*len(self.force[i])*3+j*3),
                (16+i*len(self.force[i])*3+j*3)+1,
                (16+i*len(self.force[i])*3+j*3)+2
                '''
                e[j]=np.array([x[16+i*len(self.force[i])*3+(j*3)],
                               x[16+i*len(self.force[i])*3+(j*3)+1],
                               x[16+i*len(self.force[i])*3+(j*3)+2]])
                f_data[j]=f_data[j]+e[j]
                #print i,j,e[j]
            #print f_data
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]
            #rc=self.o_f[i]-self.rc
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]
            H=H_rc+m*(np.linalg.norm(rc)**2)
            #print rc
            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            cost+=1.0*np.ravel(np.linalg.norm(m*(self.linear_acc[i]-g_vec)-f_sum)**2)[0]
            #cost+=0.0*np.ravel(np.linalg.norm(m*(-g_vec)-f_sum)**2)[0]

            # compute net torque:
            #rc_vec=self.skew_cross(rc)
            
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)

            g_v_mat=np.matrix([g_vec[0],g_vec[1],g_vec[2]]).T
            g_rc=self.skew_cross(rc)
            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            
            #tau_g=m*np.ravel(g_rc*g_v_mat)
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #omega_d=np.zeros(3)

            #cost+=np.ravel(np.linalg.norm(omega_d-tau-tau_g)**2)[0]
            cost+=1.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=1.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=1.0*(omega_d[2]-tau[2]-tau_g[2])**2
            for j in range(len(self.force[0])):
                cost+=10.0*np.linalg.norm(e[j])**2
        
        # inequality constraints
        nic1=np.copysign((H_rc[0,0]-H_rc[1,1]-H_rc[2,2])**2,H_rc[0,0]-H_rc[1,1]-H_rc[2,2])
        nic2=np.copysign((H_rc[1,1]-H_rc[0,0]-H_rc[2,2])**2,H_rc[1,1]-H_rc[0,0]-H_rc[2,2])
        nic3=np.copysign((H_rc[2,2]-H_rc[0,0]-H_rc[1,1])**2,H_rc[2,2]-H_rc[0,0]-H_rc[1,1])
        # equality constraints:
        r11=x[6+0]
        r22=x[6+1]
        r33=x[6+2]
        r12=x[6+3]
        r13=x[6+4]
        r23=x[6+5]
        nec1=1.0*(ixx-r11*r11)**2
        nec2=1.0*(iyy-r12*r12-r22*r22)**2
        nec3=1.0*(izz-r13*r13-r23*r23-r33*r33)**2
        nec4=1.0*(ixy-r11*r12)**2
        nec5=1.0*(ixz-r11*r13)**2
        nec6=1.0*(iyz-r12*r13-r22*r23)**2
        
        nec1=1.0*(nec1+nec2+nec3+nec4+nec5+nec6)**2
        cost=cost
        return [cost]#,nec1,nic1,nic2,nic3]

    def gradient(self, x):
        #print x[16:]
        grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)
        return grad_fd
        #print x
        grad=np.zeros(self.m_dim*(1+1+3))
        m=x[12]
        o_rc=np.array([x[13],x[14],x[15]])
        

        H_rc=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H_rc[0,:]=np.array([ixx,ixy,ixz])
        H_rc[1,:]=np.array([ixy,iyy,iyz])
        H_rc[2,:]=np.array([ixz,iyz,izz])
        
        # nic1:
        nic1=np.copysign((H_rc[0,0]-H_rc[1,1]-H_rc[2,2])**2,H_rc[0,0]-H_rc[1,1]-H_rc[2,2])
        
        if(nic1>0):
            nic1=2.0*(H_rc[0,0]-H_rc[1,1]-H_rc[2,2])

            grad[self.m_dim+self.m_dim+0]=nic1
            grad[self.m_dim+self.m_dim+1]=-1.0*nic1
            grad[self.m_dim+self.m_dim+2]=-1.0*nic1

        nic2=np.copysign((H_rc[1,1]-H_rc[0,0]-H_rc[2,2])**2,H_rc[1,1]-H_rc[0,0]-H_rc[2,2])
        if(nic2>0):
            nic2=2.0*(H_rc[1,1]-H_rc[0,0]-H_rc[2,2])

            grad[self.m_dim+self.m_dim+self.m_dim+1]=nic2
            grad[self.m_dim+self.m_dim+self.m_dim+0]=-1.0*nic2
            grad[self.m_dim+self.m_dim+self.m_dim+2]=-1.0*nic2

        nic3=np.copysign((H_rc[2,2]-H_rc[0,0]-H_rc[1,1])**2,H_rc[2,2]-H_rc[0,0]-H_rc[1,1])
        #print nic3
        if(nic3>0):
            nic3=2.0*(H_rc[2,2]-H_rc[0,0]-H_rc[1,1])    
            grad[self.m_dim+self.m_dim+2*self.m_dim+2]=nic3
            grad[self.m_dim+self.m_dim+2*self.m_dim+0]=-1.0*nic3
            grad[self.m_dim+self.m_dim+2*self.m_dim+1]=-1.0*nic3

        # nec gradient:
        r11=x[6+0]
        r22=x[6+1]
        r33=x[6+2]
        r12=x[6+3]
        r13=x[6+4]
        r23=x[6+5]

        nec1=1.0*(ixx-r11*r11)**2
        nec2=1.0*(iyy-r12*r12-r22*r22)**2
        nec3=1.0*(izz-r13*r13-r23*r23-r33*r33)**2
        nec4=1.0*(ixy-r11*r12)**2
        nec5=1.0*(ixz-r11*r13)**2
        nec6=1.0*(iyz-r12*r13-r22*r23)**2
        nec=2.0*(nec1+nec2+nec3+nec4+nec5+nec6)

        
        nec1=2.0*(ixx-r11*r11)
        nec2=2.0*(iyy-r12*r12-r22*r22)       
        nec3=2.0*(izz-r13*r13-r23*r23-r33*r33)
        nec4=2.0*(ixy-r11*r12)
        nec5=2.0*(ixz-r11*r13)
        nec6=2.0*(iyz-r12*r13-r22*r23)
                
        grad[self.m_dim+0]+=nec*nec1
        grad[self.m_dim+6+0]+=-1.0*nec1*r11*nec

        
        grad[self.m_dim+1]+=nec2*nec
        grad[self.m_dim+6+3]+=-2.0*nec2*r12*nec
        grad[self.m_dim+6+1]+=-2.0*nec2*r22*nec
        
        
        grad[self.m_dim+2]+=nec3*nec
        grad[self.m_dim+6+4]+=-2.0*nec3*r13*nec
        grad[self.m_dim+6+5]+=-2.0*nec3*r23*nec
        grad[self.m_dim+6+2]+=-2.0*nec2*r33*nec
        
        grad[self.m_dim+3]+=nec4*nec
        grad[self.m_dim+6+0]+=-nec4*r12*nec
        grad[self.m_dim+6+3]+=-nec4*r11*nec
        
        grad[self.m_dim+4]+=nec5*nec
        grad[self.m_dim+6+0]+=-nec5*r13*nec
        grad[self.m_dim+6+4]+=-nec5*r11*nec
        
        grad[self.m_dim+5]+=nec6*nec
        grad[self.m_dim+6+3]+=-nec6*r13*nec
        grad[self.m_dim+6+4]+=-nec6*r12*nec
        
        grad[self.m_dim+6+1]+=-nec6*r23*nec
        grad[self.m_dim+6+5]+=-nec6*r22*nec
        for i in range(len(self.force)):
            #rc=self.rc-self.o_f[i]
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.linalg.inv(o_T_fref)*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]

            H=H_rc+m*(np.linalg.norm(rc)**2)

            #rc=self.o_f[i]-self.rc

            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)

            # gradient for linear acc
            grad[12]+=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T


            #continue
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec

            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            g_v_mat=np.matrix([g_vec[0],g_vec[1],g_vec[2]]).T
            g_rc=self.skew_cross(rc)
            #tau_g=m*np.ravel(g_rc*g_v_mat)

            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #omega_d=np.zeros(3)
            # gradient for mass in tau_g:
            grad[12]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*tau_g[0]/m
            grad[12]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*tau_g[1]/m
            grad[12]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*tau_g[2]/m

            # gradient for mass in H=H_rc+m*(||rc||)**2
            grad[12]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]*np.linalg.norm(rc)**2
            grad[12]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]*np.linalg.norm(rc)**2
            grad[12]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]*np.linalg.norm(rc)**2

            f_grad_rc=np.zeros(4)
            
            f_grad_rc[0:3]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*2.0*m*rc
            f_grad_rc[0:3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*2.0*m*rc
            f_grad_rc[0:3]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*2.0*m*rc

            # center of mass gradients:
            #f_grad_rc[3]=1.0
            f_grad_rc[0]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[2]
            f_grad_rc[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[1]
            
            
            f_grad_rc[1]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[2]
            f_grad_rc[1]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[0]

            f_grad_rc[2]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[1]
            f_grad_rc[2]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[0]
            
            grad[13:16]+=np.ravel(o_T_fref*np.matrix(f_grad_rc).T)[0:3]
            # inertia gradients:
            grad[0]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]

            grad[1]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]


            grad[2]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]

            # ixy
            grad[3]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][1]
            grad[3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][0]

            # ixz
            grad[4]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][2]
            grad[4]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][0]

            # iyz
            grad[5]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][2]
            grad[5]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][1]
        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        low_bounds=[0.0,0.0,0.0, -1.0,-1.0,-1.0, -1.0,-1.0,-1.0,-1.0,-1.0,-1.0, 0.01, -0.5,-0.5,-0.5]
        up_bounds =[1.0,1.0,1.0, 1.0,1.0,1.0, 1.0,1.0,1.0,1.0,1.0,1.0, 2.0,0.5,0.5,0.5]
        # add bounds for error:
        f_limits=np.ones(4*3)*0.001
        for i in range(self.t_steps):
            low_bounds=np.concatenate((np.ravel(low_bounds),f_limits*-1.0))
            up_bounds=np.concatenate((np.ravel(up_bounds),f_limits*1.0))
        
        return (low_bounds,up_bounds)
    def get_nic(self):
        return 0#1+3
    def get_nec(self):
        return 0

class InertiaModel(object):
    def __init__(self):
        self.tls_opt_fn=tls_inertial_function()
        self.opt_fn=cholesky_inertial_function()

        return None
    def optimize_inertia(self,linear_acc,angle_acc,forces,f_loc,g_vec,m,rc,prior,o_f):
        opt_fn=cholesky_inertia_function()
        opt_fn.update_data(linear_acc,angle_acc,forces,f_loc,g_vec,m,rc,o_f)
        prob = pg.problem(opt_fn)
        con_tol=np.ones(1+3)*1e-6
        prob.c_tol=con_tol
        #sn=ppnf.snopt7(screen_output = True, absolute_lib_path = "/usr/local/lib/")
       
        nl=pg.nlopt('slsqp')
        #nl.xtol_rel = 1E-6
        nl.maxeval=10
        #nl.set_maxeval(100)
        algo = pg.algorithm(nl)
        algo.set_verbosity(1)
        #algo.set_maxeval(100)
        pop = pg.population(prob,1)
        #prior_set=np.zeros(len(prior)+4)
        #prior_set[0:len(prior)]=prior
        #prior_set[len(prior)]=m
        #prior_set[-3:]=rc
        #print prior_set
        #prior=np.concatenate(np.ravel(prior),np.array([m,rc[0],rc[1],rc[2]]))
        f_in=prob.fitness(prior)
        print "Initial fitness: ",f_in
        pop.set_xf(0,prior,f_in)
        print "running optimization"
        pop = algo.evolve(pop)
        best_x=pop.champion_x
        #print pop.champion_x
        print "Final cost:",pop.champion_f

        print best_x
        H=np.zeros((3,3))
        ixx=best_x[0]
        iyy=best_x[1]
        izz=best_x[2]
        ixy=best_x[3]
        ixz=best_x[4]
        iyz=best_x[5]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])
        print "Inertia matrix: "
        print H
        print "cholesky constraint:"
        L=np.zeros((3,3))
        L[0,0]=best_x[6]
        L[0,1]=best_x[6+3]
        L[0,2]=best_x[6+4]
        L[1,1]=best_x[6+1]
        L[1,2]=best_x[6+5]
        L[2,2]=best_x[6+2]
        print L.T*L
        
        return H
    def optimize_tls(self,linear_acc,angle_acc,forces,f_loc,g_vec,m,rc,prior,o_f):
        self.opt_fn.update_data(linear_acc,angle_acc,forces,f_loc,g_vec,o_f)
        prob = pg.problem(self.opt_fn)
        con_tol=np.ones(1+3)*1e-6
        #prob.c_tol=con_tol
        #sn=ppnf.snopt7(screen_output = True, absolute_lib_path = "/usr/local/lib/")
       
        nl=pg.nlopt('slsqp')
        nl.xtol_rel = 1e-6
        nl.maxeval=100
        #nl.set_maxeval(100)
        algo = pg.algorithm(nl)
        algo.set_verbosity(1)
        #algo.set_maxeval(100)
        pop = pg.population(prob,10)
        prior_set=np.zeros(len(prior)+4+12*len(forces))
        prior_set[0:len(prior)]=prior
        prior_set[len(prior)]=m
        prior_set[len(prior)+1:len(prior)+4]=rc
        
        #print prior_set
        #prior=np.concatenate(np.ravel(prior),np.array([m,rc[0],rc[1],rc[2]]))
        f_in=prob.fitness(prior_set)
        print "Initial fitness: ",f_in
        pop.set_xf(0,prior_set,f_in)
        print "running optimization"
        pop = algo.evolve(pop)
        best_x=pop.champion_x
        #print pop.champion_x
        print "Final cost:",pop.champion_f

        print best_x
        H=np.zeros((3,3))
        ixx=best_x[0]
        iyy=best_x[1]
        izz=best_x[2]
        ixy=best_x[3]
        ixz=best_x[4]
        iyz=best_x[5]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])
        print "Inertia matrix: "
        print H
        print "cholesky constraint:"
        L=np.zeros((3,3))
        L[0,0]=best_x[6]
        L[0,1]=best_x[6+3]
        L[0,2]=best_x[6+4]
        L[1,1]=best_x[6+1]
        L[1,2]=best_x[6+5]
        L[2,2]=best_x[6+2]
        print L.T*L
        m=best_x[12]
        rc=best_x[13:16]
        return H,m,rc
    def optimize_ols(self,linear_acc,angle_acc,forces,f_loc,g_vec,m,rc,prior,o_f):
        self.opt_fn.update_data(linear_acc,angle_acc,forces,f_loc,g_vec,o_f)
        prob = pg.problem(self.opt_fn)
        con_tol=np.ones(1+3)*1e-6
        prob.c_tol=con_tol
        #sn=ppnf.snopt7(screen_output = True, absolute_lib_path = "/usr/local/lib/")
       
        nl=pg.nlopt('slsqp')
        nl.xtol_rel = 1e-6
        nl.maxeval=100
        #nl.set_maxeval(100)
        algo = pg.algorithm(nl)
        algo.set_verbosity(1)
        #algo.set_maxeval(100)
        pop = pg.population(prob,10)
        prior_set=np.zeros(len(prior)+4)
        prior_set[0:len(prior)]=prior
        prior_set[len(prior)]=m
        prior_set[len(prior)+1:len(prior)+4]=rc
        
        #print prior_set
        #prior=np.concatenate(np.ravel(prior),np.array([m,rc[0],rc[1],rc[2]]))
        f_in=prob.fitness(prior_set)
        print "Initial fitness: ",f_in
        pop.set_xf(0,prior_set,f_in)
        print "running optimization"
        pop = algo.evolve(pop)
        best_x=pop.champion_x
        #print pop.champion_x
        print "Final cost:",pop.champion_f
        
        #print best_x
        H=np.zeros((3,3))
        ixx=best_x[0]
        iyy=best_x[1]
        izz=best_x[2]
        ixy=best_x[3]
        ixz=best_x[4]
        iyz=best_x[5]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])
        print "Inertia matrix: "
        print H
        '''
        print "cholesky constraint:"
        L=np.zeros((3,3))
        L[0,0]=best_x[6]
        L[0,1]=best_x[6+3]
        L[0,2]=best_x[6+4]
        L[1,1]=best_x[6+1]
        L[1,2]=best_x[6+5]
        L[2,2]=best_x[6+2]
        print L.T*L
        '''
        m=best_x[12]
        rc=best_x[13:16]
        return H,m,rc

    def variance(self,m,rc,h):
        return self.opt_fn.variance(m,rc,h)
        
