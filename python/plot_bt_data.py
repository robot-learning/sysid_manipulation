import rospy
from geometry_msgs.msg import WrenchStamped
import numpy as np
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle
from sysid_manipulation.msg import FullMeasurementData
import copy
from sysid_manipulation.srv import *
from gtsam_toggle import *
from sensor_msgs.msg  import JointState
#from sysid_manipulation.msg import MeasurementData
from geometry_msgs.msg import PoseStamped,WrenchStamped
import matplotlib.pyplot as plt
from spline_util import *

import matplotlib as mpl

from matplotlib import cm
from colorspacious import cspace_converter
from collections import OrderedDict
from matplotlib.patches import Rectangle

#mpl.rcParams['pdf.fonttype'] = 42
#mpl.rcParams['ps.fonttype'] = 42

cmaps = OrderedDict()
#from biotac_sensors.msg import BioTacHand,BioTacTareHand
class readData(object):
    def __init__(self,f_name):
        # read data:
        #data_dir='/hdd/data1/sysid_grasp/pickle/'+f_name

        self.ft_data=pickle.load(open(f_name,"rb"))
     


if __name__=='__main__':
    HYBRID = True
    NOISE = False
    sigma = np.sqrt(1.0)
    print "sigma:",sigma
    rospy.init_node('data_logger')
    rate=rospy.Rate(60)

    # obj2: 100-600
    # obj3: 100-800
    #f_name='/hdd/data1/sysid_grasp/fixed_base_pickle/obj_0_slow.p'
    f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/bt/obj_4_slow.p'
    #f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/sim_interp'
    #f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/dart_data_3'
    
    r_data=readData(f_name)
    pub = rospy.Publisher('/sysid/bt_data', FullMeasurementData, queue_size=100)
    print 'data init',len(r_data.ft_data)

    data=[]
    i_start= 0 # 600 for obj_2
    i_max = len(r_data.ft_data)
    #i_max = 600
    #raw_input("Process data?")
    # r_data
    # plot force
    # add noise

    if(NOISE):
        print "adding noise"
                    
        f_vec = np.matrix([np.zeros(12) for i in range(i_max)])
        fsmooth_vec = np.matrix([np.zeros(12) for i in range(i_max)])
        # read data from file:
        for i in range(len(r_data.ft_data)):
            for j in range(len(r_data.ft_data[i].cpt_forces)):
                f_vec[i,j*3] = r_data.ft_data[i].cpt_forces[j].wrench.force.x
                f_vec[i,j*3+1] = r_data.ft_data[i].cpt_forces[j].wrench.force.y
                f_vec[i,j*3+2] = r_data.ft_data[i].cpt_forces[j].wrench.force.z

        # filter data:
        for j  in range(12):
            rnd_noise=np.random.normal(0,sigma,len(r_data.ft_data))
            for k in range(len(f_vec[:,j])):
                
                fsmooth_vec[k,j] = f_vec[k,j]  + rnd_noise[k]
        for i in range(i_max):
            for j in range(len(r_data.ft_data[i].cpt_forces)):
                r_data.ft_data[i].cpt_forces[j].wrench.force.x=fsmooth_vec[i,j*3]
                r_data.ft_data[i].cpt_forces[j].wrench.force.y=fsmooth_vec[i,j*3+1]
                r_data.ft_data[i].cpt_forces[j].wrench.force.z=fsmooth_vec[i,j*3+2]
        plt.plot(range(i_max),f_vec[:,11],'b',range(i_max),fsmooth_vec[:,4],'r')
        #plt.plot(range(i_max),f_vec[:,1],'b',range(i_max),fsmooth_vec[:,1],'r')
        #plt.show()
        
        #plt.show()

    if(True):
        # plot variance
        f_vec = np.matrix([np.zeros(12) for i in range(i_max)])
        fvar_vec=np.matrix([np.zeros(12) for i in range(i_max)])

        for i in range(i_max):
            for j in range(len(r_data.ft_data[i].cpt_forces)):
                f_vec[i,j*3]=r_data.ft_data[i].cpt_forces[j].wrench.force.x
                f_vec[i,j*3+1]=r_data.ft_data[i].cpt_forces[j].wrench.force.y
                f_vec[i,j*3+2]=r_data.ft_data[i].cpt_forces[j].wrench.force.z
                
                fvar_vec[i,j*3]=np.sqrt(r_data.ft_data[i].cpt_forces[j].wrench.torque.x)
                fvar_vec[i,j*3+1]=np.sqrt(r_data.ft_data[i].cpt_forces[j].wrench.torque.y)
                fvar_vec[i,j*3+2]=np.sqrt(r_data.ft_data[i].cpt_forces[j].wrench.torque.z)

        # plot variance:
        #print fvar_vec[:,0]
        if(False):
            cm_map =  plt.cm.tab20c( (4./3*np.arange(20*3/4)).astype(int) )
            fig = plt.figure(figsize=(6,3))
            
            ax = fig.add_subplot(111)
            #ax.set_color_cycle(cm_map)
        
            for j in range(2,3):
                #j=1
                ax.plot(range(i_start,i_max),f_vec[i_start:i_max,j])
                #plt.plot(range(i_start,i_max),fvar_vec[i_start:i_max,2])
                #print f_vec[i_start:i_max,2]-fvar_vec[i_start:i_max,2]
                ax.fill_between(range(i_start,i_max),
                                 np.ravel(f_vec[i_start:i_max,j]-fvar_vec[i_start:i_max,j]),
                                 np.ravel(f_vec[i_start:i_max,j]+fvar_vec[i_start:i_max,j]),
                                 color=(0.8,0.8,1.0,1))

            #plt.grid(True)
            plt.xlim(i_start,i_max)

            ax.set(ylabel='Force (N)',xlabel='Timestep')
            plt.savefig('/home/bala/force_variance.pdf', bbox_inches='tight')

            #plt.show()
            exit()
        # plot noisy data with cyclic colors
        color_list=[]
        #cm_map = plt.get_cmap('tab20c')#cmaps['Sequential']['tab20c']
        #print np.arange(20*3/4)
        order=list((4./3*np.arange(20*3/4)).astype(int))
        order.reverse()
        print order
        cm_map =  plt.cm.tab20c(order)
        #print cm_map
        fig = plt.figure(figsize=(6,3))

        ax = fig.add_subplot(111)
        '''
        for i in range(4*3):
            color_list.append(cm_map(1.*i/(12.0)))
        '''
        ax.set_color_cycle(cm_map)

        for j  in range(12):
            #fsmooth_vec[:,j] = np.matrix(savitzky_golay(np.ravel(f_vec[:,j]),21,3)).T
            plt.plot(range(i_start,i_max),f_vec[i_start:i_max,j],linewidth=1.0)
        #plt.grid(True)
        plt.xlim(i_start,i_max)


        ax.set(ylabel='Force (N)',xlabel='Timestep')
        import matplotlib.transforms as mtransforms
        trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
        ax.fill_between(range(i_start,i_max), 0, 1, where= np.ravel(f_vec[i_start:i_max,8])==0.0,
                        facecolor='red', alpha=0.2, transform=trans)
        plt.savefig('/home/bala/force_traj.pdf', bbox_inches='tight')
        #ax.add_patch(Rectangle((0,0), 1000, 10, facecolor="grey"))

        #plt.show()
    if(False):
        print "smoothed"
        f_vec = np.matrix([np.zeros(12) for i in range(i_max)])
        fsmooth_vec = np.matrix([np.zeros(12) for i in range(i_max)])
        # read data from file:
        for i in range(i_max):
            for j in range(len(r_data.ft_data[i].cpt_forces)):
                f_vec[i,j*3]=r_data.ft_data[i].cpt_forces[j].wrench.force.x
                f_vec[i,j*3+1]=r_data.ft_data[i].cpt_forces[j].wrench.force.y
                f_vec[i,j*3+2]=r_data.ft_data[i].cpt_forces[j].wrench.force.z

        # filter data:
        for j  in range(12):
            fsmooth_vec[:,j] = np.matrix(savitzky_golay(np.ravel(f_vec[:,j]),21,3)).T
            plt.plot(range(i_max),f_vec[:,j])
            #print fsmooth_vec[:,j]
        #print f_vec
        #plt.plot(range(i_max),f_vec[:,7],'g',range(i_max),f_vec[:,8],'b',range(i_max),f_vec[:,6],'r')
        
        #plt.plot(range(i_max),fsmooth_vec[:,7],'g',range(i_max),fsmooth_vec[:,8],'b',range(i_max),fsmooth_vec[:,6],'r')

        #plt.plot(fsmooth_vec[:,2],'r')
        
        plt.show()
        exit()
        #
        for i in range(i_max):
            for j in range(len(r_data.ft_data[i].cpt_forces)):
                r_data.ft_data[i].cpt_forces[j].wrench.force.x=fsmooth_vec[i,j*3]
                r_data.ft_data[i].cpt_forces[j].wrench.force.y=fsmooth_vec[i,j*3+1]
                r_data.ft_data[i].cpt_forces[j].wrench.force.z=fsmooth_vec[i,j*3+2]
    raw_input("start publishing data?")
    graph = isam_graph()
    # enable graph
    if HYBRID:
        graph.collect_data()
        
        
    i=i_start
    while (not rospy.is_shutdown() and i<len(r_data.ft_data) and i<i_max):
        r_data.ft_data[i].rr_T_obj = PoseStamped()
        r_data.ft_data[i].robot_js = JointState()
        
        d_msg=copy.deepcopy(r_data.ft_data[i])

        #print d_msg
        #print d_msg.header.stamp,d_msg.base_T_palm.pose.position.x
        d_msg.base_gvec.header.stamp=d_msg.header.stamp
        d_msg.header.stamp=rospy.Time.now()
        d_msg.base_T_palm.header.frame_id='lbr4_base_link'
        #print d_msg.cpt_forces
        #raw_input("?")
        pub.publish(d_msg)
        rate.sleep()
        i=i+1

    # optimize pose
    #if HYBRID:
    #    raw_input("Infer pose?")
    #    graph.infer_pose()


    # optimize dynamics
    if HYBRID:
        #raw_input("Infer dynamics?")

        graph.infer_dynamics()
    
    # create a pickle file
    #pickle.dump( data, open( "gt_ft_obj4_fast.p ", "wb" ) )
