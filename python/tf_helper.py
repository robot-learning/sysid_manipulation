import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped,Pose
import tf.transformations
import tf
from std_msgs.msg import Header
from numpy.linalg import inv



class tfHelper:
    def get_tf_pose(self,tf_listener,frame2,frame1,time=rospy.Time(0),rpy=False):
        # Get T mat from TF:
        temp_header = Header()
        temp_header.frame_id = frame1
        temp_header.stamp = time
        got_pose=False
        while(not got_pose):
            try:
                frame1_to_frame2 = tf_listener.asMatrix(frame2, temp_header)
                got_pose=True
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue

        return self.get_pose(frame1_to_frame2,rpy=rpy)
    def get_T(self,tf_listener,frame2,frame1,time=rospy.Time(0)):
        # Get T mat from TF:
        temp_header = Header()
        temp_header.frame_id = frame1
        temp_header.stamp = time
        got_pose=False

        frame1_to_frame2=None
        while(not got_pose):
            
            try:
                frame1_to_frame2=tf_listener.asMatrix(frame2, temp_header)
                #tf_listener.waitForTransform(frame1, frame2, rospy.Time.now(), rospy.Duration(5.0))
                got_pose=True
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
        
        #frame1_to_frame2 = tf_listener.asMatrix(frame2, temp_header)
        return frame1_to_frame2
    def get_T_euler(self,pose):
        pos = np.matrix([pose[0], pose[1], pose[2]]).T
        mat = self.get_R_rpy(pose[3:6])
        mat=np.matrix(mat)
        mat[0:3, 3] = pos
        
        return mat
        
    def get_T_pose(self,pose):
        # build T mat from pose:
        quat = [pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w]
        pos = np.matrix([pose.position.x, pose.position.y, pose.position.z]).T
        mat = np.matrix(tf.transformations.quaternion_matrix(quat))
        mat[0:3, 3] = pos
        return mat
    
    def get_R_rpy(self,rpy):
        # build T mat from pose:
        mat=tf.transformations.euler_matrix(rpy[0],rpy[1],rpy[2],'rxyz')
        return mat

    def get_pose_arr(self,mat,rpy=True):
        pose=np.zeros(6)
        pose[0]=mat[0,3]
        pose[1]=mat[1,3]
        pose[2]=mat[2,3]
        pose[3],pose[4], pose[5] = tf.transformations.euler_from_matrix(mat)
        return pose
    def get_pose(self,mat,rpy=False):
        pose = Pose()
        pose.position.x = mat[0,3]
        pose.position.y = mat[1,3]
        pose.position.z = mat[2,3]
        quat = tf.transformations.quaternion_from_matrix(mat)
        pose.orientation.x = quat[0]
        pose.orientation.y = quat[1]
        pose.orientation.z = quat[2]
        pose.orientation.w = quat[3]
        return pose    

