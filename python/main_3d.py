import numpy as np
from opt_3d import *
from opt_inertia import *
import pickle
import matplotlib as mpl
import matplotlib.pyplot as plt
import tf_helper
import PyKDL
def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    """Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial
    
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def compute_acc_ftips(data,begin,end):
    # get number of ftips:
    n_tips=2#len(data['forces'][0])
    lin_acc=[]
    ang_acc=[]
    forces=[]
    f_loc=[]
    g_v=[]
    o_f=[]
    for i in range(n_tips):
        l_acc,a_acc,force,f_locs,g_vecs,o_fs=compute_acc_ftip(data,begin,end,i)
        lin_acc.extend(l_acc)
        ang_acc.extend(a_acc)
        forces.extend(force)
        f_loc.extend(f_locs)
        g_v.extend(g_vecs)
        o_f.extend(o_fs)
    '''
    for i in range(len(forces)):
        forces[i][3]=-1.0*forces[i][3]
    '''
    return lin_acc,ang_acc,forces,f_loc,g_v,o_f

def compute_acc_ftip(data,begin,end,ftip):
    tf_h=tf_helper.tfHelper()
    w_f_poses=[]
    f_loc=[]
    o_f=[]
    # compute acceleration of ftip
    for i in range(begin,end):
        w_o_pose=data['obj_pose'][i]
        o_f_pose=data['f_loc'][i][ftip]
        w_T_o=tf_h.get_T_euler(w_o_pose)
        o_T_f=tf_h.get_T_euler(np.array([o_f_pose[0],o_f_pose[1],o_f_pose[2],0,0,0]))
        w_f=w_T_o*o_T_f
        #print w_f
        w_f_pose=tf_h.get_pose_arr(w_f)
        #print w_f_pose
        w_f_poses.append(w_f_pose)
        # transform f_locations to reference ftip
        o_f_locs=[np.zeros(3) for j in range(len(data['f_loc'][i]))]
        o_ref_loc=data['f_loc'][i][ftip][0:3]
        o_f.append(o_ref_loc)
        # build reference matrix
        o_T_fref=np.eye(4)
        o_T_fref[0:3,3]=o_ref_loc
        o_T_fref=np.matrix(np.linalg.inv(o_T_fref))
        for j in range(len(data['f_loc'][i])):
            f1_vec=np.matrix([data['f_loc'][i][j][0],data['f_loc'][i][j][1],data['f_loc'][i][j][2],1.0]).T
            o_f_locs[j]=np.ravel(o_T_fref*f1_vec)[0:3]
        f_loc.append(o_f_locs)
    
    forces=data['forces'][begin:end-2]
    g_vec=data['g_vec'][begin:end-2]
    time_arr=data['pose_time'][begin:end]
    #print time_arr
    lin_acc,ang_acc=compute_acc(w_f_poses,time_arr)

    # smooth out force:
    for j in range(len(forces[0])):
        f_mat=np.zeros((len(forces),3))
        for i in range(len(forces)):
            f_mat[i,:]=forces[i][j]
        # smoothout:
        smooth_mat=f_mat
        for k in range(3):
            smooth_mat[:,k]=np.matrix(savitzky_golay(np.ravel(f_mat[:,k]),51,3))
        #plt.plot(smooth_mat)
        #plt.show()
        for i in range(len(forces)):
            forces[i][j]=np.array([smooth_mat[i,0],smooth_mat[i,1],smooth_mat[i,2]])
    return lin_acc,ang_acc,forces,f_loc[:-2],g_vec,o_f[:-2]

def compute_ang_vel(pose1,pose2,dt):
    vel=np.zeros(3)
    # compute angular velocities:
    # simple radial method:
    for j in range(3):
        theta_1=pose1[3+j]
        theta_2=pose2[3+j]
        a=np.array([np.cos(theta_1),np.sin(theta_1)])
        b=np.array([np.cos(theta_2),np.sin(theta_2)])
        vel[j]=np.arccos(np.dot(a,b))/dt
    # pykdl twist computation:
    frame1=PyKDL.Frame(PyKDL.Rotation.RPY(pose1[3+0],pose1[3+1],pose1[3+2]),
                       PyKDL.Vector(pose1[0],pose1[1],pose1[2]))
    frame2=PyKDL.Frame(PyKDL.Rotation.RPY(pose2[3+0],pose2[3+1],pose2[3+2]),
                       PyKDL.Vector(pose2[0],pose2[1],pose2[2]))
    twist=PyKDL.diff(frame1,frame2,1)
    vel=np.array([twist.vel.x(),twist.vel.y(),twist.vel.z(),twist.rot.x(),twist.rot.y(),twist.rot.z()])/(dt)
    
    return vel
def compute_acc(poses,times):
    linear_acc=[]
    angl_acc=[]
    lin_vel=[]
    kdl_vel=[]
    
    time_arr=[]
    # filter poses too:
    pos_mat=np.matrix(poses)
    '''
    for i in range(6):
        pos_mat[:,i]=np.matrix(savitzky_golay(np.ravel(pos_mat[:,i]),51,3)).T
    '''
    for i in range(1,len(poses)):
        dt=(times[i]-times[i-1]).to_sec()
        time_arr.append(dt)
    #plt.plot(time_arr)
    #plt.show()
    time_arr=savitzky_golay(np.ravel(time_arr),51,3)

    for i in range(1,len(poses)):
        dt=time_arr[i-1]#(times[i]-times[i-1]).to_sec()
        
        #vel=np.ravel(pos_mat[i,:]-pos_mat[i-1,:])/dt
        if(dt==0):
            kdl_twist_vel=np.zeros(6)
        else:
            kdl_twist_vel=compute_ang_vel(np.ravel(pos_mat[i-1,:]),np.ravel(pos_mat[i,:]),dt)
        kdl_vel.append(kdl_twist_vel)
    '''
    plt.plot(kdl_vel)
    plt.legend()
    plt.show()
    plt.plot(lin_vel)
    plt.show()
    '''
    vel_mat=np.matrix(kdl_vel)
    for i in range(6):
        vel_mat[:,i]=np.matrix(savitzky_golay(np.ravel(vel_mat[:,i]),51,3)).T
    # compute acc
    for i in range(1,len(vel_mat[:,0])):
        dt=time_arr[i-1]#(times[i]-times[i-1]).to_sec()
        if(dt==0):
            acc=np.zeros(6)
        else:
            acc=np.ravel((vel_mat[i,:]-vel_mat[i-1,:])/dt)
        linear_acc.append(acc[0:3])
        angl_acc.append(acc[3:7])

    # smooth acc:
    acc_mat=np.matrix(linear_acc)
    for i in range(3):
        acc_mat[:,i]=np.matrix(savitzky_golay(np.ravel(acc_mat[:,i]),51,3)).T
    # convert matrix to list:
    for i in range(len(linear_acc)):
        linear_acc[i]=np.ravel(acc_mat[i,:])

    acc_mat=np.matrix(angl_acc)
    for i in range(3):
        acc_mat[:,i]=np.matrix(savitzky_golay(np.ravel(acc_mat[:,i]),51,3)).T
    # convert matrix to list:
    for i in range(len(angl_acc)):
        angl_acc[i]=np.ravel(acc_mat[i,:])
    #plt.plot(time_arr)
    #plt.show()
    #plt.plot(vel_mat)
    #plt.show()
    return linear_acc,angl_acc#,vel_mat,time_arr

def filter_data(lin_acc,ang_acc,forces,f_loc,g_v,o_f,thresh=0.1):
    f_lin_acc=[]
    f_ang_acc=[]
    f_forces=[]
    f_f_loc=[]
    f_g_v=[]
    f_o_f=[]
    print len(ang_acc),len(forces),len(f_loc),len(g_v)
    for i in range(len(forces)):
        if not ((abs(ang_acc[i][0])>thresh or abs(ang_acc[i][1])>thresh or abs(ang_acc[i][2])>thresh)):
            f_lin_acc.append(lin_acc[i])
            f_ang_acc.append(ang_acc[i])
            f_forces.append(forces[i])
            f_f_loc.append(f_loc[i])
            f_g_v.append(g_v[i])
            f_o_f.append(o_f[i])

    return f_lin_acc,f_ang_acc,f_forces,f_f_loc,f_g_v,f_o_f
def zero_data(lin_acc,ang_acc):
    thresh=0.1
    f_lin_acc=[]
    f_ang_acc=[]
    for i in range(len(ang_acc)):
        f_ang_acc.append(np.zeros(3))
        f_lin_acc.append(np.zeros(3))
        '''
        if not (abs(ang_acc[i][0])>thresh or abs(ang_acc[i][1])>thresh or abs(ang_acc[i][2])>thresh):
            f_lin_acc.append(lin_acc[i])
            f_ang_acc.append(ang_acc[i])
            f_forces.append(forces[i])
            f_f_loc.append(f_loc[i])
        f_g_v.append(g_v[i])
        '''
    return f_lin_acc,f_ang_acc

def fit3d(file_name):
    begin=0
    data={}
    for i in file_name:
        data_arr=pickle.load(open(i, "rb" ))
        for k in data_arr.keys():
            if(not (k in data)):
                data[k]=data_arr[k][len(data_arr[k])-100:len(data_arr[k])-80]
            else:
                data[k].extend(data_arr[k][len(data_arr[k])-100:len(data_arr[k])-80])
        
    end=len(data['obj_pose'])#-40
    print end
    raw_input('t?')
    # compute object acceleration:
    #o_poses=data['obj_pose'][begin:end]
    #o_time=data['pose_time'][begin:end]

    # compute linear and angular acceleration from fingertip frames
    #lin_acc,ang_acc,forces,f_loc,g_v,o_f=compute_acc_ftip(data,begin,end,0)
    #lin_acc,ang_acc,forces,f_loc,g_v,o_f=compute_acc_ftips(data,begin,end)
    #lin_acc,ang_acc,forces,f_loc,g_v,o_f=compute_acc_ftip(data,begin,end,3)

    #print lin_acc
    
    #print o_poses
    #lin_acc,ang_acc,vel,t_arr=compute_acc(o_poses,o_time)
    #lin_acc=lin_acc
    #ang_acc=ang_acc
    #forces=data['forces'][begin:end-2]
    #g_v=data['g_vec'][begin:end-2]

    #f_loc=data['f_loc'][begin:end-2]
    #plt.plot(lin_acc)
    #plt.show()
    #plt.plot(ang_acc)
    #plt.show()
    # filter data
    #lin_acc,ang_acc,forces,f_loc,g_v,o_f=filter_data(lin_acc,ang_acc,forces,f_loc,g_v,o_f,thresh=0.1)
    #plt.plot(lin_acc)
    #plt.show()
    #plt.plot(ang_acc)
    #plt.show()

    #plt.plot(t_arr)
    #plt.show()
    #plt.plot(o_mat[:,0:3])
    #plt.show()
    
    # send to optimizer:
    #ftip=0
    #o_f1=data['f_loc'][10][ftip]
    #print data['f_loc'][10]
    #inf_model=GraspModel()
    #rc=np.zeros(3)
    #prior=np.array([0.2,0.0,0.0,0.0])
    #m,rc=inf_model.get_optimized_values(lin_acc,ang_acc,forces,f_loc,g_v,o_f,prior)
    #raw_input("get inertia?")
    #print m,rc
    m=0.2
    rc=np.array([0.0,0.0,0.0])
    lin_acc,ang_acc,forces,f_loc,g_v,o_f=compute_acc_ftips(data,begin,end)

    
    for i in range(len(lin_acc)):
        lin_acc[i]=np.zeros(3)
        ang_acc[i]=np.zeros(3)
    lin_acc,ang_acc,forces,f_loc,g_v,o_f=filter_data(lin_acc,ang_acc,forces,f_loc,g_v,o_f,thresh=0.0)
    print len(lin_acc)
    inf_model=GraspModel()
    rc=np.zeros(3)
    prior=np.array([0.1,0.0,0.0,0.0])
    m,rc=inf_model.get_optimized_values(lin_acc,ang_acc,forces,f_loc,g_v,o_f,prior)
    print m,rc
    raw_input("get inertia?")

    exit()
    #plt.plot(lin_acc)
    #plt.show()
    #plt.plot(ang_acc)
    #plt.show()

    inertia_model=InertiaModel()
    #f_rc=rc-o_f1
    #prior_x=np.array([0.064301,0.064301,0.10376,0.0,0.0,0.0, 0.1,0.1,0.1,0.1,0.1,0.1])
    prior_x=np.array([0.1,0.01,0.1,0.0,0.0,0.0, 0.1,0.1,0.1,0.1,0.1,0.1])
    
    H=np.zeros((3,3))
    ixx=prior_x[0]
    iyy=prior_x[1]
    izz=prior_x[2]
    ixy=prior_x[3]
    ixz=prior_x[4]
    iyz=prior_x[5]
    H[0,:]=np.array([ixx,ixy,ixz])
    H[1,:]=np.array([ixy,iyy,iyz])
    H[2,:]=np.array([ixz,iyz,izz])
    A=np.linalg.cholesky(H)
    prior_x[6]=A[0,0]
    prior_x[7]=A[1,1]
    prior_x[8]=A[2,2]
    prior_x[9]=A[0,1]
    prior_x[10]=A[0,2]
    prior_x[11]=A[1,2]
    
    
    '''
    H=inertia_model.optimize_inertia(lin_acc,ang_acc,forces,f_loc,g_v,m,rc,prior_x,o_f)
    A=np.linalg.cholesky(H)
    print A.dot(A.T)
    '''
    H,m,rc=inertia_model.optimize_ols(lin_acc,ang_acc,forces,f_loc,g_v,m,rc,prior_x,o_f)
    print "Mass: ",m,rc
    #exit()
    #A=np.linalg.cholesky(H)
    #print A.dot(A.T)
    
    # compute variance:
    var,cov=inertia_model.variance(m,rc,H)
    print 'Variance: ', var
    print np.diag(cov)
    plt.matshow(cov)
    groups=['m','rx','ry','rz','ixx','iyy','izz','ixy','ixz','iyz']
    x_pos = np.arange(len(groups))
    plt.xticks(x_pos,groups)

    y_pos = np.arange(len(groups))
    plt.yticks(y_pos,groups)
    plt.colorbar()
    plt.show()
    

if __name__=='__main__':
    list_files=['../data/bt_kuka_0.p',
                '../data/bt_kuka_1.p',
                '../data/bt_kuka_2.p',
                '../data/bt_kuka_4.p',
                '../data/bt_kuka_5.p',
                '../data/bt_kuka_6.p',
                '../data/bt_kuka_8.p']
    fit3d(list_files)
    #fit3d('../data/dart_traj5.p')
