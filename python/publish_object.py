import rospy
#from viz_utils import *
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Quaternion, Pose, Point, Vector3
from std_msgs.msg import Header, ColorRGBA
if __name__=='__main__':

    rospy.init_node("object_node")
    # publish object

    marker = Marker( type=Marker.MESH_RESOURCE,
                     id=0,
                     lifetime=rospy.Duration(1.5),
                     pose=Pose(Point(0.0, 0.0,0.0), Quaternion(0, 0, 0, 1)),
                     scale=Vector3(1,1,1),
                     header=Header(frame_id='object'),
                     color=ColorRGBA(0.0, 1.0, 0.0, 0.8),
                     mesh_resource="package://ll4ma_collision_wrapper/data/mustard_dart.stl"
                     )
    
    marker_publisher = rospy.Publisher('visualization_marker', Marker, queue_size=5)

    rate=rospy.Rate(100)
    while not rospy.is_shutdown():
        marker.header.stamp=rospy.Time.now()
        rate.sleep()
        marker_publisher.publish(marker)

    # publish com marker
