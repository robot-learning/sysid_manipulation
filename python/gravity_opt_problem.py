import numpy as np
import pygmo as pg

class gravity_grasp_function:
    def __init__(self):
        # input data
        self.r_c=np.zeros(3)
        self.m=0.0
        self.r_i=[]
        self.g=np.zeros(3)
        self.mu=0.0
        # surface normals:
        self.s_n=[]
        # max force vectors:
        self.f_max=[]
        self.s_idx=3
    def fitness(self,x):
        # x=[g_vec, f_i]
        # max bounds on force 
        # build rotation matrix from orientation:
        g=np.array([x[0],x[1],x[2]])
        f_i=[]
        for i in range(len(self.s_n)):
            f_i.append(np.array([x[(i+1)*3+0],x[(i+1)*3+1],x[(i+1)*3+2]]))
        cost=0.0
        #
        g_cost=(9.8-np.linalg.norm(g))**2
        cost+=g_cost

        f_sum=self.m*g
        t_sum=np.cross(self.r_c,self.m*g)
        # mu_cost: Friction cone
        for i in range(len(self.s_n)):
            f_sum+=f_i[i]

            t_sum+=np.cross(self.r_i[i],f_i[i])
            if(np.linalg.norm(f_i[i])<0.0001):
                continue
            # find angle between force and surface_normal
            f_cone=np.arctan(self.mu)
            #print np.arccos(np.dot(self.s_n[i],f_i[i]/np.linalg.norm(f_i[i]))),f_cone
            cost+=(f_cone-np.amax([np.arccos(np.dot(self.s_n[i],f_i[i]/np.linalg.norm(f_i[i]))),f_cone]))**2

        # force balancing:
        cost+=np.linalg.norm(f_sum)**2
        
        # torque balancing:
        cost+=np.linalg.norm(t_sum)**2
        # minimize force in f_s
        cost+=np.linalg.norm(f_i[self.s_idx])**2
        #print cost
        return [cost]
    def gradient(self,x):
        grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)
        return grad_fd
    def get_bounds(self):
        #  g_vec,f_i
        low_bounds=[-9.8,-9.8,-9.8]
        up_bounds=[9.8,9.8,9.8]
        for i in range(len(self.f_max)):
            low_bounds.extend(-self.f_max[i])
            up_bounds.extend(self.f_max[i])
        return (low_bounds,up_bounds)
    def update_data(self,r_c,r_i,s_n,m,g,mu,f_max,s_idx=3):
        self.r_c=r_c
        self.r_i=r_i
        self.s_n=s_n
        self.m=m
        self.g=g
        self.mu=mu
        self.f_max=f_max
        self.s_idx=s_idx
        
class GravityModel(object):
    def __init__(self):
        self.opt_fn=gravity_grasp_function()

    def optimize(self,r_c,r_i,s_n,m,g,mu,f_max,prior=None):
        # update data:
        opt_fn=gravity_grasp_function()
        opt_fn.update_data(r_c,r_i,s_n,m,g,mu,f_max)
        prob=pg.problem(opt_fn)
        nl=pg.nlopt('slsqp')
        nl.maxeval=300
        algo=pg.algorithm(nl)
        algo.set_verbosity(10)
        
        f_in=prob.fitness(prior)
        
        #print f_in
        print 'creating population'

        pop=pg.population(prob,1)

        print 'optimizing'
        pop=algo.evolve(pop)
        print pop.champion_f
        best_x=pop.champion_x
        return best_x
    def find_rotation(self,g_des,g_cur):
        rot=np.zeros(3)
        return rot
