import rospy
from geometry_msgs.msg import WrenchStamped
import tf_helper
import copy
import message_filters
import numpy as np
import tf.transformations
import tf
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle

from sysid_manipulation.msg import MeasurementData
from geometry_msgs.msg import PoseStamped,WrenchStamped

from biotac_sensors.msg import BioTacHand,BioTacTareHand
class recordData(object):
    def __init__(self,obj_frame='object', f_topics=['/TacNet/index_tip_cpt/force','/TacNet/middle_tip_cpt/force','/TacNet/ring_tip_cpt/force','/TacNet/thumb_tip_cpt/force'],g_frame='lbr4_base_link',g_vec=[0.0,0.0,-9.8]):
        self.forces=[np.zeros(3) for i in range(len(f_topics))]
        self.f_loc=[np.zeros(3) for i in range(len(f_topics))]
        self.g=np.zeros(3)
        self.tf=tf_helper.tfHelper()
        self.tf_l=tf.TransformListener()
        self.obj_frame=obj_frame
        
        self.bt_sub=rospy.Subscriber('/biotac_pub',BioTacHand,self.bt_cb)
        self.bt_tare_sub=rospy.Subscriber('/biotac_tare_pub',BioTacTareHand,self.bt_tare_cb)
        self.f1_sub=message_filters.Subscriber(f_topics[0],WrenchStamped)
        self.f2_sub=message_filters.Subscriber(f_topics[1],WrenchStamped)
        self.f3_sub=message_filters.Subscriber(f_topics[2],WrenchStamped)
        self.f4_sub=message_filters.Subscriber(f_topics[3],WrenchStamped)
        self.ft_sub=message_filters.Subscriber('/ft_sensor/negated',WrenchStamped)

        self.f_frames=[None for i in range(len(f_topics))]
        self.ft_frame=None
        self.ft_force=None
        self.ft_torque=None
        self.ft_loc=None
        self.bt_data=None
        self.bt_tare_data=None
        #self.pose_sub=message_filters.Subscriber('sim/obj_pose',PoseStamped)
        self.ts = message_filters.ApproximateTimeSynchronizer([self.f1_sub, self.f2_sub, self.f3_sub, self.f4_sub, self.ft_sub], 100,slop=0.1)

        self.ts.registerCallback(self.msg_cb)
        self.time=None
        self.g_frame='lbr4_base_link'
        g_vec.append(0.0)
        self.g_vec=np.ravel(g_vec)
        self.got_data=False
        self.obj_pose=None
    def bt_cb(self,msg):
        self.bt_data=msg
    def bt_tare_cb(self,msg):
        self.bt_tare_data=msg
    def msg_cb(self,f1_msg,f2_msg,f3_msg,f4_msg,ft_msg,bt_msg=None,bt_tare_msg=None):

        #self.forces[0]=np.array([f1_msg.wrench.force.x,f1_msg.wrench.force.y,f1_msg.wrench.force.z])
        self.f_frames[0]=f1_msg.header.frame_id
        self.f_frames[1]=f2_msg.header.frame_id
        self.f_frames[2]=f3_msg.header.frame_id
        self.f_frames[3]=f4_msg.header.frame_id
        self.ft_frame=ft_msg.header.frame_id
        #self.f_loc[0]=np.array([f1_msg.wrench.torque.x,f1_msg.wrench.torque.y,f1_msg.wrench.torque.z])
        self.forces[0]=np.array([f1_msg.wrench.force.x,f1_msg.wrench.force.y,f1_msg.wrench.force.z])

        self.forces[1]=np.array([f2_msg.wrench.force.x,f2_msg.wrench.force.y,f2_msg.wrench.force.z])
        #self.f_loc[1]=np.array([f2_msg.wrench.torque.x,f2_msg.wrench.torque.y,f2_msg.wrench.torque.z])
        
        self.forces[2]=np.array([f3_msg.wrench.force.x,f3_msg.wrench.force.y,f3_msg.wrench.force.z])
        #self.f_loc[2]=np.array([f3_msg.wrench.torque.x,f3_msg.wrench.torque.y,f3_msg.wrench.torque.z])

        self.forces[3]=np.array([f4_msg.wrench.force.x,f4_msg.wrench.force.y,f4_msg.wrench.force.z])

        self.ft_force=np.array([ft_msg.wrench.force.x,ft_msg.wrench.force.y,ft_msg.wrench.force.z])
        self.ft_torque=np.array([ft_msg.wrench.torque.x,ft_msg.wrench.torque.y,ft_msg.wrench.torque.z])

        #self.f_loc[3]=np.array([f4_msg.wrench.torque.x,f4_msg.wrench.torque.y,f4_msg.wrench.torque.z])
        #self.bt_data=bt_msg
        #self.bt_tare_data=bt_tare_msg
        self.time=f1_msg.header.stamp
        self.get_tf_data(self.time)
        self.got_data=True
    def get_tf_data(self,time):
        # get gravity
        time=rospy.Time(0)
        g_vec=np.matrix(self.g_vec).T
        T_mat=copy.deepcopy(self.tf.get_T(self.tf_l,self.obj_frame,self.g_frame,time))#[0:3,0:3]
        g=np.ravel(T_mat*g_vec)
        self.g=copy.deepcopy(g[0:3])
        # get fingertip contact point vectors:
        for i in range(len(self.f_frames)):
            f_pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,self.f_frames[i],time)
            f_arr=np.matrix([self.forces[i][0],self.forces[i][1],self.forces[i][2],0.0]).T
            f_mat=self.tf.get_T_pose(f_pose)
            self.f_loc[i]=np.array([f_pose.position.x,f_pose.position.y,f_pose.position.z,f_pose.orientation.x,f_pose.orientation.y,f_pose.orientation.z,f_pose.orientation.w])

            self.forces[i]=np.ravel(f_mat*f_arr)[0:3]
            
            #if(i==0):
                #pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,self.g_frame,time)
        # transform ft data:
        ft_pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,self.ft_frame,time)
        f_arr=np.matrix([self.ft_force[0],self.ft_force[1],self.ft_force[2],0.0]).T
        t_arr=np.matrix([self.ft_torque[0],self.ft_torque[1],self.ft_torque[2],0.0]).T
        f_mat=self.tf.get_T_pose(ft_pose)
        self.ft_loc=np.array([ft_pose.position.x, ft_pose.position.y, ft_pose.position.z, ft_pose.orientation.x, ft_pose.orientation.y, ft_pose.orientation.z, ft_pose.orientation.w])

        self.ft_force=np.ravel(f_mat*f_arr)[0:3]

        # transform torque:
        self.ft_torque=np.ravel(f_mat*t_arr)[0:3]
        
        o_pose=copy.deepcopy(self.tf.get_tf_pose(self.tf_l,self.g_frame,self.obj_frame,time))
        
        quaternion = (
            o_pose.orientation.x,
            o_pose.orientation.y,
            o_pose.orientation.z,
            o_pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)


        self.obj_pose=np.array([o_pose.position.x, o_pose.position.y,o_pose.position.z,euler[0],euler[1],euler[2]])
      
            
if __name__=='__main__':
    rospy.init_node('data_logger')
    rate=rospy.Rate(30)
    r_data=recordData()


    print 'data init'

    data=[]

    i=0
    pub = rospy.Publisher('/sysid/data', MeasurementData, queue_size=10)

    while (not rospy.is_shutdown()):# and i<2000:
        if(r_data.got_data):
            # publish to ros
            d_msg=MeasurementData()
            d_msg.header.stamp=rospy.Time.now()
            for j in range(4):
                cpt=PoseStamped()
                cpt.pose.position.x=r_data.f_loc[j][0]
                cpt.pose.position.y=r_data.f_loc[j][1]
                cpt.pose.position.z=r_data.f_loc[j][2]
                d_msg.cpts.append(cpt)
            for j in range(4):
                force=WrenchStamped()
                force.wrench.force.x=r_data.forces[j][0]
                force.wrench.force.y=r_data.forces[j][0]
                force.wrench.force.z=r_data.forces[j][0]
                d_msg.forces.append(force)

            d_msg.g_vec.pose.position.x=r_data.g[0]
            d_msg.g_vec.pose.position.y=r_data.g[1]
            d_msg.g_vec.pose.position.z=r_data.g[2]
            #
            d_msg.ft_force.wrench.force.x=r_data.ft_force[0]
            d_msg.ft_force.wrench.force.y=r_data.ft_force[1]
            d_msg.ft_force.wrench.force.z=r_data.ft_force[2]
            
            d_msg.ft_force.wrench.torque.x=r_data.ft_torque[0]
            d_msg.ft_force.wrench.torque.y=r_data.ft_torque[1]
            d_msg.ft_force.wrench.torque.z=r_data.ft_torque[2]

            d_msg.ft_cpt.pose.position.x=r_data.ft_loc[0]
            d_msg.ft_cpt.pose.position.y=r_data.ft_loc[1]
            d_msg.ft_cpt.pose.position.z=r_data.ft_loc[2]
            
            
            pub.publish(d_msg)
            data.append(d_msg)
            r_data.got_data=False
        rate.sleep()
    #rospy.spin()
    # create a pickle file
    pickle.dump( data, open( "bt_ft_kuka_4.p ", "wb" ) )
