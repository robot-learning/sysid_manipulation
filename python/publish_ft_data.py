import rospy
from geometry_msgs.msg import WrenchStamped
import numpy as np
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle
from sysid_manipulation.msg import FullMeasurementData
from gtsam_toggle import *
from geometry_msgs.msg import PoseStamped,WrenchStamped
from sensor_msgs.msg  import JointState
import matplotlib.pyplot as plt
from spline_util import *

#from sysid_manipulation.msg import MeasurementData
#from geometry_msgs.msg import PoseStamped,WrenchStamped

#from biotac_sensors.msg import BioTacHand,BioTacTareHand
class readData(object):
    def __init__(self,f_name):
        # read data:
        #data_dir='/hdd/data1/sysid_grasp/pickle/'+f_name

        self.ft_data=pickle.load(open(f_name,"rb"))
     
            
if __name__=='__main__':
    HYBRID = True

    rospy.init_node('data_logger')
    rate=rospy.Rate(60)
    f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/ft/full_gt_ft_obj4_slow.p'
    r_data=readData(f_name)
    pub = rospy.Publisher('/sysid/ft_data', FullMeasurementData, queue_size=10)
    print 'data init',len(r_data.ft_data)
    graph = isam_graph()
    i_start=0

    i_max = len(r_data.ft_data)
    data=[]
    #if HYBRID:
    #    graph.collect_data()

    if(True):
        f_vec = np.matrix([np.zeros(6) for i in range(i_max)])
        # read data from file:
        for i in range(i_max):
            f_vec[i,0]=r_data.ft_data[i].ft_force.wrench.force.x
            f_vec[i,1]=r_data.ft_data[i].ft_force.wrench.force.y
            f_vec[i,2]=r_data.ft_data[i].ft_force.wrench.force.z
            f_vec[i,3]=r_data.ft_data[i].ft_force.wrench.torque.x
            f_vec[i,4]=r_data.ft_data[i].ft_force.wrench.torque.y
            f_vec[i,5]=r_data.ft_data[i].ft_force.wrench.torque.z


        # plot noisy data with cyclic colors
        color_list=[]
        #cm_map = plt.get_cmap('tab20c')#cmaps['Sequential']['tab20c']
        cm_map =  plt.cm.tab20c( (4./3*np.arange(20*3/4)).astype(int) )
        #print cm_map
        fig = plt.figure(figsize=(6,3))
        
        ax = fig.add_subplot(111)
        '''
        for i in range(4*3):
            color_list.append(cm_map(1.*i/(12.0)))
        '''
        ax.set_color_cycle(cm_map)
        labels=['x','y','z']
        for j  in range(0,3):
            #fsmooth_vec[:,j] = np.matrix(savitzky_golay(np.ravel(f_vec[:,j]),21,3)).T
            plt.plot(range(i_start,i_max),f_vec[i_start:i_max,j],linewidth=1.0,label=labels[j])
        #plt.grid(True)
        plt.legend()
        plt.xlim(i_start,i_max)


        ax.set(ylabel='Force (N)',xlabel='Timestep')
        plt.savefig('/home/bala/ft_force_traj.pdf', bbox_inches='tight')
        plt.show()
    if(True):
        print "smoothed"
        f_vec = np.matrix([np.zeros(6) for i in range(i_max)])
        fsmooth_vec = np.matrix([np.zeros(6) for i in range(i_max)])
        # read data from file:
        for i in range(i_max):
            f_vec[i,0]=r_data.ft_data[i].ft_force.wrench.force.x
            f_vec[i,1]=r_data.ft_data[i].ft_force.wrench.force.y
            f_vec[i,2]=r_data.ft_data[i].ft_force.wrench.force.z
            f_vec[i,3]=r_data.ft_data[i].ft_force.wrench.torque.x
            f_vec[i,4]=r_data.ft_data[i].ft_force.wrench.torque.y
            f_vec[i,5]=r_data.ft_data[i].ft_force.wrench.torque.z

        # filter data:
        
        #for j  in range(6):
            #fsmooth_vec[:,j] = np.matrix(savitzky_golay(np.ravel(f_vec[:,j]),51,3)).T
            #print fsmooth_vec[:,j]
        #print f_vec
        plt.plot(range(i_start,i_max),f_vec[i_start:,1],'b',range(i_max),f_vec[:,2],'r')
        #plt.plot(fsmooth_vec[:,2],'r')
        
        plt.show()
        #
        '''
        for i in range(i_max):
            r_data.ft_data[i].ft_force.wrench.force.x=fsmooth_vec[i,0]
            r_data.ft_data[i].ft_force.wrench.force.y=fsmooth_vec[i,1]
            r_data.ft_data[i].ft_force.wrench.force.z=fsmooth_vec[i,2]
            r_data.ft_data[i].ft_force.wrench.torque.x=fsmooth_vec[i,3]
            r_data.ft_data[i].ft_force.wrench.torque.y=fsmooth_vec[i,4]
            r_data.ft_data[i].ft_force.wrench.torque.z=fsmooth_vec[i,5]
        '''
    raw_input("start publishing data?")
    i = i_start
    while (not rospy.is_shutdown() and i<len(r_data.ft_data) and i<i_max ):
        
        r_data.ft_data[i].rr_T_obj = PoseStamped()
        r_data.ft_data[i].robot_js = JointState()
        d_msg=r_data.ft_data[i]
        #print d_msg.header.stamp,d_msg.base_T_palm.pose.position.x
        d_msg.base_gvec.header.stamp=d_msg.header.stamp
        d_msg.header.stamp=rospy.Time.now()
        d_msg.base_T_palm.header.frame_id='lbr4_base_link'
        pub.publish(d_msg)
        rate.sleep()
        i=i+1
    # create a pickle file
    #pickle.dump( data, open( "gt_ft_obj4_fast.p ", "wb" ) )
    # optimize pose


    # optimize dynamics
    #if HYBRID:
        #raw_input("Infer dynamics?")

    graph.infer_dynamics()
    
