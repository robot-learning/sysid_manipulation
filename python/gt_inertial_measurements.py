import rospy
from geometry_msgs.msg import WrenchStamped
import tf_helper
import copy
import message_filters
import numpy as np
import tf.transformations
import tf
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle
from sensor_msgs.msg import JointState
from sysid_manipulation.msg import FtData
#from sysid_manipulation.msg import MeasurementData
from geometry_msgs.msg import PoseStamped,WrenchStamped

#from biotac_sensors.msg import BioTacHand,BioTacTareHand
class recordData(object):
    def __init__(self):
        
        
        self.jt_sub=message_filters.Subscriber('/lbr4/joint_states',JointState)
        self.ft_sub=message_filters.Subscriber('/ethdaq_data',WrenchStamped)

        
        self.ts = message_filters.ApproximateTimeSynchronizer([self.jt_sub,self.ft_sub], 100,slop=1.0)

        self.ts.registerCallback(self.msg_cb)
        self.time=None
        self.got_data=False
        self.ft_data=None

        self.js_data=None
        
    def msg_cb(self,jt_msg,ft_msg):

        self.ft_data=ft_msg
        self.js_data=jt_msg
        self.time=ft_msg.header.stamp

        self.got_data=True
     
            
if __name__=='__main__':
    rospy.init_node('data_logger')
    rate=rospy.Rate(500)
    r_data=recordData()


    print 'data init'

    data=[]

    raw_input("start data collection?")
    while (not rospy.is_shutdown()):# and i<2000:
        if(r_data.got_data):
            #print 'got data!'
            # publish to ros
            d_msg=FtData()
            d_msg.header.stamp=rospy.Time.now()

            d_msg.ft_force=r_data.ft_data

            d_msg.robot_js=r_data.js_data
           
            data.append(d_msg)
            r_data.got_data=False
            #print d_msg
        rate.sleep()
    # create a pickle file
    pickle.dump( data, open( "gt_ft_obj4_fast.p ", "wb" ) )
