import rospy
from geometry_msgs.msg import WrenchStamped
import numpy as np
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle
from sysid_manipulation.msg import FullMeasurementData
import copy
from sysid_manipulation.srv import *
from gtsam_toggle import *
import tf

#from sysid_manipulation.msg import MeasurementData
#from geometry_msgs.msg import PoseStamped,WrenchStamped

#from biotac_sensors.msg import BioTacHand,BioTacTareHand
class readData(object):
    def __init__(self,f_name):
        # read data:
        #data_dir='/hdd/data1/sysid_grasp/pickle/'+f_name

        self.ft_data=pickle.load(open(f_name,"rb"))
     

if __name__=='__main__':
    rospy.init_node('data_logger')
    rate=rospy.Rate(30)
    #f_name='/hdd/data1/sysid_grasp/new_pickle/obj_2_slow.p'

    #f_name='/hdd/data1/sysid_grasp/pickle/obj4_0_slow.p'
    f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/bt/obj_4_slow.p'
    #f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/euler_constant_acc'
    #f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/dart_data'
    #f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/sim_interp'

    r_data=readData(f_name).ft_data
    
    # create publishers:
    # forces
    
    f_pub =[ rospy.Publisher('/sim/force1', WrenchStamped, queue_size=10),
             rospy.Publisher('/sim/force2', WrenchStamped, queue_size=10),
             rospy.Publisher('/sim/force3', WrenchStamped, queue_size=10),
             rospy.Publisher('/sim/force4', WrenchStamped, queue_size=10)]

    br = tf.TransformBroadcaster()
    print 'data read from file, total size:',len(r_data)

    data=[]

    raw_input("playback data?")

    i = 0
    while (not rospy.is_shutdown() and i<len(r_data)): # and i<1700):
        print i
        # publish object and finger frames
        br.sendTransform((r_data[i].base_T_palm.pose.position.x,
                          r_data[i].base_T_palm.pose.position.y,
                          r_data[i].base_T_palm.pose.position.z),
                         (r_data[i].base_T_palm.pose.orientation.x,
                          r_data[i].base_T_palm.pose.orientation.y,
                          r_data[i].base_T_palm.pose.orientation.z,
                          r_data[i].base_T_palm.pose.orientation.w),
                         rospy.Time.now(),
                         "palm",
                         "world")

        br.sendTransform((r_data[i].palm_T_obj.pose.position.x,
                          r_data[i].palm_T_obj.pose.position.y,
                          r_data[i].palm_T_obj.pose.position.z),
                         (r_data[i].palm_T_obj.pose.orientation.x,
                          r_data[i].palm_T_obj.pose.orientation.y,
                          r_data[i].palm_T_obj.pose.orientation.z,
                          r_data[i].palm_T_obj.pose.orientation.w),
                         rospy.Time.now(),
                         "object",
                         "palm")


        # publish finger forces
        for k in range(4):
            #print k
            br.sendTransform((r_data[i].palm_T_ftips[k].pose.position.x,
                              r_data[i].palm_T_ftips[k].pose.position.y,
                              r_data[i].palm_T_ftips[k].pose.position.z),
                             (r_data[i].palm_T_ftips[k].pose.orientation.x,
                              r_data[i].palm_T_ftips[k].pose.orientation.y,
                              r_data[i].palm_T_ftips[k].pose.orientation.z,
                              r_data[i].palm_T_ftips[k].pose.orientation.w),
                             rospy.Time.now(),
                             'ftip'+str(k+1),
                             "palm")

            br.sendTransform((r_data[i].ftips_T_cpts[k].pose.position.x,
                              r_data[i].ftips_T_cpts[k].pose.position.y,
                              r_data[i].ftips_T_cpts[k].pose.position.z),
                             (r_data[i].ftips_T_cpts[k].pose.orientation.x,
                              r_data[i].ftips_T_cpts[k].pose.orientation.y,
                              r_data[i].ftips_T_cpts[k].pose.orientation.z,
                              r_data[i].ftips_T_cpts[k].pose.orientation.w),
                             rospy.Time.now(),
                             'f'+str(k+1),
                             "ftip"+str(k+1))
            
            force = r_data[i].cpt_forces[k]
            force.header.stamp = rospy.Time.now()
            force.header.frame_id= 'f'+str(k+1)
            f_pub[k].publish(force)
        # publish gravity?

        # publish object frame
        br.sendTransform((r_data[i].rr_T_obj.pose.position.x,
                          r_data[i].rr_T_obj.pose.position.y,
                          r_data[i].rr_T_obj.pose.position.z),
                         (r_data[i].rr_T_obj.pose.orientation.x,
                          r_data[i].rr_T_obj.pose.orientation.y,
                          r_data[i].rr_T_obj.pose.orientation.z,
                          r_data[i].rr_T_obj.pose.orientation.w),
                         rospy.Time.now(),
                         'rr_obj',
                         'ftip4')
        
        rate.sleep()
        i=i+1

