from dyn_sim import planarBox
from viz_utils import *
import numpy as np
from mc_model import McModel

if __name__=='__main__':
    # true parameters:
    m=0.1
    r_c=np.array([0.2,0.0])
    H=0.1
    dim_x=0.5
    dim_y=0.5

    oracle_dyn_sim=planarBox(m,r_c,H,dt=1e-2)

    # initial state:
    s0=[np.zeros(3),np.zeros(3),np.zeros(3)]
    s0[0][2]=0.0
    s0[0][1]=0.0
    s0[0][0]=0.0

    # command:
    u=np.array([0.01,0.04])

    # timesteps to simulate
    timesteps=300

    # training model
    
    # initial values:
    m_est=0.1*0.1
    r_c_est=np.array([0.0,0.0])
    H_est=0.01

    est_dyn_sim=planarBox(m_est,r_c_est,H_est,dt=1e-2)



    # initialize learning model
    
    # priors on parameters:
    
    # give new samples:

    # update params if better:
