import rospy
import pickle
import tf
from geometry_msgs.msg import WrenchStamped
import tf_helper
import numpy as np
def get_g(o_pose):
    tf_h=tf_helper.tfHelper()
    o_R_w=np.matrix(np.linalg.inv(tf_h.get_R_rpy(o_pose[3:7])))
    w_g=np.matrix([0.0,0.0,-9.8,0.0]).T
    o_g=np.ravel(o_R_w*w_g)[0:3]
    return o_g
def dartsim_play(file_name):
    begin=0
    end=-1
    data=pickle.load(open( file_name, "rb" ))
    # compute object acceleration:
    o_poses=data['obj_pose']
    #o_time=data['pose_time']
    forces=data['forces'][begin:end]
    f_loc=data['f_loc'][begin:end]
    g_v=data['g_vec'][begin:end]

    rospy.init_node('dartsim_playback_node')

    g_pub=rospy.Publisher('/data/g_force',WrenchStamped,queue_size=1)
    f1_pub=rospy.Publisher('/data/f1_force',WrenchStamped,queue_size=1)
    f2_pub=rospy.Publisher('/data/f2_force',WrenchStamped,queue_size=1)
    f3_pub=rospy.Publisher('/data/f3_force',WrenchStamped,queue_size=1)
    f4_pub=rospy.Publisher('/data/f4_force',WrenchStamped,queue_size=1)
        
    rate=rospy.Rate(100)
    while not rospy.is_shutdown():
        for i in range(len(g_v)):
            force=g_v[i]#0.2*g_v[i]+forces[i][0]+forces[i][1]+forces[i][2]+forces[i][3]#get_g(o_poses[i])
            o_pose=o_poses[i]
            w=WrenchStamped()
            w.header.frame_id='object'
            w.wrench.force.x=force[0]#g_v[i][0]
            w.wrench.force.y=force[1]#g_v[i][1]
            w.wrench.force.z=force[2]#g_v[i][2]
            g_pub.publish(w)
            force=forces[i][0] #get_g(o_poses[i])
            o_pose=o_poses[i]
            w=WrenchStamped()
            w.header.frame_id='f1'
            w.wrench.force.x=force[0]#g_v[i][0]
            w.wrench.force.y=force[1]#g_v[i][1]
            w.wrench.force.z=force[2]#g_v[i][2]
            f1_pub.publish(w)
            force=forces[i][1]#get_g(o_poses[i])
            o_pose=o_poses[i]
            w=WrenchStamped()
            w.header.frame_id='f2'
            w.wrench.force.x=force[0]#g_v[i][0]
            w.wrench.force.y=force[1]#g_v[i][1]
            w.wrench.force.z=force[2]#g_v[i][2]
            f2_pub.publish(w)
            force=forces[i][2]#get_g(o_poses[i])
            o_pose=o_poses[i]
            w=WrenchStamped()
            w.header.frame_id='f3'
            w.wrench.force.x=force[0]#g_v[i][0]
            w.wrench.force.y=force[1]#g_v[i][1]
            w.wrench.force.z=force[2]#g_v[i][2]
            f3_pub.publish(w)
            force=forces[i][3]#get_g(o_poses[i])
            o_pose=o_poses[i]
            w=WrenchStamped()
            w.header.frame_id='f4'
            w.wrench.force.x=force[0]#g_v[i][0]
            w.wrench.force.y=force[1]#g_v[i][1]
            w.wrench.force.z=force[2]#g_v[i][2]
            f4_pub.publish(w)
            br = tf.TransformBroadcaster()
            br.sendTransform((o_pose[0],o_pose[1],o_pose[2]),
                             tf.transformations.quaternion_from_euler(o_pose[3], o_pose[4],o_pose[5]),
                             rospy.Time.now(),
                             "object",
                             "world")
            br.sendTransform((f_loc[i][0][0],f_loc[i][0][1],f_loc[i][0][2]),
                             tf.transformations.quaternion_from_euler(0,0,0),
                             rospy.Time.now(),
                             "f1",
                             "object")
            
            br.sendTransform((f_loc[i][1][0],f_loc[i][1][1],f_loc[i][1][2]),
                             tf.transformations.quaternion_from_euler(0,0,0),
                             rospy.Time.now(),
                             "f2",
                             "object")
            
            br.sendTransform((f_loc[i][2][0],f_loc[i][2][1],f_loc[i][2][2]),
                             tf.transformations.quaternion_from_euler(0,0,0),
                             rospy.Time.now(),
                             "f3",
                             "object")


            
            
            br.sendTransform((f_loc[i][3][0],f_loc[i][3][1],f_loc[i][3][2]),
                             tf.transformations.quaternion_from_euler(0, 0,0),
                             rospy.Time.now(),
                             "f4",
                             "object")
        
            rate.sleep()
        #raw_input('next?')
if __name__=='__main__':
    dartsim_play('../data/bt_kuka_5.p')
    #dartsim_play('dart_traj1.p')
