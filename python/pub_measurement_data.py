import rospy
import numpy as np
import pickle
from sysid_manipulation.msg import MeasurementData
from geometry_msgs.msg import PoseStamped,WrenchStamped
def get_data(file_name):
    begin=0
    data={}
    for i in file_name:
        data_arr=pickle.load(open(i, "rb" ))
        for k in data_arr.keys():
            if(not (k in data)):
                data[k]=data_arr[k][len(data_arr[k])-200-80:len(data_arr[k])-80]
            else:
                data[k].extend(data_arr[k][len(data_arr[k])-200-80:len(data_arr[k])-80])
        
    end=len(data['obj_pose'])#-40
    return data

if __name__=='__main__':
    list_files=['../data/bt_kuka_0.p',
                '../data/bt_kuka_1.p',
                '../data/bt_kuka_2.p',
                '../data/bt_kuka_4.p',
                '../data/bt_kuka_5.p',
                '../data/bt_kuka_6.p',
                '../data/bt_kuka_8.p']
    data=get_data(list_files)
    rospy.init_node('pub_measurements_node')
    pub = rospy.Publisher('/sysid/data', MeasurementData, queue_size=10)
    print len(data[data.keys()[0]])
    raw_input("ready to publish")
    rate=rospy.Rate(100)
    while(not rospy.is_shutdown()):
        for i in range(len(data[data.keys()[0]])):
            d_msg=MeasurementData()
            # get contact points
            for j in range(4):
                cpt=PoseStamped()
                cpt.pose.position.x=data['f_loc'][i][j][0]
                cpt.pose.position.y=data['f_loc'][i][j][1]
                cpt.pose.position.z=data['f_loc'][i][j][2]
                d_msg.cpts.append(cpt)
            # get forces
            
            for j in range(4):
                force=WrenchStamped()
                force.wrench.force.x=data['forces'][i][j][0]
                force.wrench.force.y=data['forces'][i][j][1]
                force.wrench.force.z=data['forces'][i][j][2]
                d_msg.forces.append(force)
        
                
            # get gravity
            d_msg.g_vec.pose.position.x=data['g_vec'][i][0]
            d_msg.g_vec.pose.position.y=data['g_vec'][i][1]
            d_msg.g_vec.pose.position.z=data['g_vec'][i][2]

            pub.publish(d_msg)
            rate.sleep()
