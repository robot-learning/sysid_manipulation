# this file collects data from ros and writes to a pickle file
import rospy
from tf_helper import *
from geometry_msgs.msg import WrenchStamped,PoseStamped
import pickle
import copy
import message_filters

class recordData(object):
    def __init__(self,obj_frame='object',f_topics=['/sim/force1','/sim/force2','/sim/force3']):
        # subscribe to data
        self.forces=[np.zeros(3) for i in range(len(f_topics))]
        self.f_loc=[np.zeros(3) for i in range(len(f_topics))]
        self.tf=tfHelper()
        self.tf_l=tf.TransformListener()
        self.obj_frame=obj_frame

        '''
        f_sub=[]
        for i in range(len(f_topics)):
            f_sub.append(message_filters.Subscriber(f_topics[i],WrenchStamped,
        '''
        self.f1_sub=message_filters.Subscriber(f_topics[0],WrenchStamped)
        self.f2_sub=message_filters.Subscriber(f_topics[1],WrenchStamped)
        self.f3_sub=message_filters.Subscriber(f_topics[2],WrenchStamped)
        
        self.pose_sub=message_filters.Subscriber('sim/obj_pose',PoseStamped)
        self.ts = message_filters.TimeSynchronizer([self.f1_sub, self.f2_sub, self.f3_sub, self.pose_sub], 1)

        self.ts.registerCallback(self.msg_cb)

        self.obj_pose=np.zeros(6)
        self.got_data=False
        self.g=np.zeros(3)
        self.time=0.0
        #self.f_time=[0.0 for len(f_topics)]
    def msg_cb(self,f1_msg,f2_msg,f3_msg,p_msg):
        self.forces[0]=np.array([f1_msg.wrench.force.x,f1_msg.wrench.force.y,f1_msg.wrench.force.z])
        self.f_loc[0]=np.array([f1_msg.wrench.torque.x,f1_msg.wrench.torque.y,f1_msg.wrench.torque.z])
        
        self.forces[1]=np.array([f2_msg.wrench.force.x,f2_msg.wrench.force.y,f2_msg.wrench.force.z])
        self.f_loc[1]=np.array([f2_msg.wrench.torque.x,f2_msg.wrench.torque.y,f2_msg.wrench.torque.z])
        self.forces[2]=np.array([f3_msg.wrench.force.x,f3_msg.wrench.force.y,f3_msg.wrench.force.z])
        self.f_loc[2]=np.array([f3_msg.wrench.torque.x,f3_msg.wrench.torque.y,f3_msg.wrench.torque.z])

        pose=p_msg.pose
        quaternion = (
            pose.orientation.x,
            pose.orientation.y,
            pose.orientation.z,
            pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion,'rxyz')

        self.obj_pose=np.array([pose.position.x,pose.position.y,pose.position.z,euler[0],euler[1],euler[2]])
        g_vec=np.matrix([0.0,0.0,-9.8,0.0]).T
        T_mat=np.linalg.inv(self.tf.get_T_pose(pose))
        g=np.ravel(T_mat*g_vec)
        self.g=g[0:3]
        self.time=p_msg.header.stamp
        self.got_data=True

    def f_cb(self,msg,args):
        idx=args
        self.forces[idx]=np.array([msg.wrench.force.x,msg.wrench.force.y,msg.wrench.force.z])
        self.f_loc[idx]=np.array([msg.wrench.torque.x,msg.wrench.torque.y,msg.wrench.torque.z])
        self.got_data=True
    def p_cb(self,msg):
        pose=msg.pose
        quaternion = (
            pose.orientation.x,
            pose.orientation.y,
            pose.orientation.z,
            pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)

        self.obj_pose=np.array([pose.position.x,pose.position.y,pose.position.z,euler[0],euler[1],euler[2]])
        g_vec=np.matrix([0.0,0.0,-9.8,0.0]).T
        T_mat=np.linalg.inv(self.tf.get_T_pose(pose))
        g=np.ravel(T_mat*g_vec)
        self.g=g[0:3]
        self.time=msg.header.stamp

    def update_obj_pose(self):
        pose=self.tf.get_tf_pose(self.tf_l,self.obj_frame,'world')
        quaternion = (
            pose.orientation.x,
            pose.orientation.y,
            pose.orientation.z,
            pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)
        self.obj_pose=np.array([pose.position.x,pose.position.y,pose.position.z,euler[0],euler[1],euler[2]])
        # store gravity vector:
        g_vec=np.matrix([0.0,0.0,-9.8,0.0]).T
        T_mat=np.linalg.inv(self.tf.get_T_pose(pose))
        g=np.ravel(T_mat*g_vec)
        self.g=g[0:3]
        self.time=rospy.Time.now()
if __name__=='__main__':
    rospy.init_node('data_logger')
    rate=rospy.Rate(100)
    r_data=recordData()
    data={}
    data['obj_pose']=[]
    data['g_vec']=[]
    data['forces']=[]
    data['f_loc']=[]
    data['pose_time']=[]
    i=0

    while (not rospy.is_shutdown()) and i<10000:
        if(r_data.got_data):
            #r_data.update_obj_pose()
            data['obj_pose'].append(r_data.obj_pose)
            data['pose_time'].append(copy.deepcopy(r_data.time))
            data['g_vec'].append(copy.deepcopy(r_data.g))
            data['forces'].append(copy.deepcopy(r_data.forces))
            data['f_loc'].append(copy.deepcopy(r_data.f_loc))
            i+=1
            r_data.got_data=False
            print i
        rate.sleep()
    #rospy.spin()
    # create a pickle file
    pickle.dump( data, open( "../data/dart_traj6.p", "wb" ) )
    
