import rospy
from geometry_msgs.msg import WrenchStamped
import numpy as np
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle
from sysid_manipulation.msg import FullMeasurementData
import copy
from sysid_manipulation.srv import *
from gtsam_toggle import *
#from sysid_manipulation.msg import MeasurementData
#from geometry_msgs.msg import PoseStamped,WrenchStamped

#from biotac_sensors.msg import BioTacHand,BioTacTareHand
class readData(object):
    def __init__(self,f_name):
        # read data:
        #data_dir='/hdd/data1/sysid_grasp/pickle/'+f_name

        self.ft_data=pickle.load(open(f_name,"rb"))
     

if __name__=='__main__':
    HYBRID = True
    rospy.init_node('data_logger')
    rate=rospy.Rate(250)
    #f_name='/hdd/data1/sysid_grasp/pickle/obj1_0_slow.p'
    #f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/bt/obj2_0_slow.p'
    f_name='/home/bala/catkin_ws/src/sysid_manipulation/data/sim/test3'

    r_data=readData(f_name)
    pub = rospy.Publisher('/sysid/bt_data', FullMeasurementData, queue_size=10)
    print 'data init',len(r_data.ft_data)

    data=[]

    raw_input("start publishing data?")
    graph = isam_graph()
    # enable graph
    if HYBRID:
        graph.collect_data()
        
        
    i=0
    while (not rospy.is_shutdown() and i<len(r_data.ft_data)):# and i<700:
        d_msg=copy.deepcopy(r_data.ft_data[i])

        #print d_msg
        #print d_msg.header.stamp,d_msg.base_T_palm.pose.position.x
        d_msg.base_gvec.header.stamp=d_msg.header.stamp
        d_msg.header.stamp=rospy.Time.now()
        d_msg.base_T_palm.header.frame_id='lbr4_base_link'
        #print d_msg.cpt_forces
        #raw_input("?")
        pub.publish(d_msg)
        rate.sleep()
        i=i+1

    # optimize pose
    if HYBRID:
        raw_input("Infer pose?")
        graph.infer_pose()


    # optimize dynamics
    if HYBRID:
        raw_input("Infer dynamics?")

        graph.infer_dynamics()
    
    # create a pickle file
    #pickle.dump( data, open( "gt_ft_obj4_fast.p ", "wb" ) )
