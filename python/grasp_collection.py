import rospy
import sys
import os
import numpy as np
from sysid_manipulation.srv import *

from rospkg import RosPack

rp=RosPack()
rp.list()

path=rp.get_path('hand_services')+'/scripts'
sys.path.insert(0,path)
from hand_client import *

path=rp.get_path('in_hand_utils')+'/scripts'
sys.path.insert(0,path)
from utils_client import *


class graspClient(object):
    def __init__(self):
        self.hand_client=handClient()

    def open_hand(self):
        #js=np.zeros(16)
        home_js=np.array([0.0,0.3,0.4,0.0,
                          0.0,0.3,0.4,0.0,
                          0.0,0.3,0.4,0.0,
                          1.6,0.0,0.2,0.0])

        reached=self.hand_client.send_pos_cmd(home_js)
    
        return reached
    
    def reset_bt(self):
        
        return self.hand_client.reset_tare([0,1,2,3])
        
    def make_contact(self):
        f_idx=[0,1,2,3]
        app_vec=[np.zeros(3) for i in range(len(f_idx))]
        app_vec[0]=np.array([0.0,0.0,-1.0])
        app_vec[1]=np.array([0.0,0.0,-1.0])
        app_vec[2]=np.array([0.0,0.0,-1.0])
        app_vec[3]=np.array([0.0,0.0,1.0])
        
        self.hand_client.reset_tare(f_idx)
    
        self.hand_client.approach_contact(f_idx,app_vec)
        q_des=self.hand_client.get_joint_state()
        return q_des
        
    def grasp_object(self,q_des):
        j_idx=[1,2,5,6,9,10,14,15]

        _,q_des_stiff=self.hand_client.increase_stiffness(j_idx,q_des)
        return q_des_stiff

        
    def enable_recording(self,data_file):
        rospy.wait_for_service('sysid/data_logger/start')
        try:
            rec_data = rospy.ServiceProxy('sysid/data_logger/start', recordData)
            resp1 = rec_data(data_file)
            return resp1.enabled_recording
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def store_recording(self):
        rospy.wait_for_service('sysid/data_logger/stop')
        try:
            rec_data = rospy.ServiceProxy('sysid/data_logger/stop', storeData)
            resp1 = rec_data('')
            return resp1.result
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

