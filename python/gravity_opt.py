import rospy
from gravity_opt_problem import *

# get data from ros service?

if __name__=='__main__':    
    # setup data
    r_c=np.array([0,0,0])
    r_i=[np.array([0.1,0.1,0.1]) for i in range(4)]
    s_n=[np.array([1.0,0.0,0.0]) for i in range(4)]
    m=0.1
    g=np.array([0,0,9.8])
    mu=0.1
    f_max=[np.array([0.4,0.5,0.2]) for i in range(4)]
    prior=np.zeros(3+4*3)
    
    #
    g_model=GravityModel()
    best_x=g_model.optimize(r_c,r_i,s_n,m,g,mu,f_max,prior)
    #print best_x
    g_v=np.array([best_x[0],best_x[1],best_x[2]])
    
    print 'G',g_v/np.linalg.norm(g_v)
    # optimize
    # 
    
