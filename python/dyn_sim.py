import numpy as np

class planarBox(object):
    def __init__(self,m,r_c,H,dt=1e-3):
        self.M=[m,r_c,H] # loading constant parameters
        self.dt=dt # seconds
        c_mat=np.zeros((3,3))
        c_mat[0,2]=r_c[1]
        c_mat[2,0]=-r_c[1]
        c_mat[1,2]=-r_c[0]
        c_mat[2,1]=r_c[0]
        self.c_mat=c_mat

    def update_params(self,m,r_c,H):
        self.M=[m,r_c,H]
        c_mat=np.zeros((3,3))
        c_mat[0,2]=r_c[1]
        c_mat[2,0]=-r_c[1]
        c_mat[1,2]=-r_c[0]
        c_mat[2,1]=r_c[0]
        self.c_mat=c_mat
        
    def smooth_state(self,s):
        s[2]=np.arctan2(np.sin(s[2]),np.cos(s[2]))
        return s
    def next_state(self,s,u):
        # get acceleration
        acc=self.get_accel(u)
        s_1=[np.zeros(3),np.zeros(3),np.zeros(3)]
        
        s_1[2]=acc
        s_1[1]=s[1]+s_1[2]*self.dt
        s_1[0]=s[0]+s_1[1]*self.dt
        s_1[0]=self.smooth_state(s_1[0])
        return s_1

    def get_accel(self,u):
        acc=np.zeros(3)
        acc[0:2]=u/self.M[0]
        u_vec=np.matrix([u[0],u[1],0.0]).T
        acc[2]=(self.c_mat*u_vec)[2]/self.M[2]
        return acc

class planarBoxForce(object):
    def __init__(self,m,r_c,H,dims,dt=1e-3):
        self.M=[m,r_c,H] # loading constant parameters
        self.dt=dt # seconds
        c_mat=np.zeros((3,3))
        c_mat[0,2]=r_c[1]
        c_mat[2,0]=-r_c[1]
        c_mat[1,2]=-r_c[0]
        c_mat[2,1]=r_c[0]
        self.c_mat=c_mat
        # force contact points:
        self.dims=dims
        self.p_mat=[]
        # edge points:
        # pt1:
        x=dims[0]/2.0
        y=dims[1]/2.0
        vec=[-x,-y]
        self.p_vec=[]
        self.p_vec.append(vec)
        self.p_mat.append(self.skew_cross(vec))
        vec=[-x,y]
        self.p_vec.append(vec)

        self.p_mat.append(self.skew_cross(vec))
        vec=[x,-y]
        self.p_vec.append(vec)

        self.p_mat.append(self.skew_cross(vec))        
        vec=[x,y]
        self.p_vec.append(vec)

        self.p_mat.append(self.skew_cross(vec))
        

    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,2]=vec[1]
        sk_mat[2,0]=-vec[1]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,1]=vec[0]
        return sk_mat
    def update_params(self,m,r_c,H):
        self.M=[m,r_c,H]
        c_mat=np.zeros((3,3))
        c_mat[0,2]=r_c[1]
        c_mat[2,0]=-r_c[1]
        c_mat[1,2]=-r_c[0]
        c_mat[2,1]=r_c[0]
        self.c_mat=c_mat
        
    def smooth_state(self,s):
        s[2]=np.arctan2(np.sin(s[2]),np.cos(s[2]))
        return s
    def next_state(self,s,u):
        # get acceleration
        acc=self.get_accel(u)
        s_1=[np.zeros(3),np.zeros(3),np.zeros(3)]
        
        s_1[2]=acc
        s_1[1]=s[1]+s_1[2]*self.dt
        s_1[0]=s[0]+s_1[1]*self.dt
        s_1[0]=self.smooth_state(s_1[0])
        return s_1

    def get_accel(self,u):
        acc=np.zeros(3)
        acc[0:2]=np.sum(u,axis=0)/self.M[0]
        mg_vec=np.matrix([0.0,-9.8,0.0]).T#/self.M[0] # gravity is along y axis
        acc[0:2]+=np.ravel(mg_vec)[0:2]
        mg_vec=self.M[0]*np.matrix([0.0,-9.8,0.0]).T # gravity is along y axis

        # angular acceleration
        # simpler simulation:

        for i in range(len(u)):
            u_vec=np.matrix([u[i][0],u[i][1],0.0]).T
            #print (self.p_mat[i]*u_vec)
            acc[2]+=(self.p_mat[i]*u_vec)[2]/self.M[2]
        acc[2]+=(self.c_mat*mg_vec)[2]/self.M[2]
        #print (self.c_mat*mg_vec)[2]/self.M[2]
        '''
        
        for i in range(len(u)):
            u_vec=np.matrix([u[i][0],u[i][1],0.0]).T
            #print (self.p_mat[i]*u_vec)
            acc[2]+=(self.c_mat*u_vec)[2]/self.M[2]+(self.p_mat[i]*u_vec)[2]/self.M[2]
        '''
        return acc

def debug_force_class():
    m=0.1
    r_c=np.array([0.0,0.0])
    H=0.01
    box_dims=[0.5,0.5]
    dyn_sim=planarBoxForce(m,r_c,H,box_dims)
    u=[np.array([0.01,0.01]) for i in range(4)]
    s0=np.zeros(3)
    s_next=dyn_sim.next_state(s0,u)
    print s_next

def debug_class():
    m=0.1 # kg
    r_c=np.array([0.0,0.0]) # [x,y] with reference to object's center
    H=0.01 # 
    dyn_sim=planarBox(m,r_c,H)
    s_0=np.zeros(3)
    u=np.array([0.0,0.1]) # [acc_x,acc_y]
    s_next=dyn_sim.next_state(s_0,u)


if __name__=='__main__':
    debug_force_class()
