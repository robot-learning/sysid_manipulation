import rospy
from geometry_msgs.msg import WrenchStamped,PoseStamped
import tf_helper
import copy
import message_filters
import numpy as np
import tf.transformations
import tf
from std_msgs.msg import Header
from numpy.linalg import inv
import pickle

from sysid_manipulation.msg import FullMeasurementData
from geometry_msgs.msg import PoseStamped,WrenchStamped

#from biotac_sensors.msg import BioTacHand,BioTacTareHand

from kdl_utils import *
class readData(object):
    def __init__(self,f_name,robot_model):

        self.ft_data=pickle.load(open(f_name,"rb"))
        self.kdl=robot_model
        
        
        # frames:
        
        self.gvec=WrenchStamped()
        self.gvec.wrench.force.x=0
        self.gvec.wrench.force.y=0
        self.gvec.wrench.force.z=-9.8
        
        self.base_T_palm=PoseStamped()
        self.palm_T_obj=PoseStamped()
        self.palm_T_ft=PoseStamped()
        
        self.ft_force=WrenchStamped()


        
        self.time=None
        self.got_data=False
        self.got_TF=False

        
    def get_tf_data(self,js):

        # get robot joint state
        js_pos=js.position
        #print js_pos
        # update robot joint state

        # get pose of palm w.r.t. base
        self.base_T_palm.pose=self.kdl.fk(js_pos,0)
        #print self.base_T_palm.pose.position.x
        #js_pos=js.positions
        
        # get pose of ft sensor w.r.t. palm
        self.palm_T_ft.pose=self.kdl.fk(js_pos,2)

        #js_pos=js.positions

        # get pose of object w.r.t. palm
        self.palm_T_obj.pose=self.kdl.fk(js_pos,1)
            
        self.got_data=True

def process_data(f_dir,f_suff,robot_model):
    f_name=f_dir+f_suff
    fw_name=f_dir+'full_'+f_suff
    r_data=readData(f_name,robot_model)




    data=[]
    i=0
    while (i<len(r_data.ft_data)):
        ft_dat=r_data.ft_data[i]
        r_data.ft_force=ft_dat.ft_force
        #print ft_dat.robot_js
        r_data.get_tf_data(ft_dat.robot_js)
        if(r_data.got_data):
            # publish to ros
            d_msg=FullMeasurementData()
            d_msg.header.stamp=ft_dat.robot_js.header.stamp

            # poses:
            d_msg.base_T_palm=r_data.base_T_palm
            #print d_msg.header.stamp,d_msg.base_T_palm.pose.position.x

            d_msg.palm_T_obj=r_data.palm_T_obj

            d_msg.palm_T_ft=r_data.palm_T_ft
            #print d_msg.palm_T_ft
            # forces:
            d_msg.ft_force=r_data.ft_force
            d_msg.base_gvec=r_data.gvec
            
            #pub.publish(d_msg)
            i+=1

            r_data.got_data=False
            data.append(copy.deepcopy(d_msg))
    #raw_input("continue?")
        
    # create a pickle file
    pickle.dump( data, open( fw_name, "wb" ) )
    
if __name__=='__main__':
    #rospy.init_node('data_logger')

    # load robot mode:
    
    base_frame="lbr4_base_link"
    palm="optoforce_model_link"
    obj_frame="obj_link"
    ft_frame="optoforce_sensor_link"
    #self.ft_frame="optoforce_sensor_link"
    base_frames = [base_frame,palm,palm]
    ee_frames = [palm,obj_frame,ft_frame]

    robot_model=robot_kinematics(base_frames,ee_frames)
    f_dir='/home/bala/catkin_ws/src/sysid_manipulation/data/ft/'
    f_suff_list=[
        'gt_ft_obj_slow.p', 'gt_ft_obj_fast.p',
        'gt_ft_obj1_slow.p', 'gt_ft_obj1_fast.p',
        'gt_ft_obj2_slow.p', 'gt_ft_obj2_fast.p',
        'gt_ft_obj3_slow.p', 'gt_ft_obj3_fast.p',
        'gt_ft_obj4_slow.p', 'gt_ft_obj4_fast.p']
    
    for f_suff in f_suff_list:
        
        process_data(f_dir,f_suff,robot_model)
        print 'processed',f_suff
