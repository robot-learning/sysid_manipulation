from dyn_sim import planarBox, planarBoxForce
from viz_utils import *
import numpy as np
from mc_model import mcModel,lsqModel,lsqModelForce

def complex_fit():
    # true parameters:
    m=0.2
    r_c=np.array([0.4,-0.2])
    H=0.6
    dim_x=0.5
    dim_y=0.5
    dims=[dim_x,dim_y]
    
    oracle_dyn_sim=planarBoxForce(m,r_c,H,dims,dt=1e-2)

    # initial state:
    s0=[np.zeros(3),np.zeros(3),np.zeros(3)]
    s0[0][2]=0.0
    s0[0][1]=0.0
    s0[0][0]=0.0

    # command:
    #u[2]=np.array([0.08,0.01])

    # timesteps to simulate
    timesteps=50

    # training model
    
    # initial values:
    m_est=0.1*0.1
    r_c_est=np.array([0.0,0.0])
    H_est=0.01

    est_dyn_sim=planarBoxForce(m_est,r_c_est,H_est,dims,dt=1e-2)

    actions=[]
    oracle_s0=s0
    oracle_sdata=[]

    u_arr=[[0.1,0.1],[-0.3,0.2],[-3.5,3.1],[-2.3,3.9],[3.5,1.9],[3.4,3.8],[2.1,2.8],[2.1,1.3]]
    #oracle_sdata.append(s0)
    #actions.append(u)
    u=[np.array(u_arr[0]) for i in range(4)]
    u[1]=np.array(u_arr[1])
    u[3]=-1.0*u[1]

    # get oracle values and estimated values:
    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        
    # command:
    u=[np.array(u_arr[1]) for i in range(4)]
    u[1]=np.array(u_arr[2])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1

    # command:
    u=[np.array(u_arr[3]) for i in range(4)]
    u[1]=np.array(u_arr[4])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1

    u=[np.array(u_arr[4]) for i in range(4)]
    u[2]=np.array(u_arr[5])
    u[0]=-1.0*u[2]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
    u=[np.array(u_arr[5]) for i in range(4)]
    u[2]=np.array(u_arr[6])
    u[0]=-1.0*u[2]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1

    u=[np.array(u_arr[6]) for i in range(4)]
    u[2]=np.array(u_arr[7])
    u[0]=-1.0*u[2]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1

    print len(oracle_sdata)
    # get fitness:
    lsq_model=lsqModelForce(dims)
    m_est,H_est,rc_est=lsq_model.get_optimized_values(oracle_sdata,None,actions)
    print "Estimated mass:",m_est
    print "Estimated inertia:", H_est
    print "Estimated COM:", rc_est

    #exit()
    # test estimate on new action:

    
    est_dyn_sim.update_params(m_est,rc_est,H_est)
    #est_dyn_sim.update_params(m,r_c,H)

    oracle_s0=s0
    oracle_sdata=[]
    
    oracle_sdata.append(s0)
    
    #u=[np.array([0.04,-0.1]) for i in range(4)]
    #u[0]=np.array([-0.03,0.1])
    # get oracle values and estimated values:

    est_s0=s0
    est_sdata=[]
    est_sdata.append(s0)

    u=[np.array(u_arr[0]) for i in range(4)]
    u[1]=np.array(u_arr[1])

    

    # get oracle values and estimated values:
    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1
        
    # command:
    u=[np.array(u_arr[1]) for i in range(4)]
    u[1]=np.array(u_arr[2])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1


    # command:
    u=[np.array(u_arr[3]) for i in range(4)]
    u[1]=np.array(u_arr[4])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1
    u=[np.array(u_arr[4]) for i in range(4)]
    u[1]=np.array(u_arr[5])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1

    u=[np.array(u_arr[5]) for i in range(4)]
    u[1]=np.array(u_arr[6])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1

    u=[np.array(u_arr[6]) for i in range(4)]
    u[1]=np.array(u_arr[7])
    u[3]=-1.0*u[1]

    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #print oracle_s1[2]
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1


        
    # plot results:
    s_data=[]
    oracle_s0=s0
        
    est_s0=s0
    for i in range(len(oracle_sdata)):
        oracle_s1=oracle_sdata[i]
        est_s1=est_sdata[i]
        s_data.append([oracle_s1[0],est_s1[0]])


    viz_sim=visualizeBox(dim_x,dim_y)
        
    viz_sim.show_animation(s_data)
    
    
def simple_fit():
    # true parameters:
    m=0.1
    r_c=np.array([0.2,0.0])
    H=0.1
    dim_x=0.5
    dim_y=0.5

    oracle_dyn_sim=planarBox(m,r_c,H,dt=1e-2)

    # initial state:
    s0=[np.zeros(3),np.zeros(3),np.zeros(3)]
    s0[0][2]=0.0
    s0[0][1]=0.0
    s0[0][0]=0.0

    # command:
    u=np.array([0.01,0.04])

    # timesteps to simulate
    timesteps=300

    # training model
    
    # initial values:
    m_est=0.1*0.1
    r_c_est=np.array([0.0,0.0])
    H_est=0.01

    est_dyn_sim=planarBox(m_est,r_c_est,H_est,dt=1e-2)


    actions=[]
    oracle_s0=s0
    oracle_sdata=[]
    
    oracle_sdata.append(s0)
    actions.append(u)
   
    # get oracle values and estimated values:
    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1

    #print len(oracle_sdata), len(est_sdata),len(actions)
    # get fitness:
    lsq_model=lsqModel()
    m_est,rc_est=lsq_model.get_optimized_values(oracle_sdata,None,actions)
    H=1.0
    print "Estimated mass:",m_est
    print "Estimated inertia*rc:", rc_est


    # test estimate on new action:

    
    est_dyn_sim.update_params(m_est,rc_est,H)

    oracle_s0=s0
    oracle_sdata=[]
    
    oracle_sdata.append(s0)
    u=np.array([0.04,-0.03])

    # get oracle values and estimated values:
    for i in range(timesteps):
        actions.append(u)
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        oracle_sdata.append(oracle_s1)
        oracle_s0=oracle_s1

    est_s0=s0
    est_sdata=[]
    est_sdata.append(s0) 

    # get estimated simulation:
    for i in range(timesteps):
        est_s1=est_dyn_sim.next_state(oracle_s0,u)
        est_sdata.append(est_s1)
        est_s0=est_s1
    
    # plot results:
    s_data=[]
    oracle_s0=s0
        
    est_s0=s0
    for i in range(timesteps):
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        est_s1=est_dyn_sim.next_state(est_s0,u)
        
        s_data.append([oracle_s1[0],est_s1[0]])
        oracle_s0=oracle_s1
        est_s0=est_s1


    viz_sim=visualizeBox(dim_x,dim_y)
        
    viz_sim.show_animation(s_data)



if __name__=='__main__':
    #simple_fit()
    complex_fit()
