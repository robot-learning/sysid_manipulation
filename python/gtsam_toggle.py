import rospy
from sysid_manipulation.srv import *

class isam_graph(object):
    def collect_data(self):
        return self.call_toggle_srv(collect_data = True)
    def reset_data(self):
        return self.call_toggle_srv(reset_data = True)

    def infer_pose(self):
        return self.call_toggle_srv(infer_pose = True)

    def infer_dynamics(self):
        return self.call_toggle_srv(infer_dyn = True)

    def call_toggle_srv(self,collect_data = False, reset_data = False, infer_pose = False, infer_dyn = False):
        rospy.wait_for_service('/sysid/toggle_graph')
        try:
            srv_call = rospy.ServiceProxy('/sysid/toggle_graph', toggleInference)
            resp1 = srv_call(collect_data,reset_data,infer_pose,infer_dyn,'1')
            return resp1.status
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
