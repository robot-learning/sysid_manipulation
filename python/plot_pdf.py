from scipy.stats import norm  
import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(0.0,0.5,10000)
y = norm.pdf(x,loc=0.05,scale=1e-3)
plt.plot(x,y)
plt.xlabel('Mass (kg)')
plt.ylabel('PDF')
