from dyn_sim import planarBoxForce
from viz_utils import *
import numpy as np
if __name__=='__main__':
    # parameters:
    m=0.1
    r_c=np.array([0.2,0.0])
    H=0.1
    dim_x=0.5
    dim_y=0.5

    s0=[np.zeros(3),np.zeros(3),np.zeros(3)]
    s0[0][2]=0.0
    s0[0][1]=0.0
    s0[0][0]=0.0
    u=[np.array([0.01,0.04]) for i in range(4)]
    u[1]=np.array([-0.01,-0.03])
    
    oracle_dyn_sim=planarBoxForce(m,r_c,H,[dim_x,dim_y],dt=1e-2)

    timesteps=300

    # collect sim data:
    s_data=[]
    oracle_s0=s0


    o_data=[]
    for i in range(timesteps):
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        #est_s1=est_dyn_sim.next_state(est_s0,u)
        
        o_data.append([oracle_s1[0]])
        oracle_s0=oracle_s1
        #est_s0=est_s1


    # estimate parameters:
    m_est=0.08
    #r_c=np.array([0.0,0.0])
    H_est=0.1
    est_dyn_sim=planarBoxForce(m_est,r_c,H_est,[dim_x,dim_y],dt=1e-2)
        
    # get plotting data:
    oracle_s0=s0
        
    est_s0=s0
    for i in range(timesteps):
        oracle_s1=oracle_dyn_sim.next_state(oracle_s0,u)
        est_s1=est_dyn_sim.next_state(est_s0,u)
        
        s_data.append([oracle_s1[0],est_s1[0]])
        oracle_s0=oracle_s1
        est_s0=est_s1

        
    # plot animation
    

    viz_sim=visualizeBox(dim_x,dim_y)
        
    viz_sim.show_animation(s_data)
