import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import seaborn as sns
from matplotlib.animation import FuncAnimation
import matplotlib

matplotlib.rc('xtick', labelsize=15)
matplotlib.rc('ytick', labelsize=15)

# Plot between -10 and 10 with .001 steps.
x_axis = np.arange(-0.4, 0.4, 0.00001)

fig = plt.figure(figsize=(12, 8))
ax = fig.add_subplot(111)

ln, = plt.plot([], [], animated=True)

mu = [0.05268, -0.00273, -0.062265, -0.103638, -0.148292]
sigma_2 = [7e-4, 0.0004528, 0.0002908, 0.0001924, 0.0001237]

colors = ['dusty purple',]# 'denim blue', 'medium green', 'pale red']#, 'amber', 'black', 'pink', "greyish"]
colors = sns.color_palette(sns.xkcd_palette(colors))


def init():
    ax.set_color_cycle(colors)
    ax.set_xlabel('Cabinet mean position', fontsize=20)
    ax.set_ylabel('Probability ( x 1e-3 )', fontsize=20)
    ax.set_xlim([-0.15, 0.2])
    ax.set_ylim([0, 0.4])
    return ln,

def update(frame):

    ax.clear()
    ax.set_color_cycle(colors)
    ax.set_xlabel('Cabinet mean position', fontsize=20)
    ax.set_ylabel('Probability ( x 1e-3 )', fontsize=20)
    ax.set_xlim([-0.15, 0.2])
    ax.set_ylim([0, 0.4])

    ax.axvline(x=-0.05268, color='black', linestyle='dotted', label='source cabinet X-coordinate mean')
    ax.axvline(x=0.17268, color='blue', linestyle='dotted', label='target cabinet X-coordinate mean')
    for i in range(0, 5):

        y = norm.pdf(x_axis, mu[i]*-1.0, np.sqrt(sigma_2[i]))
        y = y / np.sum(y)

        if i is not int(frame)-1:
            ax.fill_between(x_axis, y*1e3, alpha=0.1)
        else:
            ax.fill_between(x_axis, y*1e3, alpha=0.7)

    plt.legend(prop={'size': 17}, loc='upper center', bbox_to_anchor=(0.55, 1))

    return ln, ax

ani = FuncAnimation(fig, update, frames=np.linspace(0, 5, 6),
                    init_func=init, blit=True, interval=400)

#ani.save('lines.mp4')
plt.legend(prop={'size': 17}, loc='upper center', bbox_to_anchor=(0.55, 1))

plt.title('Distribution Updates with SimOpt', fontsize=20)
plt.show()
