import rospy
import numpy as np

from sysid_manipulation.srv import *

import tf

import subprocess
import os
from os.path import join
# topic msg types:
#from sensor_msgs.msg import JointState
#from biotac_sensors.msg import BioTacHand,BioTacForce,BioTacTareHand
#from geometry_msgs.msg import WrenchStamped
class dataLogger:
    def __init__(self,rate=100):
        # initialize subscribers
        # initialize service
        rospy.init_node('data_server')

        start_srv = rospy.Service('sysid/data_logger/start',recordData,self.start_srv)
        stop_srv=rospy.Service('sysid/data_logger/stop',storeData,self.store_srv)
        
        self.rate=rospy.Rate(rate)
        
        
    def start_srv(self,req):
        # create file
        print req.data_dir
        self.rosbag_proc = subprocess.Popen(['./rosbag_grasp.sh', req.data_dir+'.bag'])
        #self.rosbag_proc = subprocess.Popen(['./record_data.sh', fpath, bagname],shell=True)
        
        return True

    def store_srv(self,req):
        # write array to file

        #bag_pid=self.rosbag_proc.pid
        #self.kill_process(bag_pid)
        os.system("rosnode kill "+ 'record_bag')
        self.rosbag_proc.send_signal(subprocess.signal.SIGKILL)
        print 'Stored bag file'
        #self.record=False
        return True
    def kill_process(self,ppid):
        output = subprocess.check_output(['ps', '--ppid=' + str(ppid), '--no-headers'])
        for process_line in output.split('\n'):
            strip_process_line = process_line.strip()
            if strip_process_line:
                pid = strip_process_line.split(' ')[0]
                name = strip_process_line.split(' ')[-1]
                print 'killing %s' % (name)
                os.kill(int(pid), subprocess.signal.SIGINT)
    
    def run(self):
        while(not rospy.is_shutdown()):           
            self.rate.sleep()
if __name__=='__main__':
    data_log=dataLogger(100)

    data_log.run()
