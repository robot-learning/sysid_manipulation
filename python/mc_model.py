import numpy as np
import pymc as mc
import pygmo as pg

class force_inertia_function:
    def __init__(self,dims):
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.dims=dims
        self.p_mat=[]
        # edge points:
        # pt1:
        x=dims[0]/2.0
        y=dims[1]/2.0
        vec=[-x,-y]
        self.p_vec=[]
        self.p_vec.append(vec)
        self.p_mat.append(self.skew_cross(vec))
        vec=[-x,y]
        self.p_vec.append(vec)

        self.p_mat.append(self.skew_cross(vec))
        vec=[x,-y]
        self.p_vec.append(vec)

        self.p_mat.append(self.skew_cross(vec))        
        vec=[x,y]
        self.p_vec.append(vec)

        self.p_mat.append(self.skew_cross(vec))
        self.g=np.matrix([0.0,-9.8,0.0])
        
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,2]=vec[1]
        sk_mat[2,0]=-vec[1]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        print "SLSQP: Updated parameters"
        
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=x[0]
        H=x[1]
        #rc=np.array([0.2,0.0])
                
        rc=np.array([x[2],x[3]])
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            # compute linear acceleration:
            mg_vec=m*np.ravel(self.g)[0:2]
            g_vec=np.ravel(self.g)[0:2]
                        
            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec
            cost+= np.ravel(np.linalg.norm(m*(self.linear_acc[i]-g_vec)-f_sum)**2)
            # compute net torque:
            inv_mg_vec=np.matrix([-mg_vec[1],mg_vec[0]])
            tau=np.ravel(inv_mg_vec*np.matrix([rc[0],rc[1]]).T)
            for j in range(4):
                f_vec=np.matrix([-f_data[j][1],f_data[j][0]])
                tau+=np.ravel(f_vec*np.matrix(self.p_vec[j]).T)
            cost+= (H*self.ang_acc[i]+tau)**2
        return cost
    def gradient(self, x):
        grad=np.zeros(4)
        m=x[0]
        H=x[1]
        rc=np.array([x[2],x[3]])
        for i in range(len(self.force)):
            f_data=self.force[i]
            # compute linear acceleration:
            mg_vec=m*np.ravel(self.g)[0:2]
            g_vec=np.ravel(self.g)[0:2]

            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec
            grad[0]=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T

            
            #cost+= np.ravel(np.linalg.norm(m*self.linear_acc[i]-f_sum-mg_vec)**2)
            # compute net torque:
            inv_mg_vec=np.matrix([-mg_vec[1],mg_vec[0]])
            tau=np.ravel(inv_mg_vec*np.matrix([rc[0],rc[1]]).T)
            for j in range(4):
                f_vec=np.matrix([-f_data[j][1],f_data[j][0]])
                tau+=np.ravel(f_vec*np.matrix(self.p_vec[j]).T)

            w_mag=2.0*(H*self.ang_acc[i]+tau)
            grad[0]+=w_mag*(g_vec[0]*rc[1]-g_vec[1]*rc[0])
            grad[1]+=w_mag*self.ang_acc[i]
            grad[2]+=w_mag*(-m*g_vec[1])
            grad[3]+=w_mag*(m*g_vec[0])
        # check gradient with fd:
        #grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)
        #print grad,grad_fd
        return grad
    def get_bounds(self):
        # bounds on mass,inertia, r_x,r_y
        low_bounds=[0.01,0.0,-0.5,-0.5]
        up_bounds=[1.0,1.0,0.5,0.5]
        return (low_bounds,up_bounds)

class lsqModel(object):
    def __init__(self):
        return None
    def get_optimized_values(self,oracle_states,est_states,action):
        # perform least squared estimate
        # build acceleration model
        acc_err=[]
        actions=[]
        for i in range(len(oracle_states)):
            acc_=np.ravel(oracle_states[i][2][0])
            acc_err.append(acc_)
            actions.append(action[i][0])
            acc_=np.ravel(oracle_states[i][2][1])
            acc_err.append(acc_)
            actions.append(action[i][1])

        
        mass_est=np.ravel(np.linalg.pinv(np.matrix(acc_err))*np.matrix(actions).T)

        # inertia est:
        f_mat=[]
        theta=[]
        for i in range(len(oracle_states)):
            f_mat.append(np.array([action[i][0],action[i][1]]))
            theta.append(oracle_states[i][2][2])
        
        
        # center of mass estimate:
        r_mat=np.linalg.pinv(np.matrix(f_mat))*np.matrix(theta).T
        r_c=[r_mat[1],-r_mat[0]]
        return mass_est[0],np.ravel(r_c)
    
class lsqModelForce(object):
    def __init__(self,dims):
        self.dims=dims
        return None
    def get_optimized_values(self,oracle_states,est_states,action):
        # perform least squared estimate
        # build acceleration model
        acc_err=[]
        actions=[]
        for i in range(len(oracle_states)):
            action_sum=np.sum(action[i],axis=0)
            acc_=np.ravel(oracle_states[i][2][0])
            acc_err.append(acc_)
            actions.append(action_sum[0])
            acc_=np.ravel(oracle_states[i][2][1])
            acc_err.append(acc_)
            actions.append(action_sum[1])
            
        mass_est=np.ravel(np.linalg.pinv(np.matrix(acc_err))*np.matrix(actions).T)

        # inertia estimation using pygmo
        actions=[]
        angle_acc=[]
        linear_acc=[]
        for i in range(len(oracle_states)):
            actions.append(action[i])
            angle_acc.append(np.ravel(oracle_states[i][2][2]))
            linear_acc.append(np.ravel(oracle_states[i][2][0:2]))

        opt_fn=force_inertia_function(self.dims)
        opt_fn.update_data(linear_acc,angle_acc,actions)
        prob = pg.problem(opt_fn)
        algo = pg.algorithm(pg.nlopt('slsqp'))
        algo.set_verbosity(1)
        #algo.extract(pg.nlopt).ftol_rel = 1e-1
        #algo.extract(pg.nlopt).ftol_abs = 1e-8

        pop = pg.population(prob,10)
        print "running optimization"
        pop = algo.evolve(pop)
        best_x=pop.champion_x
        #print pop.champion_x
        print "Final cost:",pop.champion_f
        
        #print mass_est, best_x
        return best_x[0],best_x[1],[best_x[2],best_x[3]]

class mcModel(object):
    def __init__(self):
        return True
    def get_optimized_values(self,oracle_states,est_states):
        # get parametric mode:
        return False

    
