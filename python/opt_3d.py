import numpy as np
import pymc as mc
import pygmo as pg
#import pygmo_plugins_nonfree as ppnf
#import pygmo_plugins_nonfree as ppnf
class static_mass_function:
    def __init__(self):
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        self.o_f=None
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec,o_f):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        self.o_f=o_f
        print "SLSQP: Updated parameters"
        
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=x[0]
        #print x
        o_rc=np.array([x[1],x[2],x[3]])
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            #cost+=1.0*np.ravel(np.linalg.norm(m*(self.linear_acc[i]-g_vec)-f_sum)**2)[0]
            cost+=0.001*np.ravel(np.linalg.norm(m*(-g_vec)-f_sum)**2)[0]
            #print 1.0*np.ravel(np.linalg.norm((-g_vec)-f_sum)**2)[0]
            
            # compute net torque:
            #rc_vec=self.skew_cross(rc)
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T

            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]
            #if(i==0):
            #    print rc,self.o_f[i] # f_T_rc, o_f
            
            #rc=self.o_f[i]-o_rc
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            #print self.ang_acc[i]
            omega_d=np.zeros(3)#np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #cost+=np.ravel(np.linalg.norm(omega_d-tau-tau_g)**2)[0]
            cost+=1.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=1.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=1.0*(omega_d[2]-tau[2]-tau_g[2])**2
        #print x
        return [cost]
    def gradient(self, x):
    
        grad=np.zeros(4)
        m=x[0]
                
        o_rc=np.array([x[1],x[2],x[3]])
        
        for i in range(len(self.force)):
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.linalg.inv(o_T_fref)*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]

            #rc=self.o_f[i]-o_rc

            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]
            
            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec
            #grad[0]+=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T
            grad[0]+=0.001*2.0*np.matrix((m*(-g_vec)-f_sum))*np.matrix(-g_vec).T
            
            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            omega_d=np.zeros(3)#np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            
            grad[0]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*tau_g[0]/m
            grad[0]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*tau_g[1]/m
            grad[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*tau_g[2]/m

            # center of mass gradients:
            f_grad_rc=np.zeros(4)
            #f_grad_rc[3]=1.0
            f_grad_rc[0]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[2]
            f_grad_rc[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[1]

            f_grad_rc[1]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[2]
            f_grad_rc[1]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[0]

            f_grad_rc[2]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[1]
            f_grad_rc[2]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[0]
            grad[1:4]+=np.ravel(o_T_fref*np.matrix(f_grad_rc).T)[0:3]*0.01
        #print grad
        #grad= pg.estimate_gradient(lambda x: self.fitness(x), x)
        #print grad
        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        low_bounds=[0.01, -0.5,-0.5,-0.5]# -1.0,-1.0,-1.0,-1.0,-1.0,-1.0]
        up_bounds =[2.0, 0.5,0.5,0.5]# 1.0,1.0,1.0,1.0,1.0,1.0]

        return (low_bounds,up_bounds)

class inv_static_mass_function:
    def __init__(self):
        return None
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        self.o_f=None
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec,o_f):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        self.o_f=o_f
        print "SLSQP: Updated parameters"
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=x[0]
                
        o_rc=np.array([x[1],x[2],x[3]])
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)
            '''
            # compute force required for given acceleration:
            e_vec=np.matrix(np.ones(len(f_data)))
            print e_vec.T*e_vec
            f_hat=-np.linalg.inv(e_vec.T*e_vec)*e_vec.T*mg_vec.T
            f_sensed=np.zeros((len(f_data),3))
            for j in range(len(f_data)):
                f_sensed[j,:]=f_data[j]
            cost+=1.0*np.ravel(np.linalg.norm(f_sensed-f_hat)**2)[0]
            
            cost+=0.0*np.ravel(np.linalg.norm(m*(self.linear_acc[i]-g_vec)-f_sum)**2)[0]
            #cost+=1.0*np.ravel(np.linalg.norm(m*(-g_vec)-f_sum)**2)[0]
            #print 1.0*np.ravel(np.linalg.norm((-g_vec)-f_sum)**2)[0]
            '''
            # compute net torque:
            #rc_vec=self.skew_cross(rc)
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T

            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.matrix(np.linalg.inv(o_T_fref))*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]

            skew_mat=np.zeros((3,3*len(f_data)))
            #if(i==0):
            #    print rc,self.o_f[i] # f_T_rc, o_f

            f_sensed=np.zeros((3*len(f_data),1))

            #rc=self.o_f[i]-o_rc
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                skew_mat[:,j*3:(j+1)*3]=f_p
                tau+=np.ravel(f_p*f_vec)
                f_sensed[j*3:(j+1)*3,0]=np.ravel(f_vec)
            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])

            skew_mat=np.matrix(skew_mat)
            Q=np.linalg.pinv(skew_mat)
            cost+=np.ravel(np.linalg.norm(f_sensed-Q*np.matrix(tau_g).T)**2)[0]
            #err_=np.linalg.inv(skew_mat.T*skew_mat)#p.linalg.inv(skew_mat.T*skew_mat)#*skew_mat.T
            #print err_
            #print self.ang_acc[i]
            omega_d=np.zeros(3)#np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #cost+=np.ravel(np.linalg.norm(omega_d-tau-tau_g)**2)[0]
            cost+=0.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=0.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=0.0*(omega_d[2]-tau[2]-tau_g[2])**2
        #print x
        return [cost]
    def gradient(self, x):
        grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)

        return grad_fd
    
        grad=np.zeros(4)
        m=x[0]
                
        o_rc=np.array([x[1],x[2],x[3]])
        
        for i in range(len(self.force)):
            rc_vec=np.matrix([o_rc[0],o_rc[1],o_rc[2],1.0]).T
            o_T_fref=np.eye(4)
            o_T_fref[0:3,3]=self.o_f[i]
            fref_T_rc=np.linalg.inv(o_T_fref)*rc_vec
            rc=np.ravel(fref_T_rc)[0:3]

            #rc=self.o_f[i]-o_rc

            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]
            
            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec
            grad[0]+=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T
            #grad[0]+=2.0*np.matrix((m*(-g_vec)-f_sum))*np.matrix(-g_vec).T
            
            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([g_vec[2]*rc[1]-g_vec[1]*rc[2],
                              g_vec[0]*rc[2]-g_vec[2]*rc[0],
                              g_vec[1]*rc[0]-g_vec[0]*rc[1]])
            omega_d=np.zeros(3)#np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            
            grad[0]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*tau_g[0]/m
            grad[0]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*tau_g[1]/m
            grad[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*tau_g[2]/m

            # center of mass gradients:
            f_grad_rc=np.zeros(4)
            #f_grad_rc[3]=1.0
            f_grad_rc[0]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[2]
            f_grad_rc[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[1]

            f_grad_rc[1]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[2]
            f_grad_rc[1]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[0]

            f_grad_rc[2]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[1]
            f_grad_rc[2]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[0]
            grad[1:4]+=np.ravel(o_T_fref*np.matrix(f_grad_rc).T)[0:3]
        #grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)

        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        low_bounds=[0.01, -0.5,-0.5,-0.5]# -1.0,-1.0,-1.0,-1.0,-1.0,-1.0]
        up_bounds =[2.0, 0.5,0.5,0.5]# 1.0,1.0,1.0,1.0,1.0,1.0]

        return (low_bounds,up_bounds)

    
    
class GraspModel(object):
    def __init__(self):
        return None
    def get_optimized_values(self,linear_acc,angle_acc,forces,f_loc,g_vec,o_f,prior):
        opt_fn=static_mass_function()
        opt_fn.update_data(linear_acc,angle_acc,forces,f_loc,g_vec,o_f)
        prob = pg.problem(opt_fn)
        algo = pg.algorithm(pg.nlopt('slsqp'))
        #algo = pg.algorithm(pg.cmaes(gen = 500,cc=-1,cs=-1,c1=-1,cmu=-1,sigma0=0.5,ftol=1e-4,xtol=1e-4,memory=False,force_bounds=True))
        algo.set_verbosity(0)
        
        #algo.extract(pg.nlopt).ftol_rel = 1e-1
        #algo.extract(pg.nlopt).ftol_abs = 1e-6

        pop = pg.population(prob,1)
        f_in=prob.fitness(prior)
        print "Initial fitness: ",f_in
        pop.set_xf(0,prior,f_in)

        print "running optimization"
        pop = algo.evolve(pop)
        best_x=pop.champion_x
        #print pop.champion_x
        print "Final cost:",pop.champion_f

        print best_x
        print "Mass: ",best_x[0]
        f1_rc=np.array([best_x[1],best_x[2],best_x[3]])
        print 'COM:',f1_rc

        # estimate inertia:
        
        '''
        H=np.zeros((3,3))
        ixx=best_x[4]
        iyy=best_x[5]
        izz=best_x[6]
        ixy=best_x[7]
        ixz=best_x[8]
        iyz=best_x[9]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])
        print "Inertia matrix: "
        print H
        '''
        #print mass_est, best_x
        return best_x[0],f1_rc
class mcModel(object):
    def __init__(self):
        return True
    def get_optimized_values(self,oracle_states,est_states):
        # get parametric mode:
        return False

    
