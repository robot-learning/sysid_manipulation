class force_inertia_function:
    def __init__(self):
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        print "SLSQP: Updated parameters"
        
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=x[0]
                
        rc=np.array([x[1],x[2],x[3]])

        H=np.zeros((3,3))
        ixx=x[4]
        iyy=x[5]
        izz=x[6]
        ixy=x[7]
        ixz=x[8]
        iyz=x[9]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            cost+=1.0*np.ravel(np.linalg.norm(m*(self.linear_acc[i]-g_vec)-f_sum)**2)[0]
            
            # compute net torque:
            #rc_vec=self.skew_cross(rc)
            
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([-g_vec[2]*rc[1]+g_vec[1]*rc[2],
                              -g_vec[0]*rc[2]+g_vec[2]*rc[0],
                              -g_vec[1]*rc[0]+g_vec[0]*rc[1]])
            #print self.ang_acc[i]
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #cost+=np.ravel(np.linalg.norm(omega_d-tau-tau_g)**2)[0]
            cost+=1.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=1.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=1.0*(omega_d[2]-tau[2]-tau_g[2])**2
        return [cost]
    def gradient(self, x):
        grad=np.zeros(10)
        m=x[0]
                
        rc=np.array([x[1],x[2],x[3]])

        H=np.zeros((3,3))
        ixx=x[4]
        iyy=x[5]
        izz=x[6]
        ixy=x[7]
        ixz=x[8]
        iyz=x[9]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])

        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec
            grad[0]+=2.0*np.matrix((m*(self.linear_acc[i]-g_vec)-f_sum))*np.matrix(self.linear_acc[i]-g_vec).T
            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([-g_vec[2]*rc[1]+g_vec[1]*rc[2],
                              -g_vec[0]*rc[2]+g_vec[2]*rc[0],
                              -g_vec[1]*rc[0]+g_vec[0]*rc[1]])
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            
            grad[0]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*tau_g[0]/m
            grad[0]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*tau_g[1]/m
            grad[0]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*tau_g[2]/m

            # center of mass gradients:
            grad[1]+=-2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[2]
            grad[1]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[1]

            grad[2]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[2]
            grad[2]+=-2.0*(omega_d[2]-tau_g[2]-tau[2])*g_vec[0]

            grad[3]+=-2.0*(omega_d[0]-tau_g[0]-tau[0])*g_vec[1]
            grad[3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*g_vec[0]
            #continue
            # inertia gradients:
            grad[4]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]
            grad[5]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]
            grad[6]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]

            # ixy
            grad[7]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][1]
            grad[7]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][0]

            # ixz
            grad[8]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][2]
            grad[8]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][0]

            # iyz
            grad[9]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][2]
            grad[9]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][1]

        # check gradient with fd:
        #grad_fd= pg.estimate_gradient(lambda x: self.fitness(x), x)
        #print "grad:"
        #print grad
        #print grad_fd
        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        #low_bounds=[0.01,-0.5,-0.5,-0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        #up_bounds =[1.0,0.5,0.5,0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        low_bounds=[0.01, -0.1,-0.1,-0.1, -1.0,-1.0,-1.0,-1.0,-1.0,-1.0]
        up_bounds =[1.0, 0.1,0.1,0.1, 1.0,1.0,1.0,1.0,1.0,1.0]
        
        #low_bounds=[0.297, -0.0,-0.0,-0.0, 0.0001,0.001,0.001,0.0,0.0,0.0]
        #up_bounds =[0.297, 0.0,0.0,0.0,     0.005,0.001,0.001,0.0,0.0,0.0]
        return (low_bounds,up_bounds)
    
class inertia_function:
    def __init__(self):
        self.ang_acc=None
        self.linear_acc=None
        self.force=None
        self.g=None#np.matrix([0.0,-9.8,0.0])
        self.f_loc=None
        self.m=0.0
        self.rc=0.0
        
    def skew_cross(self,vec):
        sk_mat=np.zeros((3,3))
        sk_mat[0,1]=-vec[2]
        sk_mat[0,2]=vec[1]
        sk_mat[1,0]=vec[2]
        sk_mat[1,2]=-vec[0]
        sk_mat[2,0]=-vec[1]
        sk_mat[2,1]=vec[0]
        return sk_mat
    
    def update_data(self,linear_acc,angular_acc,force,f_loc,g_vec,m,rc):
        self.linear_acc=linear_acc
        self.ang_acc=angular_acc
        self.force=force
        self.f_loc=f_loc
        self.g=g_vec
        self.m=m
        self.rc=rc
        print "Inertia SLSQP: Updated parameters"
        
    def fitness(self,x):
        # cost function
        # x contains current estimate of inertia, r_cx,r_cy
        m=self.m
                
        rc=self.rc

        H=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])
        cost=0.0
        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)
            
            # compute net torque:
            #rc_vec=self.skew_cross(rc)
            
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([-g_vec[2]*rc[1]+g_vec[1]*rc[2],
                              -g_vec[0]*rc[2]+g_vec[2]*rc[0],
                              -g_vec[1]*rc[0]+g_vec[0]*rc[1]])
            #print self.ang_acc[i]
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            #cost+=np.ravel(np.linalg.norm(omega_d-tau-tau_g)**2)[0]
            cost+=1.0*(omega_d[0]-tau[0]-tau_g[0])**2
            cost+=1.0*(omega_d[1]-tau[1]-tau_g[1])**2
            cost+=1.0*(omega_d[2]-tau[2]-tau_g[2])**2

        # inequality constraints
        nic1=np.copysign((H[0,0]-H[1,1]-H[2,2])**2,H[0,0]-H[1,1]-H[2,2])
        nic2=np.copysign((H[1,1]-H[0,0]-H[2,2])**2,H[1,1]-H[0,0]-H[2,2])
        nic3=np.copysign((H[2,2]-H[0,0]-H[1,1])**2,H[2,2]-H[0,0]-H[1,1])
        return [cost,nic1,nic2,nic3]
    def gradient(self, x):
        grad=np.zeros(6+6*3)
        m=self.m
        
        rc=self.rc

        H=np.zeros((3,3))
        ixx=x[0]
        iyy=x[1]
        izz=x[2]
        ixy=x[3]
        ixz=x[4]
        iyz=x[5]
        H[0,:]=np.array([ixx,ixy,ixz])
        H[1,:]=np.array([ixy,iyy,iyz])
        H[2,:]=np.array([ixz,iyz,izz])


        # nic1:
        nic1=2.0*(H[0,0]-H[1,1]-H[2,2])
        if(nic1>0):
            grad[6+0]=nic1
            grad[6+1]=-1.0*nic1
            grad[6+2]=-1.0*nic1

        nic2=2.0*(H[1,1]-H[0,0]-H[2,2])
        if(nic2>0):
            grad[6+6+1]=nic2
            grad[6+6+0]=-1.0*nic2
            grad[6+6+2]=-1.0*nic2

        nic3=2.0*(H[2,2]-H[0,0]-H[1,1])
        if(nic3>0):
            grad[6+2*6+2]=nic3
            grad[6+2*6+0]=-1.0*nic3
            grad[6+2*6+1]=-1.0*nic3

        for i in range(len(self.force)):
            f_data=self.force[i]
            f_loc=self.f_loc[i]
            # compute linear acceleration:
            g_vec=self.g[i]

            mg_vec=np.matrix(m*np.ravel(g_vec)[0:3]).T
                        
            f_sum=np.sum(f_data,axis=0)

            f_sum=np.sum(f_data,axis=0)
            #print self.linear_acc[i]
            #print m*self.linear_acc[i]-f_sum-mg_vec

            #continue
            tau=np.zeros(3)
            for j in range(len(f_data)):
                f_vec=np.matrix([f_data[j][0],f_data[j][1],f_data[j][2]]).T
                f_p=self.skew_cross(f_loc[j])
                tau+=np.ravel(f_p*f_vec)
            tau_g=m*np.array([-g_vec[2]*rc[1]+g_vec[1]*rc[2],
                              -g_vec[0]*rc[2]+g_vec[2]*rc[0],
                              -g_vec[1]*rc[0]+g_vec[0]*rc[1]])
            omega_d=np.ravel(H*np.matrix(np.ravel(self.ang_acc[i])).T)
            # inertia gradients:
            grad[0]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][0]
            grad[1]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][1]
            grad[2]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][2]

            # ixy
            grad[3]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][1]
            grad[3]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][0]

            # ixz
            grad[4]+=2.0*(omega_d[0]-tau_g[0]-tau[0])*self.ang_acc[i][2]
            grad[4]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][0]

            # iyz
            grad[5]+=2.0*(omega_d[1]-tau_g[1]-tau[1])*self.ang_acc[i][2]
            grad[5]+=2.0*(omega_d[2]-tau_g[2]-tau[2])*self.ang_acc[i][1]

        
        return grad
    def get_bounds(self):
        # bounds on mass, r_x, r_y, r_z, I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
        #low_bounds=[0.01,-0.5,-0.5,-0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        #up_bounds =[1.0,0.5,0.5,0.5,1.0,1.0,1.0,0.0,0.0,0.0]
        low_bounds=[0.001,0.001,0.001,-1.0,-1.0,-1.0]
        up_bounds =[1.0,1.0,1.0,1.0,1.0,1.0]
        return (low_bounds,up_bounds)
    def get_nic(self):
        return 3
    def get_nec(self):
        return 0
