#ifndef SPARSE_GRADIENT
#define SPARSE_GRADIENT true
#endif
// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <vector>
#include <unsupported/Eigen/MatrixFunctions>

//#include <pagmo/pagmo.hpp>
#include <pagmo/utils/gradients_and_hessians.hpp>

namespace sysid_opt
{
class optProblem
{
public:
  optProblem(int dummy);

  bool update_data();
  double objFunction(const vector<double> x) const;
  double objGradient(const vector<double> x) const;
  vector<double> inEqConstraints(vector<double> x) const;
  vector<double> EqConstraints(vector<double> x) const;
  vector<double> inEqGradient(vector<double> x) const;
  vector<double> EqGradient(vector<double> x) const;
  vector<vector<double>> bounds() const;
#if SPARSE_GRADIENT==true
  vector<double> sparse_gradient(vector<double> x) const;
  std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> gradient_sparsity() const;
#endif
  int m_dim,m_nec,m_nic;

private:
  vector<double> weights;
};
}
