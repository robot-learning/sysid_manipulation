#pragma once

#include <gtsam/geometry/Pose3.h>

namespace sysid_manipulation
{
struct sysid_data
{
  double t_step; // timestep

  gtsam::Point3 g; // gravity

  vector<gtsam::Vector> ft_data;

  vector<gtsam::Vector> bt_data;

  vector<gtsam::Pose3> w_cpts;
  gtsam::Pose3 obj_pose;

  gtsam::Vector6 obj_vel;

  gtsam::Vector6 obj_acc;  
  
};

// compute acceleration from poses

gtsam::Vector6 get_accel(const gtsam::Pose3 &x_t_2, const gtsam::Pose3 &x_t_1, const gtsam::Pose3 &x_t, const double &dt)
{
  return (gtsam::Pose3::Logmap(x_t) - 2.0*gtsam::Pose3::Logmap(x_t_1) + gtsam::Pose3::Logmap(x_t_2))/(dt*dt);
}
// compute velocity from poses
gtsam::Vector6 get_velocity(const gtsam::Pose3 &x_t_1, const gtsam::Pose3 &x_t, const double &dt) 
{
  return (gtsam::Pose3::Logmap(x_t) - gtsam::Pose3::Logmap(x_t_1))/(dt);
}

}
