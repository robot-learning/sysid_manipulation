#pragma once
#include <gtsam/nonlinear/NonlinearFactor.h>

namespace supp_fns
{
inline  gtsam::Matrix33 skew(const Eigen::Vector3d &v) 
{

  return (gtsam::Matrix33() <<
                      0, -v[2], v[1],
                      v[2], 0 , -v[0],
                      -v[1], v[0], 0).finished();
}
}
