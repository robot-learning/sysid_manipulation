// In planar cases we use Pose2 variables (x, y, theta) to represent the robot poses in SE(2)
#include <gtsam/geometry/Pose2.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Point3.h>


// class for factor graph, a container of various factors
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

// class for graph nodes values, a container of various geometric types
// here Values is used as a container of SE(2)
#include <gtsam/nonlinear/Values.h>

// symbol class is used to index varible in values
// e.g. pose varibles are generally indexed as 'x' + number, and landmarks as 'l' + numbers 
#include <gtsam/inference/Symbol.h>

// Factors used in this examples
// PriorFactor gives the prior distribution over a varible
// BetweenFactor gives odometry constraints
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>

// optimizer class, here we use Gauss-Newton
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/DoglegOptimizer.h>

// Once the optimized values have been calculated, we can also calculate the 
// (appoximated / linearized) marginal covariance of desired variables
#include <gtsam/nonlinear/Marginals.h>

#include <gtsam/nonlinear/ISAM2.h>

#include <sysid_manipulation/noise_models.hpp>
#include <gtsam/slam/expressions.h>

#include "gtsam_factors/sysid_expressions.h"
#include <gtsam_unstable/linear/LinearInequality.h>
#include <boost/optional/optional_io.hpp>

using namespace std;

class poseSmooth : public noiseModels
{
public:
  poseSmooth();
  
  bool initialize_graph();

  
bool update_forward_sim_integral(const gtsam::Pose3 &x_t_2,
                                 const gtsam::Pose3 &x_t_1,
                                 const double &t_2, const double &t_1,
                                 const double &t_0,
                                 const vector<gtsam::Vector> &bt_data,
                                 const vector<gtsam::Pose3> &w_cpts,
                                 const gtsam::Vector3 &g_vec,
                                 const double &opt_mass, const gtsam::Vector3 &opt_com,
                                 const gtsam::Matrix33 &opt_inertia_matrix);
  
  bool update_manifold_graph_baseline(const gtsam::Pose3 &x_t, 
                                      const vector<gtsam::Vector> &bt_data,  
                                      const vector<gtsam::Pose3> &w_cpts,
                                      const gtsam::Vector3 &g_vec,
                                      const double &time_step,
                                      const bool inequality=false,
                                      const bool geodesic=false,
                                      const bool linear_parameters=false,
                                      const bool fix_inertia=false);

  bool update_dynamics_param_graph(const gtsam::Pose3 &x_t,
                                   const gtsam::Pose3 &x_t_1,
                                   const gtsam::Pose3 &x_t_2,
                                   const vector<gtsam::Vector> &bt_data,
                                   const gtsam::Vector3 &g_vec, const double &time_step);

  bool update_rr_so3_manifold_graph(const gtsam::Pose3 &x_t, 
                                    const vector<gtsam::Vector> &bt_data,  
                                    const vector<gtsam::Pose3> &w_cpts,
                                    const gtsam::Vector3 &g_vec,
                                    const double &time_step);

  //bool update_pose_dynamics_graph(const gtsam::Pose3 &x_t,
  //const vector<gtsam::Vector> &bt_data,
  //const gtsam::Vector3 &g_vec, const double &time_step);
  bool update_forward_sim(const gtsam::Pose3 &x_t_2,
                          const gtsam::Pose3 &x_t_1,
                          const double &t_2, const double &t_1, const double &t_0,
                          const vector<gtsam::Vector> &bt_data,
                          const gtsam::Vector3 &g_vec,
                          const double &opt_mass, const gtsam::Vector3 &opt_com,
                          const gtsam::Matrix33 &opt_inertia_matrix);


  /*
  gtsam::Vector6_ create_dynamics_expression(const gtsam::Key &w, const gtsam::Key &R, const gtsam::Pose3 &x_t_2, const
                                             gtsam::Pose3 &x_t_1, const gtsam::Pose3 &x_t,
                                             const double &dt, const gtsam::Vector3 &g, const gtsam::Vector12 &f, const gtsam::Vector12 &tau);
  */
  //bool update_graph(vector<Eigen::Vector3d> forces, Eigen::Vector3d g, vector<Eigen::Vector3d> c_pts);
  //bool update_graph_pose(const Eigen::VectorXd &obj_pose,const double &time_step);  
  bool optimize(gtsam::Values &results, double &error_after, double &error_before);
  
  //bool optimize(gtsam::Values &results, double *error_after=new double(), double *error_before = new double() );
  bool debug_print(gtsam::Values &estimates);
  bool debug_print();

  bool get_marginals(gtsam::Values results);
  gtsam::NonlinearFactorGraph graph;
  int t_steps_=0;
  int l_step = 0;
  int l_delta = 5;
  bool debug_jacobian();

  bool update_manifold_graph(const gtsam::Pose3 &x_t, 
                             const vector<gtsam::Vector> &bt_data,  
                             const gtsam::Vector3 &g_vec,
                             const double &time_step);
  bool update_so3_graph(const gtsam::Pose3 &x_t, 
                        const vector<gtsam::Vector> &bt_data,  
                        const gtsam::Vector3 &g_vec, const double &time_step);
  bool update_linear_graph(const gtsam::Pose3 &x_t, 
                           const vector<gtsam::Vector> &bt_data,  
                           const gtsam::Vector3 &g_vec, const double &time_step);
  bool update_hybrid_so3_manifold_graph(const gtsam::Pose3 &x_t, 
                                        const vector<gtsam::Vector> &bt_data,
                                        const vector<gtsam::Pose3> &w_cpts,
                                        const gtsam::Vector3 &g_vec,
                                        const double &time_step,
                                        const bool inequality=true,
                                        const bool geodesic=true,
                                        const bool linear_parameters = false,
                                        const bool fix_inertia=false);



  // ft data :
  bool update_linear_graph_ft(const gtsam::Pose3 &x_t, 
                              const vector<gtsam::Vector> &ft_data,  
                              const gtsam::Vector3 &g_vec, const double &time_step);

  bool update_graph_ft(const gtsam::Pose3 &x_t, 
                       const vector<gtsam::Vector> &ft_data,
                       const gtsam::Vector3 &g_vec,
                       const double &time_step,
                       const bool inequality=true,
                       const bool geodesic=true,
                       const bool linear_parameters=false,
                       const bool fix_inertia=false);


  
  bool update_prior(const gtsam::Vector7 &w, const gtsam::Vector3 r_linear = gtsam::Vector3().setConstant(0), const gtsam::Rot3 i_rot = gtsam::Rot3());
  bool update_seed(const gtsam::Vector7 &w, const gtsam::Vector3 r_linear = gtsam::Vector3().setConstant(0), const gtsam::Rot3 i_rot = gtsam::Rot3());

  gtsam::Values initialEstimate;

protected:
  bool add_pose_mm(const gtsam::Pose3 &x_t, const int &t);
  bool add_bt_mm(const vector<gtsam::Vector> &bt_data, const int &t);
  bool add_ft_mm(const vector<gtsam::Vector> &ft_data, const int &t);
  bool add_cpt_mm(const vector<gtsam::Pose3> &w_cpts, const int &t);

  // variables:
private:
  double old_time_step;

  
  // store readings?
  //vector<vector<Eigen::Vector3d>> forces_,cpts_;
  //vector<Eigen::Vector3d> g_vecs_;
  gtsam::ISAM2 isam;
  vector<gtsam::Pose3> obj_poses_;
  //vector<vector<gtsam::Pose3>> w_cpts_arr_;
  
  gtsam::Vector7 w_i,w_prior;
  gtsam::Rot3 r_prior,r_i;
  gtsam::Vector3 r_p;
  vector<double> time_data;
  
};
