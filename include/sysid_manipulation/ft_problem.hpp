// In planar cases we use Pose2 variables (x, y, theta) to represent the robot poses in SE(2)
#include <gtsam/geometry/Pose2.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Point3.h>


// class for factor graph, a container of various factors
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

// class for graph nodes values, a container of various geometric types
// here Values is used as a container of SE(2)
#include <gtsam/nonlinear/Values.h>

// symbol class is used to index varible in values
// e.g. pose varibles are generally indexed as 'x' + number, and landmarks as 'l' + numbers 
#include <gtsam/inference/Symbol.h>

// Factors used in this examples
// PriorFactor gives the prior distribution over a varible
// BetweenFactor gives odometry constraints
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>

// optimizer class, here we use Gauss-Newton
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

// Once the optimized values have been calculated, we can also calculate the 
// (appoximated / linearized) marginal covariance of desired variables
#include <gtsam/nonlinear/Marginals.h>

#include <gtsam/nonlinear/ISAM2.h>

#include <sysid_manipulation/noise_models.hpp>
using namespace std;

class poseSmooth : public noiseModels
{
public:
  poseSmooth();
  
  bool initialize_graph();
  
  
  //bool update_graph(vector<Eigen::Vector3d> forces, Eigen::Vector3d g, vector<Eigen::Vector3d> c_pts);

  bool update_graph(const Eigen::VectorXd &obj_pose, const vector<gtsam::Vector> &ft_data,const double &time_step,  const gtsam::Vector3 &g_vec);


  bool update_pose_graph(const gtsam::Pose3 &obj_pose, const double &time_step);

  bool update_dynamics_graph(const gtsam::Pose3 &obj_pose, const gtsam::Vector6 &obj_vel, const gtsam::Vector6 &obj_acc, const vector<gtsam::Vector> &ft_data,  const gtsam::Vector3 &g_vec);

  bool optimize(gtsam::Values &results, double &error_after, double &error_before);
  
  //bool optimize(gtsam::Values &results, double *error_after=new double(), double *error_before = new double() );

  bool get_marginals(gtsam::Values results);
  gtsam::NonlinearFactorGraph graph;
  int t_steps_ = 0;
  int l_step = 0;
  int l_delta = 5;
  // variables:
private:
  double old_time_step;
  
  // store readings?
  //vector<vector<Eigen::Vector3d>> forces_,cpts_;
  //vector<Eigen::Vector3d> g_vecs_;
  gtsam::Values initialEstimate;
  gtsam::ISAM2 isam;

  // noise models:
  
};
