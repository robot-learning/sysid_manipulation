#include <gtsam/linear/NoiseModel.h>

class noiseModels
{
public:
  bool init_noise_models();
  bool smoothing();
protected:
  gtsam::noiseModel::Constrained::shared_ptr priorGeodesicModel;
  gtsam::noiseModel::Constrained::shared_ptr T_NM;
  //gtsam:: noiseModel::Constrained::shared_ptr dynamics_NM;
  gtsam::noiseModel::Constrained::shared_ptr priorRotModel, priorIModel, priorQSModel;

  gtsam::noiseModel::Constrained::shared_ptr priorFDModel;
    
  // Between noise models: (close to zero)

  
  gtsam::noiseModel::Constrained::shared_ptr betweenRotModel;

  gtsam::noiseModel::Constrained::shared_ptr betweenIModel;

  // Velocity, acceleration finite differencing variance:
  
  gtsam::noiseModel::Constrained::shared_ptr fdModel;
  gtsam::noiseModel::Constrained::shared_ptr smoothAccModel;
  
  // Physics error noise models:

  gtsam::noiseModel::Constrained::shared_ptr physicsErrModel;


  // inequality constraint nosie model:
  gtsam::noiseModel::Constrained::shared_ptr l7Model;



  // Sensor noise models:

  // force torque sensor:
  gtsam::noiseModel::Diagonal::shared_ptr ftForceNM;
  gtsam::noiseModel::Diagonal::shared_ptr ftTorqueNM;
  gtsam::noiseModel::Constrained::shared_ptr ftPointNM;
  gtsam::noiseModel::Constrained::shared_ptr allegroFTPointNM;

 
  gtsam::noiseModel::Constrained::shared_ptr poseMeasureNoiseModel;
  
  
  gtsam::noiseModel::Constrained::shared_ptr bt_NM;
  
  gtsam::noiseModel::Constrained::shared_ptr cpt_NM;
  


};
