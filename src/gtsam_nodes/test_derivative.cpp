#include <gtsam/base/numericalDerivative.h>
#include <gtsam/geometry/Rot3.h>
#include <gtsam/inference/Symbol.h>
//#include <gtsam/slam/BetweenFactor.h>
//#include <sysid_manipulation/pose_problem_isam.hpp>
#include "gtsam_factors/pose_factors.h"
#include "gtsam_factors/dynamics_factors.h"
#include "gtsam_factors/sysid_factors.h"
#include "gtsam_factors/sysid_expressions.h"
//#include "gtsam_factors/sysid_expressions_dart.h"
#include <gtsam/inference/Symbol.h>

#include "gtsam_factors/inequality_factors.h"
#include <gtsam/slam/BetweenFactor.h>

#include <CppUnitLite/TestHarness.h>
using namespace std;
using namespace gtsam;
//using namespace gtsam::symbol_shorthand;
using namespace gtsam::noiseModel;
using namespace gtsam::symbol_shorthand;
//using namespace gtsam::noiseModel;


bool test_sysid_expressions()
{
  Vector3 g;
  g<< 0,0,-9.8;

  Eigen::VectorXd w;
  w.resize(7);
  //w<< 1.3, -0.1,0.8,0.2, 0.10,0.10,0.25;
  w <<-0.05,-1.1,1.1,0.1,-0.1,0.1,-0.1;

  
  Rot3 R(gtsam::Rot3::Ypr(1.1,-0.5,0.6));
  
  Pose3 x_t_2 (Rot3(gtsam::Quaternion(1,0,0,0)),
               Point3(1,-5,-30));
  Pose3 x_t_1 (Rot3(gtsam::Quaternion(0.707,0.707,0,0)),
               Point3(-1,2,3));
  Pose3 x_t (Rot3(gtsam::Quaternion(1,0,0,0)),
               Point3(1,5,3));
  //x_t_2.tran
  Vector6 net_f_tau;
  
  net_f_tau.setConstant(0.2);
  net_f_tau(0) = -10.0;
  double dt=0.1;

  Vector6 acc;
  acc.setConstant(1.0);
  acc(2) = -15.0;
  Vector3 omega;
  omega.setConstant(2.0);
  omega(1) = -11;
  Matrix33 skew_omega = sysid_expr::skew_fn(omega);
  
  Vector1 m = sysid_expr::mass(w);
  Vector3 r_c = sysid_expr::com(w);
  Matrix33 H = sysid_expr::inertia(w,R);
  Matrix actualH1,actualH2,actualH3;
  Matrix numericalH1,numericalH2,numericalH3;

  Matrix33 skew_rc = sysid_expr::skew_fn(r_c);

  // retractions
  cout<<"retract_linear_inertial()"<<endl;
  sysid_expr::retract_linear_inertial(w,actualH1);
  numericalH1 = numericalDerivative11<Vector7,Vector7>
    ( boost::function<Vector7(const Vector7&)>
      ( boost::bind( &sysid_expr::retract_linear_inertial, _1,boost::none)),
      w,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
 
  cout<<"soft_physics_constraint()"<<endl;
  cout<<sysid_expr::soft_physics_constraint(w,actualH1).transpose()<<endl;
  numericalH1 = numericalDerivative11<Vector7,Vector7>
    ( boost::function<Vector7(const Vector7&)>
      ( boost::bind( &sysid_expr::soft_physics_constraint, _1,boost::none)),
      w,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

    cout<<"physics_constraint()"<<endl;
  cout<<sysid_expr::physics_constraint(w,actualH1).transpose()<<endl;
  numericalH1 = numericalDerivative11<Vector7,Vector7>
    ( boost::function<Vector7(const Vector7&)>
      ( boost::bind( &sysid_expr::physics_constraint, _1,boost::none)),
      w,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

// mass
  cout<<"mass()"<<endl;
  sysid_expr::mass(w,actualH1);
  numericalH1 = numericalDerivative11<Vector1,Vector7>
    ( boost::function<Vector1(const Vector7&)>
      ( boost::bind( &sysid_expr::mass, _1,boost::none)),
      w,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  
  // r_c
  cout<<"com()"<<endl;
  sysid_expr::com(w,actualH1);
  numericalH1 = numericalDerivative11<Vector3,Vector7>
    ( boost::function<Vector3(const Vector7&)>
      ( boost::bind( &sysid_expr::com, _1,boost::none)),
      w,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  // inertia
  cout<<"inertia()"<<endl;
  sysid_expr::inertia(w,R,actualH1,actualH2);
  //cout<<actualH2<<endl;
  numericalH1 = numericalDerivative21<Matrix33,Vector7,Rot3>
    ( boost::function<Matrix33(const Vector7&, const Rot3&)>
      ( boost::bind( &sysid_expr::inertia, _1,_2,boost::none, boost::none)),
      w,R,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Matrix33,Vector7,Rot3>
    ( boost::function<Matrix33(const Vector7&, const Rot3&)>
      ( boost::bind( &sysid_expr::inertia, _1,_2,boost::none, boost::none)),
      w,R,1e-2);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
 

  cout<<"inertia_linear()"<<endl;
  Vector3 r_rand = Vector3().Random();
  
  sysid_expr::inertia_linear(w,r_rand,actualH1,actualH2);
  numericalH1 = numericalDerivative21<Matrix33,Vector7,Vector3>
    ( boost::function<Matrix33(const Vector7&, const Vector3&)>
      ( boost::bind( &sysid_expr::inertia_linear, _1,_2,boost::none, boost::none)),
      w,r_rand,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Matrix33,Vector7,Vector3>
    ( boost::function<Matrix33(const Vector7&, const Vector3&)>
      ( boost::bind( &sysid_expr::inertia_linear, _1,_2,boost::none, boost::none)),
      w,r_rand,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
 
  
  cout<<"Testing spatial_inertia()"<<endl;
  // compute spatial gradient:
  Matrix66 A = sysid_expr::spatial_inertia(m,r_c,H,actualH1,actualH2,actualH3);

  numericalH1 = numericalDerivative31<Eigen::Matrix<double,6,6>,Vector1,Vector3,Matrix33>
    ( boost::function<Eigen::Matrix<double,6,6>(const Vector1&,
                                                 const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::spatial_inertia, _1,_2, _3, boost::none, boost::none,boost::none)),
      m, r_c,H,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
  numericalH2 = numericalDerivative32<Eigen::Matrix<double,6,6>,Vector1,Vector3,Matrix33>
    ( boost::function<Eigen::Matrix<double,6,6>(const Vector1&,
                                                 const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::spatial_inertia, _1,_2, _3, boost::none, boost::none,boost::none)),
      m, r_c,H,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  numericalH3 = numericalDerivative33<Eigen::Matrix<double,6,6>,Vector1,Vector3,Matrix33>
    ( boost::function<Eigen::Matrix<double,6,6>(const Vector1&,
                                                 const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::spatial_inertia, _1,_2, _3, boost::none, boost::none,boost::none)),
      m, r_c,H,1e-5);
  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;

  cout<<"testing get_translation()"<<endl;
  cout<<sysid_expr::get_translation(x_t_2,actualH1);
  
  numericalH1 = numericalDerivative11<Vector3,Pose3>
    (boost::function<Vector3(const Pose3&)>
      (boost::bind(&sysid_expr::get_translation, _1, boost::none)), x_t_2, 1e-5);

  
  assert_equal(numericalH1,actualH1, 1E-5);
  

  
  // pose_diff
  
  cout<<"Testing pose_diff()"<<endl;
  
  sysid_expr::pose_diff(x_t_2,x_t_1,actualH1,actualH2);

  numericalH1 = numericalDerivative21<Vector6,Pose3,Pose3>
    (boost::function<Vector6(const Pose3&, const Pose3&)>
      (boost::bind(&sysid_expr::pose_diff, _1, _2, boost::none,
                   boost::none)), x_t_2, x_t_1, 1e-5);
  
  assert_equal(numericalH1,actualH1, 1E-5);
  
  numericalH2 = numericalDerivative22<Vector6,Pose3,Pose3>
    (boost::function<Vector6(const Pose3&, const Pose3&)>
     (boost::bind(&sysid_expr::pose_diff, _1, _2, boost::none,
                   boost::none)), x_t_2, x_t_1, 1e-5);
  
  assert_equal(numericalH2,actualH2, 1E-5);

  
    // pose_vel
  cout<<"Testing pose_vel()"<<endl;

  sysid_expr::pose_vel(x_t_2,x_t_1,dt,actualH1,actualH2,actualH3);
  
  numericalH1 = numericalDerivative31<Vector6,Pose3,Pose3,double>
    ( boost::function<Vector6(const Pose3&,
                              const Pose3&, const double&)>
      ( boost::bind( &sysid_expr::pose_vel, _1,_2, _3, boost::none, boost::none,boost::none)),
      x_t_2,x_t_1,dt,1e-3);
  if(assert_equal(numericalH1,actualH1,1E-2)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative32<Vector6,Pose3,Pose3,double>
    ( boost::function<Vector6(const Pose3&,
                              const Pose3&, const double&)>
      ( boost::bind( &sysid_expr::pose_vel, _1,_2, _3, boost::none, boost::none,boost::none)),
      x_t_2,x_t_1,dt,1e-3);
  if(assert_equal(numericalH2,actualH2,1E-2)) cout<<"Passed"<<endl;

  numericalH3 = numericalDerivative33<Vector6,Pose3,Pose3,double>
    ( boost::function<Vector6(const Pose3&,
                              const Pose3&, const double&)>
      ( boost::bind( &sysid_expr::pose_vel, _1,_2, _3, boost::none, boost::none,boost::none)),
      x_t_2,x_t_1,dt,1e-3);
  if(assert_equal(numericalH3,actualH3,1E-2)) cout<<"Passed"<<endl;

  cout<<"proj_vec()"<<endl;
  Vector3 w_g=Vector3::Random();
  cout<<sysid_expr::proj_vec(x_t_2,w_g,actualH1,actualH2);
  numericalH1 = numericalDerivative21<Vector3,Pose3,Vector3>(
      boost::function<Vector(const Pose3&, const Vector3&)>
      (boost::bind(&sysid_expr::proj_vec, _1, _2, boost::none,
                   boost::none)), x_t_2, w_g, 1e-5);
  assert_equal(numericalH1,actualH1, 1E-5);

  numericalH2 = numericalDerivative22<Vector3,Pose3,Vector3>(
      boost::function<Vector(const Pose3&, const Vector3&)>
      (boost::bind(&sysid_expr::proj_vec, _1, _2, boost::none,
                   boost::none)), x_t_2, w_g, 1e-5);
  assert_equal(numericalH2,actualH2, 1E-5);

  
  cout<<"inv_proj_vec()"<<endl;
  //Vector3 w_g=Vector3::Random();
  cout<<sysid_expr::inv_proj_vec(x_t_2,w_g,actualH1,actualH2);
  numericalH1 = numericalDerivative21<Vector3,Pose3,Vector3>(
      boost::function<Vector(const Pose3&, const Vector3&)>
      (boost::bind(&sysid_expr::inv_proj_vec, _1, _2, boost::none,
                   boost::none)), x_t_2, w_g, 1e-5);
  assert_equal(numericalH1,actualH1, 1E-5);

  numericalH2 = numericalDerivative22<Vector3,Pose3,Vector3>(
      boost::function<Vector(const Pose3&, const Vector3&)>
      (boost::bind(&sysid_expr::inv_proj_vec, _1, _2, boost::none,
                   boost::none)), x_t_2, w_g, 1e-5);
  assert_equal(numericalH2,actualH2, 1E-5);


  cout<<"inv_proj_pt()"<<endl;
  //Vector3 w_g=Vector3::Random();
  cout<<sysid_expr::inv_proj_pt(x_t_2,w_g,actualH1,actualH2);
  numericalH1 = numericalDerivative21<Vector3,Pose3,Vector3>(
      boost::function<Vector(const Pose3&, const Vector3&)>
      (boost::bind(&sysid_expr::inv_proj_pt, _1, _2, boost::none,
                   boost::none)), x_t_2, w_g, 1e-5);
  assert_equal(numericalH1,actualH1, 1E-5);

  numericalH2 = numericalDerivative22<Vector3,Pose3,Vector3>(
      boost::function<Vector(const Pose3&, const Vector3&)>
      (boost::bind(&sysid_expr::inv_proj_pt, _1, _2, boost::none,
                   boost::none)), x_t_2, w_g, 1e-5);
  assert_equal(numericalH2,actualH2, 1E-5);


  Matrix44 p_1,p_2;
     
  p_1 = sysid_expr::proj_inertia(m,m[0]*r_c,H);
  p_2 = sysid_expr::proj_inertia(m*2.0,m[0]*2.0*r_c*1.0,H);
  cout<<p_1.inverse()*p_2<<endl;
  cout<<"inertia_distance()"<<endl;

  cout<<sysid_expr::inertia_distance(p_1,p_2,actualH1,actualH2)<<endl;
  numericalH1 = numericalDerivative21<Vector1,Matrix44,Matrix44>
    ( boost::function<Vector1(const Matrix44&, const Matrix44&)>
      ( boost::bind( &sysid_expr::inertia_distance, _1,_2,boost::none, boost::none)),
      p_1,p_2,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
  numericalH2 = numericalDerivative22<Vector1,Matrix44,Matrix44>
    ( boost::function<Vector1(const Matrix44&, const Matrix44&)>
      ( boost::bind( &sysid_expr::inertia_distance, _1,_2,boost::none, boost::none)),
      p_1,p_2,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  /*
  cout<<"Between factor"<<endl;
  Pose3 R1 = x_t_2;
  Pose3 R2 = x_t_1;
  Pose3 noise =Pose3(); // Rot3::Rodrigues(0.01, 0.01, 0.01); // Uncomment to make unit test fail
  Pose3 measured = x_t_2.between(x_t_1)*noise  ;
  Symbol r_1=Symbol('R',0);
  Symbol r_2=Symbol('R',1);

  BetweenFactor<Pose3> factor(r_1,r_2, measured, Isotropic::Sigma(6, 0.05));
  Vector actual = factor.evaluateError(R1, R2, actualH1, actualH2);

  Vector expected = Pose3::Logmap(measured.inverse() * R1.between(R2));
  //assert_equal(expected,actual, 1e-3); // Uncomment to make unit test fail

  numericalH1 = numericalDerivative21<Vector6,Pose3,Pose3>(
      boost::function<Vector(const Pose3&, const Pose3&)>(boost::bind(
          &BetweenFactor<Pose3>::evaluateError, factor, _1, _2, boost::none,
          boost::none)), R1, R2, 1e-5);
  assert_equal(numericalH1,actualH1, 1E-5);

  numericalH2 = numericalDerivative22<Vector6,Pose3,Pose3>(
      boost::function<Vector(const Pose3&, const Pose3&)>(boost::bind(
          &BetweenFactor<Pose3>::evaluateError, factor, _1, _2, boost::none,
          boost::none)), R1, R2, 1e-5);
  assert_equal(numericalH2,actualH2, 1E-5);
  */
  // acc_rate
  /*
  cout<<"Testing acc_rate()"<<endl;
  sysid_expr::acc_rate(acc,dt,actualH1,actualH2);

  numericalH1 = numericalDerivative21<Vector6,Vector6,double>
    ( boost::function<Vector6(const Vector6&, const double&)>
      ( boost::bind( &sysid_expr::acc_rate, _1,_2,boost::none, boost::none)),
      acc,dt,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Vector6,Vector6,double>
    ( boost::function<Vector6(const Vector6&, const double&)>
      ( boost::bind( &sysid_expr::acc_rate, _1,_2,boost::none, boost::none)),
      acc,dt,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  // pose_acc
  cout<<"Testing pose_acc()"<<endl;
    
  Vector6 Xt_diff = sysid_expr::pose_acc(x_t_2,x_t_1,x_t,actualH1,actualH2,actualH3);
  
  numericalH1 = numericalDerivative31<Vector6,Pose3,Pose3,Pose3>
    ( boost::function<Vector6(const Pose3&,
                              const Pose3&, const Pose3&)>
      ( boost::bind( &sysid_expr::pose_acc, _1,_2, _3, boost::none, boost::none,boost::none)),
      x_t_2, x_t_1, x_t, 1e-5);

  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  
  numericalH2 = numericalDerivative32<Vector6,Pose3,Pose3,Pose3>
    ( boost::function<Vector6(const Pose3&,
                              const Pose3&, const Pose3&)>
      ( boost::bind( &sysid_expr::pose_acc, _1,_2, _3, boost::none, boost::none,boost::none)),
      x_t_2,x_t_1,x_t,1e-5);

  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  numericalH3 = numericalDerivative33<Vector6,Pose3,Pose3,Pose3>
    ( boost::function<Vector6(const Pose3&,
                              const Pose3&, const Pose3&)>
      ( boost::bind( &sysid_expr::pose_acc, _1,_2, _3, boost::none, boost::none,boost::none)),
      x_t_2,x_t_1,x_t,1e-5);

  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;


  // b1_a
  cout<<"Testing b1_a()"<<endl;

  sysid_expr::b1_a(m,g,actualH1,actualH2);
  
  numericalH1 = numericalDerivative21<Vector6,Vector1,Vector3>
    ( boost::function<Vector6(const Vector1&,const Vector3&)>
      ( boost::bind( &sysid_expr::b1_a, _1,_2,boost::none, boost::none)),
      m,g,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
  numericalH2 = numericalDerivative22<Vector6,Vector1,Vector3>
    ( boost::function<Vector6(const Vector1&,const Vector3&)>
      ( boost::bind( &sysid_expr::b1_a, _1,_2,boost::none, boost::none)),
      m,g,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  // b1_b
  cout<<"Testing b1_b()"<<endl;
  sysid_expr::b1_b(r_c,skew_omega,actualH1,actualH2);

  numericalH1 = numericalDerivative21<Vector6,Vector3,Matrix33>
    ( boost::function<Vector6(
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b1_b, _1,_2, boost::none, boost::none)),
      r_c,skew_omega,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  
  numericalH2 = numericalDerivative22<Vector6,Vector3,Matrix33>
    ( boost::function<Vector6(
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b1_b, _1,_2, boost::none, boost::none)),
      r_c,skew_omega,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  

  // b2_a
  cout<<"Testing b2_a()"<<endl;
  sysid_expr::b2_a(H,omega,skew_omega,actualH1,actualH2,actualH3);
  
  numericalH1 = numericalDerivative31<Vector6,Matrix33,Vector3,Matrix33>
    ( boost::function<Vector6(const Matrix33&,
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b2_a, _1,_2, _3, boost::none, boost::none,boost::none)),
      H,omega,skew_omega,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative32<Vector6,Matrix33,Vector3,Matrix33>
    ( boost::function<Vector6(const Matrix33&,
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b2_a, _1,_2, _3, boost::none, boost::none,boost::none)),
      H,omega,skew_omega,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

    numericalH3 = numericalDerivative33<Vector6,Matrix33,Vector3,Matrix33>
    ( boost::function<Vector6(const Matrix33&,
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b2_a, _1,_2, _3, boost::none, boost::none,boost::none)),
      H,omega,skew_omega,1e-5);
  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;


  // b2_b
  cout<<"Testing b2_b()"<<endl;
  Vector6 B =   sysid_expr::b2_b(g,skew_rc,actualH1,actualH2);
  
  numericalH1 = numericalDerivative21<Vector6,Vector3,Matrix33>
    ( boost::function<Vector6(
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b2_b, _1,_2, boost::none, boost::none)),
      g,skew_rc,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Vector6,Vector3,Matrix33>
    ( boost::function<Vector6(
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::b2_b, _1,_2, boost::none, boost::none)),
      g,skew_rc,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
 
  // dynamics_error
  cout<<"Testing dynamics_error()"<<endl;

  sysid_expr::dynamics_error(A,B,Xt_diff,actualH1,actualH2,actualH3);

  numericalH1 = numericalDerivative31<Vector6,Matrix66,Vector6,Vector6>
    ( boost::function<Vector6(const Matrix66&,
                              const Vector6&, const Vector6&)>
      ( boost::bind( &sysid_expr::dynamics_error, _1,_2, _3, boost::none, boost::none,boost::none)),
      A,B,Xt_diff,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
  numericalH2 = numericalDerivative32<Vector6,Matrix66,Vector6,Vector6>
    ( boost::function<Vector6(const Matrix66&,
                              const Vector6&, const Vector6&)>
      ( boost::bind( &sysid_expr::dynamics_error, _1,_2, _3, boost::none, boost::none,boost::none)),
      A,B,Xt_diff,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  numericalH3 = numericalDerivative33<Vector6,Matrix66,Vector6,Vector6>
    ( boost::function<Vector6(const Matrix66&,
                              const Vector6&, const Vector6&)>
      ( boost::bind( &sysid_expr::dynamics_error, _1,_2, _3, boost::none, boost::none,boost::none)),
      A,B,Xt_diff,1e-5);
  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;


  
  cout<<"net_force_torque()"<<endl;
  Vector12 f,r_f;
  f = Vector12::Random();
  r_f = Vector12::Random();
  
  sysid_expr::net_force_torque(f,r_f,actualH1,actualH2);
  
  numericalH1 = numericalDerivative21<Vector6,Vector12,Vector12>
    ( boost::function<Vector6(const Vector12&, const Vector12&)>
      ( boost::bind( &sysid_expr::net_force_torque, _1,_2,boost::none, boost::none)),
      f,r_f,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Vector6,Vector12,Vector12>
    ( boost::function<Vector6(const Vector12&, const Vector12&)>
      ( boost::bind( &sysid_expr::net_force_torque, _1,_2,boost::none, boost::none)),
      f,r_f,1e-5);

  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  // Verify reimmannian gradients:
  Matrix44 p_1,p_2;

  p_1 = sysid_expr::proj_inertia(m,r_c,H);
  p_2 = sysid_expr::proj_inertia(m*1.0,r_c*1.0,H);
  //cout<<p_2.determinant()<<endl;
  //cout<<p_2<<endl;
  cout<<"inertia_distance()"<<endl;

  cout<<sysid_expr::inertia_distance(p_1,p_2,actualH1,actualH2)<<endl;
  numericalH1 = numericalDerivative21<Vector1,Matrix44,Matrix44>
    ( boost::function<Vector1(const Matrix44&, const Matrix44&)>
      ( boost::bind( &sysid_expr::inertia_distance, _1,_2,boost::none, boost::none)),
      p_1,p_2,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
  numericalH2 = numericalDerivative22<Vector1,Matrix44,Matrix44>
    ( boost::function<Vector1(const Matrix44&, const Matrix44&)>
      ( boost::bind( &sysid_expr::inertia_distance, _1,_2,boost::none, boost::none)),
      p_1,p_2,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  cout<<"proj_inertia()"<<endl;
  sysid_expr::proj_inertia(m,r_c,H,actualH1,actualH2,actualH3);

    numericalH1 = numericalDerivative31<Matrix44,Vector1,Vector3,Matrix33>
    ( boost::function<Matrix44(const Vector1&,
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::proj_inertia, _1,_2, _3, boost::none, boost::none,boost::none)),
      m,r_c,H,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  
  numericalH2 = numericalDerivative32<Matrix44,Vector1,Vector3,Matrix33>
    ( boost::function<Matrix44(const Vector1&,
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::proj_inertia, _1,_2, _3, boost::none, boost::none,boost::none)),
      m,r_c,H,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  numericalH3 = numericalDerivative33<Matrix44,Vector1,Vector3,Matrix33>
    ( boost::function<Matrix44(const Vector1&,
                              const Vector3&, const Matrix33&)>
      ( boost::bind( &sysid_expr::proj_inertia, _1,_2, _3, boost::none, boost::none,boost::none)),
      m,r_c,H,1e-5);
  cout<<actualH3<<endl;
  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;

  Matrix44 P;
  P.Random();
  cout<<"retract_inertia_mass()"<<endl;
  sysid_expr::retract_inertia_mass(P,actualH1);
  numericalH1 = numericalDerivative11<Vector1,Matrix44>
    ( boost::function<Vector1(const Matrix44&)>
      ( boost::bind( &sysid_expr::retract_inertia_mass, _1,boost::none)),
      P,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  
  // r_c
  cout<<"retract_inertia_com()"<<endl;
  sysid_expr::retract_inertia_com(P,actualH1);
  numericalH1 = numericalDerivative11<Vector3,Matrix44>
    ( boost::function<Vector3(const Matrix44&)>
      ( boost::bind( &sysid_expr::retract_inertia_com, _1,boost::none)),
      P,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  // inertia
  cout<<"retract_inertia_h()"<<endl;
  sysid_expr::retract_inertia_h(P,actualH1);
  numericalH1 = numericalDerivative11<Matrix33,Matrix44>
    ( boost::function<Matrix33(const Matrix44&)>
      ( boost::bind( &sysid_expr::retract_inertia_h, _1,boost::none)),
      P,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;
  */
  // mat mul

  cout<<"matmul()"<<endl;
  Matrix53 A_r;
  A_r = A_r.Random();
  Matrix33 B_r;
  B_r = B_r.Random();
  sysid_expr::matmul<5,3,3,3>(A_r,B_r,actualH1,actualH2);

  numericalH1 = numericalDerivative21<Matrix53,Matrix53,Matrix33>
    ( boost::function<Matrix53(const Matrix53&, const Matrix33&)>
      ( boost::bind( &sysid_expr::matmul<5,3,3,3>, _1,_2,boost::none, boost::none)),
      A_r,B_r,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Matrix53,Matrix53,Matrix33>
    ( boost::function<Matrix53(const Matrix53&, const Matrix33&)>
      ( boost::bind( &sysid_expr::matmul<5,3,3,3>, _1,_2,boost::none, boost::none)),
      A_r,B_r,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;


  cout<<"scalar_divide()"<<endl;
  double scalar = 0.1;
  sysid_expr::scalar_divide<5,3>(A_r,scalar,actualH1,actualH2);

  numericalH1 = numericalDerivative21<Matrix53,Matrix53,double>
    ( boost::function<Matrix53(const Matrix53&, const double&)>
      ( boost::bind( &sysid_expr::scalar_divide<5,3>, _1,_2,boost::none, boost::none)),
      A_r,scalar,1e-5);
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  numericalH2 = numericalDerivative22<Matrix53,Matrix53,double>
    ( boost::function<Matrix53(const Matrix53&, const double&)>
      ( boost::bind( &sysid_expr::scalar_divide<5,3>, _1,_2,boost::none, boost::none)),
      A_r,scalar,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;


}

/*
bool test_velocity()
{
  double dt=0.1;
  Vector6 n_d;
  n_d.setIdentity();
  n_d=n_d*0.000;
  noiseModel::Diagonal::shared_ptr poseNoiseModel=noiseModel::Diagonal::Sigmas(n_d);
  Symbol p1=Symbol('a',0);
  Symbol p2=Symbol('a',1);
  Symbol p3=Symbol('a',2);

  Pose3 test;
  //cerr<<test<<endl;
  Pose3 x1(gtsam::Rot3::Ypr(1,0,0),
           gtsam::Point3(0,0,1));
  
  Pose3 x2(gtsam::Rot3::Ypr(-0.1,0.1,0),
           gtsam::Point3(0,4,0));
  
  Pose3 xd(gtsam::Rot3::Ypr(0,0,0.1),
           gtsam::Point3(0,0,2));;

  Vector6 vd=Vector6::Zero();
  vd.setZero();

  // test factor:



  LocalPoseAccFactor factor(p1,p2,p3,poseNoiseModel,dt);



  
  Matrix actualH1, actualH2, actualH3;

  Vector actual = factor.evaluateError(x1, x2, vd, actualH1, actualH2, actualH3);
  cerr<<actual<<endl;
  Matrix numericalH1 = numericalDerivative31<Vector6,Pose3, Pose3,Vector6>
    ( boost::function<Vector(const Pose3&,const Pose3&, const Vector6&)>(boost::bind( &LocalPoseAccFactor::evaluateError,
                                                                                    factor, _1,
                                                                                    _2, _3,
                                                                                    boost::none,
                                                                                    boost::none,
                                                                                    boost::none)),
      x1, x2, vd,1e-5);


  
  cerr<<numericalH1<<endl;
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  Matrix numericalH2 = numericalDerivative32<Vector6,Pose3, Pose3,Vector6>
    ( boost::function<Vector(const Pose3&,const Pose3&, const Vector6&)>(boost::bind( &LocalPoseAccFactor::evaluateError,
                                                                                    factor, _1,
                                                                                    _2, _3,
                                                                                    boost::none,
                                                                                    boost::none,
                                                                                    boost::none)),
      x1, x2, vd,1e-5);


  
  
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  
  Matrix numericalH3 = numericalDerivative33<Vector6,Pose3, Pose3,Vector6>
    ( boost::function<Vector(const Pose3&,const Pose3&, const Vector6&)>(boost::bind( &LocalPoseAccFactor::evaluateError,
                                                                                    factor, _1,
                                                                                    _2, _3,
                                                                                    boost::none,
                                                                                    boost::none,
                                                                                    boost::none)),
      x1, x2, vd,1e-5);


  
  
  
  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;

 
}
*/

bool test_ft_acc()
{
  cout<<"AccFtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol a=Symbol('a',0);
  Symbol tau=Symbol('n',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  

  Vector3 force(0.0,0.1,0.0);
  
  Vector3 torque(0.0,0.1,0.0);
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  
  Vector10 w_dynamics;
  w_dynamics.tail(6) = Matrix61().Constant(1.0);
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  AccFtFactor factor(a,f,tau,W,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4;
  Vector actual = factor.evaluateError(acc,force,torque,w_dynamics,actualH1,actualH2,actualH3, actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;


  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative44<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;

  return true;

}

bool test_manifold_ft_acc()
{
  cout<<"ManifoldAccFtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol a=Symbol('a',0);
  Symbol tau=Symbol('n',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));
  Vector3 force(0.0,0.0,0.0);
  
  Vector3 torque(0.1,-8,0.1);
  Vector3 g_v(0.0,0.0,-0.8);

  Vector6 acc = (Matrix61()<<10,-1,10,
                             0.0,0.0,0.0).finished();
  
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  ManifoldAccFtFactor factor(a,f,tau,W,R,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4,actualH5;
  Vector actual = factor.evaluateError(acc,force,torque,w_dynamics,r_mat,actualH1,actualH2,actualH3, actualH4,actualH5);
  cout<<"Factor error: "<<actual.transpose()<<endl;

  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative51<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative52<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative53<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative54<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccFtFactor::gradientTester, factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 5: ";
  Matrix numericalH5 = numericalDerivative55<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH5,actualH5,1E-5))  cout<<"Passed"<<endl;

  return true;

}
bool test_qs_ft()
{
  cout<<"QSFtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol v=Symbol('v',0);

  Symbol tau=Symbol('n',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  

  Vector3 force(0.0,0.0,0.0);
  
  Vector3 torque(0.1,-8,0.1);
  Vector3 g_v(0.0,0.0,-0.8);


  Vector6 vel = (Matrix61()<<1,1,1,
                             1.0,1.0,1.0).finished();

  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  QSFtFactor factor(v,f,tau,W,factorNM,g_v,1e-5);
  
  Matrix actualH1,  actualH2, actualH3,actualH4;
  Vector actual = factor.evaluateError(vel,force,torque,w_dynamics,actualH1,actualH2, actualH3,actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;

  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector6,Vector3,Vector3,Vector7>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&)>
     (boost::bind( &QSFtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, torque, w_dynamics,1e-5);

  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector6,Vector3,Vector3,Vector7>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&)>
     (boost::bind( &QSFtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, torque, w_dynamics,1e-5);

  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector6,Vector3,Vector3,Vector7>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&)>
     (boost::bind( &QSFtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, torque, w_dynamics,1e-5);

  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative44<Vector6,Vector6,Vector3,Vector3,Vector7>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&)>
     (boost::bind( &QSFtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, torque, w_dynamics,1e-5);

  if(assert_equal(numericalH4,actualH4,1E-5)) cout<<"Passed"<<endl;

  
}
bool test_qs_bt()
{
  cout<<"QSBtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  
  Vector12 force;
  force.setConstant(0.1);
  
  Vector12 c_pts;
  c_pts.setConstant(-0.1);

  Symbol v=Symbol('v',0);
  Symbol r=Symbol('r',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  

  Vector3 g_v(0.0,0.0,-0.8);


  Vector6 vel = (Matrix61()<<1,1,1,
                             1.0,1.0,1.0).finished();

  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  QSBtFactor factor(v,f,r,W,factorNM,g_v,1);
  
  Matrix actualH1,  actualH2, actualH3,actualH4;
  Vector actual = factor.evaluateError(vel,force,c_pts,w_dynamics,actualH1,actualH2, actualH3,actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;

  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector6,Vector12,Vector12,Vector7>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&)>
     (boost::bind( &QSBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, c_pts, w_dynamics,1e-5);

  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector6,Vector12,Vector12,Vector7>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&)>
     (boost::bind( &QSBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, c_pts, w_dynamics,1e-5);

  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector6,Vector12,Vector12,Vector7>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&)>
     (boost::bind( &QSBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, c_pts, w_dynamics,1e-5);

  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative44<Vector6,Vector6,Vector12,Vector12,Vector7>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&)>
     (boost::bind( &QSBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),
     vel,force, c_pts, w_dynamics,1e-5);

  if(assert_equal(numericalH4,actualH4,1E-5)) cout<<"Passed"<<endl;

  
}

bool test_newton_ft_acc()
{
  cout<<"NewtonEulerAccFtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol a=Symbol('a',0);
  Symbol v=Symbol('v',0);

  Symbol tau=Symbol('n',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));
  Vector3 force(0.0,0.0,0.0);
  
  Vector3 torque(0.1,-8,0.1);
  Vector3 g_v(0.0,0.0,-0.8);

  Vector6 acc = (Matrix61()<<1,1,1,
                             1.0,1.0,1.0).finished();

  Vector6 vel = (Matrix61()<<1,1,1,
                             1.0,1.0,1.0).finished();

  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  NewtonEulerAccFtFactor factor(a,v,f,tau,W,R,factorNM,g_v);
  
  Matrix actualH1, actualH6, actualH2, actualH3,actualH4,actualH5;
  Vector actual = factor.evaluateError(acc,vel,force,torque,w_dynamics,r_mat,actualH6,actualH1,actualH2, actualH3,actualH4,actualH5);
  cout<<"Factor error: "<<actual.transpose()<<endl;

  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative51<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &NewtonEulerAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative52<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &NewtonEulerAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative53<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &NewtonEulerAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative54<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &NewtonEulerAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
    
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 5: ";
  Matrix numericalH5 =numericalDerivative55<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &NewtonEulerAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
    
  if(assert_equal(numericalH5,actualH5,1E-5))  cout<<"Passed"<<endl;

  return true;

}

bool test_manifold_bt_acc()
{
  cout<<"ManifoldAccBtLocalFactor()"<<endl;
  Symbol a=Symbol('a',0);
  Symbol r=Symbol('r',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));

  Vector9 force;
  force.setConstant(0.1);
  
  Vector9 c_pts;
  c_pts.setConstant(-0.1);
  
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  ManifoldAccBtLocalFactor factor(a,f,r,W,R,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4,actualH5;
  Vector actual = factor.evaluateError(acc,force,c_pts,w_dynamics,r_mat,actualH1,actualH2,actualH3, actualH4,actualH5);
  cout<<"Factor error: "<<actual.transpose()<<endl;
  cout<<"Testing derivatives..."<<endl;
    
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative51<Vector6,Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccBtLocalFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative52<Vector6,Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccBtLocalFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative53<Vector6,Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>     (boost::bind( &ManifoldAccBtLocalFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 =numericalDerivative54<Vector6,Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccBtLocalFactor::gradientTester, factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 5: ";
  Matrix numericalH5 = numericalDerivative55<Vector6,Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccBtLocalFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH5,actualH5,1E-5))  cout<<"Passed"<<endl;

  return true;
}
bool test_manifold_bt()
{
  cout<<"ManifoldFixedAccBtFactor()"<<endl;
  Symbol a=Symbol('a',0);
  Symbol r=Symbol('r',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));

  Vector9 force;
  force.setConstant(0.1);
  
  Vector9 c_pts;
  c_pts.setConstant(-0.1);
  
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  ManifoldFixedAccBtFactor factor(f,r,W,R,factorNM,g_v,Vector6().setZero());
  
  Matrix actualH1, actualH2, actualH3, actualH4;
  cerr<<factor.evaluateError(force,c_pts,w_dynamics,r_mat,actualH1)<<endl;

  Vector actual = factor.evaluateError(force,c_pts,w_dynamics,r_mat,actualH1,actualH2,actualH3, actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;
  cout<<"Testing derivatives..."<<endl;
    
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldFixedAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldFixedAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldFixedAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
    Matrix numericalH4 = numericalDerivative44<Vector6,Vector9,Vector9,Vector7,Rot3>
    (boost::function<Vector(const Vector9&,
                            const Vector9&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldFixedAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;
  
  return true;
}

bool test_physics()
{
  cout<<"PhysicsFactor()"<<endl;
 
  Symbol W=Symbol('W',0);  
  Vector7 w_dynamics;
  w_dynamics <<1.1,0.001,0.003,0.002,0.015,0.010,0.005;

  //w_dynamics.segment(4,3) = Vector3(-0.3,-0.1,-0.8);
  //w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.05);
  //w_dynamics(0) = 0.01;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector7().setConstant(1e-8));
  PhysicsFactor factor(W,factorNM);
  cerr<<"computing error.."<<endl;
  Matrix actualH1;
  Vector actual = factor.evaluateError(w_dynamics,actualH1);
  cout<<"Factor error: "<<actual.transpose()<<endl;


  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative11<Vector7,Vector7>
    (boost::function<Vector(const Vector7&)>
     (boost::bind( &PhysicsFactor::evaluateError , factor , _1 , boost::none)), w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5))
  {
    cout<<"Passed"<<endl;
  }
  else
  {
    return false;
  }

}
/*
bool test_bt_acc()
{
  cout<<"AccBtFactor()"<<endl;
  Symbol a=Symbol('a',0);
  Symbol r=Symbol('r',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  

  Vector12 force;
  force.setConstant(0.1);
  
  Vector12 c_pts;
  c_pts.setConstant(-0.1);
  
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  
  Vector10 w_dynamics;
  w_dynamics.head(6) = Matrix61().Constant(1.0);
  w_dynamics.segment(6,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(9) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  AccBtFactor factor(a,f,r,W,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4;
  Vector actual = factor.evaluateError(acc,force,c_pts,w_dynamics,actualH1,actualH2,actualH3, actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;


  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5))
  {
    cout<<"Passed"<<endl;
  }
  else
  {
    return false;
  }


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
    Matrix numericalH4 = numericalDerivative44<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;

  return true;

}
*/
bool test_newton_bt_acc()
{
  cout<<"NewtonEulerBtFactor()"<<endl;

  Vector12 force;
  force.setConstant(0.1);
  
  Vector12 c_pts;
  c_pts.setConstant(-0.1);
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol a=Symbol('a',0);
  Symbol v=Symbol('v',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  
  Symbol r=Symbol('r',0);

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));

  Vector3 g_v(1.0,10.0,-0.8);
  Vector6 acc = (Matrix61()<<1,1,1,
                             1.0,1.0,1.0).finished();

  Vector6 vel = (Matrix61()<<-1,1,1,
                             -0.1,1.0,1.0).finished();
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));

  
  BtFactor factor(f,r,W,R,factorNM,g_v,acc,vel);
  
  Matrix actualH1, actualH2, actualH3, actualH4;
  Vector actual = factor.evaluateError(force,c_pts,w_dynamics,r_mat,actualH1,actualH2,actualH3,actualH4);//, actualH4,actualH5);
  cerr<<"Works!"<<endl;

  // gradients:
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
     (boost::bind( &BtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
     (boost::bind( &BtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
     (boost::bind( &BtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH3,actualH3,1E-5)) cout<<"Passed"<<endl;
  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative44<Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
     (boost::bind( &BtFactor::gradientTester , factor , _1 , _2 , _3 , _4)),force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH4,actualH4,1E-5)) cout<<"Passed"<<endl;

}
bool test_bt_param()
{
  cout<<"ParamBtFactor()"<<endl;

  Vector12 force;
  force.setConstant(0.1);
  
  Vector12 c_pts;
  c_pts.setConstant(-0.1);
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  //Symbol a=Symbol('a',0);
  //Symbol v=Symbol('v',0);
  //Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  
  //Symbol r=Symbol('r',0);

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));

  Vector3 g_v(1.0,10.0,-0.8);
  Vector6 acc = (Matrix61()<<1,1,1,
                             1.0,1.0,1.0).finished();

  Vector6 vel = (Matrix61()<<-1,1,1,
                             -0.1,1.0,1.0).finished();
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));

  
  ParamBtFactor factor(W,R,factorNM,g_v,acc,vel,force,c_pts);
  
  Matrix actualH1, actualH2;
  Vector actual = factor.evaluateError(w_dynamics,r_mat,actualH1,actualH2);//, actualH4,actualH5);
  cerr<<"Works!"<<endl;

  // gradients:
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative21<Vector6,Vector7,Rot3>
    (boost::function<Vector(const Vector7&, const Rot3&)>
     (boost::bind( &ParamBtFactor::gradientTester , factor , _1 , _2 )),w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;

  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative22<Vector6,Vector7,Rot3>
    (boost::function<Vector(const Vector7&, const Rot3&)>
     (boost::bind( &ParamBtFactor::gradientTester , factor , _1 , _2 )), w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;

}

int main (int argc, char** argv)
{
  test_sysid_expressions();
  //test_velocity();
  //test_bt_param();
  //test_ft_acc();
  //test_manifold_ft_acc();
  //test_manifold_bt_acc();
  //test_bt_acc();
  //test_manifold_bt();
  //test_physics();
  //test_newton_ft_acc();
  //test_newton_bt_acc();
  
  //test_qs_ft();
  //test_qs_bt();

  return 0;
}
