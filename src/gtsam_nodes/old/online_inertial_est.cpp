#include <ros/ros.h>
#include <fstream>

#include <sysid_manipulation/inertial_problem.hpp>
#include <sysid_manipulation/MeasurementData.h>

vector<Eigen::Vector3d> forces,cpts;
Eigen::Vector3d g_vec;
bool got_new_data=false;
void md_cb(sysid_manipulation::MeasurementData msg)
{
  forces.clear();
  cpts.clear();
  for(int i=0;i<msg.forces.size();++i)
  {
    forces.push_back(Eigen::Vector3d(msg.forces[i].wrench.force.x,
                                  msg.forces[i].wrench.force.y,
                                  msg.forces[i].wrench.force.z));
  }
  for(int i=0;i<msg.cpts.size();++i)
  {
    cpts.push_back(Eigen::Vector3d(msg.cpts[i].pose.position.x,
                                msg.cpts[i].pose.position.y,
                                msg.cpts[i].pose.position.z));
  }
    
  g_vec=Eigen::Vector3d(msg.g_vec.pose.position.x,
                        msg.g_vec.pose.position.y,
                        msg.g_vec.pose.position.z);
  got_new_data=true;
}
  
int main(int argc, char** argv)
{
  ros::init(argc,argv, "online_inertial_estimation");
  ros::NodeHandle nh;

  int t_step=0;
  int max_tstep=100;
  inertialEst inertial_est;
  ros::Rate loop_rate(1000);
  ros::Subscriber sub = nh.subscribe("/sysid/data", 10, md_cb);

  while(nh.ok())
  {
    if(got_new_data && t_step<max_tstep)
    {
      //cerr<<t_step<<endl;
      inertial_est.update_measurements(forces,g_vec,cpts,t_step);
      t_step++;
      got_new_data=false;
    }

    if(t_step>=max_tstep)
    {
      break;
    }
    ros::spinOnce();
    loop_rate.sleep();
 
  }
  
  ROS_INFO("Obtained Values, building graph..");
  
  inertial_est.build_graph(t_step);

  ROS_INFO("Initializing values..");
  
  Values initials=inertial_est.initialize_values(t_step);

  ROS_INFO("Optimizing..");
  
  Values results;
  inertial_est.optimize(initials,results);
  
  inertial_est.optimize(results,results);

  results.at(Symbol('M',0)).print("Optimized mass: ");
  results.at(Symbol('m',0)).print("Optimized mass (com): ");
    
  results.at(Symbol('C',0)).print("Optimized center of mass: ");
  ofstream os("inertial.dot");
  inertial_est.graph.saveGraph(os, results);

  //inertial_est.
  //results.print("Optimized Results\n");

  return 0;
  
}
