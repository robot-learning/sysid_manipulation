// this gtsam node takes prior on object pose and performs smoothing to get object position, velocity and acceleration estimates
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>

#include <sysid_manipulation/pose_problem_isam.hpp>
#include <sysid_manipulation/resetGraph.h>

//#include <pose_factors.h>
class poseClient
{
public:
  double old_time=0.0;
  
  bool got_new_data=false;
  bool reset_graph=false;
  ros::NodeHandle* nh;
  int t_step=0;
  int max_tstep=5000;
  ros::Publisher pub, pub_arr;
  ros::Subscriber sub;
  ros::ServiceServer service;
  geometry_msgs::PoseStamped pose_data;
  poseSmooth pose_fg;
  
  ros::Rate* loop_rate;

  bool init(ros::NodeHandle &n,const double &loop_rate_hz=60)
  {
    nh=&n;
    sub = nh->subscribe("/object/pose", 10, &poseClient::md_cb, this);
    service = nh->advertiseService("/pose_isam/reset_graph", &poseClient::graph_call, this);
    pose_fg.initialize_graph();
    loop_rate=new ros::Rate(loop_rate_hz);
  
    pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/smoothed_pose",1);
    pub_arr=nh->advertise<geometry_msgs::PoseArray>("/isam/pose_batch",1);
        
    // read robot kdl parameters:
    // build kdl model:
    bool success = true;

    
    return success;
    
  }

  ~poseClient()
  {
    delete loop_rate;
  }
  // msg cb:
  void md_cb(const geometry_msgs::PoseStamped &msg)
  {
    pose_data=msg;
    
    got_new_data=true;
  
  }
  bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
  {
    reset_graph=true;
    res.success=true;
    
    return true;
  }

  void run()
  {
    gtsam::Values current_estimate;

    //Eigen::Vector3d obj_g;
    Eigen::VectorXd obj_pose;
    double time_st;
    obj_pose.resize(7);
    vector<Eigen::Vector3d> ft_data;
    int batch_t=100;
    geometry_msgs::PoseStamped opt_pose;
    geometry_msgs::PoseArray opt_pose_arr;

    while(nh->ok())
    {
      //cerr<<"running fg"<<endl;
      if(reset_graph)
      {
        pose_fg.initialize_graph();
        t_step=0;
        reset_graph=false;
      }
      if(got_new_data && t_step<max_tstep)
      {
        // get factor data:
        obj_pose[0]=pose_data.pose.position.x;
        obj_pose[1]=pose_data.pose.position.y;
        obj_pose[2]=pose_data.pose.position.z;  
        obj_pose[3]=pose_data.pose.orientation.x;
        obj_pose[4]=pose_data.pose.orientation.y;
        obj_pose[5]=pose_data.pose.orientation.z; 
        obj_pose[6]=pose_data.pose.orientation.w;
        time_st=pose_data.header.stamp.toSec();
        pose_fg.update_graph(obj_pose,time_st);
        
        if(t_step%batch_t==0 && t_step>2)
        {
          double err=10.0;
          double err_b=10.0;
          pose_fg.optimize(current_estimate,err,err_b);
          //current_estimate.at(gtsam::Symbol('M',0)).print("Optimized mass: ");
          //current_estimate.at(gtsam::Symbol('m',0)).print("Optimized mass(com): ");        
          //current_estimate.at(gtsam::Symbol('x',t_step)).print("Optimized pose: ");
          //current_estimate.at(gtsam::Symbol('v',t_step)).print("Optimized velocity: ");
          gtsam::Vector6 vel=current_estimate.at<gtsam::Vector6>(gtsam::Symbol('v',t_step));
          //if(t_step%30==0)
          {
            cerr<<"Optimization error: "<<err<<endl;
            cerr<<"velocity: "<<vel.transpose()<<endl;
          }
          //gtsam::Vector3 com=current_estimate.at<gtsam::Point3>(gtsam::Symbol('C',0));
          //Vector3 com= com_v.cast<Vector3>();//.cast();
          // publish stream of poses:
          geometry_msgs::Pose pose;
          opt_pose_arr.poses.clear();

          for(int j=t_step-batch_t+1;j<t_step;++j)
          {
            int fg_t=j;
            gtsam::Pose3 o_pose=current_estimate.at<gtsam::Pose3>(gtsam::Symbol('x',fg_t));

            pose.position.x=o_pose.x();
            pose.position.y=o_pose.y();
            pose.position.z=o_pose.z();
            gtsam::Vector q=o_pose.rotation().quaternion();
            pose.orientation.w=q[0];
            pose.orientation.x=q[1];
            pose.orientation.y=q[2];
            pose.orientation.z=q[3];
            opt_pose_arr.poses.emplace_back(pose);
          }
          opt_pose.header.stamp=ros::Time::now();
          opt_pose.header.frame_id=pose_data.header.frame_id;
          opt_pose.pose=pose;
          
          opt_pose_arr.header.stamp=ros::Time::now();
          opt_pose_arr.header.frame_id=pose_data.header.frame_id;
          pub.publish(opt_pose);
          pub_arr.publish(opt_pose_arr);
        }
        t_step++;
        got_new_data=false;
      }
      if(t_step>=max_tstep)
      {
        break;
      }
      ros::spinOnce();
      loop_rate->sleep();

    }
  }
  
};


int main(int argc, char** argv)
{
  ros::init(argc,argv,"pose_gtsam");
  ros::NodeHandle nh;
  poseClient p_cl;
  
  p_cl.init(nh);

  ROS_INFO("Initialized");
  p_cl.run();
  
                                 
  //results.print("Optimized Results\n");

  return 0;
  
}

