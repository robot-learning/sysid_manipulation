// this gtsam node takes prior on object pose and performs smoothing to get object position, velocity and acceleration estimates
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>

#include <sysid_manipulation/ft_problem.hpp>
#include <sysid_manipulation/resetGraph.h>
#include <sysid_manipulation/FullMeasurementData.h>

//#include <pose_factors.h>
class poseClient
{
public:
  double old_time=0.0;
  
  bool got_new_data=false;
  bool reset_graph=false;
  ros::NodeHandle* nh;
  int t_step=0;
  int max_tstep=50000;
  ros::Publisher pub, pub_arr,com_pub;
  ros::Subscriber sub;
  ros::ServiceServer service;
  geometry_msgs::PoseStamped pose_data;
  poseSmooth pose_fg;
  vector<gtsam::Vector> ft_data;
  gtsam::Point3 obj_g;
  ros::Rate* loop_rate;
  gtsam::Pose3 o_T_Q;
  bool init(ros::NodeHandle &n,const double &loop_rate_hz=100)
  {
    nh=&n;
    sub = nh->subscribe("/sysid/ft_data", 10, &poseClient::md_cb, this);
    service = nh->advertiseService("/pose_isam/reset_graph", &poseClient::graph_call, this);
    pose_fg.initialize_graph();
    loop_rate=new ros::Rate(loop_rate_hz);
  
    pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/smoothed_pose",1);
    com_pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/com",1);

    pub_arr=nh->advertise<geometry_msgs::PoseArray>("/isam/pose_batch",1);
        
    // read robot kdl parameters:
    // build kdl model:
    bool success = true;

    
    return success;
    
  }

  ~poseClient()
  {
    delete loop_rate;
  }
  // msg cb:

  gtsam::Pose3 pose_cvt(const geometry_msgs::Pose &in_pose)
  {
    gtsam::Pose3 pose_gtsam(gtsam::Rot3(gtsam::Quaternion(in_pose.orientation.w, in_pose.orientation.x,
                                                          in_pose.orientation.y, in_pose.orientation.z)),
                            gtsam::Point3(in_pose.position.x,in_pose.position.y,in_pose.position.z));
    return pose_gtsam;
  }
  void md_cb(const sysid_manipulation::FullMeasurementData &msg)
  {
    // object pose:

    // get matrix from pose:
    // get w_T_palm
    gtsam::Pose3 w_palm=pose_cvt(msg.base_T_palm.pose);
    // get palm_T_obj
    gtsam::Pose3 palm_obj=pose_cvt(msg.palm_T_obj.pose);
    

    // get object pose
    gtsam::Pose3 palm_ft=pose_cvt(msg.palm_T_ft.pose);

    
    // transform force. torque to object frame (obj_T_ft):
    gtsam::Pose3 obj_ft = palm_obj.inverse() * palm_ft;

    // create a new frame~'Q' at 'ft frame' origin with axes aligned with 'obj' frame:
    gtsam::Pose3 ft_T_Q(obj_ft.rotation().inverse(),gtsam::Point3(0,0,0));

    gtsam::Pose3 obj_T_Q(gtsam::Rot3(),obj_ft.translation());
    o_T_Q=obj_T_Q;
    gtsam::Pose3 w_obj=w_palm * palm_obj;

    gtsam::Pose3 w_Q=w_palm * palm_obj * obj_T_Q;
        
    // object pose:
    pose_data.pose.position.x=w_Q.x();
    pose_data.pose.position.y=w_Q.y();
    pose_data.pose.position.z=w_Q.z();
    pose_data.header.stamp=msg.base_gvec.header.stamp;
    pose_data.header.frame_id=msg.base_T_palm.header.frame_id;
    gtsam::Vector q=w_Q.rotation().quaternion();
    pose_data.pose.orientation.w=q[0];
    pose_data.pose.orientation.x=q[1];
    pose_data.pose.orientation.y=q[2];
    pose_data.pose.orientation.z=q[3];
    

    // get force, torque sensed by force torque sensor
    
    // Transform all measurements to 'Q'frame:
    gtsam::Point3 ft_force(msg.ft_force.wrench.force.x, msg.ft_force.wrench.force.y ,msg.ft_force.wrench.force.z);
    gtsam::Point3 ft_torque(msg.ft_force.wrench.torque.x, msg.ft_force.wrench.torque.y ,msg.ft_force.wrench.torque.z);
    
    ft_force = ft_T_Q.inverse().rotation() * ft_force;
    ft_torque = ft_T_Q.inverse().rotation() * ft_torque; 
    //ft_force = obj_ft.rotation() * ft_force;
    //ft_torque = obj_ft.rotation() * ft_torque; 

    // get gravity:
    gtsam::Point3 w_g(msg.base_gvec.wrench.force.x, msg.base_gvec.wrench.force.y, msg.base_gvec.wrench.force.z);
    // we transform gravity to Q frame:
    obj_g = obj_T_Q.rotation().inverse() * w_obj.rotation().inverse() * w_g;
    
    //
    
    
    // get contact point of ft, w.r.t. object (r_c=obj_T_ft position)
    gtsam::Point3 r_ft = ft_T_Q.inverse().translation();//(obj_ft.x(),obj_ft.y(), obj_ft.z());
    
    
    ft_data.resize(3);
    ft_data[0]=r_ft;
    ft_data[1]=ft_force;
    ft_data[2]=ft_torque;

    
    got_new_data=true;
  
  }
  bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
  {
    reset_graph=true;
    res.success=true;
    
    return true;
  }

  void run()
  {
    gtsam::Values current_estimate;

    //Eigen::Vector3d obj_g;
    Eigen::VectorXd obj_pose;
    double time_st;
    obj_pose.resize(7);
    //vector<Eigen::Vector3d> ft_data;
    int batch_t=10;
    geometry_msgs::PoseStamped opt_pose;
    geometry_msgs::PoseArray opt_pose_arr;

    while(nh->ok())
    {
      //cerr<<"running fg"<<endl;
      if(reset_graph)
      {
        pose_fg.initialize_graph();
        t_step=0;
        reset_graph=false;
      }
      if(got_new_data && t_step<max_tstep)
      {
        // get factor data:
        obj_pose[0]=pose_data.pose.position.x;
        obj_pose[1]=pose_data.pose.position.y;
        obj_pose[2]=pose_data.pose.position.z;  
        obj_pose[3]=pose_data.pose.orientation.x;
        obj_pose[4]=pose_data.pose.orientation.y;
        obj_pose[5]=pose_data.pose.orientation.z; 
        obj_pose[6]=pose_data.pose.orientation.w;
        time_st=pose_data.header.stamp.toSec();
        pose_fg.update_graph(obj_pose,ft_data,time_st,obj_g);
        
        if(t_step%batch_t==0 && t_step>2)
        {
          double err=10.0;
          double err_b=10.0;
          pose_fg.optimize(current_estimate,err,err_b);
          //current_estimate.at(gtsam::Symbol('M',0)).print("Optimized mass: ");
          //current_estimate.at(gtsam::Symbol('m',0)).print("Optimized mass(com): ");        
          //current_estimate.at(gtsam::Symbol('x',t_step)).print("Optimized pose: ");
          //current_estimate.at(gtsam::Symbol('v',t_step)).print("Optimized velocity: ");
          
          //if(t_step%30==0)
          //{
            gtsam::Vector6 vel=current_estimate.at<gtsam::Vector6>(gtsam::Symbol('a',pose_fg.t_steps_-1));
            gtsam::Vector7 W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));
            gtsam::Rot3 R=current_estimate.at<gtsam::Rot3>(gtsam::Symbol('R',pose_fg.l_step));
                    
            gtsam::Vector3 com=W.segment(1,3);
            
            gtsam::Vector3 I=W.tail(3);
          
            gtsam::Vector1 mass=W.head(1);//current_estimate.at<gtsam::Vector1>(gtsam::Symbol('M',0));

            cerr<<"Optimization error: "<<err<<endl;
            //cerr<<"Acceleration: "<<vel.transpose()<<endl;
            cerr<<"Mass: "<<mass.transpose()<<endl;
            cerr<<"Center of Mass: "<< (o_T_Q * com).transpose()<<endl;

            gtsam::Matrix33 inertia_diag = (gtsam::Matrix33() <<
                                     W[4+1] + W[4+2],0,0,
                                     0,W[4+0] + W[4+2],0,
                                     0,0,W[4+0]+ W[4+1]).finished();

            gtsam::Matrix33 inertia_matrix = R.matrix() * inertia_diag * R.matrix().transpose() ;// * inertia_diag ;//* 
            cerr<<"Inertia mat: " <<endl<<inertia_matrix<<endl;
            
            //}
          //gtsam::Vector3 com=current_estimate.at<gtsam::Point3>(gtsam::Symbol('C',0));
          //Vector3 com= com_v.cast<Vector3>();//.cast();
          // publish stream of poses:
          geometry_msgs::Pose pose;
          gtsam::Pose3 o_pose;
          opt_pose_arr.poses.clear();

          for(int j=t_step-batch_t+1;j<t_step;++j)
          {
            int fg_t=j;
            o_pose = current_estimate.at<gtsam::Pose3>(gtsam::Symbol('x',fg_t));

            pose.position.x=o_pose.x();
            pose.position.y=o_pose.y();
            pose.position.z=o_pose.z();
            gtsam::Vector q=o_pose.rotation().quaternion();
            pose.orientation.w=q[0];
            pose.orientation.x=q[1];
            pose.orientation.y=q[2];
            pose.orientation.z=q[3];
            //opt_pose_arr.poses.emplace_back(pose);
          }
          opt_pose.header.stamp=ros::Time::now();
          opt_pose.header.frame_id=pose_data.header.frame_id;
          opt_pose.pose=pose;
          pub.publish(opt_pose);

          // transform com to world frame and publish:
          gtsam::Point3 com_pt = com;
          //cerr<< gtsam::Pose3(gtsam::Rot3::identity(),com_pt)<<endl;
          //gtsam::Pose3 w_com = o_pose * gtsam::Pose3(gtsam::Rot3::identity(),com_pt);
          gtsam::Pose3 w_com = o_T_Q * gtsam::Pose3(gtsam::Rot3::identity(),com_pt);

          //opt_pose_arr.header.stamp=ros::Time::now();
          //opt_pose_arr.header.frame_id=pose_data.header.frame_id;
          opt_pose.header.frame_id = "obj_link";
          opt_pose.pose.position.x = w_com.x();
          opt_pose.pose.position.y = w_com.y();
          opt_pose.pose.position.z = w_com.z();

          opt_pose.pose.orientation.x = 0;
          opt_pose.pose.orientation.y = 0;
          opt_pose.pose.orientation.z = 0;
          opt_pose.pose.orientation.w = 1;
          com_pub.publish(opt_pose);
        }
        t_step++;
        got_new_data=false;
      }
      if(t_step>=max_tstep)
      {
        break;
      }
      ros::spinOnce();
      loop_rate->sleep();

    }
  }
  
};


int main(int argc, char** argv)
{
  ros::init(argc,argv,"pose_gtsam");
  ros::NodeHandle nh;
  poseClient p_cl;
  
  p_cl.init(nh);

  ROS_INFO("Initialized");
  p_cl.run();
  
                                 
  //results.print("Optimized Results\n");

  return 0;
  
}

