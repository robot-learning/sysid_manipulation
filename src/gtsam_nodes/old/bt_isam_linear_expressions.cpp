// this gtsam node takes prior on object pose and performs smoothing to get object position, velocity and acceleration estimates
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>

#include <sysid_manipulation/bt_problem_expressions.hpp>
#include <sysid_manipulation/resetGraph.h>
#include <sysid_manipulation/toggleInference.h>

#include <sysid_manipulation/FullMeasurementData.h>

#include <sysid_manipulation/data_utils.hpp>
#include <tf/transform_broadcaster.h>

#include <visualization_msgs/MarkerArray.h>
#include "gtsam_factors/sysid_expressions.h"

using namespace gtsam;
using namespace std;

//#include "gtsam_factors/sysid_factors.h"

//#include <pose_factors.h>
class poseClient
{
public:
  double old_time=0.0;

  vector<sysid_manipulation::sysid_data> data_arr;
  sysid_manipulation::sysid_data t_data;

  bool got_new_data=false;
  bool reset_graph=false;
  ros::NodeHandle* nh;
  int t_step=0;
  int max_tstep=5000;
  ros::Publisher pub, pub_arr,com_pub, marker_pub;
  ros::Subscriber sub;
  ros::ServiceServer service,service_1;
  geometry_msgs::PoseStamped pose_data;
  poseSmooth pose_fg;
  vector<gtsam::Vector> ft_data, bt_data;
  gtsam::Point3 obj_g;
  ros::Rate* loop_rate;
  ros::Rate* pub_loop_rate;
  //gtsam::Pose3 o_T_Q;
  gtsam::Point3 offset_pt;

  bool collect_data = true;
  bool infer_pose = false;
  bool infer_dynamics = false;

  int batch_t=5; 

  bool init(ros::NodeHandle &n,const double &loop_rate_hz=100)
  {
    
    nh=&n;
    sub = nh->subscribe("/sysid/bt_data", 1000, &poseClient::md_cb, this);
    service = nh->advertiseService("/pose_isam/reset_graph", &poseClient::graph_call, this);
    service_1 = nh->advertiseService("/sysid/toggle_graph", &poseClient::node_call, this);
    marker_pub=nh->advertise<visualization_msgs::MarkerArray>("/isam/forces", 1);


    pose_fg.initialize_graph();
    loop_rate=new ros::Rate(loop_rate_hz);
    pub_loop_rate=new ros::Rate(10);
  
    pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/smoothed_pose",1);
    com_pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/com",1);

    pub_arr=nh->advertise<geometry_msgs::PoseArray>("/isam/pose_batch",1);
        
    // read robot kdl parameters:
    // build kdl model:
    bool success = true;

    
    return success;
    
  }

  ~poseClient()
  {
    delete loop_rate;
    delete pub_loop_rate;
  }
  // msg cb:
  gtsam::Pose3 pose_cvt(const geometry_msgs::Pose &in_pose)
  {
    gtsam::Pose3 pose_gtsam(gtsam::Rot3(gtsam::Quaternion(in_pose.orientation.w, in_pose.orientation.x,
                                                          in_pose.orientation.y, in_pose.orientation.z)),
                            gtsam::Point3(in_pose.position.x,in_pose.position.y,in_pose.position.z));
    return pose_gtsam;
  }
  void md_cb(const sysid_manipulation::FullMeasurementData &msg)
  {
    //sysid_manipulation::sysid_data t_data;
    
    // object pose:

    // get matrix from pose:
    // get w_T_palm
    gtsam::Pose3 w_palm=pose_cvt(msg.base_T_palm.pose);
    // get palm_T_obj
    gtsam::Pose3 palm_obj=pose_cvt(msg.palm_T_obj.pose);
    

    // get object pose
    gtsam::Pose3 palm_ft = pose_cvt(msg.palm_T_ft.pose);

    gtsam::Pose3 w_obj= (w_palm) * palm_obj ;


    
    //gtsam::Pose3 O_T_Q = w_obj.inverse() *  pose_cvt(msg.base_T_palm.pose) * pose_cvt(msg.palm_T_ftips[0].pose) * pose_cvt(msg.ftips_T_cpts[0].pose);
    

    //gtsam::Pose3 w_Q= (w_palm) * palm_obj * O_T_Q ;

    // object pose:
    //gtsam::Pose3 w_obj_pose  (gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
    //gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));

    pose_data.pose.position.x=w_obj.x();
    pose_data.pose.position.y=w_obj.y();
    pose_data.pose.position.z=w_obj.z();
    pose_data.header.stamp=msg.base_gvec.header.stamp;
    pose_data.header.frame_id=msg.base_T_palm.header.frame_id;
    gtsam::Vector q=w_obj.rotation().quaternion();
    pose_data.pose.orientation.w=q[0];
    pose_data.pose.orientation.x=q[1];
    pose_data.pose.orientation.y=q[2];
    pose_data.pose.orientation.z=q[3];
    
    // get gravity:
    gtsam::Point3 w_g(msg.base_gvec.wrench.force.x, msg.base_gvec.wrench.force.y, msg.base_gvec.wrench.force.z);
    
    obj_g =w_obj.rotation().inverse() * w_g;


    // get force, torque sensed by force torque sensor
    
    // transform force. torque to object frame (obj_T_ft):

    /*
    gtsam::Pose3 obj_ft = palm_obj.inverse() * palm_ft;

    // create a new frame~'Q' at 'ft frame' origin with axes aligned with 'obj' frame:
    //gtsam::Pose3 ft_T_Q(obj_ft.rotation().inverse(),gtsam::Point3(0,0,0));
    //gtsam::Pose3 obj_T_Q(gtsam::Rot3(),obj_ft.translation());
    o_T_Q=O_T_Q;

    gtsam::Point3 ft_force(msg.ft_force.wrench.force.x, msg.ft_force.wrench.force.y ,msg.ft_force.wrench.force.z);
    gtsam::Point3 ft_torque(msg.ft_force.wrench.torque.x, msg.ft_force.wrench.torque.y ,msg.ft_force.wrench.torque.z);
    
    //ft_force = ft_T_Q.inverse().rotation() * ft_force;
    //ft_torque = ft_T_Q.inverse().rotation() * ft_torque; 

    ft_force = obj_ft.rotation() * ft_force;
    ft_torque = obj_ft.rotation() * ft_torque; 
    // get contact point of ft, w.r.t. object (r_c=obj_T_ft position)
    //gtsam::Point3 r_ft(obj_ft.x(),obj_ft.y(), obj_ft.z());
    
    
    ft_data.resize(3);
    ft_data[0]=r_ft;
    ft_data[1]=ft_force;
    ft_data[2]=ft_torque;

    */

    bt_data.resize(4*3); // [contact point , force, force acc] 
    // get biotac data:
    // forces, contact points w.r.t. object:
    for(int i=0;i<4;++i)
    {
      // read ftip force:
      gtsam::Point3 bt_force(msg.cpt_forces[i].wrench.force.x,
                             msg.cpt_forces[i].wrench.force.y ,
                             msg.cpt_forces[i].wrench.force.z);//; // force in biotac frame

      gtsam::Pose3 w_cpts = pose_cvt(msg.base_T_palm.pose) * pose_cvt(msg.palm_T_ftips[i].pose) *
        pose_cvt(msg.ftips_T_cpts[i].pose);//();
      
      gtsam::Point3 o_bt_force= w_obj.rotation().inverse() * w_cpts.rotation() * bt_force;
      // contact point:
      gtsam::Point3 o_bt_cpt = ( w_obj.inverse() * w_cpts).translation();

      // force uncertainty: // TODO
      gtsam::Point3 bt_acc(0,0,0);//; // force in biotac frame;//();

      /*
      gtsam::Point3 bt_acc(sqrt(msg.cpt_forces[i].wrench.torque.x),
                           sqrt(msg.cpt_forces[i].wrench.torque.y) ,
                           sqrt(msg.cpt_forces[i].wrench.torque.z));//; // force in biotac frame;//();
      */
      gtsam::Point3 o_bt_force_acc = w_obj.rotation().inverse() * bt_acc;
      bt_data[i*3] = o_bt_cpt;
      bt_data[i*3+1] = o_bt_force;
      bt_data[i*3+2] = o_bt_force_acc;
      //cerr<<o_bt_force<<endl;

      
    }

    /*
    offset_pt = bt_data[3*3];

    bt_data[3*3+1] = gtsam::Point3(0,0,0);// making force to be zero

    // add offset to object frame:
    
    pose_data.pose.position.x -=offset_pt.x();
    pose_data.pose.position.y -=offset_pt.y();
    pose_data.pose.position.z -=offset_pt.z();
    
    for(int i =0; i<4; ++i)
    {
      bt_data[i*3] = bt_data[i*3] - offset_pt;
    }
    */

    // store data:

    t_data.t_step = msg.base_gvec.header.stamp.toSec();

    t_data.g = w_g;
    t_data.bt_data = bt_data ;
    t_data.obj_pose = w_obj;

    if(data_arr.size() == 0)
    {
      data_arr.emplace_back(t_data);
      got_new_data=true;
    }
    else
    {
      if(t_data.t_step > data_arr[data_arr.size()].t_step)
      {
        data_arr.emplace_back(t_data);
        got_new_data=true;
      }
      else
      {
        cerr<<"received old data"<<endl;

      }
    }
  
  }
  bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
  {
    reset_graph=true;
    res.success=true;
    
    return true;
  }

  // toggle data reading

  bool node_call(sysid_manipulation::toggleInference::Request &req,sysid_manipulation::toggleInference::Response &res)
  {
    if(req.collect_data)
    {
      //start reading data
      collect_data = true;
    }
    if(req.infer_pose)
    {
      // stop reading data & process pose
      ROS_INFO("Inferring Pose");
      collect_data = false;
      infer_pose = true;
    }
    if(req.infer_dynamics)
    {
      // perform dynamics estimation
      collect_data = false;
      optimize_dynamics();
    }

    if(req.reset_data)
    {
      reset();
    }

    res.status = true;
    return true;
    
  }

  // publish data:

  bool publish_data()
  {
    // publish old pose

    // publish pose

    // publish com

  }
  


    //cerr<<data_arr.size()<<endl;
  visualization_msgs::Marker get_force_line(gtsam::Vector3 contact_pt, gtsam::Vector3 force, vector<double> color = {1,0.1,0.1,1.0})
  {
    visualization_msgs::Marker marker;
    //marker.header.frame_id = base_frame_;
    marker.ns = "basic_shapes";
    marker.type = 0;
    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;
    
    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    // compute quaternion from rpy:
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.01;
    marker.scale.y = 0.02;
    marker.scale.z = 0.0;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = color[3];

    
    marker.lifetime = ros::Duration();


    // points:
    geometry_msgs::Point pt;
    pt.x=contact_pt[0];
    pt.y=contact_pt[1];
    pt.z=contact_pt[2];
    marker.points.emplace_back(pt);

    force = contact_pt + force;
    pt.x=force[0];
    pt.y=force[1];
    pt.z=force[2];
    marker.points.emplace_back(pt);
    
    //Vect
    //marker.points.emplace_back(geometry_msgs::Point(contact_pt[0],contact_pt[1],contact_pt[2]));

    return marker;
 
  }
  
  bool visualize_forces(const sysid_manipulation::sysid_data &msg )
  {
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(msg.obj_pose.x(),msg.obj_pose.y(), msg.obj_pose.z()) );
    gtsam::Vector q_gt=msg.obj_pose.rotation().quaternion();
    tf::Quaternion q(q_gt[1],q_gt[2],q_gt[3],q_gt[0]);
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(),"lbr4_base_link","object"));
    
    // visualize gravity vector from origin:
    visualization_msgs::MarkerArray m_ar;
    gtsam::Vector3 contact_pt,force;
    
    contact_pt = msg.bt_data[0];
    force = msg.bt_data[1];
    
    visualization_msgs::Marker marker = get_force_line(contact_pt, force);
    marker.header.frame_id = "object";
    marker.id = 0; // 

    m_ar.markers.push_back(marker);

    contact_pt = msg.bt_data[1*3];
    force = msg.bt_data[1*3+1];    
    marker = get_force_line(contact_pt, force);
    marker.id = 1; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    
    contact_pt = msg.bt_data[2*3];
    force = msg.bt_data[2*3+1];    
    marker = get_force_line(contact_pt, force);
    marker.id = 2; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    
    contact_pt = msg.bt_data[3*3];
    force = msg.bt_data[3*3+1];

    marker = get_force_line(contact_pt, force);
    marker.id = 3; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    contact_pt.setZero();
    //cerr<<msg.g<<endl;
    //force = msg.obj_pose.rotation().inverse() * msg.g;
    force = msg.obj_pose.rotation().inverse() * msg.g * 0.1;
    vector<double> col = {1.0,1.0,0,1};
    marker = get_force_line(contact_pt, force,col);
    marker.id = 4; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    
    //marker_pub.publish(m_ar);
    return true;
  }
  bool visualize_pose(const gtsam::Pose3 &o_pose,const string &frame_id)
  {
    // publish pose

    geometry_msgs::PoseStamped pose_st;
    pose_st.pose.position.x=o_pose.x();
    pose_st.pose.position.y=o_pose.y();
    pose_st.pose.position.z=o_pose.z();
    gtsam::Vector q=o_pose.rotation().quaternion();
    pose_st.pose.orientation.w=q[0];
    pose_st.pose.orientation.x=q[1];
    pose_st.pose.orientation.y=q[2];
    pose_st.pose.orientation.z=q[3];

    pose_st.header.stamp=ros::Time::now();
    pose_st.header.frame_id=frame_id;
    pub.publish(pose_st);
              
    return true;
  }
  
  bool optimize_dynamics()
  {
    cerr<<data_arr.size()<<endl;
    gtsam::Values current_estimate;
    double err=10.0;
    double err_b=10.0;

 
    // reset graph
    reset();
    Vector3 g;
    g<< 0,0,-9.8;
    
    Vector7 w;
    w<< 1.3, 0.1,0.1,0.1, 0.25,0.1,0.1;

    Rot3 R_t_;
    int pre_step = 0;
    // optimize pose from data array:
    for(int i=pre_step;i<data_arr.size(); ++i)
    {
      // add data to graph
      //pose_fg.update_pose_graph(data_arr[i].obj_pose,data_arr[i].t_step);
      //pose_fg.update_param_graph(data_arr[i-1].obj_pose,data_arr[i].obj_vel,data_arr[i].obj_acc,data_arr[i-1].ft_data,data_arr[i-1].bt_data,data_arr[i-1].g);

      // print error:
      //cout<<data_arr[i].g.transpose()<<endl;
      pose_fg.update_pose_dynamics_graph(
                                      data_arr[i].obj_pose,
                                      data_arr[i].bt_data,
                                      data_arr[i].g,
                                      data_arr[i].t_step);
      /*
      pose_fg.update_dynamics_param_graph(
                                          data_arr[i].obj_pose,
                                          data_arr[i-1].obj_pose,
                                          data_arr[i-2].obj_pose,
                                          data_arr[i].bt_data,
                                          data_arr[i].g,
                                          data_arr[i].t_step);
      */
      
      //cerr<<"Error: "<<retVal.lpNorm<1>()<<endl;
      //cerr<<retVal.transpose()<<endl;

      //cout<<data_arr[i].g.transpose()<<endl;
      //return true;
      // ISAM update
      continue;
      if((i-pre_step)%batch_t == 0 && ((i-pre_step)>0))
      {
        //cerr<<i<<endl;
        //pose_fg.optimize();
        //pose_fg.debug_jacobian();
        pose_fg.optimize(current_estimate,err,err_b);
        
        cerr<<"Optimization error: "<<err<<endl;

        
        gtsam::Vector7 W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));
        gtsam::Vector3 R=current_estimate.at<gtsam::Vector3>(gtsam::Symbol('R',pose_fg.l_step));
        
        gtsam::Vector3 com=W.segment(1,3);
        
        
        gtsam::Vector1 mass=W.head(1);//current_estimate.at<gtsam::Vector1>(gtsam::Symbol('M',0));
        
        //cerr<<"Optimization error: "<<err<<endl;
        //cerr<<"Acceleration: "<<vel.transpose()<<endl;
        cerr<<"Mass: "<<mass.transpose()<<endl;
        cerr<<"Center of Mass: "<< (com).transpose()/mass[0]<<endl;

        gtsam::Matrix33 inertia_matrix = sysid_expr::inertia_linear(W,R);

        cerr<<"Inertia"<<inertia_matrix<<endl;
        
      }
      
    }
    //return true;
    cerr<<"######  Final Optimizing...."<<endl;
    // guass newton optimization:
    pose_fg.optimize(current_estimate,err,err_b);
    //pose_fg.get_marginals(current_estimate);


    
        
    gtsam::Vector7 W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));
    gtsam::Vector3 R=current_estimate.at<gtsam::Vector3>(gtsam::Symbol('R',pose_fg.l_step));
        
    gtsam::Vector3 com=W.segment(1,3);
    
    
    gtsam::Vector1 mass=W.head(1);//current_estimate.at<gtsam::Vector1>(gtsam::Symbol('M',0));
        
    //cerr<<"Optimization error: "<<err<<endl;
    //cerr<<"Acceleration: "<<vel.transpose()<<endl;
    cerr<<"Mass: "<<mass.transpose()<<endl;
    cerr<<"Center of Mass: "<< (com).transpose()/mass(0)<<endl;

    gtsam::Matrix33 inertia_matrix = sysid_expr::inertia_linear(W,R);
    
    cerr<<"Inertia"<<inertia_matrix<<endl;
    cerr<<"Optimization error: "<<err<<endl;
    
    return true;
    gtsam::Vector6 error;
    error.setZero();
    // publish optimized values:
    gtsam::Symbol a=gtsam::Symbol('a',0);
    gtsam::Symbol v=gtsam::Symbol('v',0);
    gtsam::Symbol f=gtsam::Symbol('f',0);
    gtsam::Symbol W_t=gtsam::Symbol('W',0);  
    gtsam::Symbol R_t=gtsam::Symbol('R',0);  
    gtsam::Symbol r=gtsam::Symbol('r',0);

    gtsam::noiseModel::Diagonal::shared_ptr factorNM = gtsam::noiseModel::Diagonal::Sigmas(gtsam::Vector6().setConstant(1e-8));
    gtsam::Vector7 w_dynamics;
    w_dynamics.tail(3) = (gtsam::Matrix31()<<0.3,0.5,0.9).finished();
    w_dynamics.segment(1,3) = gtsam::Vector3(0.3,0.1,0.8);
    w_dynamics(0) = 0.1;
    gtsam::Rot3 r_mat(gtsam::Rot3::Ypr(1,1,1));

    vector<sysid_manipulation::sysid_data> opt_data = data_arr;
    vector<sysid_manipulation::sysid_data> sim_data = data_arr;

    //return true;
    for(int i=0;i<data_arr.size();++i)
    {
      opt_data[i].obj_pose = current_estimate.at<gtsam::Pose3>(gtsam::Symbol('x',i));

      // get force data
      if(i>1)
      {
        sim_data[i].obj_pose = pose_fg.forward_sim_pose(sim_data[i-2].obj_pose,
                                                        sim_data[i-1].obj_pose,
                                                        sim_data[i].bt_data, sim_data[i].g,
                                                        sim_data[i].t_step-sim_data[i-1].t_step,W,R);
      }
      else if(i>0)
      {
        sim_data[i].obj_pose = pose_fg.forward_sim_pose(sim_data[i-1].obj_pose,
                                                        sim_data[i-1].obj_pose,
                                                        sim_data[i].bt_data, sim_data[i].g,
                                                        sim_data[i].t_step-sim_data[i-1].t_step,W,R);
      }
      else
      {
        sim_data[i].obj_pose = pose_fg.forward_sim_pose(sim_data[i].obj_pose,
                                                        sim_data[i].obj_pose,
                                                        sim_data[i].bt_data, sim_data[i].g,
                                                        sim_data[i+1].t_step-sim_data[i].t_step,W,R);
      }
      visualize_pose(sim_data[i].obj_pose,pose_data.header.frame_id);
      visualize_forces(data_arr[i]);
      pub_loop_rate->sleep();

    }
    
  }

  bool reset()
  {
    pose_fg.initialize_graph();
    t_step=0;
    reset_graph=false;
   
  }
  void run()
  {
    while(nh->ok())
    {
      ros::spinOnce();
      loop_rate->sleep();
    }
  }
  
};


int main(int argc, char** argv)
{
  ros::init(argc,argv,"pose_gtsam");
  ros::NodeHandle nh;
  poseClient p_cl;
  
  p_cl.init(nh);

  ROS_INFO("Initialized");
  p_cl.run();
  
                                 
  //results.print("Optimized Results\n");

  return 0;
  
}

