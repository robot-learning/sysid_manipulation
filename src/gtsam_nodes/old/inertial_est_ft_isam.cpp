// This factor graph node takes input from force torque sensor and estimates load inertial 
#include <ros/ros.h>
#include <fstream>

#include <sysid_manipulation/inertial_problem_isam_ft.hpp>
#include <sysid_manipulation/FtData.h>
#include <sysid_manipulation/resetGraph.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Wrench.h>
#include <sensor_msgs/JointState.h>


#include <ll4ma_opt_utils/plan_env_utils.hpp>


class inertialClient
{
public:
  sensor_msgs::JointState robot_state;
  geometry_msgs::Wrench ft_reading;
  double old_time=0.0;
  
  Eigen::Vector3d g_vec;
  bool got_new_data=false;
  bool reset_graph=false;
  ros::NodeHandle* nh;
  int t_step=0;
  int max_tstep=5000;
  inertialEst inertial_est;
  ros::Publisher pub;
  ros::Subscriber sub;
  ros::ServiceServer service;
  // create subscribers
  //ll4ma_opt_utils::envUtils robotEnv;
  manipulator_kdl::robotKDL robot_kdl_;

  ros::Rate* loop_rate;

  bool init(ros::NodeHandle &n,const double &loop_rate_hz=30)
  {
    nh=&n;
    sub = nh->subscribe("/sysid/data", 10, &inertialClient::md_cb, this);
    service = nh->advertiseService("/sysid/reset_graph", &inertialClient::graph_call, this);
    inertial_est.initialize_graph();
    ros::Rate loop_r(loop_rate_hz);
    *loop_rate=ros::Rate(loop_rate_hz);
  
    pub=nh->advertise<geometry_msgs::PoseStamped>("/sysid/com",1);
    // read robot kdl parameters:
    // build kdl model:
    bool success = true;
    string urdf_param;

    vector<string > base_names,ee_names,j_names,robot_link_names;
    
    success &= nh->getParam("urdf_param",urdf_param);

    success &= nh->getParam("base_frames",base_names);
    //base_frame_=base_names[0];
    success &= nh->getParam("ee_frames",ee_names);
    success &= nh->getParam("joint_names",j_names);
    success &= nh->getParam("robot_coll_links",robot_link_names);
    // store indexing of collision links and contact links:
    // we store collision checking links first and then contact links;
    

    vector<double> g_vec={0.0,0.0,-9.8};
    robot_kdl_=manipulator_kdl::robotKDL(urdf_param,*nh,base_names,ee_names,robot_link_names,g_vec);
    
    robot_kdl_.getJointLimits(j_names,robot_kdl_.up_bounds,robot_kdl_.low_bounds);
    success &= robot_kdl_.updateJointStateNames(j_names);
    success &= robot_kdl_.generateChainJointidx();
    
    return success;
    
  }
  
    
  // get conversion function to convert measurement data to factor graph data
  bool get_factor_data(const sensor_msgs::JointState &js, const geometry_msgs::Wrench &ft, Eigen::Vector3d &obj_g, vector<Eigen::Vector3d> &ft_data)
  {
    // get object pose w.r.t robot base
    vector<double> v=js.position;
    Eigen::Map <Eigen::VectorXd> j_arr(&v[0],v.size());

    // get g_vec w.r.t. object

    Eigen::MatrixXd b_T_o;
    robot_kdl_.getFK(0,j_arr,b_T_o);
    obj_g=b_T_o.inverse().block(3,3,3,3)*g_vec;
    
    // get ft sensor pose w.r.t. object
    Eigen::MatrixXd b_T_ft;
    robot_kdl_.getFK(0,j_arr,b_T_ft);
    
    // get ft reading w.r.t. object
    
  }
  
  // msg cb:
  void md_cb(sysid_manipulation::FtData msg)
  {
    robot_state=msg.robot_js;
    ft_reading=msg.ft_force.wrench;
    got_new_data=true;
  
  }
  bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
  {
    reset_graph=true;
    res.success=true;
    
    return true;
  }

  void run()
  {
    gtsam::Values current_estimate;

    Eigen::Vector3d obj_g;
    vector<Eigen::Vector3d> ft_data;
    while(nh->ok())
    {
      geometry_msgs::PoseStamped com_data;
      if(reset_graph)
      {
        inertial_est.initialize_graph();
        t_step=0;
        reset_graph=false;
      }
      if(got_new_data && t_step<max_tstep)
      {
        // get factor data:
        get_factor_data(robot_state,ft_reading,obj_g,ft_data);
        //inertial_est.update_graph(obj_g,ft_data);
        if(t_step%15==0)
        {
          inertial_est.optimize(current_estimate);
          current_estimate.at(gtsam::Symbol('M',0)).print("Optimized mass: ");
          current_estimate.at(gtsam::Symbol('m',0)).print("Optimized mass(com): ");        
          current_estimate.at(gtsam::Symbol('C',0)).print("Optimized center of mass: ");
          gtsam::Vector3 com=current_estimate.at<gtsam::Point3>(gtsam::Symbol('C',0));
          //Vector3 com= com_v.cast<Vector3>();//.cast();
          com_data.header.frame_id="object";
          com_data.pose.position.x=com[0];
          com_data.pose.position.y=com[1];
          com_data.pose.position.z=com[2];
          com_data.pose.orientation.w=1.0;
          pub.publish(com_data);
          
        }
        //cerr<<t_step<<endl;
        t_step++;
        got_new_data=false;
      }
      
      if(t_step>=max_tstep)
      {
        break;
      }
      ros::spinOnce();
      loop_rate->sleep();
    }
  }
  
};


  
int main(int argc, char** argv)
{
  ros::init(argc,argv, "ft_inertial_estimation");
  ros::NodeHandle nh;
  inertialClient in_cl;
  in_cl.init(nh);

  
  in_cl.run();
  
                                 
  //results.print("Optimized Results\n");

  return 0;
  
}
