// this gtsam node takes prior on object pose and performs smoothing to get object position, velocity and acceleration estimates
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>

#include <sysid_manipulation/ft_problem.hpp>
#include <sysid_manipulation/resetGraph.h>
#include <sysid_manipulation/FullMeasurementData.h>

#include <sysid_manipulation/toggleInference.h>
#include <sysid_manipulation/data_utils.hpp>

//#include <pose_factors.h>
class poseClient
{
public:
  vector<sysid_manipulation::sysid_data> data_arr;
  sysid_manipulation::sysid_data t_data;

  double old_time=0.0;
  
  bool got_new_data=false;
  bool reset_graph=false;
  ros::NodeHandle* nh;
  int t_step=0;
  int max_tstep=50000;
  ros::Publisher pub, pub_arr,com_pub;
  ros::Subscriber sub;
  ros::ServiceServer service,service_1;
  geometry_msgs::PoseStamped pose_data;
  poseSmooth pose_fg;
  vector<gtsam::Vector> ft_data;
  gtsam::Point3 obj_g;
  ros::Rate* loop_rate;
  bool collect_data = true;
  bool infer_pose = false;
  bool infer_dynamics = false;
  int batch_t = 10;

  bool init(ros::NodeHandle &n,const double &loop_rate_hz=300)
  {
    
    nh=&n;
    sub = nh->subscribe("/sysid/ft_data", 10, &poseClient::md_cb, this);
    service = nh->advertiseService("/pose_isam/reset_graph", &poseClient::graph_call, this);
    service_1 = nh->advertiseService("/sysid/toggle_graph", &poseClient::node_call, this);

    pose_fg.initialize_graph();
    loop_rate=new ros::Rate(loop_rate_hz);
  
    pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/smoothed_pose",1);
    com_pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/com",1);

    pub_arr=nh->advertise<geometry_msgs::PoseArray>("/isam/pose_batch",1);
        
    // read robot kdl parameters:
    // build kdl model:
    bool success = true;
    return success;
    
  }

  ~poseClient()
  {
    delete loop_rate;
  }
  // msg cb:

  gtsam::Pose3 pose_cvt(const geometry_msgs::Pose &in_pose)
  {
    gtsam::Pose3 pose_gtsam(gtsam::Rot3(gtsam::Quaternion(in_pose.orientation.w, in_pose.orientation.x,
                                                          in_pose.orientation.y, in_pose.orientation.z)),
                            gtsam::Point3(in_pose.position.x,in_pose.position.y,in_pose.position.z));
    return pose_gtsam;
  }
  void md_cb(const sysid_manipulation::FullMeasurementData &msg)
  {
    // object pose:
    
    // get matrix from pose:
    // get w_T_palm
    gtsam::Pose3 w_palm=pose_cvt(msg.base_T_palm.pose);
    // get palm_T_obj
    gtsam::Pose3 palm_obj=pose_cvt(msg.palm_T_obj.pose);
    

    // get object pose
    gtsam::Pose3 palm_ft=pose_cvt(msg.palm_T_ft.pose);

    gtsam::Pose3 w_obj=w_palm * palm_obj;

    // object pose:
    pose_data.pose.position.x=w_obj.x();
    pose_data.pose.position.y=w_obj.y();
    pose_data.pose.position.z=w_obj.z();
    pose_data.header.stamp=msg.base_gvec.header.stamp;
    pose_data.header.frame_id=msg.base_T_palm.header.frame_id;
    gtsam::Vector q=w_obj.rotation().quaternion();
    pose_data.pose.orientation.w=q[0];
    pose_data.pose.orientation.x=q[1];
    pose_data.pose.orientation.y=q[2];
    pose_data.pose.orientation.z=q[3];
    

    // get force, torque sensed by force torque sensor
    
    // transform force. torque to object frame (obj_T_ft):
    gtsam::Pose3 obj_ft = palm_obj.inverse() * palm_ft;
    gtsam::Point3 ft_force(msg.ft_force.wrench.force.x, msg.ft_force.wrench.force.y ,msg.ft_force.wrench.force.z);
    gtsam::Point3 ft_torque(msg.ft_force.wrench.torque.x, msg.ft_force.wrench.torque.y ,msg.ft_force.wrench.torque.z);
    ft_force =  obj_ft.rotation() * ft_force;
    ft_torque =  obj_ft.rotation() * ft_torque; 

    // get gravity:
    gtsam::Point3 w_g(msg.base_gvec.wrench.force.x, msg.base_gvec.wrench.force.y, msg.base_gvec.wrench.force.z);
    obj_g = w_obj.rotation().inverse() * w_g;

    // get contact point of ft, w.r.t. object (r_c=obj_T_ft position)
    gtsam::Point3 r_ft(obj_ft.x(),obj_ft.y(), obj_ft.z());
    
    
    ft_data.resize(3);
    ft_data[0]=r_ft;
    ft_data[1]=ft_force;
    ft_data[2]=ft_torque;


    t_data.obj_pose = w_obj;
    t_data.ft_data = ft_data;
    t_data.g=w_g;
    t_data.t_step =msg.base_gvec.header.stamp.toSec();
    data_arr.emplace_back(t_data);
    got_new_data=true;
  
  }
  bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
  {
    reset_graph=true;
    res.success=true;
    
    return true;
  }
  bool node_call(sysid_manipulation::toggleInference::Request &req,sysid_manipulation::toggleInference::Response &res)
  {
    if(req.collect_data)
    {
      //start reading data
      collect_data = true;
    }
    if(req.infer_pose)
    {
      // stop reading data & process pose
      ROS_INFO("Inferring Pose");
      collect_data = false;
      optimize_pose();
      infer_pose = true;
    }
    if(req.infer_dynamics)
    {
      // perform dynamics estimation
      collect_data = false;
      if(infer_pose)        
      {
        optimize_dynamics();
      }
    }

    if(req.reset_data)
    {
      reset();
    }

    res.status = true;
    return true;
    
  }

  bool optimize_pose()
  {
    cerr<<data_arr.size()<<endl;
    gtsam::Values current_estimate;
    double err=10.0;
    double err_b=10.0;

 
    // reset graph
    reset();
    // optimize pose from data array:
    for(int i=0;i<data_arr.size(); ++i)
    {
      // add data to graph
      pose_fg.update_pose_graph(data_arr[i].obj_pose,data_arr[i].t_step);

      /*
      // ISAM update
      
      if(i%batch_t ==0)
      {
        //pose_fg.optimize();
        pose_fg.optimize(current_estimate,err,err_b);
        

      }
      */
      
    }
    // guass newton optimization:
    pose_fg.optimize(current_estimate,err,err_b);
            

    cerr<<"Optimization error: "<<err<<endl;

    gtsam::Pose3 o_pose;
          
    for(int i=0;i<data_arr.size();++i)
    {
      //visualize smoothed object pose:
      o_pose = current_estimate.at<gtsam::Pose3>(gtsam::Symbol('x',i));

      //update data in data_arr
      data_arr[i].obj_pose = o_pose;
      data_arr[i].obj_vel = current_estimate.at<gtsam::Vector6>(gtsam::Symbol('v',i));
      data_arr[i].obj_acc = current_estimate.at<gtsam::Vector6>(gtsam::Symbol('a',i));
        
      
    }

    
    // visualize optimized poses:
    //visualize_pose(data_arr);

    vector<sysid_manipulation::sysid_data> filtered_data;
    for(int i =0; i<data_arr.size();++i)
    {
      // filter bad poses
      if(data_arr[i].obj_acc.head(3).squaredNorm() < 1.0)
      {
        filtered_data.emplace_back(data_arr[i]);

        visualize_pose(data_arr[i].obj_pose,pose_data.header.frame_id);
        loop_rate->sleep();
  
      }
      
      
    }
    cout<<"Filtered size"<<data_arr.size()<<" "<<filtered_data.size()<<endl;
    data_arr = filtered_data;
    return true;
  }

  bool visualize_pose(const gtsam::Pose3 &o_pose,const string &frame_id)
  {
    // publish pose

    geometry_msgs::PoseStamped pose_st;
    pose_st.pose.position.x=o_pose.x();
    pose_st.pose.position.y=o_pose.y();
    pose_st.pose.position.z=o_pose.z();
    gtsam::Vector q=o_pose.rotation().quaternion();
    pose_st.pose.orientation.w=q[0];
    pose_st.pose.orientation.x=q[1];
    pose_st.pose.orientation.y=q[2];
    pose_st.pose.orientation.z=q[3];

    pose_st.header.stamp=ros::Time::now();
    pose_st.header.frame_id=frame_id;
    pub.publish(pose_st);
              
    return true;
  }
    
  bool optimize_dynamics()
  {
    cerr<<data_arr.size()<<endl;
    gtsam::Values current_estimate;
    double err=10.0;
    double err_b=10.0;

 
    // reset graph
    reset();
    // optimize pose from data array:
    for(int i=0;i<data_arr.size(); ++i)
    {
      // add data to graph
      //pose_fg.update_pose_graph(data_arr[i].obj_pose,data_arr[i].t_step);
      pose_fg.update_dynamics_graph(data_arr[i].obj_pose,data_arr[i].obj_vel,data_arr[i].obj_acc,data_arr[i].ft_data,data_arr[i].g);

      /*
      // ISAM update
      
      if(i%batch_t ==0)
      {
        //pose_fg.optimize();
        pose_fg.optimize(current_estimate,err,err_b);
        

      }
      */
      
    }
    // guass newton optimization:
    pose_fg.optimize(current_estimate,err,err_b);
            

    //cerr<<"Optimization error: "<<err<<endl;

    gtsam::Vector7 W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));
    gtsam::Rot3 R=current_estimate.at<gtsam::Rot3>(gtsam::Symbol('R',pose_fg.l_step));
    
    gtsam::Vector3 com=W.segment(1,3);
            
    gtsam::Vector3 I=W.tail(3);
    
    gtsam::Vector1 mass=W.head(1);//current_estimate.at<gtsam::Vector1>(gtsam::Symbol('M',0));
    
    cerr<<"Optimization error: "<<err<<endl;
    //cerr<<"Acceleration: "<<vel.transpose()<<endl;
    cerr<<"Mass: "<<mass.transpose()<<endl;
    cerr<<"Center of Mass: "<< (com).transpose()<<endl;

  }

  bool reset()
  {
    pose_fg.initialize_graph();
    t_step=0;
    reset_graph=false;
   
  }

  void run()
  {
    while(nh->ok())
    {
      ros::spinOnce();
      loop_rate->sleep();
    }
  }
  
};


int main(int argc, char** argv)
{
  ros::init(argc,argv,"pose_gtsam");
  ros::NodeHandle nh;
  poseClient p_cl;
  
  p_cl.init(nh);

  ROS_INFO("Initialized");
  p_cl.run();
  
                                 
  //results.print("Optimized Results\n");

  return 0;
  
}
