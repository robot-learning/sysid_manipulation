// this gtsam node takes prior on object pose and performs smoothing to get object position, velocity and acceleration estimates
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>

#include <sysid_manipulation/bt_problem_expressions.hpp>
#include <sysid_manipulation/resetGraph.h>
#include <sysid_manipulation/toggleInference.h>

#include <sysid_manipulation/FullMeasurementData.h>

#include <sysid_manipulation/data_utils.hpp>
#include <tf/transform_broadcaster.h>

#include <visualization_msgs/MarkerArray.h>
#include "gtsam_factors/sysid_expressions.h"
#include "gtsam_factors/forward_sim.hpp"

#include <stdio.h> 
#include <sys/resource.h> 
#include <string.h> 
#include <errno.h> 
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h>
#include <ctime>
#include <chrono>

using namespace gtsam;
using namespace std;

//#include "gtsam_factors/sysid_factors.h"

//#include <pose_factors.h>
class poseClient
{
public:
  double old_time=0.0;

  vector<sysid_manipulation::sysid_data> data_arr;
  sysid_manipulation::sysid_data t_data;
  gtsam::Pose3 rr_obj;
  bool got_new_data=false;
  bool reset_graph=false;
  ros::NodeHandle* nh;
  int t_step=0;
  int max_tstep=5000;
  ros::Publisher pub, pub_arr,com_pub, marker_pub;
  ros::Subscriber sub;
  ros::ServiceServer service,service_1;
  geometry_msgs::PoseStamped pose_data;
  poseSmooth pose_fg;
  vector<gtsam::Vector> ft_data, bt_data;
  gtsam::Point3 obj_g;
  ros::Rate* loop_rate;
  ros::Rate* pub_loop_rate;
  //gtsam::Pose3 o_T_Q;
  gtsam::Point3 offset_pt;

  bool collect_data = true;
  bool infer_pose = false;
  bool infer_dynamics = false;

  int batch_t=5; 

  bool init(ros::NodeHandle &n,const double &loop_rate_hz=100)
  {
    
    nh=&n;
    sub = nh->subscribe("/sysid/bt_data", 1000, &poseClient::md_cb, this);
    service = nh->advertiseService("/pose_isam/reset_graph", &poseClient::graph_call, this);
    service_1 = nh->advertiseService("/sysid/toggle_graph", &poseClient::node_call, this);
    marker_pub=nh->advertise<visualization_msgs::MarkerArray>("/isam/forces", 1);


    pose_fg.initialize_graph();
    loop_rate=new ros::Rate(loop_rate_hz);
    pub_loop_rate=new ros::Rate(30);
  
    pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/smoothed_pose",1);
    com_pub=nh->advertise<geometry_msgs::PoseStamped>("/isam/com",1);

    pub_arr=nh->advertise<geometry_msgs::PoseArray>("/isam/pose_batch",1);
        
    // read robot kdl parameters:
    // build kdl model:
    bool success = true;

    
    return success;
    
  }

  ~poseClient()
  {
    delete loop_rate;
    delete pub_loop_rate;
  }
  // msg cb:
  gtsam::Pose3 pose_cvt(const geometry_msgs::Pose &in_pose)
  {
    gtsam::Pose3 pose_gtsam(gtsam::Rot3(gtsam::Quaternion(in_pose.orientation.w, in_pose.orientation.x,
                                                          in_pose.orientation.y, in_pose.orientation.z)),
                            gtsam::Point3(in_pose.position.x,in_pose.position.y,in_pose.position.z));
    return pose_gtsam;
  }
  void md_cb(const sysid_manipulation::FullMeasurementData &msg)
  {
    //sysid_manipulation::sysid_data t_data;
    
    // object pose:

    // get matrix from pose:
    // get w_T_palm
    gtsam::Pose3 w_palm=pose_cvt(msg.base_T_palm.pose);
    // get palm_T_obj
    gtsam::Pose3 palm_obj=pose_cvt(msg.palm_T_obj.pose);

    if(data_arr.size() == 0)
    {
      gtsam::Pose3 rr_T_w = (pose_cvt(msg.base_T_palm.pose)
                             * pose_cvt(msg.palm_T_ftips[3].pose)).inverse();
      rr_obj = rr_T_w * (w_palm) * palm_obj ;
      //if(msg.rr_T_obj.pose.orientation.w!=0.0)
      //{d
      //rr_obj = pose_cvt(msg.rr_T_obj.pose);
      //}

    }
    //gtsam::Pose3 w_obj = pose_cvt(msg.base_T_palm.pose)
    //* pose_cvt(msg.palm_T_ftips[3].pose)
    //* rr_obj;
    
    // get object pose
    gtsam::Pose3 palm_ft = pose_cvt(msg.palm_T_ft.pose);

    gtsam::Pose3 w_obj= (w_palm) * palm_obj ;


    
    //gtsam::Pose3 O_T_Q = w_obj.inverse() *  pose_cvt(msg.base_T_palm.pose) * pose_cvt(msg.palm_T_ftips[0].pose) * pose_cvt(msg.ftips_T_cpts[0].pose);
    

    //gtsam::Pose3 w_Q= (w_palm) * palm_obj * O_T_Q ;

    // object pose:
    //gtsam::Pose3 w_obj_pose  (gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
    //gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));

    pose_data.pose.position.x=w_obj.x();
    pose_data.pose.position.y=w_obj.y();
    pose_data.pose.position.z=w_obj.z();
    pose_data.header.stamp=msg.base_gvec.header.stamp;
    pose_data.header.frame_id=msg.base_T_palm.header.frame_id;
    gtsam::Vector q=w_obj.rotation().quaternion();
    pose_data.pose.orientation.w=q[0];
    pose_data.pose.orientation.x=q[1];
    pose_data.pose.orientation.y=q[2];
    pose_data.pose.orientation.z=q[3];
    
    // get gravity:
    gtsam::Point3 w_g(msg.base_gvec.wrench.force.x, msg.base_gvec.wrench.force.y, msg.base_gvec.wrench.force.z);
    
    obj_g =w_obj.rotation().inverse() * w_g;


    // get force, torque sensed by force torque sensor
    
    // transform force. torque to object frame (obj_T_ft):

    /*
    gtsam::Pose3 obj_ft = palm_obj.inverse() * palm_ft;

    // create a new frame~'Q' at 'ft frame' origin with axes aligned with 'obj' frame:
    //gtsam::Pose3 ft_T_Q(obj_ft.rotation().inverse(),gtsam::Point3(0,0,0));
    //gtsam::Pose3 obj_T_Q(gtsam::Rot3(),obj_ft.translation());
    o_T_Q=O_T_Q;

    gtsam::Point3 ft_force(msg.ft_force.wrench.force.x, msg.ft_force.wrench.force.y ,msg.ft_force.wrench.force.z);
    gtsam::Point3 ft_torque(msg.ft_force.wrench.torque.x, msg.ft_force.wrench.torque.y ,msg.ft_force.wrench.torque.z);
    
    //ft_force = ft_T_Q.inverse().rotation() * ft_force;
    //ft_torque = ft_T_Q.inverse().rotation() * ft_torque; 

    ft_force = obj_ft.rotation() * ft_force;
    ft_torque = obj_ft.rotation() * ft_torque; 
    // get contact point of ft, w.r.t. object (r_c=obj_T_ft position)
    //gtsam::Point3 r_ft(obj_ft.x(),obj_ft.y(), obj_ft.z());
    
    
    ft_data.resize(3);
    ft_data[0]=r_ft;
    ft_data[1]=ft_force;
    ft_data[2]=ft_torque;

    */
    t_data.t_step = msg.base_gvec.header.stamp.toSec();

    bt_data.resize(4*3); // [contact point , force, force acc]
    vector<Pose3> w_cpt_arr;
    w_cpt_arr.resize(4);
    // get biotac data:
    // forces, contact points w.r.t. object:
    for(int i=0;i<4;++i)
    {
      // read ftip force:
      gtsam::Point3 bt_force(msg.cpt_forces[i].wrench.force.x,
                             msg.cpt_forces[i].wrench.force.y ,
                             msg.cpt_forces[i].wrench.force.z);//; // force in biotac frame
      gtsam::Pose3 w_cpts = pose_cvt(msg.base_T_palm.pose) * pose_cvt(msg.palm_T_ftips[i].pose) *
        pose_cvt(msg.ftips_T_cpts[i].pose);//();

      w_cpt_arr[i] = w_cpts;
      //

      //
      //gtsam::Point3 w_bt_force= w_cpts.rotation() * bt_force;
      gtsam::Point3 bt_bt_force =  bt_force;
      //gtsam::Point3 w_bt_cpt = (w_cpts).translation();

      gtsam::Point3 o_bt_force= w_obj.rotation().inverse() * w_cpts.rotation() * bt_force;
      gtsam::Point3 o_bt_cpt = ( w_obj.inverse() * w_cpts).translation();

      //gtsam::Point3 bt_acc(0,0,0);//; // force in biotac frame;//();

      gtsam::Point3 bt_acc(sqrt(msg.cpt_forces[i].wrench.torque.x),
                           sqrt(msg.cpt_forces[i].wrench.torque.y) ,
                           sqrt(msg.cpt_forces[i].wrench.torque.z));//; // force in biotac frame;//();
      //gtsam::Point3 o_bt_force_acc = w_obj.rotation().inverse() * bt_acc;
      bt_data[i*3] = o_bt_cpt;
      
      bt_data[i*3+1] = o_bt_force;
      bt_data[i*3+2] = bt_acc;
      //cout<<bt_acc.transpose()<<endl;
      //cout<<pose_cvt(msg.ftipes_T_cpts[i].pose)
      //w_cpts.rotation()
      //cerr<<o_bt_force<<endl;

      
    }

    /*
    offset_pt = bt_data[3*3];

    bt_data[3*3+1] = gtsam::Point3(0,0,0);// making force to be zero

    // add offset to object frame:
    
    pose_data.pose.position.x -=offset_pt.x();
    pose_data.pose.position.y -=offset_pt.y();
    pose_data.pose.position.z -=offset_pt.z();
    
    for(int i =0; i<4; ++i)
    {
      bt_data[i*3] = bt_data[i*3] - offset_pt;
    }
    */

    // store data:


    t_data.g = w_g;
    t_data.bt_data = bt_data ;
    t_data.obj_pose = w_obj;
    t_data.w_cpts = w_cpt_arr;
    if(data_arr.size() == 0)
    {
      data_arr.emplace_back(t_data);
      got_new_data=true;
    }
    else
    {
        if(t_data.t_step > data_arr[data_arr.size()-1].t_step)
        {
          data_arr.emplace_back(t_data);
          got_new_data=true;
        }
        else
        {
          cerr<<"received old data"<<endl;
          
        }
    }
  
  }
  bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
  {
    reset_graph=true;
    res.success=true;
    
    return true;
  }

  // toggle data reading

  bool node_call(sysid_manipulation::toggleInference::Request &req,sysid_manipulation::toggleInference::Response &res)
  {
    if(req.collect_data)
    {
      //start reading data
      collect_data = true;
    }
    if(req.infer_pose)
    {
      // stop reading data & process pose
      ROS_INFO("Inferring Pose");
      collect_data = false;
      infer_pose = true;
    }
    if(req.infer_dynamics)
    {
      // perform dynamics estimation
      collect_data = false;

      double opt_mass;
      gtsam::Vector3 opt_com;
      gtsam::Matrix33 opt_inertia_matrix;
      vector<sysid_manipulation::sysid_data> opt_data,sim_data;
      // if method==so3_manifold
      optimize_so3_manifold_dynamics(opt_mass,
                                     opt_com,
                                     opt_inertia_matrix,
                                     opt_data);


      // compute tracking error
      //sim_data = forward_sim(opt_mass,opt_com,opt_inertia_matrix,opt_data);
      //sim_data = forward_sim_full(opt_mass,opt_com,opt_inertia_matrix,opt_data);
      
      //get error between the final poses:
      //cout<<opt_data[opt_data.size()-1].obj_pose<<endl;
      //cout<<sim_data[sim_data.size()-1].obj_pose<<endl;
      //publish_trajectory(opt_data,sim_data);
    }

    if(req.reset_data)
    {
      reset();
    }

    res.status = true;
    return true;
    
  }

  // publish data:

  bool publish_data()
  {
    // publish old pose

    // publish pose

    // publish com

  }
  


    //cerr<<data_arr.size()<<endl;
  visualization_msgs::Marker get_force_line(gtsam::Vector3 contact_pt, gtsam::Vector3 force, vector<double> color = {1,0.1,0.1,1.0})
  {
    visualization_msgs::Marker marker;
    //marker.header.frame_id = base_frame_;
    marker.ns = "basic_shapes";
    marker.type = 0;
    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;
    
    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    // compute quaternion from rpy:
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.01;
    marker.scale.y = 0.02;
    marker.scale.z = 0.0;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = color[3];

    
    marker.lifetime = ros::Duration();


    // points:
    geometry_msgs::Point pt;
    pt.x=contact_pt[0];
    pt.y=contact_pt[1];
    pt.z=contact_pt[2];
    marker.points.emplace_back(pt);

    force = contact_pt + force;
    pt.x=force[0];
    pt.y=force[1];
    pt.z=force[2];
    marker.points.emplace_back(pt);
    
    //Vect
    //marker.points.emplace_back(geometry_msgs::Point(contact_pt[0],contact_pt[1],contact_pt[2]));

    return marker;
 
  }
  
  bool visualize_forces(const sysid_manipulation::sysid_data &msg )
  {
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(msg.obj_pose.x(),msg.obj_pose.y(), msg.obj_pose.z()) );
    gtsam::Vector q_gt=msg.obj_pose.rotation().quaternion();
    tf::Quaternion q(q_gt[1],q_gt[2],q_gt[3],q_gt[0]);
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(),"lbr4_base_link","object"));
    
    // visualize gravity vector from origin:
    visualization_msgs::MarkerArray m_ar;
    gtsam::Vector3 contact_pt,force;
    
    contact_pt = msg.bt_data[0];
    force = msg.bt_data[0*3+1];
    //cout<<"0: "<<force.transpose()<<endl;

    visualization_msgs::Marker marker = get_force_line(contact_pt, force);
    marker.header.frame_id = "object";
    marker.id = 0; // 

    m_ar.markers.push_back(marker);

    contact_pt = msg.bt_data[1*3];
    force = msg.bt_data[1*3+1];
    //cout<<"1: "<<force.transpose()<<endl;
    marker = get_force_line(contact_pt, force);
    marker.id = 1; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    
    contact_pt = msg.bt_data[2*3];
    force = msg.bt_data[2*3+1];
    //cout<<"2: "<<force.transpose()<<endl;

    marker = get_force_line(contact_pt, force);
    marker.id = 2; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    
    contact_pt = msg.bt_data[3*3];
    force = msg.bt_data[3*3+1];
    //cout<<"3: "<<force.transpose()<<endl;

    marker = get_force_line(contact_pt, force);
    marker.id = 3; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    contact_pt.setZero();
    //cerr<<msg.g<<endl;
    //force = msg.obj_pose.rotation().inverse() * msg.g;
    force = msg.obj_pose.rotation().inverse() * msg.g * 0.1;
    vector<double> col = {1.0,1.0,0,1};
    marker = get_force_line(contact_pt, force,col);
    marker.id = 4; // 
    marker.header.frame_id = "object";
    m_ar.markers.push_back(marker);
    
    marker_pub.publish(m_ar);
    return true;
  }
  bool visualize_pose(const gtsam::Pose3 &o_pose,const string &frame_id)
  {
    // publish pose

    geometry_msgs::PoseStamped pose_st;
    pose_st.pose.position.x=o_pose.x();
    pose_st.pose.position.y=o_pose.y();
    pose_st.pose.position.z=o_pose.z();
    gtsam::Vector q=o_pose.rotation().quaternion();
    pose_st.pose.orientation.w=q[0];
    pose_st.pose.orientation.x=q[1];
    pose_st.pose.orientation.y=q[2];
    pose_st.pose.orientation.z=q[3];

    pose_st.header.stamp=ros::Time::now();
    pose_st.header.frame_id=frame_id;
    pub.publish(pose_st);
              
    return true;
  }
  vector<sysid_manipulation::sysid_data> forward_sim( const double &opt_mass,
                                                      const gtsam::Vector3 &opt_com,
                                                      const gtsam::Matrix33 &opt_inertia_matrix,
                                                      const vector<sysid_manipulation::sysid_data> &t_data)
  {
    
    reset();
    gtsam::Values current_estimate;

    pose_fg.t_steps_ = 2;
    vector<sysid_manipulation::sysid_data> sim_data;
    
    // get initial object pose
    // get initial object velocity

    // loop over timesteps
    for(int i=2; i<t_data.size();++i)
    {
      pose_fg.update_forward_sim(t_data[i-2].obj_pose,
                                 t_data[i-1].obj_pose,
                                 t_data[i-2].t_step,
                                 t_data[i-1].t_step,
                                 t_data[i].t_step,
                                 t_data[i-2].bt_data,
                                 t_data[i-2].g, 
                                 opt_mass,opt_com,opt_inertia_matrix);
      
      // compute acceleration due to force:
      
      // compute delta time
      // get force applied and compute object pose by integrating
      
      // integrate acceleration to velocity
      
    }
    double e1,e2;
    pose_fg.optimize(current_estimate,e1,e2);

    // get optimized poses
    sim_data = t_data;
    for(int t=2; t<t_data.size();++t)
    {
      sim_data[t].obj_pose = current_estimate.at<gtsam::Pose3>(gtsam::Symbol('x',t));

    }
    return sim_data;
    
  }
  vector<sysid_manipulation::sysid_data> forward_sim_full
  ( const double &opt_mass,
    const gtsam::Vector3 &opt_com,
    const gtsam::Matrix33 &opt_inertia_matrix,
    const vector<sysid_manipulation::sysid_data> &opt_data)    
  {

    
    //cout<<opt_mass<<endl;

    //pose_fg.t_steps_ = 2;
    vector<sysid_manipulation::sysid_data> sim_data = opt_data;
    
    // get initial object pose
    Vector6 obj_vel= Vector6().setZero();
    Vector6 obj_acc = Vector6().setZero();
    Pose3 obj_pose = opt_data[0].obj_pose;
    Vector12 bt_forces;
    Vector12 bt_cpts;

    vector<Pose3> w_cpts;
    // loop over timesteps
    for(int i=0; i<opt_data.size()-1;++i)
    {
      // compute acceleration due to force
      
      // get bt_data:
      for(int j=0;j<4;++j)
      {
        bt_cpts.segment(3*j,3) =opt_data[i].bt_data[j*3];
        bt_forces.segment(3*j,3) = opt_data[i].bt_data[j*3+1];
      }


      // transform bt_forces to object frame
      w_cpts.resize(4);
      for(int j=0;j<4;++j)
      {
        w_cpts[j] =  obj_pose * opt_data[i].obj_pose.inverse() * opt_data[i].w_cpts[j];
        
      }
      bt_forces = sysid_expr::proj_force_obj_frame_(obj_pose,
                                        w_cpts[0],
                                        w_cpts[1],
                                        w_cpts[2],
                                        w_cpts[3],
                                        bt_forces).value(Values());
                                        
      // transform bt_cpts to object frame:

      bt_cpts = sysid_expr::proj_cpt_obj_frame_(obj_pose,
                                                    w_cpts[0],
                                                    w_cpts[1],
                                                    w_cpts[2],
                                                    w_cpts[3]).value(Values());
      Vector6 net_f_tau = sysid_expr::net_force_torque(bt_forces,bt_cpts);
      
      obj_acc = dyn_sim_fns::get_acc(obj_pose,
                                     opt_mass,
                                     opt_com,
                                     opt_inertia_matrix,
                                     obj_vel,
                                     opt_data[i].g,
                                     net_f_tau);
      
      // integrate object pose
      
      obj_pose = obj_pose *
        gtsam::Pose3::Expmap(obj_vel*(opt_data[i+1].t_step - opt_data[i].t_step)
                             + 0.5 * obj_acc * (opt_data[i+1].t_step - opt_data[i].t_step)*
                             (opt_data[i+1].t_step - opt_data[i].t_step));
      obj_vel = obj_vel + obj_acc * (opt_data[i+1].t_step - opt_data[i].t_step);
      //cout<<"T: "<<(opt_data[i+1].t_step - opt_data[i].t_step)<<endl;
      //cout<<obj_acc.transpose()<<endl;
      //cout<<obj_pose.translation()<<endl;
      sim_data[i+1].obj_pose = obj_pose;
      
    }

    return sim_data;
    
  }

  bool optimize_so3_manifold_dynamics(double &opt_mass, gtsam::Vector3 &opt_com,
                                      gtsam::Matrix33 &opt_inertia_matrix,
                                      vector<sysid_manipulation::sysid_data> &opt_data)
  {
    //cerr<<data_arr.size()<<endl;
    gtsam::Values current_estimate;
    double err=10.0;
    double err_b=10.0;
    gtsam::Vector7 w_model_prior;
    w_model_prior<<0.05,0.07,-0.03,0.03,0.0001,0.0001,0.0001;
    w_model_prior<<0.147,0.108741,-0.035,0.031,0.000433,0.00239,0.00242;
    //w_model_prior<<1.0,0.2,0.5,0.1,0.401521,0.129151,0.47989;
    Vector3 r_prior;
    r_prior<<-0.000537,0.000496,-0.00016;
    //r_prior<<-0.1297,-0.02594,-0.06485;//-0.0035, -0.05,   0.14;
    //r_prior.setZero();
    pose_fg.update_prior(w_model_prior,r_prior);

    // reset graph
    reset();
    
    using milli = std::chrono::milliseconds;
    auto start = std::chrono::high_resolution_clock::now();
    auto finish = std::chrono::high_resolution_clock::now();
    double elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
    double ms=1000.0;

    
    Rot3 R_t_;
    int pre_step = 0;
    gtsam::Vector1 mass_;
    gtsam::Vector7 W;
    gtsam::Rot3 R;

    gtsam::Vector3 com_;
    gtsam::Matrix33 inertia_diag ;
    gtsam::Matrix33 inertia_matrix,inertia_com;
    
    gtsam::Vector7 w_i;

reset();
    pre_step = 2;
    current_estimate = gtsam::Values();

    
    // optimize pose from data array:
    for(int i=pre_step;i<data_arr.size(); ++i)
    {
      pose_fg.update_manifold_graph_baseline(
                                             data_arr[i].obj_pose,
                                             data_arr[i-2].bt_data,
                                             data_arr[i-2].w_cpts,
                                             data_arr[i-2].g,
                                             data_arr[i].t_step,false,false,true);
    }
    finish = std::chrono::high_resolution_clock::now();
    cerr<<"Time to load graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
    start = std::chrono::high_resolution_clock::now();

    //cerr<<"Debug jacbian.."<<endl;
    //pose_fg.debug_print();
    //return true;
    //pose_fg.debug_jacobian();
    //return true;
    //return true;
    cerr<<"Optimizing: linear"<<endl;
    pose_fg.optimize(current_estimate,err,err_b);
    //current_estimate=pose_fg.initialEstimate;
    finish = std::chrono::high_resolution_clock::now();
    cerr<<"###Time to optimize graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;

   
    
    W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));
    Vector3 r_lin=current_estimate.at<gtsam::Vector3>(gtsam::Symbol('R',pose_fg.l_step));
    
    mass_=W.head(1);
    com_=W.segment(1,3);
    
    cerr<<"Optimization error: "<<err<<endl;
    
    cerr<<"Mass: "<<mass_.transpose()<<endl;
    cerr<<"Center of Mass: "<< (com_).transpose()<<endl;
    inertia_matrix = (gtsam::Matrix33() <<
                      W[4+1] + W[4+2],r_lin(0),r_lin(1),
                      r_lin(0),W[4+0] + W[4+2],r_lin(2),
                      r_lin(1),r_lin(2),W[4+0]+ W[4+1]).finished(); ;// * inertia_diag

    inertia_com = inertia_matrix + mass_(0) * com_ * com_.transpose();
    
    cerr<<"Inertia"<<endl<<inertia_matrix<<endl;
    cerr<<"Inertia com"<<endl<<inertia_com<<endl;

    cout<<"Data format:"<<endl;
    cout<<mass_(0)<<","<<com_(0)<<","<<com_(1)<<","<<com_(2)<<","<<
      inertia_matrix(0,0)<<","<<inertia_matrix(1,1)<<","<<inertia_matrix(2,2)<<","<<
      inertia_matrix(0,1)<<","<<inertia_matrix(0,2)<<","<<inertia_matrix(1,2)<<endl;

    
    return true;
    reset();
    pre_step = 0;
    current_estimate = gtsam::Values();

    
    // optimize pose from data array:
    for(int i=2;i<data_arr.size(); ++i)
    {
      pose_fg.update_manifold_graph_baseline(
                                             data_arr[i].obj_pose,
                                             data_arr[i-2].bt_data,
                                             data_arr[i-2].w_cpts,
                                             data_arr[i-2].g,
                                             data_arr[i].t_step,false,false);
    }
    finish = std::chrono::high_resolution_clock::now();
    cerr<<"Time to load graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
    start = std::chrono::high_resolution_clock::now();

    //cerr<<"Debug jacbian.."<<endl;
    //pose_fg.debug_print();
    //return true;
    //pose_fg.debug_jacobian();
    //return true;
    //return true;
    cerr<<"Optimizing: no geodesic, no constraint"<<endl;
    pose_fg.optimize(current_estimate,err,err_b);
    //current_estimate=pose_fg.initialEstimate;
    finish = std::chrono::high_resolution_clock::now();
    cerr<<"###Time to optimize graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;

   
    
    W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));

    R=current_estimate.at<gtsam::Rot3>(gtsam::Symbol('R',pose_fg.l_step));
    
    mass_=W.head(1);
    com_=W.segment(1,3);
    
    cerr<<"Optimization error: "<<err<<endl;
    
    cerr<<"Mass: "<<mass_.transpose()<<endl;
    cerr<<"Center of Mass: "<< (com_).transpose()<<endl;
    inertia_diag = (gtsam::Matrix33() <<
                                    W[4+1] + W[4+2],0,0,
                                    0,W[4+0] + W[4+2],0,
                                    0,0,W[4+0]+ W[4+1]).finished();
    
    inertia_matrix = R.matrix() * inertia_diag * R.matrix().transpose() ;// * inertia_diag 

    inertia_com = inertia_matrix + mass_(0) * com_ * com_.transpose();
    
    cerr<<"Inertia"<<endl<<inertia_matrix<<endl;
    cerr<<"Inertia com"<<endl<<inertia_com<<endl;

    cout<<R.ypr()<<endl;
    cout<<"Data format:"<<endl;
    cout<<mass_(0)<<","<<com_(0)<<","<<com_(1)<<","<<com_(2)<<","<<
      inertia_matrix(0,0)<<","<<inertia_matrix(1,1)<<","<<inertia_matrix(2,2)<<","<<
      inertia_matrix(0,1)<<","<<inertia_matrix(0,2)<<","<<inertia_matrix(1,2)<<endl;
 
    // optimize for all values:
    //return true;
    // inertia prior:
    //cout<<"Inertia prior:"<<inertia_prior<<endl;
    //w_model_prior(0) = mass_(0);
    //w_i = w_model_prior;

    //pose_fg.update_prior(w_i);

    reset();
    current_estimate = gtsam::Values(); 

    pre_step = 0;
    start = std::chrono::high_resolution_clock::now();
    
    // optimize pose from data array:
    for(int i=2;i<data_arr.size(); ++i)
    {
            pose_fg.update_manifold_graph_baseline(
                                             data_arr[i].obj_pose,
                                             data_arr[i-2].bt_data,
                                             data_arr[i-2].w_cpts,
                                             data_arr[i-2].g,
                                             data_arr[i].t_step,true,false);
    }
    finish = std::chrono::high_resolution_clock::now();
    cerr<<"Time to load graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
    start = std::chrono::high_resolution_clock::now();

    //cerr<<"Debug jacbian.."<<endl;
    //pose_fg.debug_print();
    //return true;
    //pose_fg.debug_jacobian();
    //return true;
    //return true;
    cerr<<"Optimizing: no geodesic, constraint"<<endl;

    //cerr<<"###### Optimizing all variables ...."<<endl;
    pose_fg.optimize(current_estimate,err,err_b);
    //current_estimate=pose_fg.initialEstimate;

    finish = std::chrono::high_resolution_clock::now();
    cerr<<"###Time to optimize graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;


    
    W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));

    R=current_estimate.at<gtsam::Rot3>(gtsam::Symbol('R',pose_fg.l_step));
    
    mass_=W.head(1);
    com_=W.segment(1,3);
    
    cerr<<"Optimization error: "<<err<<endl;
    
    cerr<<"Mass: "<<mass_.transpose()<<endl;
    cerr<<"Center of Mass: "<< (com_).transpose()<<endl;
    inertia_diag = (gtsam::Matrix33() <<
                                    W[4+1] + W[4+2],0,0,
                                    0,W[4+0] + W[4+2],0,
                                    0,0,W[4+0]+ W[4+1]).finished();
    
     inertia_matrix = R.matrix() * inertia_diag * R.matrix().transpose() ;// * inertia_diag 

    inertia_com = inertia_matrix + mass_(0) * com_ * com_.transpose();
    
    cerr<<"Inertia"<<endl<<inertia_matrix<<endl;
    cerr<<"Inertia com"<<endl<<inertia_com<<endl;
    cout<<R.ypr()<<endl;

    cout<<"Data format:"<<endl;
    cout<<mass_(0)<<","<<com_(0)<<","<<com_(1)<<","<<com_(2)<<","<<
      inertia_matrix(0,0)<<","<<inertia_matrix(1,1)<<","<<inertia_matrix(2,2)<<","<<
      inertia_matrix(0,1)<<","<<inertia_matrix(0,2)<<","<<inertia_matrix(1,2)<<endl;

    return true;
    cerr<<"Optimizing: geodesic , constraint"<<endl;
    
    pose_fg.update_prior(w_model_prior,r_prior);

    reset();
    current_estimate = gtsam::Values(); 

    pre_step = 0;
    start = std::chrono::high_resolution_clock::now();

    // optimize pose from data array:
    for(int i=pre_step;i<data_arr.size(); ++i)
    {
      pose_fg.update_hybrid_so3_manifold_graph(
                                      data_arr[i].obj_pose,
                                      data_arr[i].bt_data,
                                      data_arr[i].w_cpts,
                                      data_arr[i].g,
                                      data_arr[i].t_step,true,true);
    }
    finish = std::chrono::high_resolution_clock::now();
    cerr<<"Time to load graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
    start = std::chrono::high_resolution_clock::now();


    //cerr<<"Debug jacbian.."<<endl;
    //pose_fg.debug_print();
    //return true;
    //pose_fg.debug_jacobian();
    //return true;
    //return true;
    pose_fg.optimize(current_estimate,err,err_b);
    //current_estimate=pose_fg.initialEstimate;

    finish = std::chrono::high_resolution_clock::now();
    cerr<<"###Time to optimize graph: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;


    W=current_estimate.at<gtsam::Vector7>(gtsam::Symbol('W',pose_fg.l_step));

    R=current_estimate.at<gtsam::Rot3>(gtsam::Symbol('R',pose_fg.l_step));
    
    mass_=W.head(1);
    com_=W.segment(1,3);
    
    cerr<<"Optimization error: "<<err<<endl;
    
    cerr<<"Mass: "<<mass_.transpose()<<endl;
    cerr<<"Center of Mass: "<< (com_).transpose()<<endl;
    inertia_diag = (gtsam::Matrix33() <<
                                    W[4+1] + W[4+2],0,0,
                                    0,W[4+0] + W[4+2],0,
                                    0,0,W[4+0]+ W[4+1]).finished();
    
     inertia_matrix = R.matrix() * inertia_diag * R.matrix().transpose() ;// * inertia_diag 

    inertia_com = inertia_matrix + mass_(0) * com_ * com_.transpose();
    
    cerr<<"Inertia"<<endl<<inertia_matrix<<endl;
    cerr<<"Inertia com"<<endl<<inertia_com<<endl;

    cout<<"Data format:"<<endl;
    cout<<mass_(0)<<","<<com_(0)<<","<<com_(1)<<","<<com_(2)<<","<<
      inertia_matrix(0,0)<<","<<inertia_matrix(1,1)<<","<<inertia_matrix(2,2)<<","<<
      inertia_matrix(0,1)<<","<<inertia_matrix(0,2)<<","<<inertia_matrix(1,2)<<endl;

    
    opt_mass = mass_(0);
    opt_com = com_;
    // inertia at body frame:
    opt_inertia_matrix = inertia_matrix;
    opt_data = data_arr;

    
    for(int i=0;i<data_arr.size();++i)
    {
      opt_data[i].obj_pose = current_estimate.at<gtsam::Pose3>(gtsam::Symbol('x',i));
      // get optimized force data:
      Vector12 bt_force = current_estimate.at<gtsam::Vector12>(gtsam::Symbol('b',i));
      //Vector12 bt_pts = current_estimate.at<gtsam::Vector12>(gtsam::Symbol('c',i));
      for(int j=0;j<4;++j)
      {
        // get frame:
        Pose3 w_cpt = current_estimate.at<gtsam::Pose3>(gtsam::Symbol('c',i*4+j));
        Pose3 obj_bt = opt_data[i].obj_pose.inverse() * w_cpt;
        // tranform bt data to object frame:

        //Vector3 bt_f = opt_data[i].bt_data[j*3+1];
        opt_data[i].bt_data[j*3] = obj_bt.translation();
        opt_data[i].bt_data[j*3+1] = obj_bt.rotation() * bt_force.segment(3*j,3);
        opt_data[i].w_cpts[j] = w_cpt;
      }
    }
    char t;
    cout<<"Optimized, publish trajectory?"<<endl;
    cin>>t;
    publish_trajectory(opt_data,data_arr);
    return true;
    //publish_trajectory(opt_data,data_arr);

    //return true;
        
    // set inertia prior to new values:
    //w_i<<mass_(0),0.0902,-0.03339,0.03145,0.0005,0.002046,0.002062;
    w_i = W;
    Vector3 r_linear;
    r_linear<< inertia_matrix(0,1), inertia_matrix(0,2), inertia_matrix(1,2);
    //r_linear<< -0.000541,0.000514,-0.000188;

    opt_mass = w_i(0);
    opt_com = w_i.segment(1,3);
    // inertia at body frame:
    opt_inertia_matrix<<w_i(4),r_linear(0),r_linear(1),
      r_linear(0), w_i(5), r_linear(2),
      r_linear(2), r_linear(1), w_i(6);
    
    pose_fg.update_prior(w_i,r_linear);
    pose_fg.smoothing();

    reset();

    
      
    // optimize without inertia:
    current_estimate = gtsam::Values(); 

    pre_step = 0;
    
    // optimize pose from data array:
    for(int i=pre_step;i<data_arr.size(); ++i)
    {
      pose_fg.update_hybrid_so3_manifold_graph(
                                      data_arr[i].obj_pose,
                                      data_arr[i].bt_data,
                                      data_arr[i].w_cpts,
                                      data_arr[i].g,
                                      data_arr[i].t_step,false,false,true);
    }
    
    //cerr<<"Debug jacbian.."<<endl;
    //pose_fg.debug_print();
    //return true;
    //pose_fg.debug_jacobian();
    //return true;
    return true;
    cerr<<"###### Optimizing with fixed inertia...."<<endl;
    pose_fg.optimize(current_estimate,err,err_b);
    //return true;

    return true;
  }
  bool publish_trajectory( const vector<sysid_manipulation::sysid_data> &opt_data,
                           const vector<sysid_manipulation::sysid_data> &org_data)
  {
    for(int i=0;i<org_data.size();++i)
    {
      visualize_pose(org_data[i].obj_pose,pose_data.header.frame_id);
      visualize_forces(opt_data[i]);
      pub_loop_rate->sleep();

    }
}
  bool reset()
  {
    pose_fg.initialize_graph();
    t_step=0;
    reset_graph=false;
   
  }
  void run()
  {
    while(nh->ok())
    {
      ros::spinOnce();
      loop_rate->sleep();
    }
  }
  
};


int main(int argc, char** argv)
{

  // set memory limits:
   struct rlimit old_lim, lim, new_lim; 
  
    // Get old limits 
    if( getrlimit(RLIMIT_STACK, &old_lim) == 0) 
        printf("Old limits -> soft limit= %ld \t"
           " hard limit= %ld \n", old_lim.rlim_cur,  
                                 old_lim.rlim_max); 
    else
        fprintf(stderr, "%s\n", strerror(errno)); 
      
    // Set new value 
    lim.rlim_cur = 536870912;//134217728;  // 536870912
    lim.rlim_max = old_lim.rlim_max; 
  
    // Set limits 
    if(setrlimit(RLIMIT_STACK, &lim) == -1) 
        fprintf(stderr, "%s\n", strerror(errno)); 
      
    // Get new limits 
    if( getrlimit(RLIMIT_STACK, &new_lim) == 0) 
        printf("New limits -> soft limit= %ld "
         "\t hard limit= %ld \n", new_lim.rlim_cur,  
                                  new_lim.rlim_max); 
    else
        fprintf(stderr, "%s\n", strerror(errno));

    //return 0;
  ros::init(argc,argv,"pose_gtsam");
  ros::NodeHandle nh;
  poseClient p_cl;
  
  p_cl.init(nh);

  ROS_INFO("Initialized");
  p_cl.run();
  
                                 
  //results.print("Optimized Results\n");

  return 0;
  
}

