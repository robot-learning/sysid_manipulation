// This factor graph takes input from 
#include <ros/ros.h>
#include <fstream>

#include <sysid_manipulation/inertial_problem_isam.hpp>
#include <sysid_manipulation/MeasurementData.h>
#include <sysid_manipulation/resetGraph.h>
#include <geometry_msgs/PoseStamped.h>
vector<Eigen::Vector3d> forces,cpts,ft_data;
Eigen::Vector3d g_vec;
bool got_new_data=false;
bool reset_graph=false;

bool graph_call(sysid_manipulation::resetGraph::Request &req,sysid_manipulation::resetGraph::Response &res)
{
  reset_graph=true;
  res.success=true;
  
  return true;
}

void md_cb(sysid_manipulation::MeasurementData msg)
{
  forces.clear();
  cpts.clear();
  ft_data.clear();
  for(int i=0;i<msg.forces.size();++i)
  {
    forces.push_back(Eigen::Vector3d(msg.forces[i].wrench.force.x,
                                  msg.forces[i].wrench.force.y,
                                  msg.forces[i].wrench.force.z));
  }
  for(int i=0;i<msg.cpts.size();++i)
  {
    cpts.push_back(Eigen::Vector3d(msg.cpts[i].pose.position.x,
                                msg.cpts[i].pose.position.y,
                                msg.cpts[i].pose.position.z));
  }
    
  g_vec=Eigen::Vector3d(msg.g_vec.pose.position.x,
                        msg.g_vec.pose.position.y,
                        msg.g_vec.pose.position.z);
  // get force torque data:
  //if(msg.ft_force
  ft_data.push_back(Eigen::Vector3d(msg.ft_force.wrench.force.x,msg.ft_force.wrench.force.y,
                                    msg.ft_force.wrench.force.z));
                 
  ft_data.push_back(Eigen::Vector3d(msg.ft_force.wrench.torque.x,msg.ft_force.wrench.torque.y,
                                    msg.ft_force.wrench.torque.z));

  ft_data.push_back(Eigen::Vector3d(msg.ft_cpt.pose.position.x, msg.ft_cpt.pose.position.y,
                                    msg.ft_cpt.pose.position.z));
                 

                    
  got_new_data=true;
}
  
int main(int argc, char** argv)
{
  ros::init(argc,argv, "online_inertial_estimation");
  ros::NodeHandle nh;

  int t_step=0;
  int max_tstep=5000;
  inertialEst inertial_est;
  ros::Rate loop_rate(30);
  ros::Subscriber sub = nh.subscribe("/sysid/data", 10, md_cb);
  ros::ServiceServer service = nh.advertiseService("/sysid/reset_graph", graph_call   );
  inertial_est.initialize_graph();
  Values current_estimate;
  ros::Publisher pub=nh.advertise<geometry_msgs::PoseStamped>("/sysid/com",1);
  while(nh.ok())
  {
    geometry_msgs::PoseStamped com_data;
    if(reset_graph)
    {
      inertial_est.initialize_graph();
      t_step=0;
      reset_graph=false;
    }
    if(got_new_data && t_step<max_tstep)
    {
      inertial_est.update_graph(forces,g_vec,cpts,ft_data);
      if(t_step%15==0)
      {
        inertial_est.optimize(current_estimate);
        current_estimate.at(Symbol('M',0)).print("Optimized mass: ");
        current_estimate.at(Symbol('m',0)).print("Optimized mass(com): ");        
        current_estimate.at(Symbol('C',0)).print("Optimized center of mass: ");
        Vector3 com=current_estimate.at<Point3>(Symbol('C',0));
        //Vector3 com= com_v.cast<Vector3>();//.cast();
        com_data.header.frame_id="object";
        com_data.pose.position.x=com[0];
        com_data.pose.position.y=com[1];
        com_data.pose.position.z=com[2];
        com_data.pose.orientation.w=1.0;
        pub.publish(com_data);
        
      }
      //cerr<<t_step<<endl;
      t_step++;
      got_new_data=false;
    }

    if(t_step>=max_tstep)
    {
      break;
    }
    ros::spinOnce();
    loop_rate.sleep();
 
  }
  

                                 
  //results.print("Optimized Results\n");

  return 0;
  
}
