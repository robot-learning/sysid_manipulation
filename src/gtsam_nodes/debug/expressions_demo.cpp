/* ----------------------------------------------------------------------------

 * GTSAM Copyright 2010, Georgia Tech Research Corporation,
 * Atlanta, Georgia 30332-0415
 * All Rights Reserved
 * Authors: Frank Dellaert, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

/**
 * @file InverseKinematicsExampleExpressions.cpp
 * @brief Implement inverse kinematics on a three-link arm using expressions.
 * @date April 15, 2019
 * @author Frank Dellaert
 */
#include <gtsam/base/Matrix.h>
#include <gtsam/geometry/Pose2.h>
#include <gtsam/nonlinear/ExpressionFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/expressions.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/expressions.h>

#include "gtsam_factors/sysid_expressions.h"

#include <cmath>


using namespace std;
using namespace gtsam;
using namespace sysid_expr;

typedef Eigen::Matrix< double, 6, 1 > 	Vector6d;

inline  Matrix33 skew(const Eigen::Vector3d &v) 
{

  return (Matrix33() <<
                      0, -v[2], v[1],
                      v[2], 0 , -v[0],
                      -v[1], v[0], 0).finished();
}


Eigen::VectorXd inertialError( const Eigen::VectorXd &inertia,
                               const gtsam::Rot3 &R_i,
                               const gtsam::Pose3 &x_t_2,
                               const gtsam::Pose3 &x_t_1,const gtsam::Pose3 &x_t, const double &dt,
                               const Eigen::Vector3d &g_,
                               const Vector6d &net_f_tau)
{
  // inertia = i_xx,i_yy,i_zz,i_xy,i_xz,i_yz
  // build inertia matrix:
  Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();
  Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) *
    
  Matrix33 skew_rc = skew(inertia.segment(1,3));

  Vector3 q_rc = (Vector3()<<inertia(1), inertia(2), inertia(3)).finished(); 

  Vector6d vel =(gtsam::Pose3::Logmap(x_t_1) - gtsam::Pose3::Logmap(x_t_2))/dt;
  //cerr<<vel.transpose()<<endl;
  Matrix33 skew_vel = skew(vel.head(3));

  
  // build acceleration product tensor:
  gtsam::Matrix66 acc_prod;
  acc_prod.setZero();
  acc_prod.block(0,0,3,3) = inertia[0] * gtsam::Matrix33().Identity();
  acc_prod.block(0,3,3,3) = inertia[0] * skew_rc;
  acc_prod.block(3,0,3,3) = -inertia[0] * skew_rc;
  acc_prod.block(3,3,3,3) = inertia_matrix;
  
  
  gtsam::Matrix61 acc_bias;
  acc_bias.setZero();
  
  acc_bias.block(0,0,3,1) = net_f_tau.tail(3) + inertia[0] * g_ - skew_vel * skew_vel * q_rc *inertia[0];
  
  acc_bias.block(3,0,3,1) = net_f_tau.head(3) - skew_vel * inertia_matrix * vel.head(3)
    - inertia[0] * skew_rc * g_;

  Vector6d acc_temp = acc_prod.inverse() * acc_bias;

  
  
    // compute error in Lie algebra:
    gtsam::Matrix61 B;
    gtsam::Matrix66 A;
    A.block(0,0,3,6) = acc_prod.block(3,0,3,6);
    A.block(3,0,3,6) = acc_prod.block(0,0,3,6);
    
    B.block(0,0,3,1) = acc_bias.block(3,0,3,1);
    B.block(3,0,3,1) = acc_bias.block(0,0,3,1);
    Vector6d retVal_log = A*(gtsam::Pose3::Logmap(x_t) - 2.0 * gtsam::Pose3::Logmap(x_t_1) + gtsam::Pose3::Logmap(x_t_2)) - B * dt *dt;
    cerr<<"Numeric"<<endl;
    cerr<<(B*dt*dt).transpose()<<endl;
    return retVal_log;
}


Vector6 compute_symbolic_error(  const Vector7 &w_,
                                 const Rot3 &R,
                                 const Pose3 &x_t_2, const Pose3 &x_t_1, const Pose3 &x_t,
                                 const double &dt,
                                 const Vector3 &g,
                                 const Vector6 &f_tau)
{
  
  Expression<double> dt_ =dt;
  Expression<Vector3> g_('g',0);// = g;

  
  // base values:
  Values initial;
  initial.insert(Symbol('g',0),g);
  initial.insert(Symbol('w',0),w_);
  // derived values:
  Expression<Vector7> w('w',0);
  Expression<Vector3> omega = pose_vel_(x_t_2,x_t_1,dt_);
  Expression<Vector1> m = Mass(w);
  Expression<Vector3> r_c = Com(w);
  Expression<Matrix33> H = Inertia(w,R);
  Expression<Matrix33> skew_rc = Skew(r_c);
  Expression<Matrix33> skew_omega = Skew(omega);

  Expression<Vector6> x_diff = pose_acc_(x_t_2,x_t_1,x_t);
  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,r_c,H);
  // B matrix
  Vector6_ b1a = b1_a_(m,g_);
  Vector6_ b1b = b1_b_(m,r_c,skew_omega);
  Vector6_ b2a = b2_a_(H,omega,skew_omega);
  Vector6_ b2b = b2_b_(m,g_,skew_rc);
  Vector6_ net_f_tau = f_tau;
  Vector6_ dyn_bias = net_f_tau + b1a+b1b+b2a+b2b;
  Vector6_ dyn_bias_rate = AccRate(dyn_bias,dt_);
  Vector6_ err = dynamics_error_(spatial_H,dyn_bias_rate,x_diff);

  NonlinearFactorGraph new_graph;

  //cerr<<spatial_H.value(initial)<<endl;
  
  cerr<<(dyn_bias_rate.value(initial)).transpose()<<endl;
  //cerr<<err.value(initial)<<endl;
  //err.print();
  Vector6 retVal = err.value(initial);// = err.value;
  return retVal; 

}
// Main function
int main(int argc, char** argv)

{
  // create initial parameters:
  Vector3 g;
  g<< 0,0,-9.8;

  Eigen::VectorXd w;
  w.resize(7);
  w<< 1.3, 0.1,0.1,0.1, 0.25,0.25,0.25;

  Rot3 R;
  Pose3 x_t_2,x_t_1,x_t;
  
  Vector6 net_f_tau;
  net_f_tau.setConstant(0.2);
  double dt=0.1;


  Eigen::VectorXd num_err = inertialError(w,R,x_t_2,x_t_1,x_t,dt,g,net_f_tau);
  //cerr <<num_err.transpose()<<endl;
  
  Eigen::VectorXd sym_err = compute_symbolic_error(w,R,x_t_2,x_t_1,x_t,dt,g,net_f_tau);

  // create symbolic expression:
  Values initial;
  Vector7 w_ = w.head(7);
  w_(0) = 0.1;
  initial.insert(Symbol('W',0), w_);
  initial.insert(Symbol('R',0), Rot3());
  initial.insert(Symbol('x',0), Pose3());
  initial.insert(Symbol('x',1), Pose3());

  Vector6_ exp = create_symbolic_dynamics(Symbol('W',0),Symbol('R',0),x_t_2,x_t_1,Symbol('x',0),dt,g,net_f_tau);


  NonlinearFactorGraph graph;
  Vector6 des_err;
  des_err.setZero();
  auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  graph.addExpressionFactor(model,des_err,exp);// des_err, model);
  // add second factor:
  Vector6_ exp1 = create_symbolic_dynamics(Symbol('W',0),Symbol('R',0),x_t_2,x_t_1,Symbol('x',1),dt,g,net_f_tau); 
  graph.addExpressionFactor(model,des_err,exp1);// des_err, model);
 
  
  cout<<exp.value(initial).transpose()<<endl;
  // Optimize the initial values using a Levenberg-Marquardt nonlinear optimizer
  LevenbergMarquardtParams params;
  params.setVerbosity("ERROR");

  params.setlambdaInitial(1e6);
  LevenbergMarquardtOptimizer optimizer(graph, initial, params);
  Values result = optimizer.optimize();
  result.print("Final Result:\n");

  cout<<exp.value(result).transpose()<<endl;

  //cerr <<sym_err.transpose()<<endl;

   
  // A matrix:
  // input parameters:

  // Setup factor?
  
  /*
  // Create a factor graph with a a single expression factor.
  ExpressionFactorGraph graph;
  Pose2 desiredEndEffectorPose(3, 2, 0);
  auto model = noiseModel::Diagonal::Sigmas(Vector3(0.2, 0.2, 0.1));
  graph.addExpressionFactor(forward, desiredEndEffectorPose, model);

  // Create initial estimate
  Values initial;
  initial.insert(Q(1), 0.1);
  initial.insert(Q(2), 0.2);
  initial.insert(Q(3), 0.3);
  initial.print("\nInitial Estimate:\n");  // print
  GTSAM_PRINT(forward.value(initial));

  // Optimize the initial values using a Levenberg-Marquardt nonlinear optimizer
  LevenbergMarquardtParams params;
  params.setlambdaInitial(1e6);
  LevenbergMarquardtOptimizer optimizer(graph, initial, params);
  Values result = optimizer.optimize();
  result.print("Final Result:\n");

  GTSAM_PRINT(forward.value(result));
  */
  return 0;
}

