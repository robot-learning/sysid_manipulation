#include<>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/geometry/Rot3.h>
#include <gtsam/inference/Symbol.h>
//#include <gtsam/slam/BetweenFactor.h>
//#include <sysid_manipulation/pose_problem_isam.hpp>
//#include "gtsam_factors/pose_factors.h"
#include "gtsam_factors/dynamics_factors.h"

#include <CppUnitLite/TestHarness.h>
using namespace std;
using namespace gtsam;
//using namespace gtsam::symbol_shorthand;
using namespace gtsam::noiseModel;
/*
bool test_velocity()
{
  double dt=0.1;
  Vector6 n_d;
  n_d.setIdentity();
  n_d=n_d*0.000;
  noiseModel::Diagonal::shared_ptr poseNoiseModel=noiseModel::Diagonal::Sigmas(n_d);
  Symbol p1=Symbol('a',0);
  Symbol p2=Symbol('a',1);
  Symbol p3=Symbol('a',2);

  Pose3 test;
  //cerr<<test<<endl;
  Pose3 x1(gtsam::Rot3::Ypr(1,0,0),
           gtsam::Point3(0,0,1));
  
  Pose3 x2(gtsam::Rot3::Ypr(-0.1,0.1,0),
           gtsam::Point3(0,4,0));
  
  Pose3 xd(gtsam::Rot3::Ypr(0,0,0.1),
           gtsam::Point3(0,0,2));;

  Vector6 vd=Vector6::Zero();
  vd.setZero();

  // test factor:



  PoseCDFactor factor(p1,p2,p3,poseNoiseModel,dt);



  
  Matrix actualH1, actualH2, actualH3;

  Vector actual = factor.evaluateError(x1, x2, vd, actualH1, actualH2, actualH3);
  cerr<<actual<<endl;
  Matrix numericalH1 = numericalDerivative31<Vector6,Pose3, Pose3,Vector6>
    ( boost::function<Vector(const Pose3&,const Pose3&, const Vector6&)>(boost::bind( &PoseCDFactor::evaluateError,
                                                                                    factor, _1,
                                                                                    _2, _3,
                                                                                    boost::none,
                                                                                    boost::none,
                                                                                    boost::none)),
      x1, x2, vd,1e-5);


  
  
  cerr<<"Implemented der:"<<endl;
  cerr<<actualH1<<endl;
  cerr<<"FD:"<<endl;
  cerr<<numericalH1<<endl;


  Matrix numericalH2 = numericalDerivative32<Vector6,Pose3, Pose3,Vector6>
    ( boost::function<Vector(const Pose3&,const Pose3&, const Vector6&)>(boost::bind( &PoseCDFactor::evaluateError,
                                                                                    factor, _1,
                                                                                    _2, _3,
                                                                                    boost::none,
                                                                                    boost::none,
                                                                                    boost::none)),
      x1, x2, vd,1e-5);


  
  
  
  
  cerr<<"Implemented der:"<<endl;
  cerr<<actualH2<<endl;
  cerr<<"FD:"<<endl;
  cerr<<numericalH2<<endl;

  
  Matrix numericalH3 = numericalDerivative33<Vector6,Pose3, Pose3,Vector6>
    ( boost::function<Vector(const Pose3&,const Pose3&, const Vector6&)>(boost::bind( &PoseCDFactor::evaluateError,
                                                                                    factor, _1,
                                                                                    _2, _3,
                                                                                    boost::none,
                                                                                    boost::none,
                                                                                    boost::none)),
      x1, x2, vd,1e-5);


  
  
  
  cerr<<"Implemented der:"<<endl;
  cerr<<actualH3<<endl;
  cerr<<"FD:"<<endl;
  cerr<<numericalH3<<endl;

 
}

bool test_ft_acc()
{
  cout<<"AccFtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol a=Symbol('a',0);
  Symbol tau=Symbol('n',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  

  Vector3 force(0.0,0.1,0.0);
  
  Vector3 torque(0.0,0.1,0.0);
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  
  Vector10 w_dynamics;
  w_dynamics.tail(6) = Matrix61().Constant(1.0);
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  AccFtFactor factor(a,f,tau,W,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4;
  Vector actual = factor.evaluateError(acc,force,torque,w_dynamics,actualH1,actualH2,actualH3, actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;


  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative44<Vector6,Vector6,Vector3,Vector3,Vector10>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector10&)>
     (boost::bind( &AccFtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, torque, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;

  return true;

}
*/
bool test_manifold_ft_acc()
{
  cout<<"ManifoldAccFtFactor()"<<endl;
  //noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector3().Identity()*1e-8);
  Symbol a=Symbol('a',0);
  Symbol tau=Symbol('n',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));
  Vector3 force(0.0,0.0,0.0);
  
  Vector3 torque(0.1,-8,0.1);
  Vector3 g_v(0.0,0.0,-0.8);

  Vector6 acc = (Matrix61()<<10,-1,10,
                             0.0,0.0,0.0).finished();
  
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  
  ManifoldAccFtFactor factor(a,f,tau,W,R,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4,actualH5;
  Vector actual = factor.evaluateError(acc,force,torque,w_dynamics,r_mat,actualH1,actualH2,actualH3, actualH4,actualH5);
  cout<<"Factor error: "<<actual.transpose()<<endl;

  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative51<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative52<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative53<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 = numericalDerivative54<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccFtFactor::gradientTester, factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 5: ";
  Matrix numericalH5 = numericalDerivative55<Vector6,Vector6,Vector3,Vector3,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector3&,
                            const Vector3&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccFtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, torque, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH5,actualH5,1E-5))  cout<<"Passed"<<endl;

  return true;

}
bool test_manifold_bt_acc()
{
  cout<<"ManifoldAccBtFactor()"<<endl;
  Symbol a=Symbol('a',0);
  Symbol r=Symbol('r',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  
  Symbol R=Symbol('R',0);  

  Rot3 r_mat(gtsam::Rot3::Ypr(0.4,-1.1,0.1));

  Vector12 force;
  force.setConstant(0.1);
  
  Vector12 c_pts;
  c_pts.setConstant(-0.1);
  
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  Vector7 w_dynamics;
  w_dynamics.tail(3) = (Matrix31()<<0.3,0.5,0.9).finished();
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(0) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  ManifoldAccBtFactor factor(a,f,r,W,R,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4,actualH5;
  Vector actual = factor.evaluateError(acc,force,c_pts,w_dynamics,r_mat,actualH1,actualH2,actualH3, actualH4,actualH5);
  cout<<"Factor error: "<<actual.transpose()<<endl;
  cout<<"Testing derivatives..."<<endl;
    
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative51<Vector6,Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5)) cout<<"Passed"<<endl;


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative52<Vector6,Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
     (boost::bind( &ManifoldAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative53<Vector6,Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>     (boost::bind( &ManifoldAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
  Matrix numericalH4 =numericalDerivative54<Vector6,Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccBtFactor::gradientTester, factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 5: ";
  Matrix numericalH5 = numericalDerivative55<Vector6,Vector6,Vector12,Vector12,Vector7,Rot3>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector7&, const Rot3&)>
   (boost::bind( &ManifoldAccBtFactor::gradientTester , factor , _1 , _2 , _3 , _4, _5)),
     acc, force, c_pts, w_dynamics,r_mat,1e-5);
    
  if(assert_equal(numericalH5,actualH5,1E-5))  cout<<"Passed"<<endl;

  return true;
}
bool test_physics()
{
  cout<<"PhysicsFactor()"<<endl;
 
  Symbol W=Symbol('W',0);  
  Vector7 w_dynamics;
  w_dynamics.segment(4,3) = Vector3(-0.3,-0.1,-0.8);
  w_dynamics.segment(1,3) = Vector3(0.3,0.1,0.05);
  w_dynamics(0) = 0.01;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector7().setConstant(1e-8));
  PhysicsFactor factor(W,factorNM);
  cerr<<"computing error.."<<endl;
  Matrix actualH1;
  Vector actual = factor.evaluateError(w_dynamics,actualH1);
  cout<<"Factor error: "<<actual.transpose()<<endl;


  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative11<Vector7,Vector7>
    (boost::function<Vector(const Vector7&)>
     (boost::bind( &PhysicsFactor::evaluateError , factor , _1 , boost::none)), w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5))
  {
    cout<<"Passed"<<endl;
  }
  else
  {
    return false;
  }

}
/*
bool test_bt_acc()
{
  cout<<"AccBtFactor()"<<endl;
  Symbol a=Symbol('a',0);
  Symbol r=Symbol('r',0);
  Symbol f=Symbol('f',0);
  Symbol W=Symbol('W',0);  

  Vector12 force;
  force.setConstant(0.1);
  
  Vector12 c_pts;
  c_pts.setConstant(-0.1);
  
  Vector3 g_v(0.5,1.0,-9.8);

  Vector6 acc = (Matrix61()<<0.1,0.2,0.4,
                             0.1,0.4,0.2).finished();
  
  Vector10 w_dynamics;
  w_dynamics.head(6) = Matrix61().Constant(1.0);
  w_dynamics.segment(6,3) = Vector3(0.3,0.1,0.8);
  w_dynamics(9) = 0.1;
  
  noiseModel::Diagonal::shared_ptr factorNM = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-8));
  AccBtFactor factor(a,f,r,W,factorNM,g_v);
  
  Matrix actualH1, actualH2, actualH3, actualH4;
  Vector actual = factor.evaluateError(acc,force,c_pts,w_dynamics,actualH1,actualH2,actualH3, actualH4);
  cout<<"Factor error: "<<actual.transpose()<<endl;


  cout<<"Testing derivatives..."<<endl;
  cout<<"Variable 1: ";
  Matrix numericalH1 = numericalDerivative41<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH1,actualH1,1E-5))
  {
    cout<<"Passed"<<endl;
  }
  else
  {
    return false;
  }


  cout<<"Variable 2: ";
  Matrix numericalH2 = numericalDerivative42<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  
  if(assert_equal(numericalH2,actualH2,1E-5)) cout<<"Passed"<<endl;
  
  cout<<"Variable 3: ";
  Matrix numericalH3 = numericalDerivative43<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  
  if(assert_equal(numericalH3,actualH3,1E-5))  cout<<"Passed"<<endl;
  
  cout<<"Variable 4: ";
    Matrix numericalH4 = numericalDerivative44<Vector6,Vector6,Vector12,Vector12,Vector10>
    (boost::function<Vector(const Vector6&, const Vector12&,
                            const Vector12&, const Vector10&)>
     (boost::bind( &AccBtFactor::evaluateError , factor , _1 , _2 , _3 , _4, boost::none, boost::none, boost::none, boost::none)),
     acc, force, c_pts, w_dynamics,1e-5);
  
  
  if(assert_equal(numericalH4,actualH4,1E-5))  cout<<"Passed"<<endl;

  return true;

}
*/
int main (int argc, char** argv)
{
  //test_ft_acc();
  test_manifold_ft_acc();
  test_manifold_bt_acc();
  //test_bt_acc();
  //test_physics();
  return 0;
}
