// This file tests your dart installation
/*
 * Copyright (c) 2015-2016, Graphics Lab, Georgia Tech Research Corporation
 * Copyright (c) 2015-2016, Humanoid Lab, Georgia Tech Research Corporation
 * Copyright (c) 2016, Personal Robotics Lab, Carnegie Mellon University
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 *   Redistribution and use in source and binary forms, with or
 *   without modification, are permitted provided that the following
 *   conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 *   CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
 *   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 *   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *   AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 */

#include <dart/dart.hpp>
#include <dart/gui/gui.hpp>
#include <dart/utils/urdf/urdf.hpp>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <visualization_msgs/Marker.h>

#define DART_DATA_PATH_ "/home/bala/dart/data/"

const double default_push_force = 0.297*9.8;  // N
const int default_force_duration = 200; // # iterations
const int default_push_duration = 1000;  // # iterations

const double default_endeffector_offset = 0.05;

using namespace dart::common;
using namespace dart::dynamics;
using namespace dart::simulation;
using namespace dart::math;


class MyWindow : public dart::gui::glut::SimWindow
{
public:

  MyWindow(const WorldPtr& world)
    : mTotalAngle(0.0),
      mHasEverRun(false),
      mForceCountDown(0),
      mPushCountDown(0)
  {
    setWorld(world);
    mFirstDomino = world->getSkeleton("object");
    mFloor = world->getSkeleton("floor");
    // initialize ros publishers
    ros::NodeHandle n;
    force1_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force1",1);
    force2_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force2",1);
    force3_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force3",1);
    m_pub = n.advertise<visualization_msgs::Marker>("sim/object", 1);
    pose_pub=n.advertise<geometry_msgs::PoseStamped>("sim/obj_pose",1);
    uint32_t shape = visualization_msgs::Marker::CUBE;

    // Set the frame ID and timestamp.  See the TF tutorials for information on these.

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "basic_shapes";
    marker.id = 0;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker.type = shape;

    // Set the marker action.  Options are ADD and DELETE
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 1.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();

  }
  visualization_msgs::Marker marker;

  ros::Publisher force1_pub,force2_pub,force3_pub,m_pub,pose_pub;
  tf::TransformBroadcaster br_;
  void keyboard(unsigned char key, int x, int y) override
  {
    if(!mHasEverRun)
    {
      switch(key)
      {
        case ' ':
          mHasEverRun = true;
          break;
      }
    }
    else
    {
      switch(key)
      {
        case 'f':
          mForceCountDown = default_force_duration;
          break;

        case 'r':
          mPushCountDown = default_push_duration;
          break;
      }
    }

    SimWindow::keyboard(key, x, y);
  }

  void timeStepping() override
  {
    std::vector<Eigen::Vector3d> local_forces;
    
    std::vector<Eigen::Vector3d> force_locations;
    local_forces.resize(3);
    force_locations.resize(3);
    
    Eigen::Isometry3d w_T_o=mFirstDomino->getBodyNode(0)->getWorldTransform();
    //Eigen::Isometry3d w_T_f1=mFirstDomino->getBodyNode(0)->getWorldTransform();
    
    // apply a force to make object float: 
    // direction 
    Eigen::Vector3d force =1.0* default_push_force * Eigen::Vector3d::UnitZ();
    //location
    Eigen::Vector3d location =  Eigen::Vector3d(-0.01,0.01,0.0);
    Eigen::Isometry3d o_T_f1;
    o_T_f1.setIdentity();
    o_T_f1.translate(location);
    
    Eigen::Isometry3d w_T_f1=w_T_o*o_T_f1;
    
    // get force vec and location
    Eigen::Vector3d local_force=w_T_o.linear().transpose()*force;
    ///Eigen::Vector3d local_force=force;

    //Eigen::Vector3d local_location=w_T_o.inverse()*location;
    local_forces[0]=local_force;
    force_locations[0]=o_T_f1.inverse()*location;
      
    mFirstDomino->getBodyNode(0)->addExtForce(force, location,false,true);
   
    float low=-0.5;
    force =1.0 * Eigen::Vector3d(low+0.01*(rand() % 100),low+0.01*(rand() % 100),low+0.01*(rand() % 100));
    //location
    location =Eigen::Vector3d(0.1,-0.1,0.1);
    local_forces[1]=force;
    force_locations[1]=o_T_f1.inverse()*location;

      
    mFirstDomino->getBodyNode(0)->addExtForce(force, location,true,true);

    force =1.0 * Eigen::Vector3d(low+0.01*(rand() % 100),low+0.01*(rand() % 100),low+0.01*(rand() % 100));
    //location
    location = Eigen::Vector3d(-0.1,0.1,-0.1);

    local_forces[2]=force;
    force_locations[2]=o_T_f1.inverse()*location;
      
    mFirstDomino->getBodyNode(0)->addExtForce(force, location,true,true);

    // send data to ROS
    // convert eigen isometry to transform
    Eigen::Vector3d trans=w_T_f1.translation();
    Eigen::Quaterniond q_(w_T_f1.rotation());

    geometry_msgs::PoseStamped obj_pose;
    obj_pose.header.frame_id="world";
    obj_pose.pose.position.x=trans[0];
    obj_pose.pose.position.y=trans[1];
    obj_pose.pose.position.z=trans[2];

    obj_pose.pose.orientation.x=q_.x();
    obj_pose.pose.orientation.y=q_.y();
    obj_pose.pose.orientation.z=q_.z();
    obj_pose.pose.orientation.w=q_.w();
    obj_pose.header.stamp = ros::Time::now();
        
    pose_pub.publish(obj_pose);
    trans=w_T_o.translation();
    q_=Eigen::Quaterniond(w_T_o.rotation());

    
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(trans[0],trans[1], trans[2]) );
    tf::Quaternion q(q_.x(),q_.y(),q_.z(),q_.w());
    transform.setRotation(q);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world","object"));

    
    // forces:
    
    geometry_msgs::WrenchStamped f1;
    f1.header.frame_id="f1";
    f1.wrench.force.x=local_forces[0][0];
    f1.wrench.force.y=local_forces[0][1];
    f1.wrench.force.z=local_forces[0][2];
    f1.wrench.torque.x=force_locations[0][0];
    f1.wrench.torque.y=force_locations[0][1];
    f1.wrench.torque.z=force_locations[0][2];
    f1.header.stamp=obj_pose.header.stamp;
    transform.setOrigin( tf::Vector3(force_locations[0][0],force_locations[0][1],force_locations[0][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force1_pub.publish(f1);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f1"));

    geometry_msgs::WrenchStamped f2;
    f2.header.frame_id="f2";
    f2.wrench.force.x=local_forces[1][0];
    f2.wrench.force.y=local_forces[1][1];
    f2.wrench.force.z=local_forces[1][2];
    f2.wrench.torque.x=force_locations[1][0];
    f2.wrench.torque.y=force_locations[1][1];
    f2.wrench.torque.z=force_locations[1][2];
    f2.header.stamp=obj_pose.header.stamp;

    transform.setOrigin( tf::Vector3(force_locations[1][0],force_locations[1][1],force_locations[1][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force2_pub.publish(f2);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f2"));
    
    geometry_msgs::WrenchStamped f3;
    f3.header.frame_id="f3";
    f3.wrench.force.x=local_forces[2][0];
    f3.wrench.force.y=local_forces[2][1];
    f3.wrench.force.z=local_forces[2][2];
    f3.wrench.torque.x=force_locations[2][0];
    f3.wrench.torque.y=force_locations[2][1];
    f3.wrench.torque.z=force_locations[2][2];
    f3.header.stamp=obj_pose.header.stamp;

    transform.setOrigin( tf::Vector3(force_locations[2][0],force_locations[2][1],force_locations[2][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force3_pub.publish(f3);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f3"));
    marker.header.frame_id = "/object";
    marker.header.stamp = ros::Time::now();

    m_pub.publish(marker);


    
    SimWindow::timeStepping();
  }

protected:

  /// Base domino. Used to clone new dominoes.
  SkeletonPtr mFirstDomino;

  /// Floor of the scene
  SkeletonPtr mFloor;

  /// History of the dominoes that have been created
  std::vector<SkeletonPtr> mDominoes;

  /// History of the angles that the user has specified
  std::vector<double> mAngles;

  /// Sum of all angles so far
  double mTotalAngle;

  /// Set to true the first time spacebar is pressed
  bool mHasEverRun;

  /// The first domino will be pushed by a disembodied force while the value of
  /// this is greater than zero
  int mForceCountDown;

  /// The manipulator will attempt to push on the first domino while the value
  /// of this is greater than zero
  int mPushCountDown;

  //std::unique_ptr<Controller> mController;

};


SkeletonPtr createCylinder(const Eigen::Isometry3d& _T,
                             double _radius,
                             double _height,
                             double _mass)
{
  dart::dynamics::SkeletonPtr newSkeleton = dart::dynamics::Skeleton::create("object");

  dart::dynamics::ShapePtr newShape(new dart::dynamics::CylinderShape(_radius, _height));

  dart::dynamics::BodyNode::Properties bodyProp;
  bodyProp.mName = "cylinder_link";
  bodyProp.mInertia.setMass(_mass);


  // Set up inertia for the domino
  dart::dynamics::Inertia inertia;
  inertia.setMass(_mass);
  Eigen::Matrix3d H_mat;
  H_mat.setIdentity();
  //std::cerr<<H_mat<<std::endl;
  H_mat=0.1*H_mat;
  inertia.setMoment(H_mat);

  Eigen::Vector3d com;
  com[0]=-0.01;
  com[1]=0.01;
  com[2]=0.0;
  inertia.setLocalCOM(com);
  bodyProp.mInertia=inertia;

  dart::dynamics::FreeJoint::Properties jointProp;
  jointProp.mName = "cylinder_joint";
  jointProp.mT_ParentBodyToJoint = _T;

  
  auto pair = newSkeleton->createJointAndBodyNodePair<dart::dynamics::FreeJoint>(nullptr, jointProp, bodyProp);
  
  auto shapeNode = pair.second->createShapeNodeWith<
    dart::dynamics::VisualAspect,
    dart::dynamics::CollisionAspect,
    dart::dynamics::DynamicsAspect>(newShape);

  //shapeNode->getVisualAspect()->setColor(math::randomVector<3>(0.0, 1.0));

  return newSkeleton;
  //mWorld->addSkeleton(newSkeleton);
}
SkeletonPtr createFloor()
{
  SkeletonPtr floor = Skeleton::create("floor");

  // Give the floor a body
  BodyNodePtr body =
      floor->createJointAndBodyNodePair<WeldJoint>(nullptr).second;

  // Give the body a shape
  double floor_width = 10.0;
  double floor_height = 0.01;
  std::shared_ptr<BoxShape> box(
        new BoxShape(Eigen::Vector3d(floor_width, floor_width, floor_height)));
  auto shapeNode
          = body->createShapeNodeWith<VisualAspect, CollisionAspect, DynamicsAspect>(box);
  shapeNode->getVisualAspect()->setColor(dart::Color::Black());

  // Put the body into position
  Eigen::Isometry3d tf(Eigen::Isometry3d::Identity());
  tf.translation() = Eigen::Vector3d(0.0, 0.0, -floor_height / 2.0);
  body->getParentJoint()->setTransformFromParentBodyNode(tf);

  return floor;
}

int main(int argc, char* argv[])
{

  // create a logging file and open it.
  
  ros::init(argc,argv,"dartsim_node");

  
  Eigen::Isometry3d T = Eigen::Isometry3d::Identity();
  double mass=0.297;
  double rad=0.4;
  double ht=0.339;
  const Eigen::Vector3d position = Eigen::Vector3d(0.0,0.0,0);

  T.translation() = position;
  SkeletonPtr cyl = createCylinder(T,rad,ht,mass);
  
  SkeletonPtr floor = createFloor();

  WorldPtr world = std::make_shared<World>();
  //world->addSkeleton(floor);
  world->addSkeleton(cyl);

  
  MyWindow window(world);

  glutInit(&argc, argv);
  window.initWindow(640, 480, "Object force simulation");
  glutMainLoop();
  // write to file
}
