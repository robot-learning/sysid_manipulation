// implementing sysid factors via expressions for automatic differentiation
#include "gtsam_factors/sysid_expressions.h"

namespace sysid_expr // namespace specifically for symbolic functions to avoid conflicts with numeric fns
{

using namespace gtsam;

// helper functions:
/// TODO: make function constexpr, currently due to issues with eigen matrix data() it is not possible.
/// More info here: https://eigen.tuxfamily.org/bz/show_bug.cgi?id=820
Eigen::Map<Eigen::MatrixXd> eigen_reshape (Eigen::MatrixXd b, const size_t &n, const size_t &m) 
{
  return Eigen::Map<Eigen::MatrixXd>(b.data(), n, m);
}

template<class A1>
A1 grad_matrix(const int &i, const int&j)
{
  A1 mat;
  mat.setZero();
  mat(i,j) = 1.0;
  return mat;
}

Eigen::Matrix3d skew_grad_matrix(const int &i)
{
  Eigen::Matrix3d mat;
  mat.setZero();
  switch (i)
  {
    case 0:
      mat(1,2) = -1.0;
      mat(2,1) = 1.0;
      break;
    case 1:
      mat(0,2) = 1.0;
      mat(2,0) = -1.0;
      break;
    case 2:
      mat(1,0) = 1.0;
      mat(0,1) = -1.0;
      break;
    default:
      std::runtime_error("index for skew_grad is out of range(0,2)");
                         
  }
  
  
  return mat;
}


// base functions for building dynamics expression:
Vector1 mass(const Vector7 &w,gtsam::OptionalJacobian<1,7> H1)
{
  Vector1 h = w.head(1);
  if(H1)    
  {
    Eigen::Matrix<double,1,7> gradient;
    gradient.setZero();
    gradient[0,0] =1.0;
    *H1 = gradient;
  }
  return h;
}

Vector3 com(const Vector7 &w,gtsam::OptionalJacobian<3,7> H1 )
{
  Vector3 h = w.segment(1,3);
  if(H1)    
  {
    *H1 = Eigen::Matrix<double,3,7>().setZero();
    H1->block(0,1,3,3) = Eigen::Matrix<double,3,3>().setIdentity();
  }
  return h;
}

// inertia matrix:
Matrix33 inertia(const Vector7 &w, const Rot3 &R,gtsam::OptionalJacobian<9,7> H1, gtsam::OptionalJacobian<9,3> H2)
{
  Matrix33 h_mat =(gtsam::Matrix33() <<
                   w[4+1] + w[4+2],0,0,
                   0,w[4+0] + w[4+2],0,
                   0,0,w[4+0]+ w[4+1]).finished();
  
  Matrix33 h =  R.matrix() * h_mat * R.matrix().transpose();
  
  if(H1)    
  {
    *H1 = Eigen::Matrix<double,9,7>().setZero();

    // 4:
    H1->block(0,4,9,1) = eigen_reshape(R.matrix() * (gtsam::Matrix33() <<
                                                     0,0,0,
                                                     0,1,0,
                                                     0,0,1).finished() * R.matrix().transpose(),9,1);

    // 5:
    H1->block(0,5,9,1) = eigen_reshape(R.matrix() * (gtsam::Matrix33() <<
                                                     1,0,0,
                                                     0,0,0,
                                                     0,0,1).finished() * R.matrix().transpose(),9,1);
    // 6:
    H1->block(0,6,9,1) = eigen_reshape(R.matrix() * (gtsam::Matrix33() <<
                                                     1,0,0,
                                                     0,1,0,
                                                     0,0,0).finished() * R.matrix().transpose(),9,1);

  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,9,3>().setZero();

    for(int i=0;i<3;++i)

    {
      H2->block(0,i,9,1) = eigen_reshape(
                                      skew_grad_matrix(i) * h_mat * R.matrix().transpose()
                                       + R.matrix() * h_mat * skew_grad_matrix(i).transpose(),9,1);
    }
    
  }
  return h;
}



Expression<Matrix33> Inertia(const Expression<Vector7> &w, const Expression<Rot3> &R)
{
  return gtsam::Expression<Matrix33>(&inertia,w,R);
}

Expression<Vector1> Mass(const Expression<Vector7> &w)
{
  return gtsam::Expression<Vector1>(&mass,w);
}
Expression<Vector3> Com(const Expression<Vector7> &w)
{
  return gtsam::Expression<Vector3>(&com,w);
}

Matrix33 skew_fn(const Vector3 &vec,gtsam::OptionalJacobian<9,3> H1)
{
  Matrix33 v_c = (gtsam::Matrix33() <<
                  0, -vec(2), vec(1),
                  vec(2), 0, -vec(0),
                  -vec(1), vec(0), 0
                  ).finished();

  if(H1)
  {
    *H1= Eigen::Matrix<double,9,3>().setZero();

    for (int i=0;i<3;++i)
    {
      H1->block(0,i,9,1) = eigen_reshape(skew_grad_matrix(i),9,1);
    }
  }
  
  return v_c;

}
Expression<Matrix33> Skew(const Expression<Vector3> &v)
{
  return Expression<Matrix33>(&skew_fn,v);
}


//Matrix61 dyn_b1a(const Vector3
/*
template<typename T,class A1, class A2,class A3, class A4,class A5, class A6,class A7, class A8,class A9>
struct nineFunction {
  typedef boost::function<
    T(const A1&, const A2&,const A3&,const A4&,const A5&,const A6&,const A7&,const A8&,const A9&,
      typename MakeOptionalJacobian<T, A1>::type,
      typename MakeOptionalJacobian<T, A2>::type,
      typename MakeOptionalJacobian<T, A3>::type,
      typename MakeOptionalJacobian<T, A4>::type,
      typename MakeOptionalJacobian<T, A5>::type,
      typename MakeOptionalJacobian<T, A6>::type,
      typename MakeOptionalJacobian<T, A7>::type,
      typename MakeOptionalJacobian<T, A8>::type,
      typename MakeOptionalJacobian<T, A9>::type)> type;
};
*/

Matrix66 spatial_inertia(const Vector1 &m, const Vector3 &r_c, const Matrix33 H,gtsam::OptionalJacobian<36,1> H1, gtsam::OptionalJacobian<36,3> H2, gtsam::OptionalJacobian<36,9> H3)
{
  Matrix66 h;

  h.block(3,3,3,3) = m[0] * gtsam::Matrix33::Identity();

  h.block(0,3,3,3) = m[0] * skew_fn(r_c);

  h.block(3,0,3,3) = -1.0 * m[0] * skew_fn(r_c);

  h.block(0,0,3,3) = H;

  if(H1)
  {
    *H1 = Eigen::Matrix<double,36,1>().setZero();
    
    Eigen::Matrix<double,6,6> grad;
    
    grad.setZero();
    grad.block(3,0,3,3) =  -1.0 * skew_fn(r_c);
    grad.block(0,3,3,3) = skew_fn(r_c);
    grad.block(3,3,3,3) = Matrix33::Identity();
    
    *H1 = eigen_reshape(grad,36,1);
  }
  if(H2)    
  {
    *H2 = Eigen::Matrix<double,36,3>().setZero();
    Eigen::Matrix<double,6,6> rc_1;
    for(int i=0;i<3;++i)
    {
      rc_1.setZero();
      rc_1.block(3,0,3,3)= -1.0 * m[0] * skew_grad_matrix(i);
      rc_1.block(0,3,3,3)= m[0] * skew_grad_matrix(i);
      
      H2->block(0,i,36,1) = eigen_reshape(rc_1,36,1);
          
    }
    
  }
  if(H3)
  {
    *H3 = Eigen::Matrix<double,36,9>().setZero();

    Eigen::Matrix<double,6,6> g_mat;
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        g_mat.setZero();
        g_mat.block(0,0,3,3) = grad_matrix<Eigen::Matrix3d>(i,j); //
        //std::cout<<i<<" "<<j<<std::endl<<g_mat<<std::endl;
        H3->block(0,j*3+i,36,1) = eigen_reshape(g_mat,36,1);
      }
    }
  }
    
  
  return h;
}


Expression<Matrix66> SpatialInertia(const  Expression<Vector1> m, const Expression<Vector3> r_c, const Expression<Matrix33> H)
{
  return gtsam::Expression<Matrix66>(&spatial_inertia,m,r_c,H);
}

Vector6 acc_rate(const Vector6 &acc, const double &dt,
                        gtsam::OptionalJacobian<6,6> H1,
                        gtsam::OptionalJacobian<6,1> H2
                        )
{
  Vector6 retVal = acc * dt * dt;

  if(H1)
  {
    *H1 =  Eigen::Matrix<double,6,6>().setIdentity() * dt * dt;
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,1>(2.0 * acc * dt);
  }
  
  return retVal;
  
}

Expression<Vector6> AccRate(const Expression<Vector6> &acc, const Expression<double> &dt)
{
  return Expression<Vector6>(&acc_rate,acc,dt);
}
Vector6 pose_acc(const Pose3 &x_t_2, const Pose3 &x_t_1,const Pose3 &x_t,
                         OptionalJacobian<6, 6> H1,OptionalJacobian<6, 6> H2,OptionalJacobian<6, 6> H3)
{
  Vector6 retVal = Pose3::Logmap(x_t,H3) - 2*Pose3::Logmap(x_t_1,H2) + Pose3::Logmap(x_t_2,H1);
  
  if(H2)
  {
    *H2 = -2.0 * *H2 ;
  }
  return retVal;
}

Vector6_ pose_acc_(const Expression<Pose3> &x_t_2,const Expression<Pose3> &x_t_1,const Expression<Pose3> &x_t)
{
  return Vector6_(&pose_acc, x_t_2, x_t_1, x_t);
}
                          
Vector3 pose_vel(const Pose3 &x_t_2,const Pose3 &x_t, const double &dt, OptionalJacobian<3, 6> H1,OptionalJacobian<3, 6> H2,OptionalJacobian<3, 1> H3)
{

  Matrix66 H1_log,H2_log;
  // TODO: setup null
  /*
  if(!H1)
  {
    H1_log = NULL;
  }
  if(!H2)
  {
    H2_log = NULL;
  }
  */
  Vector3 diff = (Pose3::Logmap(x_t,H2_log) - Pose3::Logmap(x_t_2,H1_log)).head(3);
  Vector3 retVal = diff/(2.0*dt);

  if(H1)
  {
    *H1 = Eigen::Matrix<double,3,6>().setZero();
    H1->block(0,0,3,3) = (-1.0/(2.0*dt)) * H1_log.block(0,0,3,3) ;
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,3,6>().setZero();

    H2->block(0,0,3,3) = (1.0/(2.0*dt)) * H2_log.block(0,0,3,3) ;
  }


  if(H3)
  {
    *H3 = (-1.0/(2.0*dt*dt)) *  diff;
  }


  return retVal;
}

Vector3_ pose_vel_(const Expression<Pose3> &x_t_2, const Expression<Pose3> &x_t_1, const Expression<double> &dt)
{
  return Vector3_(&pose_vel,x_t_2,x_t_1,dt);
}

// bias functions:
Vector6 b1_a(const Vector1 &m, const Vector3 &g,
                   gtsam::OptionalJacobian<6,1> H1,
                   gtsam::OptionalJacobian<6,3> H2)
{
  Vector6 retVal;
  retVal.setZero();
  retVal.tail(3)= m[0] * g;
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,1>().setZero();
    H1->block(3,0,3,1) = g;
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();
    H2->block(3,0,3,3) = m[0] * Matrix33::Identity();

  }
  return retVal;
}
Vector6_ b1_a_(const Vector1_ &m, const Vector3_ &g)
{
  return Vector6_(&b1_a,m,g);
}

Vector6 b1_b(const Vector1 &m, const Vector3 &r_c, const Matrix33 &skew_omega,
                    gtsam::OptionalJacobian<6,1> H1,
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3)
{
  Vector6 retVal;
  retVal.setZero();

  retVal.tail(3) =-m[0] * skew_omega * skew_omega * r_c ; 
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,1>().setZero();
    H1->block(3,0,3,1) =  -1.0 * skew_omega * skew_omega * r_c ;
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();
    H2->block(3,0,3,3) = -m[0] * skew_omega * skew_omega * Matrix33::Identity();
  }

  if(H3)
  {
    *H3 = Eigen::Matrix<double,6,9>().setZero();
    for(int i =0; i<3; ++i)
    {
      for(int j=0;j<3;++j)
      {
        H3->block(3,j*3+i,3,1) = -m[0] * grad_matrix<Eigen::Matrix3d>(i,j) * skew_omega * r_c - m[0] * skew_omega * grad_matrix<Eigen::Matrix3d>(i,j) * r_c;
        
      }
    }
  }
  
  
  return retVal;
}
Vector6_ b1_b_(const Vector1_ &m, const Vector3_ &r_c, const Expression<Matrix33> &skew_omega)
{
  return Vector6_(&b1_b,m,r_c,skew_omega);
}


Vector6 b2_a(const Matrix33 &H, const Vector3 &omega, const Matrix33 &skew_omega,
                    gtsam::OptionalJacobian<6,9> H1,
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3)
{
  Vector6 retVal;
  retVal.setZero();

  retVal.head(3)= -skew_omega * H * omega;
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,9>().setZero();
    for(int i = 0;i<3 ;++i)
    {
      for(int j = 0;j<3;++j)
      {
        H1->block(0,j*3+i,3,1) = -skew_omega * grad_matrix<Eigen::Matrix3d>(i,j) * omega;

      }
    }
    
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();

    H2->block(0,0,3,3) = -skew_omega * H * Matrix33::Identity();
  }
  if(H3)
  {
    *H3 = Eigen::Matrix<double,6,9>().setZero();

    for(int i = 0;i<3 ;++i)
    {
      for(int j = 0;j<3;++j)
      {
        H3->block(0,j*3+i,3,1) = -1.0 * grad_matrix<Eigen::Matrix3d>(i,j) * H * omega;
      }
    }

  }

  return retVal;
}
Vector6_ b2_a_(const Expression<Matrix33> &H, const Vector3_ &omega, const Expression<Matrix33> &skew_omega)
{
  return Vector6_(&b2_a,H,omega,skew_omega);
}

Vector6 b2_b(const Vector1 &m, const Vector3 &g, const Matrix33 &skew_rc,
                    gtsam::OptionalJacobian<6,1> H1,
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3)
{
  Vector6 retVal;
  retVal.setZero();

  retVal.head(3)= m[0] * skew_rc * g;
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,1>().setZero();
    H1->block(0,0,3,1) = 1.0 * skew_rc * g;
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();

    H2->block(0,0,3,3) = m[0] * skew_rc * Matrix33::Identity();
  }
  if(H3)  
  {
    *H3 = Eigen::Matrix<double,6,9>().setZero();

    for(int i = 0;i<3 ;++i)
    {
      for(int j = 0;j<3;++j)
      {
        H3->block(0,j*3+i,3,1) = m[0] * grad_matrix<Eigen::Matrix3d>(i,j) * g;
      }
    }
  }
  return retVal;
}

Vector6_ b2_b_(const Vector1_ &m, const Vector3_ &g, const Expression<Matrix33> &skew_rc)
{
  return Vector6_(&b2_b,m,g,skew_rc);
}


Vector6 dynamics_error(const Matrix66 &A, const Vector6 &B, 
                              const Vector6 &Xt_diff,
                              
                              gtsam::OptionalJacobian<6,36> H1,
                              gtsam::OptionalJacobian<6,6> H2,
                              gtsam::OptionalJacobian<6,6> H3)
{
  Vector6 retVal = A * Xt_diff - B;

  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,36>().setZero();
    for(int i = 0;i<6 ;++i)
    {
      for(int j = 0;j<6;++j)
      {
        H1->block(0,j*6+i,6,1) = grad_matrix<Eigen::Matrix<double,6,6>>(i,j) * Xt_diff;
        //std::cout<<" "<<std::endl;
        //        std::cout<<  grad_matrix<Eigen::Matrix<double,6,6>>(i,j) * Xt_diff<<std::endl;
      }
    }
    
    
  }
  if(H2)
  {
    *H2 = -1.0 * Matrix66::Identity();
  
  }
  if(H3)
  {
    *H3 = A * Matrix66::Identity();
  
  }
  
  return retVal;
}

Vector6_ dynamics_error_(const Expression<Matrix66> &A, const Vector6_ &B, 
                                const Vector6_ &x_diff)
{
  return Vector6_(&dynamics_error,A,B,x_diff);
}

Vector6 net_force_torque(const Vector12 &f, const Vector12 &r_f,
                                gtsam::OptionalJacobian<6,12> H1,
                                gtsam::OptionalJacobian<6,12> H2)
{
  Vector6 f_tau;
  f_tau.setZero();

  f_tau.tail(3) = f.segment(0,3) + f.segment(3,3) + f.segment(6,3) + f.segment(9,3);
  for(int i=0;i<4;++i)
  {
    f_tau.head(3).noalias() +=skew_fn(r_f.segment(i*3,3)) * f.segment(i*3,3) ;
  }

  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,12>().setZero();
    for(int i=0;i<4;++i)
    {
      H1->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * 1.0;
      H1->block(0,i*3,3,3) = skew_fn(r_f.segment(i*3,3)) * gtsam::Matrix33().Identity() * 1.0;
 
    }
    
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,12>().setZero();
    
    for(int i=0;i<4;++i)
    {
      for(int j =0;j<3;++j)
      {
        H2->block(0,i*3+j,3,1) = skew_grad_matrix(j) * f.segment(i*3,3);
      }
    }
  }

  return f_tau;
}

Vector6_ net_force_torque_(const Expression<Vector12> &f, const Expression<Vector12> &r_f)
{
  return Vector6_(&net_force_torque,f,r_f);
  
}

Vector6_ create_symbolic_dynamics(  const Expression<Vector7> &w,
                                    const Expression<Rot3> &R,
                                    const Expression<Pose3> &x_t_2,
                                    const Expression<Pose3> &x_t_1,
                                    const Expression<Pose3> &x_t,
                                    const Expression<double> &dt_,
                                    const Expression<Vector3> &g_,
                                    const Expression<Vector6> &f_tau)
{
  

   
  Expression<Vector3> omega = pose_vel_(x_t_2,x_t,dt_);
  Expression<Vector1> m = Mass(w);
  Expression<Vector3> r_c = Com(w);
  Expression<Matrix33> H = Inertia(w,R);
  Expression<Matrix33> skew_rc = Skew(r_c);
  Expression<Matrix33> skew_omega = Skew(omega);

  Expression<Vector6> x_diff = pose_acc_(x_t_2,x_t_1,x_t);
  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,r_c,H);
  // B matrix
  Vector6_ b1a = b1_a_(m,g_);
  Vector6_ b1b = b1_b_(m,r_c,skew_omega);
  Vector6_ b2a = b2_a_(H,omega,skew_omega);
  Vector6_ b2b = b2_b_(m,g_,skew_rc);
  Vector6_ net_f_tau = f_tau;
  Vector6_ dyn_bias = net_f_tau + b1a+b1b+b2a+b2b;
  Vector6_ dyn_bias_rate = AccRate(dyn_bias,dt_);
  Vector6_ err = dynamics_error_(spatial_H,dyn_bias_rate,x_diff);
  return err;

}



}

