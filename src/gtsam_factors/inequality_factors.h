#pragma once
#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/expressions.h>
#include <gtsam/nonlinear/ExpressionFactorGraph.h>

#include <sysid_manipulation/supp_fns.hpp>
using namespace gtsam;
using namespace std;

class PhysicsFactor : public NoiseModelFactor1<Vector7>
{
private:
  double k_=10000.0;

public:
  PhysicsFactor(Key j1, const SharedNoiseModel& noiseModel):
    NoiseModelFactor1<Vector7>(noiseModel, j1){}
  Vector evaluateError(const Vector7 &w, boost::optional<Matrix&> H1 = boost::none) const
  {
    Vector7 retVal;
    retVal.setZero();
    // mass should be positive:
    retVal(0) = log(1.0+exp(-1.0*w(0)*k_));

    
    // center of mass should be inside the convex hull ( we treat this is as a function:)
    // for box: [0,180], [-61,0], [0,62] mm
    const static Vector3 r_min(-1.6,-1.6,-1.6);
    const static Vector3 r_max(1.6,1.6,1.6);
    
    // setup inequality cost:

    retVal(1) = log(1.0+exp(-1.0*(r_max(0) - w(1))*k_))
      + log(1.0+exp(-1.0 * (w(1) - r_min(0)) * k_));
        

    //retVal(2) = log(1.0+exp(-1.0*(0.061 + w(2))*k_)) + log(1.0+exp(-1.0*( -1.0 * w(2))*k_));
    
    retVal(2) = log(1.0+exp(-1.0*(r_max(1) - w(2))*k_)) +
      log(1.0+exp(-1.0*(w(2)-r_min(1))*k_)); 

    retVal(3) = log(1.0+exp(-1.0*(r_max(2)-w(3))*k_)) + log(1.0+exp(-1.0 * (w(3)-r_min(2)) * k_)) ;

    // inertia diagonal should be positive:
    for(int i = 0; i<3; ++i)
    {
      retVal(4+i) = log(1.0+exp(-1.0*w(4+i)*k_*k_));
    }
    
    if(H1)
    {
      *H1 = Eigen::Matrix<double,7,7>().setZero();
      H1->coeffRef(0,0) = (1.0/(1+exp(-1.0*w(0)*k_))) * exp(-1.0 * k_ * w(0)) * -1.0*k_;

      
      H1->coeffRef(1,1) = (1.0/(1+exp(-1.0*(r_max(0)-w(1))*k_))) * exp(-1.0 * k_ *(r_max(0)- w(1))) * -1.0 * k_
        + (1.0/(1+exp(-1.0*(w(1)-r_min(0))*k_))) * exp(-1.0 * k_ * (w(1)-r_min(0))) * 1.0*k_;
      

      H1->coeffRef(2,2) = (1.0/(1+exp(-1.0 *(r_max(1) - w(2))*k_))) * exp(-1.0 * k_ * (r_max(1) - w(2))) * -1.0*k_
        - (1.0/(1+exp(-1.0*( w(2) - r_min(1))*k_))) * exp(-1.0 * k_ * ( w(2) -r_min(1))) * -1.0*k_;
      
      H1->coeffRef(3,3) =  (1.0/(1+exp(-1.0*(r_max(2)-w(3))*k_))) * exp(-1.0 * k_ *(r_max(2)- w(3))) * -1.0 * k_
        + (1.0/(1+exp(-1.0*(w(3)-r_min(2))*k_))) * exp(-1.0 * k_ * (w(3)-r_min(2))) * 1.0*k_ ;
      
      for(int i = 0; i<3; ++i)
      {
        H1->coeffRef(4+i,4+i) = (1.0/(1+exp(-1.0*w(4+i)*k_*k_))) * exp(-1.0 * k_ * k_ * w(4+i)) * -1.0*k_*k_;

      }
      
    }  
    return retVal;
  }
};
class MassPriorFactor : public NoiseModelFactor1<Vector7>
{
private:
  double k_=10000.0;
  Vector7 prior_;
public:
  MassPriorFactor(Key j1, const SharedNoiseModel& noiseModel,const Vector7 &prior):
    NoiseModelFactor1<Vector7>(noiseModel, j1),prior_(prior){}
  Vector evaluateError(const Vector7 &w, boost::optional<Matrix&> H1 = boost::none) const
  {
    Vector7 retVal;
    
    retVal.setZero();
    const static double m_min = 0.144;
    retVal(0) = log(1.0+exp(-1.0*(w(0)-m_min)*k_));
    if(H1)
    {
      *H1 = Eigen::Matrix<double,7,7>().setZero();
      H1->coeffRef(0,0) = (1.0/(1+exp(-1.0*(w(0)-m_min)*k_))) * exp(-1.0 * k_ * (w(0)-m_min)) * -1.0*k_;

      
    }  
    return retVal;
  }
};

class softMassPriorFactor : public NoiseModelFactor1<Vector7>
{
private:
  double k_=10000.0;
  Vector7 prior_;
public:
  softMassPriorFactor(Key j1, const SharedNoiseModel& noiseModel,const Vector7 &prior):
    NoiseModelFactor1<Vector7>(noiseModel, j1),prior_(prior){}
  Vector evaluateError(const Vector7 &w, boost::optional<Matrix&> H1 = boost::none) const
  {
    Vector7 retVal;
    
    retVal.setZero();
    const static double m_min = 0.144;
    if(w(0)<m_min)
    {
      retVal(0) = (w(0)-m_min)*(w(0)-m_min);
    }
    if(H1)
    {
      *H1 = Eigen::Matrix<double,7,7>().setZero();
      if(w(0)<m_min)
      {
        H1->coeffRef(0,0) = 2.0*(w(0)-m_min);
      }

      
    }  
    return retVal;
  }
};


