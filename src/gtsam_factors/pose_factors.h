#pragma once
#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/geometry/Pose3.h>

using namespace gtsam;

// velocity factor in world frame:
// j1 is pose at timestep t-1, j2= pose (t), j3= velocity
class PoseCDFactor : public NoiseModelFactor3<Pose3,Pose3,Vector6>
{
private:
  double dt_;
public:
  PoseCDFactor(Key j1, Key j2, Key j3, const SharedNoiseModel &model, const double dt=1.0): NoiseModelFactor3<Pose3,Pose3,Vector6>(model,j1,j2,j3), dt_(dt) {}
  
  gtsam::Vector evaluateError(const gtsam::Pose3& x_t_1,
                              const gtsam::Pose3& x_t,
                              const gtsam::Vector6& xd_t,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const
  {

    // compute error:
    
    // convert to se(3) manifold:
    Vector6 rad = xd_t;
    /*
    rad(0)= xd_t(0)*180.0/3.1415;
    rad(1)= xd_t(1)*180.0/3.1415;
    rad(2)= xd_t(2)*180.0/3.1415;
    */
    Vector6 err=(rad*dt_  - (Pose3::Logmap(x_t,H2) - (1)*Pose3::Logmap(x_t_1,H1)));
    if (H1)
    {
     
      *H1 = *H1 * 1.0 ;//* (1.0/dt_) ;
      
    }
    if (H2)
    {
      *H2 = *H2 * -1.0 ;// * (-1.0/dt_) ;
    }
    if (H3)
    {
      *H3 =  gtsam::Matrix66().Identity() * dt_ ;
      
      //H3->block(0,0,3,3)= (180.0/3.1415) * H3->block(0,0,3,3);

    }
    return  err;
  }
};

// velocity factor in local frame:
// j1 is pose at timestep t-1, j2= pose (t), j3= velocity
class LocalPoseCDFactor : public NoiseModelFactor3<Pose3,Pose3,Vector6>
{
private:
  double dt_;
public:
  LocalPoseCDFactor(Key j1, Key j2, Key j3, const SharedNoiseModel &model, const double dt=1.0): NoiseModelFactor3<Pose3,Pose3,Vector6>(model,j1,j2,j3), dt_(dt) {}
  
  gtsam::Vector evaluateError(const gtsam::Pose3& x_t_1,
                              const gtsam::Pose3& x_t,
                              const gtsam::Vector6& xd_t,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const
  {

    // compute error:
    Pose3 result = x_t_1.inverse()*x_t;
    Vector6 err=xd_t*dt_  - Pose3::Logmap(result,H2);//(Pose3::Logmap(x_t,H2) - (1)*Pose3::Logmap(x_t_1,H1)));
    if (H1)
    {

      Pose3::Logmap(result.inverse(),H1);
      
    }
    if (H2)
    {
      *H2 = *H2 * -1.0;//gtsam::Matrix66().Identity()  ;// * (-1.0/dt_) ;
    }
    if (H3)
    {
      *H3 =  gtsam::Matrix66().Identity() * dt_ ;
      
      //H3->block(0,0,3,3)= (180.0/3.1415) * H3->block(0,0,3,3);

    }
    return  err;
  }
};

class PoseAccFactor : public NoiseModelFactor4<Pose3,Pose3,Pose3,Vector6>
{
private:
  double dt_;
public:
  PoseAccFactor(Key j1, Key j2, Key j3, Key j4, const SharedNoiseModel &model, const double dt=1.0): NoiseModelFactor4<Pose3,Pose3,Pose3,Vector6>(model,j1,j2,j3,j4), dt_(dt) {}
  
  gtsam::Vector evaluateError(const gtsam::Pose3& x_t_2,
                              const gtsam::Pose3& x_t_1,
                              const gtsam::Pose3& x_t,
                              const gtsam::Vector6& xdd_t,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none,
                              boost::optional<gtsam::Matrix&> H4 = boost::none) const
  {

    // compute error:
    Vector6 err= xdd_t*dt_*dt_  - (Pose3::Logmap(x_t,H3) - 2*Pose3::Logmap(x_t_1,H2) + Pose3::Logmap(x_t_2,H1));
    if (H1)
    {

      *H1 = *H1 * -1.0;//* (1.0/dt_) ;
      
    }
    if (H2)
    {
      *H2 = *H2 * 2.0;//* (1.0/dt_) ;
    }
    if (H3)
    {
      *H3 = *H3 * -1.0;//* (1.0/dt_) ;

    }
    if (H4)
    {
      *H4 =  gtsam::Matrix66().Identity() * dt_ * dt_;

    }
    return  err;
  }
};

// central difference with dt as optional to measure rate.
template<class VALUE>
class CDFactor : public NoiseModelFactor3<VALUE,VALUE,VALUE>
{
private:
  double dt_;
public:
  CDFactor(Key j1, Key j2, Key j3, const SharedNoiseModel &model, const double dt=1.0): NoiseModelFactor3<VALUE,VALUE,VALUE>(model,j1,j2,j3), dt_(dt) {}
  gtsam::Vector evaluateError(const VALUE& x_t1,
                              const VALUE& x_t2,
                              const VALUE& xd_t2,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const
  {


    Vector retval = (xd_t2*dt_ - ( x_t2 - x_t1 ));

    if (H1)
    {
      *H1 = gtsam::Matrix::Identity(x_t1.size(),x_t1.size()) * 1.0 ;
    }
    if (H2)
    {
      *H2 = gtsam::Matrix::Identity(x_t2.size(),x_t2.size()) * -1.0;

      
    }
    if (H3)
    {
      *H3 = gtsam::Matrix::Identity(xd_t2.size(),xd_t2.size()) * dt_ ;
    }

    return   retval ;
  }
};

template<class VALUE>
class ConstantFactor : public NoiseModelFactor2<VALUE, VALUE>
{
private:
  double w_;
public:
  ConstantFactor(Key j1, Key j2,  const SharedNoiseModel &model, const double weight=1e-6): NoiseModelFactor2<VALUE,VALUE>(model,j1,j2),w_(weight) {}
  gtsam::Vector evaluateError(const VALUE& x1,
                              const VALUE& x2,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none) const
  {


    Vector retval =w_*(x2-x1);

    if (H1)
    {
      *H1 = gtsam::Matrix::Identity(x1.size(),x1.size()) * -1.0 * w_;
    }
    if (H2)
    {
      *H2 = gtsam::Matrix::Identity(x2.size(),x2.size()) * w_;
      
    }
    return   retval ;
  }
};

template<class VALUE>
class SmoothFactor : public NoiseModelFactor3<VALUE, VALUE, VALUE>
{
public:
  SmoothFactor(Key j1, Key j2, Key j3, const SharedNoiseModel &model): NoiseModelFactor3<VALUE,VALUE,VALUE>(model,j1,j2,j3) {}
  gtsam::Vector evaluateError(const VALUE& x_t1,
                              const VALUE& x_t2,
                              const VALUE& x_t3,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const
  {


    Vector retval =x_t3 - 2.0*x_t2 + x_t1;

    if (H1)
    {
      *H1 = gtsam::Matrix::Identity(x_t1.size(),x_t1.size());
    }
    if (H2)
    {
      *H2 = gtsam::Matrix::Identity(x_t2.size(),x_t2.size()) * -2.0;

      
    }
    if (H3)
    {
      *H3 = gtsam::Matrix::Identity(x_t3.size(),x_t3.size()) ;
    }

    return   retval ;
  }
};

class PoseSmoothFactor : public NoiseModelFactor3<Pose3,Pose3,Pose3>
{
private:
  double t_2_,t_1_,t_0_;
public:
  PoseSmoothFactor(Key j1, Key j2, Key j3, const SharedNoiseModel &model,const double &t_2, const double &t_1, const double &t_0): NoiseModelFactor3<Pose3,Pose3,Pose3>(model,j1,j2,j3), t_2_(t_2),t_1_(t_1), t_0_(t_0) {}
  
  gtsam::Vector evaluateError(const gtsam::Pose3& x_t_2,
                              const gtsam::Pose3& x_t_1,
                              const gtsam::Pose3& x_t,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const
  {

    // compute error:

    // convert to se(3) manifold:
    Vector6 err=((Pose3::Logmap(x_t,H3)-Pose3::Logmap(x_t_1,H2))/(t_0_-t_1_) -
                 (Pose3::Logmap(x_t_1,H2) - Pose3::Logmap(x_t_2,H1))/(t_1_ - t_2_))/(t_0_ - t_1_);
    
    if (H1)
    {
     
      *H1 = *H1;//* (1.0/dt_) ;
      
    }
    if (H2)
    {
      *H2 = *H2 * -2.0;// * (-1.0/dt_) ;
    }
    if (H3)
    {
      *H3 =  *H3 ;
    }
    return  err;
  }
};
