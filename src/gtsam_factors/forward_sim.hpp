#include "gtsam_factors/sysid_expressions.h"

namespace dyn_sim_fns
{
using namespace gtsam;


Vector6 get_bias_force(const Vector1 &m,
                       const Vector3 &h_c,
                       const Matrix33 &H_b,
                       const Vector3 &omega,
                       const Vector3 &o_g);


/** 
 * Compute acceleration, given
 * 
 * @param w inertial parameters
 * @param r off axis inertial matrix values: h_xy,h_xz,h_yz
 * @param x_d object velocity
 * @param dt 
 * @param w_g_ gravity vector
 * @param f_tau  force in object frame
 * 
 * @return object acceleration
 */

Vector6 get_acc(const Pose3 &x,
                const double &m_,
                const Vector3 &r_c,
                const Matrix33 &H_cm,
                const Vector6 &x_d,
                const Vector3 &w_g_,
                const Vector6 &f_tau);


}
