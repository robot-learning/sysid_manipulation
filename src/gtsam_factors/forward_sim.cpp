#include "gtsam_factors/forward_sim.hpp"

namespace dyn_sim_fns
{
using namespace sysid_expr;

// compute fictious force
Vector6 get_bias_force(const Vector1 &m,
                       const Vector3 &h_c,
                       const Matrix33 &H_b,
                       const Vector3 &omega,
                       const Vector3 &o_g)
{

  Vector6 bias_f;
  
  bias_f.head(3) = skew_fn(h_c)*o_g - skew_fn(omega) * H_b * omega;
  bias_f.tail(3) = o_g * m - skew_fn(omega) * skew_fn(omega) * h_c;

  return bias_f;
}
// compute acceleration:
Vector6 get_acc(const Pose3 &x,
                const double &m_,
                const Vector3 &r_c,
                const Matrix33 &H_cm,
                const Vector6 &x_d,
                const Vector3 &w_g_,
                const Vector6 &f_tau)
{
  // t_2,t_1,( t_0 = t)
  Vector6 obj_acc;
  
  Vector3 omega = x_d.head(3);

  Vector1 m;
  m<<m_;
  Vector3 h_c = r_c * m; // m*r_c

  //Expression<Vector3> h_c = Com(w);

  Matrix33 H_b = H_cm - m_ * r_c * r_c.transpose();
  
  // A matrix
  Matrix66 spatial_H = spatial_inertia(m,h_c,H_b);
  
  
  // transform world gravity to object gravity
  Vector3 obj_g_ = inv_proj_vec(x,w_g_);
  Vector6 dyn_bias = f_tau + get_bias_force(m,h_c,H_b,omega,obj_g_);
  

  obj_acc = spatial_H.inverse() * dyn_bias;
  
  return obj_acc;

}
}
