// implementing sysid factors via expressions for automatic differentiation
#include <gtsam/slam/expressions.h>
#include <gtsam/nonlinear/ExpressionFactorGraph.h>

namespace sysid_expr_dart // namespace specifically for symbolic functions to avoid conflicts with numeric fns
{
using namespace gtsam;

// helper functions:
/// TODO: make function constexpr, currently due to issues with eigen matrix data() it is not possible.
/// More info here: https://eigen.tuxfamily.org/bz/show_bug.cgi?id=820
Eigen::Map<Eigen::MatrixXd> eigen_reshape (Eigen::MatrixXd b, const size_t &n, const size_t &m); 


template<class A1>
A1 grad_matrix(const int &i, const int&j)
{
  A1 mat;
  mat.setZero();
  mat(i,j) = 1.0;
  return mat;
}

Eigen::Matrix3d skew_grad_matrix(const int &i);
// base functions for building dynamics expression:
Vector1 mass(const Vector7 &w,gtsam::OptionalJacobian<1,7> H1 = boost::none);

Vector3 com(const Vector7 &w,gtsam::OptionalJacobian<3,7> H1 =  boost::none);

// inertia matrix:
Matrix33 inertia(const Vector7 &w, const Rot3 &R,gtsam::OptionalJacobian<9,7> H1 =  boost::none, gtsam::OptionalJacobian<9,3> H2 = boost::none);


Matrix33 inertia_linear(const Vector7 &w, const Vector3 &l,gtsam::OptionalJacobian<9,7> H1 = boost::none, gtsam::OptionalJacobian<9,3> H2 = boost::none);

Expression<Matrix33> inertia_linear_(const Expression<Vector7> &w, const Vector3_ &l);



Expression<Matrix33> Inertia(const Expression<Vector7> &w, const Expression<Rot3> &R);
Expression<Vector1> Mass(const Expression<Vector7> &w);
Expression<Vector3> Com(const Expression<Vector7> &w);

Matrix33 skew_fn(const Vector3 &vec,gtsam::OptionalJacobian<9,3> H1 = boost::none);
Expression<Matrix33> Skew(const Expression<Vector3> &v);


Matrix66 spatial_inertia(const Vector1 &m, const Vector3 &r_c, const Matrix33 H,gtsam::OptionalJacobian<36,1> H1, gtsam::OptionalJacobian<36,3> H2, gtsam::OptionalJacobian<36,9> H3);



Expression<Matrix66> SpatialInertia(const  Expression<Vector1> m, const Expression<Vector3> r_c, const Expression<Matrix33> H);

  
Vector6 pose_vel(const Pose3 &x_t_2,const Pose3 &x_t_1, const double &dt, OptionalJacobian<6, 6> H1,OptionalJacobian<6, 6> H2,OptionalJacobian<6, 1> H3);


Vector6_ pose_vel_(const Expression<Pose3> &x_t_2, const Expression<Pose3> &x_t_1, const Expression<double> &dt);

// bias functions:
Vector6 b1_a(const Vector1 &m, const Vector3 &g,
                    gtsam::OptionalJacobian<6,1> H1,
                    gtsam::OptionalJacobian<6,3> H2);


Vector6_ b1_a_(const Vector1_ &m, const Vector3_ &g);


Vector6 b1_b(const Vector3 &r_c, const Matrix33 &skew_omega,
                    gtsam::OptionalJacobian<6,3> H2 = boost::none,
                    gtsam::OptionalJacobian<6,9> H3 = boost::none);


Vector6_ b1_b_( const Vector3_ &r_c, const Expression<Matrix33> &skew_omega);

Vector6 b2_a(const Matrix33 &H, const Vector3 &omega, const Matrix33 &skew_omega,
                    gtsam::OptionalJacobian<6,9> H1,
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3);


Vector6_ b2_a_(const Expression<Matrix33> &H, const Vector3_ &omega, const Expression<Matrix33> &skew_omega);

  
Vector6 b2_b(const Vector3 &g, const Matrix33 &skew_rc,             
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3);

Vector6_ b2_b_(const Vector3_ &g, const Expression<Matrix33> &skew_rc);


Vector6 dynamics_error(const Matrix66 &A, const Vector6 &B, 
                              const Vector6 &Xt_diff,
                              
                              gtsam::OptionalJacobian<6,36> H1 = boost::none,
                              gtsam::OptionalJacobian<6,6> H2 = boost::none,
                              gtsam::OptionalJacobian<6,6> H3 = boost::none);

Vector6_ dynamics_error_(const Expression<Matrix66> &A, const Vector6_ &B, 
                                const Vector6_ &x_diff);

Vector6 net_force_torque(const Vector12 &f, const Vector12 &r_f,
                                gtsam::OptionalJacobian<6,12> H1 = boost::none,
                                gtsam::OptionalJacobian<6,12> H2 = boost::none);

Vector6_ net_force_torque_(const Expression<Vector12> &f, const Expression<Vector12> &r_f);

Vector6_ create_symbolic_dynamics(  const Expression<Vector7> &w,
                                    const Expression<Rot3> &R,
                                    const Expression<Pose3> &x_t_2,
                                    const Expression<Pose3> &x_t_1,
                                    const Expression<Pose3> &x_t,
                                    const Expression<double> &dt_,
                                    const Expression<Vector3> &g_,
                                    const Expression<Vector6> &f_tau);

Vector6_ create_symbolic_linear_dynamics(  const Expression<Vector7> &w,
                                           const Expression<Vector3> &r,
                                           const Expression<Pose3> &x_t_2,
                                           const Expression<Pose3> &x_t_1,
                                           const Expression<Pose3> &x_t,
                                           const Expression<double> &dt_,
                                           const Expression<Vector3> &g_,
                                           const Expression<Vector6> &f_tau);


Vector6_ create_symbolic_manifold_dynamics(  const Expression<Matrix44> &P,
                                             const Expression<Pose3> &x_t_2,
                                             const Expression<Pose3> &x_t_1,
                                             const Expression<Pose3> &x_t,
                                             const Expression<double> &dt_,
                                             const Expression<Vector3> &g_,
                                             const Expression<Vector6> &f_tau);


/// Projection to manifold
/// h_c = m * r_c
Vector1 retract_inertia_mass(const Matrix44 &P,
                             gtsam::OptionalJacobian<1,16> H1 = boost::none);
Vector1_ retract_inertia_mass_(const Expression<Matrix44> &P);

Vector3 retract_inertia_com(const Matrix44 &P,
                            gtsam::OptionalJacobian<3,16> H1 = boost::none);
Vector3_ retract_inertia_com_(const Expression<Matrix44> &P);

Expression<Matrix33> retract_inertia_h_(const Expression<Matrix44> &P);

Matrix33 retract_inertia_h(const Matrix44 &P,
                           gtsam::OptionalJacobian<9,16> H1 = boost::none);






Matrix44 proj_inertia(const Vector1 &m, const Vector3 &h_c, const Matrix33 &H,
                      gtsam::OptionalJacobian<16,1> H1 = boost::none,
                      gtsam::OptionalJacobian<16,3> H2 = boost::none,
                      gtsam::OptionalJacobian<16,9> H3 = boost::none);

Expression<Matrix44> proj_inertia_(const Expression<Vector1> &m,
                                   const Expression<Vector3> &h_c,
                                   const Expression<Matrix33> &H);

/// Reimmannian distance metric for two sets of projection inertial parameters:
Vector1 inertia_distance(const Matrix44 &p_des, const Matrix44 &p_curr,
                         gtsam::OptionalJacobian<1,16> H1 = boost::none,
                         gtsam::OptionalJacobian<1,16> H2 = boost::none);

Expression<Vector1> inertia_distance_(const Expression<Matrix44> &p1, const Expression<Matrix44> &p2);

Vector1_ create_between_inertia(  const Expression<Vector7> &w_des,
                                  const Expression<Rot3> &R_des,
                                  const Expression<Vector7> &w,
                                  const Expression<Rot3> &R);

Vector1_ create_between_linear_inertia(  const Expression<Vector7> &w_des,
                                  const Expression<Vector3> &R_des,
                                  const Expression<Vector7> &w,
                                  const Expression<Vector3> &R);



// Templated function declaration+definition:

// matrix multiplication
template<int r1,int r2, int c1, int c2>
Eigen::Matrix<double,r1,c2> matmul(
                               const Eigen::Matrix<double,r1,c1> &a,
                               const Eigen::Matrix<double,r2,c2> &b,
                               gtsam::OptionalJacobian<r1*c2,c1*r1> H1 = boost::none,
                               gtsam::OptionalJacobian<r1*c2,c2*r2> H2 = boost::none)
{
  Eigen::Matrix<double,r1,c2> out_mat = a * b;

  if(H1)
  {
    *H1 = Eigen::Matrix<double,r1*c2,c1*r1>().setZero();
    Eigen::Matrix<double,r1,c2> grad;
    for(int i =0;i<r1;++i)
    {
      for(int j=0;j<c1;++j)
      {
        grad = grad_matrix<Eigen::Matrix<double,r1,c1>>(i,j) * b;

        
        H1->block(0,j*r1+i,r1*c2,1) = eigen_reshape(grad,r1*c2,1);
      }
    }
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,r1*c2,c2*r2>().setZero();
    Eigen::Matrix<double,r1,c2> grad;
    for(int i =0;i<r2;++i)
    {
      for(int j=0;j<c2;++j)
      {
        grad = a * grad_matrix<Eigen::Matrix<double,r2,c2>>(i,j) ;

        
        H2->block(0,j*r2+i,r1*c2,1) = eigen_reshape(grad,r1*c2,1);
      }
    }
  }

  return out_mat;
}

template<int r1,int r2, int c1, int c2>
Expression<Eigen::Matrix<double,r1,c2>>
  matmul_(const Expression<Eigen::Matrix<double,r1,c1>> &a,
                  const Expression<Eigen::Matrix<double,r2,c2>> &b)
{
  return Expression<Eigen::Matrix<double,r1,c2>>
    (&matmul<r1,r2,c1,c2>,a,b);
}


// scalar div:
template<int r1,int c1>
Eigen::Matrix<double,r1,c1> scalar_divide(const Eigen::Matrix<double,r1,c1> &in, const double &scalar,
                                          gtsam::OptionalJacobian<r1*c1,r1*c1> H1 = boost::none,
                                          gtsam::OptionalJacobian<r1*c1,1> H2 = boost::none)
{
  Eigen::Matrix<double,r1,c1> out = (1.0/scalar) *  in;

  if(H1)    
  {
    *H1 = Eigen::Matrix<double,r1*c1,c1*r1>().setZero();
    for(int i =0;i<r1;++i)
    {
      for(int j=0;j<c1;++j)
      {
        H1->coeffRef(j*r1+i,j*r1+i) = (1.0/scalar);
        /*
        H1->block(0,j*r1+i,r1*c1,1) = (1.0/scalar) *
          eigen_reshape(grad_matrix<Eigen::Matrix<double,r1,c1>>(i,j),
                        r1*c1,1);
        */
      }
    }
    
  }
  if(H2)
  {
    *H2 =(-1.0/(scalar*scalar)) * eigen_reshape(in,r1*c1,1);//Eigen::Matrix<double,r1*c1,1>().setZero();
    
    
  }
  return out;
}
template<int r1,int c1>
Expression<Eigen::Matrix<double,r1,c1>> scalar_divide_(const Expression<Eigen::Matrix<double,r1,c1>> &in, const Expression<double> &scalar)
{
  return Expression<Eigen::Matrix<double,r1,c1>>(&scalar_divide<r1,c1>,in,scalar);
}


}

