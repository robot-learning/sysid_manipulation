// implementing sysid factors via expressions for automatic differentiation
#include "gtsam_factors/sysid_expressions.h"

namespace sysid_expr // namespace specifically for symbolic functions to avoid conflicts with numeric fns
{

using namespace gtsam;



Vector7 physics_constraint(const Vector7 &w, gtsam::OptionalJacobian<7,7> H1)
{
  Vector7 retVal;
  double k_=1;
  retVal.setZero();
  
  
  // center of mass should be inside the convex hull ( we treat this is as a function:)
  // for box: [0,180], [-61,0], [0,62] mm
  //const static Vector3 r_min= Vector3().setConstant(-10.0);//0.0,-0.061 / 0.155,0.0);
  //const static Vector3 r_max= Vector3().setConstant(10.0);//0.180 / 0.155,0.0,0.062 / 0.155);
  
  //const static Vector3 r_min(0.0,-0.061,0.0);
  //const static Vector3 r_max(0.180,0.0,0.062);

  const static Vector3 r_min(-1.0,-1.0,-1.0);
  const static Vector3 r_max(1.0,1.0,1.0);

  //const static Vector3 r_min(0.0,0.0,0.0);
  //const static Vector3 r_max(1.0,1.0,1.0);

  const static double m_min = 0.0;
  retVal(0) = log(1.0+exp(-1.0*(w(0)-m_min)*k_));
  
  //const static Vector3 r_min(-1.0,-1.061,-1.0);
  //const static Vector3 r_max(1.180,1.0,1.062);

  retVal(1) = log(1.0+exp(-1.0*(r_max(0) - w(1))*k_))
      + log(1.0+exp(-1.0 * (w(1) - r_min(0)) * k_));
        

    //retVal(2) = log(1.0+exp(-1.0*(0.061 + w(2))*k_)) + log(1.0+exp(-1.0*( -1.0 * w(2))*k_));
    
  retVal(2) = log(1.0+exp(-1.0*(r_max(1) - w(2))*k_)) +
    log(1.0+exp(-1.0*(w(2)-r_min(1))*k_)); 
  
  retVal(3) = log(1.0+exp(-1.0*(r_max(2)-w(3))*k_)) + log(1.0+exp(-1.0 * (w(3)-r_min(2)) * k_)) ;
  
  // inertia diagonal should be positive:
  for(int i = 0; i<3; ++i)
  {
    retVal(4+i) = log(1.0+exp(-1.0*w(4+i)*k_*k_));
  }
    
    if(H1)
    {
      *H1 = Eigen::Matrix<double,7,7>().setZero();
      
      H1->coeffRef(0,0) = (1.0/(1+exp(-1.0*(w(0)-m_min)*k_))) * exp(-1.0 * k_ * (w(0)-m_min)) * -1.0*k_;

      
      H1->coeffRef(1,1) = (1.0/(1+exp(-1.0*(r_max(0)-w(1))*k_))) * exp(-1.0 * k_ *(r_max(0)- w(1))) * 1.0 * k_
        + (1.0/(1+exp(-1.0*(w(1)-r_min(0))*k_))) * exp(-1.0 * k_ * (w(1)-r_min(0))) * -1.0*k_;
      

      H1->coeffRef(2,2) = (1.0/(1+exp(-1.0 *(r_max(1) - w(2))*k_))) * exp(-1.0 * k_ * (r_max(1) - w(2))) * 1.0*k_
        + (1.0/(1+exp(-1.0*( w(2) - r_min(1))*k_))) * exp(-1.0 * k_ * ( w(2) -r_min(1))) * -1.0*k_;
      
      H1->coeffRef(3,3) =  (1.0/(1+exp(-1.0*(r_max(2)-w(3))*k_))) * exp(-1.0 * k_ *(r_max(2)- w(3))) * 1.0 * k_
        + (1.0/(1+exp(-1.0*(w(3)-r_min(2))*k_))) * exp(-1.0 * k_ * (w(3)-r_min(2))) * -1.0*k_ ;
      
      for(int i = 0; i<3; ++i)
      {
        H1->coeffRef(4+i,4+i) = (1.0/(1+exp(-1.0*w(4+i)*k_*k_))) * exp(-1.0 * k_ * k_ * w(4+i)) * -1.0*k_*k_;

      }
      
    }  
    return retVal;
}
Vector7_ physics_constraint_(const Vector7_ &w)
{
  return Vector7_(&physics_constraint,w);
}



Vector7 soft_physics_constraint(const Vector7 &w, gtsam::OptionalJacobian<7,7> H1)
{
  Vector7 retVal;
  double k_=1.0;
  retVal.setZero();
  
   
  // center of mass should be inside the convex hull ( we treat this is as a function:)
  // for box: [0,180], [-61,0], [0,62] mm
  //const static Vector3 r_min= Vector3().setConstant(-10.0);//0.0,-0.061 / 0.155,0.0);
  //const static Vector3 r_max= Vector3().setConstant(10.0);//0.180 / 0.155,0.0,0.062 / 0.155);
  // FT bounds:
  //min:-0.031, -0.129, 0.004
  // max:0.031, 0.051, 0.065
  const static Vector3 r_min(0.0,-0.061,0.0);
  const static Vector3 r_max(0.180,0.0,0.062);
  
  //const static Vector3 r_min(-0.031,-0.129,0.004);
  //const static Vector3 r_max(0.031,0.051,0.065);
  
  //const static Vector3 r_min(-1.0,-1.0,-1.0);
  //const static Vector3 r_max(1.0,1.0,1.0);

  const static double zero_thresh = 1e-8;
  const static double m_min = 1e-8;
  if(w(0)<m_min)
  {
    retVal(0) = (w(0)-m_min);//*(w(0)-m_min);
  }
  

  for(int i=0;i<3;++i)
  {
    if(w(i+1)<r_min(i))
    {
      retVal(1+i)=(w(1+i)-r_min(i));//*(w(1+i)-r_min(i));
      
    }
    else if(w(i+1)>r_max(i))
    {
      retVal(1+i)=(w(1+i)-r_max(i));//*(w(1+i)-r_max(i));
    }

    
  }
  // inertia diagonal should be positive:
  for(int i = 0; i<3; i++)
  {
    
    if(w(4+i)<zero_thresh)
    {
      retVal(4+i) = (w(4+i)-zero_thresh);//*(w(4+i)-zero_thresh);
    }
  }
    
  if(H1)
  {
    *H1 = Eigen::Matrix<double,7,7>().setZero();
    if(w(0)<m_min)
    {
      //H1->coeffRef(0,0) = 1.0*(w(0)-m_min);
      H1->coeffRef(0,0) = 1.0;//(w(0));

    }
    for(int i=0;i<3;++i)
    {
      if(w(i+1)<r_min(i))
      {
        //H1->coeffRef(i+1,i+1) += 2.0*(w(1+i)-r_min(i));
        H1->coeffRef(i+1,i+1) = 1.0;//w(1+i);

      }
      else if(w(i+1)>r_max(i))
      {
        //H1->coeffRef(i+1,i+1)+=2.0 * (w(1+i)-r_max(i));
        H1->coeffRef(i+1,i+1)= 1.0;//w(1+i);

      }
        
    }
      
    for(int i = 0; i<3; i++)
    {
      if(w(4+i)<zero_thresh)
      {
        //H1->coeffRef(4+i,4+i) = 2.0 * (w(4+i)-zero_thresh);
        H1->coeffRef(4+i,4+i) = 1.0;//w(4+i);

      }
      
    }
    /*
    if(retVal.sum()!=0.0)
    {
      std::cerr<<w.transpose()<<std::endl;
      std::cerr<<"err:"<<std::endl;
      std::cerr<<retVal.transpose()<<std::endl;
      std::cerr<<"grad:"<<std::endl;
      std::cerr<<*H1<<std::endl;
        
    }
    */
  }
    
  return retVal;
}
Vector7_ soft_physics_constraint_(const Vector7_ &w)
{
  return Vector7_(&soft_physics_constraint,w);
}

// helper functions:
/// TODO: make function constexpr, currently due to issues with eigen matrix data() it is not possible.
/// More info here: https://eigen.tuxfamily.org/bz/show_bug.cgi?id=820
Eigen::Map<Eigen::MatrixXd> eigen_reshape (Eigen::MatrixXd b, const size_t &n, const size_t &m) 
{
  return Eigen::Map<Eigen::MatrixXd>(b.data(), n, m);
}
Eigen::Matrix3d skew_grad_matrix(const int &i)
{
  Eigen::Matrix3d mat;
  mat.setZero();
  switch (i)
  {
    case 0:
      mat(1,2) = -1.0;
      mat(2,1) = 1.0;
      break;
    case 1:
      mat(0,2) = 1.0;
      mat(2,0) = -1.0;
      break;
    case 2:
      mat(1,0) = 1.0;
      mat(0,1) = -1.0;
      break;
    default:
      std::runtime_error("index for skew_grad is out of range(0,2)");
                         
  }
  
  
  return mat;
}

// base functions for building dynamics expression:
Vector1 mass(const Vector7 &w,gtsam::OptionalJacobian<1,7> H1)
{
  Vector1 h = w.head(1);
  if(H1)    
  {
    Eigen::Matrix<double,1,7> gradient;
    gradient.setZero();
    gradient[0,0] =1.0;
    *H1 = gradient;
  }
  return h;
}

Vector3 com(const Vector7 &w,gtsam::OptionalJacobian<3,7> H1)
{
  Vector3 h = w.segment(1,3);
  if(H1)    
  {
    *H1 = Eigen::Matrix<double,3,7>().setZero();
    H1->block(0,1,3,3) = Eigen::Matrix<double,3,3>().setIdentity();
  }
  return h;
}

Vector3 mass_com(const Vector7 &w,gtsam::OptionalJacobian<3,7> H1)
{
  Vector3 h = w(0) * w.segment(1,3);
  if(H1)    
  {
    *H1 = Eigen::Matrix<double,3,7>().setZero();
    H1->block(0,1,3,3) = w(0) * Eigen::Matrix<double,3,3>().setIdentity();
    H1->block(0,0,3,1) = w.segment(1,3);

  }
  return h;
}


// inertia matrix:

Matrix33 inertia_linear(const Vector7 &w, const Vector3 &l, gtsam::OptionalJacobian<9,7> H1, gtsam::OptionalJacobian<9,3> H2)
{
  Matrix33 h_mat =(gtsam::Matrix33() <<
                   w[4+1] + w[4+2],l(0),l(1),
                   l(0),w[4+0] + w[4+2],l(2),
                   l(1),l(2),w[4+0]+ w[4+1]).finished();
  
  Matrix33 h =  h_mat;// R.matrix() * h_mat * R.matrix().transpose();//.transpose();
  
  if(H1)    
  {
    *H1 = Eigen::Matrix<double,9,7>().setZero();

    // 4:
    H1->block(0,4,9,1) = eigen_reshape( (gtsam::Matrix33() <<
                                         0,0,0,
                                         0,1,0,
                                         0,0,1).finished(),9,1);

    
    // 5:
    H1->block(0,5,9,1) = eigen_reshape( (gtsam::Matrix33() <<
                                         1,0,0,
                                         0,0,0,
                                         0,0,1).finished() ,9,1);
    // 6:
    H1->block(0,6,9,1) = eigen_reshape((gtsam::Matrix33() <<
                                                     1,0,0,
                                                     0,1,0,
                                                     0,0,0).finished(),9,1);


  }
  if(H1)    
  {
    *H2 = Eigen::Matrix<double,9,3>().setZero();

    // 0:
    H2->block(0,0,9,1) = eigen_reshape((gtsam::Matrix33() <<
                                                     0,1,0,
                                                     1,0,0,
                                                     0,0,0).finished(),9,1);
    // 1:
    H2->block(0,1,9,1) = eigen_reshape((gtsam::Matrix33() <<
                                                     0,0,1,
                                                     0,0,0,
                                                     1,0,0).finished(),9,1);
    // 2:
    H2->block(0,2,9,1) = eigen_reshape((gtsam::Matrix33() <<
                                                     0,0,0,
                                                     0,0,1,
                                                     0,1,0).finished(),9,1);

  }

  return h;

}
Expression<Matrix33> inertia_linear_(const Expression<Vector7> &w, const Vector3_ &l)
{
  return Expression<Matrix33>(&inertia_linear,w,l);
}
Matrix33 inertia(const Vector7 &w, const Rot3 &R,gtsam::OptionalJacobian<9,7> H1, gtsam::OptionalJacobian<9,3> H2)
{
  Matrix33 h_mat =(gtsam::Matrix33() <<
                   w[4+1] + w[4+2],0,0,
                   0,w[4+0] + w[4+2],0,
                   0,0,w[4+0]+ w[4+1]).finished();
  
  Matrix33 h =   R.matrix() * h_mat * R.matrix().transpose();//.transpose();
  
  if(H1)    
  {
    *H1 = Eigen::Matrix<double,9,7>().setZero();

    // 4:
    H1->block(0,4,9,1) = eigen_reshape(R.matrix() * (gtsam::Matrix33() <<
                                                     0,0,0,
                                                     0,1,0,
                                                     0,0,1).finished() * R.matrix().transpose(),9,1);

    // 5:
    H1->block(0,5,9,1) = eigen_reshape(R.matrix() * (gtsam::Matrix33() <<
                                                     1,0,0,
                                                     0,0,0,
                                                     0,0,1).finished() * R.matrix().transpose(),9,1);
    // 6:
    H1->block(0,6,9,1) = eigen_reshape(R.matrix() * (gtsam::Matrix33() <<
                                                     1,0,0,
                                                     0,1,0,
                                                     0,0,0).finished() * R.matrix().transpose(),9,1);

  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,9,3>().setZero();
    //Matrix33 rot_grad;
    //Vector3 r_omega;
    //r_omega=R.Logmap(R);
    //
    //Rot3::Logmap(R,rot_grad);
        
    for(int i=0;i<3;++i)

    {
      //r_omega.setZero();
      //r_omega(i) = 1.0;
      //rot_grad = Rot3::Expmap(r_omega).matrix();
      // R.matrix() * skew_grad_matrix(i) * h_mat * R.matrix().transpose()         - R.matrix() *;
      //Matrix33 g_mat= 1.0 *  h_mat *
      //  R.matrix() * skew_grad_matrix(i);// * R.matrix().transpose();// * h_mat; // *
      
      Matrix33 g_mat= R.matrix() * skew_grad_matrix(i) * h_mat * R.matrix().transpose()
        -1.0 *  R.matrix() * h_mat *
        R.matrix().transpose() * R.matrix() * skew_grad_matrix(i) * R.matrix().transpose();

      
      //Matrix33 g_mat= skew_grad_matrix(i) * h_mat;
      
      H2->block(0,i,9,1) = eigen_reshape(g_mat,9,1);
      /*
      H2->block(0,i,9,1) = eigen_reshape(rot_grad * (                                         
                                         skew_grad_matrix(i) 
                                         * h_mat * R.matrix().transpose()
                                         + R.matrix() * h_mat
                                         *   skew_grad_matrix(i)) ,
                                         9,1);
      */
    }
    
  }
  return h;
}



Expression<Matrix33> Inertia(const Expression<Vector7> &w, const Expression<Rot3> &R)
{
  return gtsam::Expression<Matrix33>(&inertia,w,R);
}

Expression<Vector1> Mass(const Expression<Vector7> &w)
{
  return gtsam::Expression<Vector1>(&mass,w);
}
Expression<Vector3> Com(const Expression<Vector7> &w)
{
  return gtsam::Expression<Vector3>(&com,w);
}

Expression<Vector3> mass_com_(const Expression<Vector7> &w)
{
  return gtsam::Expression<Vector3>(&mass_com,w);
}

Matrix33 skew_fn(const Vector3 &vec,gtsam::OptionalJacobian<9,3> H1)
{
  Matrix33 v_c = (gtsam::Matrix33() <<
                  0, -vec(2), vec(1),
                  vec(2), 0, -vec(0),
                  -vec(1), vec(0), 0
                  ).finished();

  if(H1)
  {
    *H1= Eigen::Matrix<double,9,3>().setZero();

    for (int i=0;i<3;++i)
    {
      H1->block(0,i,9,1) = eigen_reshape(skew_grad_matrix(i),9,1);
    }
  }
  
  return v_c;

}
Expression<Matrix33> Skew(const Expression<Vector3> &v)
{
  return Expression<Matrix33>(&skew_fn,v);
}


//Matrix61 dyn_b1a(const Vector3
/*
template<typename T,class A1, class A2,class A3, class A4,class A5, class A6,class A7, class A8,class A9>
struct nineFunction {
  typedef boost::function<
    T(const A1&, const A2&,const A3&,const A4&,const A5&,const A6&,const A7&,const A8&,const A9&,
      typename MakeOptionalJacobian<T, A1>::type,
      typename MakeOptionalJacobian<T, A2>::type,
      typename MakeOptionalJacobian<T, A3>::type,
      typename MakeOptionalJacobian<T, A4>::type,
      typename MakeOptionalJacobian<T, A5>::type,
      typename MakeOptionalJacobian<T, A6>::type,
      typename MakeOptionalJacobian<T, A7>::type,
      typename MakeOptionalJacobian<T, A8>::type,
      typename MakeOptionalJacobian<T, A9>::type)> type;
};
*/

Matrix66 spatial_inertia(const Vector1 &m, const Vector3 &r_c, const Matrix33 H,gtsam::OptionalJacobian<36,1> H1, gtsam::OptionalJacobian<36,3> H2 , gtsam::OptionalJacobian<36,9> H3)
{
  Matrix66 h;

  h.block(3,3,3,3) = m[0] * gtsam::Matrix33::Identity();

  h.block(0,3,3,3) = skew_fn(r_c);

  h.block(3,0,3,3) = -1.0 * skew_fn(r_c);

  h.block(0,0,3,3) = H;

  if(H1)
  {
    *H1 = Eigen::Matrix<double,36,1>().setZero();
    
    Eigen::Matrix<double,6,6> grad;
    
    grad.setZero();
    grad.block(3,3,3,3) = Matrix33::Identity();
    
    *H1 = eigen_reshape(grad,36,1);
  }
  if(H2)    
  {
    *H2 = Eigen::Matrix<double,36,3>().setZero();
    Eigen::Matrix<double,6,6> rc_1;
    for(int i=0;i<3;++i)
    {
      rc_1.setZero();
      rc_1.block(3,0,3,3)= -1.0 * skew_grad_matrix(i);
      rc_1.block(0,3,3,3)= skew_grad_matrix(i);
      
      H2->block(0,i,36,1) = eigen_reshape(rc_1,36,1);
          
    }
    
  }
  if(H3)
  {
    *H3 = Eigen::Matrix<double,36,9>().setZero();

    Eigen::Matrix<double,6,6> g_mat;
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        g_mat.setZero();
        g_mat.block(0,0,3,3) = grad_matrix<Eigen::Matrix3d>(i,j); //
        //std::cout<<i<<" "<<j<<std::endl<<g_mat<<std::endl;
        H3->block(0,j*3+i,36,1) = eigen_reshape(g_mat,36,1);
      }
    }
  }
    
  
  return h;
}


Expression<Matrix66> SpatialInertia(const  Expression<Vector1> m, const Expression<Vector3> r_c, const Expression<Matrix33> H)
{
  return gtsam::Expression<Matrix66>(&spatial_inertia,m,r_c,H);
}

Vector6 pose_vel(const Pose3 &x_t_2,const Pose3 &x_t_1, const double &dt, OptionalJacobian<6, 6> H1,OptionalJacobian<6, 6> H2,OptionalJacobian<6, 1> H3)
{

  //Matrix66 H1_log,H2_log;
  //Pose3 diff = x_t_2.inverse()*x_t_1;
  typename traits<Pose3>::ChartJacobian::Jacobian Hlocal;
  Pose3 hx = traits<Pose3>::Between(x_t_2, x_t_1, H1, H2); // h(x)

  //  Vector6 retVal =(1.0/dt) * Pose3::Logmap(diff,H2);
  Vector6 retVal = (traits<Pose3>::Local(Pose3(), hx, boost::none, (H1 || H2) ? &Hlocal : 0))/dt;///dt;
  if(H1)
  {
    *H1 = (1.0/dt) * Hlocal * (*H1);
    //Pose3::Logmap(diff.inverse(),H1);
    //*H1 = (-1.0/dt) * diff.inverse().AdjointMap();
    //Pose3::Logmap(x_t_2,H1);Ex
    //*H1 =(-1.0/dt) * x_t_2.inverse() * *H1 * x_t_2.inverse();
    
  }
  if(H2)
  {
    *H2 = (1.0/dt) * Hlocal * (*H2);

    //Matrix h_1,h_2;
    //Pose3::Logmap(x_t_2.inverse(),h_1);
    //Pose3::Logmap(x_t_1,h_2);

    //*H2  = (1.0/dt) * *H2;//   * h_2;

  }


  if(H3)
  {
    *H3 =  (-1.0/dt) * retVal;
  }


  return retVal;
}

Vector6_ pose_vel_(const Expression<Pose3> &x_t_2, const Expression<Pose3> &x_t_1, const Expression<double> &dt)
{
  return Vector6_(&pose_vel,x_t_2,x_t_1,dt);
}


Expression<Vector12> inv_proj_force_(const Expression<Pose3> &w_T_x, const Expression<Vector12> &w_f)
{
  Eigen::Matrix<double,3,12> proj_mat;
  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();

  typedef Eigen::Matrix<double,3,12> matrix3_12;

  typedef Eigen::Matrix<double,12,3> matrix12_3;

  Expression<Vector12> obj_f =
    matmul_(Expression<matrix12_3>(proj_mat.transpose()),
            inv_proj_vec_(w_T_x,matmul_(Expression<matrix3_12>(proj_mat),w_f)));
  for(int i=1;i<4;++i)
  {
    proj_mat.setZero();
    proj_mat.block(0,3*i,3,3) = Matrix33().Identity();
    obj_f+=
      matmul_(Expression<matrix12_3>(proj_mat.transpose()),
              inv_proj_vec_(w_T_x,matmul_(Expression<matrix3_12>(proj_mat),w_f)));
  }
  return obj_f;
}


Expression<Vector12> proj_cpt_obj_frame_(const Expression<Pose3> &w_T_obj,
                                           const Expression<Pose3> &w_T_cpt0,
                                           const Expression<Pose3> &w_T_cpt1,
                                           const Expression<Pose3> &w_T_cpt2,
                                           const Expression<Pose3> &w_T_cpt3)
{

  Eigen::Matrix<double,3,12> proj_mat;

  typedef Eigen::Matrix<double,3,12> matrix3_12;

  typedef Eigen::Matrix<double,12,3> matrix12_3;

  // project cpt point to world frame:
  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();

  Expression<Vector12> w_cpt =
    matmul_(Expression<matrix12_3>
            (proj_mat.transpose()),
            get_translation_(w_T_cpt0));

  proj_mat.setZero();
  proj_mat.block(0,3,3,3) = Matrix33().Identity();

  w_cpt +=
    matmul_(Expression<matrix12_3>
            (proj_mat.transpose()),
            get_translation_(w_T_cpt1));

  proj_mat.setZero();
  proj_mat.block(0,6,3,3) = Matrix33().Identity();

  w_cpt +=
    matmul_(Expression<matrix12_3>
            (proj_mat.transpose()),
            get_translation_(w_T_cpt2));

  proj_mat.setZero();
  proj_mat.block(0,9,3,3) = Matrix33().Identity();

  w_cpt +=
    matmul_(Expression<matrix12_3>
            (proj_mat.transpose()),
            get_translation_(w_T_cpt3));
  Expression<Vector12> obj_cpt = inv_proj_cpt_(w_T_obj,w_cpt);
  return obj_cpt;
  
}

Vector3 get_translation(const Pose3 &w_T_pt, OptionalJacobian<3, 6> H)
{
  Vector3 ret = w_T_pt.translation();
  if(H)
  {
    *H= gtsam::Matrix36().setZero();
    H->block(0,3,3,3) = w_T_pt.rotation().matrix();
    
  }
  return ret;
}


Vector3_ get_translation_(const Expression<Pose3> &w_T_pt)
{
  return Vector3_(&get_translation,w_T_pt);
}
                         

Expression<Vector12> proj_force_obj_frame_(const Expression<Pose3> &w_T_obj,
                                           const Expression<Pose3> &w_T_cpt0,
                                           const Expression<Pose3> &w_T_cpt1,
                                           const Expression<Pose3> &w_T_cpt2,
                                           const Expression<Pose3> &w_T_cpt3,
                                           const Expression<Vector12> &cptn_f)
{
  Eigen::Matrix<double,3,12> proj_mat;

  typedef Eigen::Matrix<double,3,12> matrix3_12;

  typedef Eigen::Matrix<double,12,3> matrix12_3;

  // project to world frame:

  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();
  Expression<Vector12> w_f = matmul_(Expression<matrix12_3>
                                     (proj_mat.transpose()), 
                                     proj_vec_(w_T_cpt0,
                                               matmul_(Expression<matrix3_12>(proj_mat),cptn_f)));

  proj_mat.setZero();
  proj_mat.block(0,3,3,3) = Matrix33().Identity();
  w_f += matmul_(Expression<matrix12_3>
                   (proj_mat.transpose()),
                   proj_vec_(w_T_cpt1,
                             matmul_(Expression<matrix3_12>(proj_mat),cptn_f)));


  proj_mat.setZero();
  proj_mat.block(0,6,3,3) = Matrix33().Identity();
  w_f += matmul_(Expression<matrix12_3>
                   (proj_mat.transpose()),
                   proj_vec_(w_T_cpt2,
                             matmul_(Expression<matrix3_12>(proj_mat),cptn_f)));

  proj_mat.setZero();
  proj_mat.block(0,9,3,3) = Matrix33().Identity();
  w_f += matmul_(Expression<matrix12_3>
                   (proj_mat.transpose()),
                   proj_vec_(w_T_cpt3,
                             matmul_(Expression<matrix3_12>(proj_mat),cptn_f)));

  // project to object frame
  /*
  // write out the math here:
  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();

  
  Expression<Vector12> obj_f =
    matmul_(Expression<matrix12_3>(proj_mat.transpose()),
            inv_proj_vec_(w_T_obj,matmul_(Expression<matrix3_12>(proj_mat),w_f)));

  for(int i=1;i<4;++i)
  {
    proj_mat.setZero();
    proj_mat.block(0,3*i,3,3) = Matrix33().Identity();
    obj_f+=
      matmul_(Expression<matrix12_3>(proj_mat.transpose()),
              inv_proj_vec_(w_T_obj,matmul_(Expression<matrix3_12>(proj_mat),w_f)));
  }

  */
  Expression<Vector12> obj_f = inv_proj_force_(w_T_obj,w_f);
  return obj_f;
}

Expression<Vector12> inv_proj_cpt_(const Expression<Pose3> &w_T_x, const Expression<Vector12> &w_cpt)
{
  Eigen::Matrix<double,3,12> proj_mat;
  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();
  
  typedef Eigen::Matrix<double,3,12> matrix3_12;

  typedef Eigen::Matrix<double,12,3> matrix12_3;

  Expression<Vector12> obj_cpt=matmul_(Expression<matrix12_3>(proj_mat.transpose()),
                                     inv_proj_pt_(w_T_x,matmul_(Expression<matrix3_12>(proj_mat),w_cpt)));
  for(int i=1;i<4;++i)
  {
    proj_mat.setZero();
    proj_mat.block(0,3*i,3,3) = Matrix33().Identity();
    obj_cpt+=matmul_(Expression<matrix12_3>(proj_mat.transpose()),
                   inv_proj_pt_(w_T_x,matmul_(Expression<matrix3_12>(proj_mat),w_cpt)));
  }
  return obj_cpt;
  
}



/// This function is wrong. Don't use this
Vector6_ inv_proj_ft_(const Expression<Pose3> &w_T_x, const Vector6_ &w_ft)
{
  static const Matrix63 f_proj = (gtsam::Matrix63() <<
                                  0, 0 ,0,
                                  0, 0, 0,
                                  0, 0, 0,
                                  1, 0, 0,
                                  0, 1, 0,
                                  0, 0, 1).finished();

    static const Matrix63 tau_proj = (gtsam::Matrix63() <<
                                    1, 0 ,0,
                                    0, 1, 0,
                                    0, 0, 1,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0).finished();

  Vector6_ x_ft =matmul_( Expression<Matrix63>(f_proj),
                          inv_proj_vec_(w_T_x, matmul_(Expression<Matrix36>(f_proj.transpose()),
                                                       w_ft)));
  x_ft+=matmul_( Expression<Matrix63>(tau_proj),
                          inv_proj_vec_(w_T_x, matmul_(Expression<Matrix36>(tau_proj.transpose()),
                                                       w_ft)));

}

// project a vector to a new frame:
Vector3 inv_proj_vec(const Pose3 &w_T_x, const Vector3 &w_v,
                     gtsam::OptionalJacobian<3,6> H1,
                     gtsam::OptionalJacobian<3,3> H2)
{
  const Matrix33 rt = w_T_x.rotation().transpose();
  Vector3 ret = rt * w_v;

  if(H1)
  {
    //*H1 = Eigen::Matrix<double,3,6>().setZero();
    const double wx = ret(0), wy = ret(1), wz = ret(2);

    (*H1) =(gtsam::Matrix36()<<
            0.0, -wz, +wy,0.0, 0.0, 0.0,
            +wz, 0.0, -wx, 0.0,0.0, 0.0,
            -wy, +wx, 0.0, 0.0, 0.0,0.0).finished();

  }
  if(H2)
  {
    *H2 = gtsam::Matrix33();
    *H2 = rt; 
  }

  return ret;
}
Vector3 proj_vec(const Pose3 &w_T_x, const Vector3 &x_v,
                     gtsam::OptionalJacobian<3,6> H1,
                     gtsam::OptionalJacobian<3,3> H2)
{
  const Matrix33 rt = w_T_x.rotation().matrix();
  Vector3 ret = rt * x_v;

  if(H1)
  {
    //*H1 = Eigen::Matrix<double,3,6>().setZero();
    const double wx = ret(0), wy = ret(1), wz = ret(2);

    *H1 << (gtsam::Matrix36() <<
            0.0, wz, -wy,0.0, 0.0, 0.0,
            -wz, 0.0, wx, 0.0,0.0, 0.0,
            wy, -wx, 0.0, 0.0, 0.0,0.0).finished();
            
  }
  if(H2)
  {
    *H2 = rt; 
  }

  return ret;
}

Vector3_ inv_proj_vec_(const Expression<Pose3> &w_T_x, const Vector3_ &w_v)
{
  return Vector3_(&inv_proj_vec,w_T_x,w_v);
}

Vector3 inv_proj_pt(const Pose3 &w_T_x, const Vector3 &w_pt,
                     gtsam::OptionalJacobian<3,6> H1,
                     gtsam::OptionalJacobian<3,3> H2)
{
  const Matrix3 rt = w_T_x.rotation().transpose();
  Vector3 ret = rt *(w_pt-w_T_x.translation());

  if(H1)
  {
    //*H1 = Eigen::Matrix<double,3,6>().setZero();
    const double wx = ret(0), wy = ret(1), wz = ret(2);

    (*H1) << (gtsam::Matrix36()<<
      0.0, -wz, +wy,-1.0, 0.0, 0.0,
      +wz, 0.0, -wx, 0.0,-1.0, 0.0,
      -wy, +wx, 0.0, 0.0, 0.0,-1.0).finished();

  }
  if(H2)
  {
    *H2 = rt;
  }

  return ret;
}

Vector3 proj_pt(const Pose3 &w_T_x, const Vector3 &x_pt,
                     gtsam::OptionalJacobian<3,6> H1,
                     gtsam::OptionalJacobian<3,3> H2)
{
  const Matrix3 rt = w_T_x.rotation().matrix();
  Vector3 ret = rt *(x_pt) + w_T_x.translation();

  if(H1)
  {
    *H1 = Eigen::Matrix<double,3,6>().setZero();
    H1->leftCols<3>() = rt * skewSymmetric(-x_pt(0), -x_pt(1), -x_pt(2));
    H1->rightCols<3>() = rt;

  }
  if(H2)
  {
    *H2 = rt;
  }

  return ret;
}


Vector3_ inv_proj_pt_(const Expression<Pose3> &w_T_x, const Vector3_ &w_v)
{
  return Vector3_(&inv_proj_pt,w_T_x,w_v);
}

Vector3_ proj_pt_(const Expression<Pose3> &w_T_x, const Vector3_ &x_v)
{
  return Vector3_(&proj_pt,w_T_x,x_v);
}

Vector3_ proj_vec_(const Expression<Pose3> &w_T_x, const Vector3_ &x_v)
{
  return Vector3_(&proj_vec,w_T_x,x_v);
}

// bias functions:
Vector6 b1_a(const Vector1 &m, const Vector3 &g,
                   gtsam::OptionalJacobian<6,1> H1,
                   gtsam::OptionalJacobian<6,3> H2)
{
  Vector6 retVal;
  retVal.setZero();
  retVal.tail(3)= m[0] * g;
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,1>().setZero();
    H1->block(3,0,3,1) = g;
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();
    H2->block(3,0,3,3) = m[0] * Matrix33::Identity();

  }
  return retVal;
}
Vector6_ b1_a_(const Vector1_ &m, const Vector3_ &g)
{
  return Vector6_(&b1_a,m,g);
}

Vector6 b1_b( const Vector3 &r_c, const Matrix33 &skew_omega,
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3)
{
  Vector6 retVal;
  retVal.setZero();

  retVal.tail(3) =-1.0 * skew_omega * skew_omega * r_c ;
  /*
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,1>().setZero();
    //H1->block(3,0,3,1) =  -1.0 * skew_omega * skew_omega * r_c ;
  }
  */
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();
    H2->block(3,0,3,3) = -1.0 * skew_omega * skew_omega * Matrix33::Identity();
  }

  if(H3)
  {
    *H3 = Eigen::Matrix<double,6,9>().setZero();
    for(int i =0; i<3; ++i)
    {
      for(int j=0;j<3;++j)
      {
        H3->block(3,j*3+i,3,1) = -1.0 * grad_matrix<Eigen::Matrix3d>(i,j) * skew_omega * r_c - skew_omega * grad_matrix<Eigen::Matrix3d>(i,j) * r_c;
        
      }
    }
  }
  
  
  return retVal;
}
Vector6_ b1_b_( const Vector3_ &r_c, const Expression<Matrix33> &skew_omega)
{
  return Vector6_(&b1_b,r_c,skew_omega);
}


Vector6 b2_a(const Matrix33 &H, const Vector3 &omega, const Matrix33 &skew_omega,
                    gtsam::OptionalJacobian<6,9> H1,
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3)
{
  Vector6 retVal;
  retVal.setZero();

  retVal.head(3)= -skew_omega * H * omega;
  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,9>().setZero();
    for(int i = 0;i<3 ;++i)
    {
      for(int j = 0;j<3;++j)
      {
        H1->block(0,j*3+i,3,1) = -skew_omega * grad_matrix<Eigen::Matrix3d>(i,j) * omega;

      }
    }
    
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();

    H2->block(0,0,3,3) = -skew_omega * H * Matrix33::Identity();
  }
  if(H3)
  {
    *H3 = Eigen::Matrix<double,6,9>().setZero();

    for(int i = 0;i<3 ;++i)
    {
      for(int j = 0;j<3;++j)
      {
        H3->block(0,j*3+i,3,1) = -1.0 * grad_matrix<Eigen::Matrix3d>(i,j) * H * omega;
      }
    }

  }

  return retVal;
}
Vector6_ b2_a_(const Expression<Matrix33> &H, const Vector3_ &omega, const Expression<Matrix33> &skew_omega)
{
  return Vector6_(&b2_a,H,omega,skew_omega);
}

Vector6 b2_b(const Vector3 &g, const Matrix33 &skew_rc,             
                    gtsam::OptionalJacobian<6,3> H2,
                    gtsam::OptionalJacobian<6,9> H3)
{
  Vector6 retVal;
  retVal.setZero();

  retVal.head(3)= skew_rc * g;
  
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,3>().setZero();

    H2->block(0,0,3,3) = skew_rc * Matrix33::Identity();
  }
  if(H3)  
  {
    *H3 = Eigen::Matrix<double,6,9>().setZero();

    for(int i = 0;i<3 ;++i)
    {
      for(int j = 0;j<3;++j)
      {
        H3->block(0,j*3+i,3,1) =  grad_matrix<Eigen::Matrix3d>(i,j) * g;
      }
    }
  }
  return retVal;
}

Vector6_ b2_b_( const Vector3_ &g, const Expression<Matrix33> &skew_rc)
{
  return Vector6_(&b2_b,g,skew_rc);
}


Vector6 dynamics_error(const Matrix66 &A, const Vector6 &B, 
                              const Vector6 &Xt_diff,
                              
                              gtsam::OptionalJacobian<6,36> H1,
                              gtsam::OptionalJacobian<6,6> H2,
                              gtsam::OptionalJacobian<6,6> H3)
{
  Vector6 retVal = A * Xt_diff - B;

  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,36>().setZero();
    for(int i = 0;i<6 ;++i)
    {
      for(int j = 0;j<6;++j)
      {
        H1->block(0,j*6+i,6,1) = grad_matrix<Eigen::Matrix<double,6,6>>(i,j) * Xt_diff;
        //std::cout<<" "<<std::endl;
        //        std::cout<<  grad_matrix<Eigen::Matrix<double,6,6>>(i,j) * Xt_diff<<std::endl;
      }
    }
    
    
  }
  if(H2)
  {
    *H2 = -1.0 * Matrix66::Identity();
  
  }
  if(H3)
  {
    *H3 = A * Matrix66::Identity();
  
  }
  
  return retVal;
}

Vector6_ dynamics_error_(const Expression<Matrix66> &A, const Vector6_ &B, 
                                const Vector6_ &x_diff)
{
  return Vector6_(&dynamics_error,A,B,x_diff);
}

Vector6 net_force_torque(const Vector12 &f, const Vector12 &r_f,
                         gtsam::OptionalJacobian<6,12> H1,
                         gtsam::OptionalJacobian<6,12> H2)
{
  Vector6 f_tau;
  f_tau.setZero();

  f_tau.tail(3) = f.segment(0,3) + f.segment(3,3) + f.segment(6,3) + f.segment(9,3);
  for(int i=0;i<4;++i)
  {
    f_tau.head(3) +=skew_fn(r_f.segment(i*3,3)) * f.segment(i*3,3) ;
  }

  if(H1)
  {
    *H1 = Eigen::Matrix<double,6,12>().setZero();
    for(int i=0;i<4;++i)
    {
      H1->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * 1.0;
      H1->block(0,i*3,3,3) = skew_fn(r_f.segment(i*3,3)) * gtsam::Matrix33().Identity() * 1.0;
 
    }
    
  }
  if(H2)
  {
    *H2 = Eigen::Matrix<double,6,12>().setZero();
    
    for(int i=0;i<4;++i)
    {
      for(int j =0;j<3;++j)
      {
        H2->block(0,i*3+j,3,1) = skew_grad_matrix(j) * f.segment(i*3,3);
      }
    }
  }

  return f_tau;
}

Vector6_ net_force_torque_(const Expression<Vector12> &f, const Expression<Vector12> &r_f)
{
  return Vector6_(&net_force_torque,f,r_f);
  
}
Vector6_ net_force_torque_symbolic_(const Expression<Vector12> &f, const Expression<Vector12> &r_f)
{
  // add forces:
  static const Matrix63 f_proj = (gtsam::Matrix63() <<
                                  0, 0 ,0,
                                  0, 0, 0,
                                  0, 0, 0,
                                  1, 0, 0,
                                  0, 1, 0,
                                  0, 0, 1).finished();

  Eigen::Matrix<double,3,12> proj_mat;
  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();

  typedef Eigen::Matrix<double,3,12> matrix3_12;

  Vector6_ f_tau =
    matmul_(Expression<Matrix63>(f_proj),matmul_(Expression<matrix3_12>(proj_mat),f));

  proj_mat.setZero();
  proj_mat.block(0,3,3,3) = Matrix33().Identity();

  f_tau +=
    matmul_(Expression<Matrix63>(f_proj),matmul_(Expression<matrix3_12>(proj_mat),f));

  proj_mat.setZero();
  proj_mat.block(0,6,3,3) = Matrix33().Identity();

  f_tau +=
    matmul_(Expression<Matrix63>(f_proj),matmul_(Expression<matrix3_12>(proj_mat),f));
  proj_mat.setZero();
  proj_mat.block(0,9,3,3) = Matrix33().Identity();

  f_tau +=
    matmul_(Expression<Matrix63>(f_proj),matmul_(Expression<matrix3_12>(proj_mat),f));

  // add torques:

  static const Matrix63 tau_proj = (gtsam::Matrix63() <<
                                    1, 0 ,0,
                                    0, 1, 0,
                                    0, 0, 1,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0).finished();

  proj_mat.setZero();
  proj_mat.block(0,0,3,3) = Matrix33().Identity();


  f_tau+= matmul_(Expression<Matrix63>(tau_proj),
                  matmul_(Skew(matmul_(Expression<matrix3_12>(proj_mat),r_f)),
                          matmul_(Expression<matrix3_12>(proj_mat),f)));
  proj_mat.setZero();
  proj_mat.block(0,3,3,3) = Matrix33().Identity();


  f_tau+= matmul_(Expression<Matrix63>(tau_proj),
                  matmul_(Skew(matmul_(Expression<matrix3_12>(proj_mat),r_f)),
                          matmul_(Expression<matrix3_12>(proj_mat),f)));      
  proj_mat.setZero();
  proj_mat.block(0,6,3,3) = Matrix33().Identity();


  f_tau+= matmul_(Expression<Matrix63>(tau_proj),
                  matmul_(Skew(matmul_(Expression<matrix3_12>(proj_mat),r_f)),
                          matmul_(Expression<matrix3_12>(proj_mat),f)));      
  proj_mat.setZero();
  proj_mat.block(0,9,3,3) = Matrix33().Identity();


  f_tau+= matmul_(Expression<Matrix63>(tau_proj),
                  matmul_(Skew(matmul_(Expression<matrix3_12>(proj_mat),r_f)),
                          matmul_(Expression<matrix3_12>(proj_mat),f)));      

  return f_tau;
  
}

void compute_pose_derivatives(const Expression<Pose3> &x_t_2,
                              const Expression<Pose3> &x_t_1,
                              const Expression<Pose3> &x_t,
                              const Expression<double> &t_2,
                              const Expression<double> &t_1,
                              const Expression<double> &t_0,
                              Expression<Vector6> &acc,
                              Expression<Vector3> &omega,
                              Expression<Vector3> &omega_10)
{

  Expression<double> dt_21 = t_1 - t_2;
  Expression<double> dt_10 = t_0 - t_1;

  Matrix36 project;
  project.setZero();
  project.block(0,0,3,3) = Matrix33::Identity();
  //Expression<Pose3> v_d = x_t_2.inverse() * x_t_1;
  Expression<Vector6>  vel = pose_vel_(x_t_2,x_t_1,dt_21);
  
  Expression<Vector6> vel_1_0 = pose_vel_(x_t_1,x_t,dt_10);

  // transform t_1 velocity to t_2 reference frame:
  
  omega = matmul_(Expression<Matrix36>(project), vel);
  
  omega_10 = matmul_(Expression<Matrix36>(project), vel_1_0);

  Matrix63 retract_mat;
  retract_mat.setZero();
  retract_mat.block(0,0,3,3) = Matrix33::Identity();

  
  vel_1_0 += scalar_mul_(matmul_(Expression<Matrix63>(retract_mat),
                                 matmul_(Skew(omega),
                                         matmul_(Expression<Matrix36>(project),vel_1_0)))
                         ,dt_10);


  retract_mat.setZero();
  retract_mat.block(3,0,3,3) = Matrix33::Identity();
  project.setZero();
  project.block(0,3,3,3) = Matrix33::Identity();

  vel_1_0 += scalar_mul_(matmul_(Expression<Matrix63>(retract_mat),
                                 matmul_(Skew(omega),
                                   matmul_(Expression<Matrix36>(project),vel_1_0))),
                         dt_10);

  //omega_10 = matmul_(Expression<Matrix36>(project), vel_1_0);

  acc  = scalar_divide_(vel_1_0-vel,dt_10);


}
void compute_pose_derivatives(const Expression<Pose3> &x_t_2,
                              const Expression<Pose3> &x_t_1,
                              const Expression<Pose3> &x_t,
                              const Expression<double> &t_2,
                              const Expression<double> &t_1,
                              const Expression<double> &t_0,
                              Expression<Vector6> &acc,
                              Expression<Vector3> &omega)
{

  Expression<double> dt_21 = t_1 - t_2;
  Expression<double> dt_10 = t_0 - t_1;

  Matrix36 project;
  project.setZero();
  project.block(0,0,3,3) = Matrix33::Identity();
  //Expression<Pose3> v_d = x_t_2.inverse() * x_t_1;
  Expression<Vector6>  vel = pose_vel_(x_t_2,x_t_1,dt_21);
  
  Expression<Vector6> vel_1_0 = pose_vel_(x_t_1,x_t,dt_10);

  // transform t_1 velocity to t_2 reference frame:
  
  omega = matmul_(Expression<Matrix36>(project), vel);
  //omega_10 = matmul_(Expression<Matrix36>(project), vel_1_0);

  Matrix63 retract_mat;
  retract_mat.setZero();
  retract_mat.block(0,0,3,3) = Matrix33::Identity();
  
  vel_1_0 += scalar_mul_(matmul_(Expression<Matrix63>(retract_mat),
                                 matmul_(Skew(omega),
                                         matmul_(Expression<Matrix36>(project),vel_1_0)))
                         ,dt_10);


  retract_mat.setZero();
  retract_mat.block(3,0,3,3) = Matrix33::Identity();
  project.setZero();
  project.block(0,3,3,3) = Matrix33::Identity();

  vel_1_0 += scalar_mul_(matmul_(Expression<Matrix63>(retract_mat),
                                 matmul_(Skew(omega),
                                   matmul_(Expression<Matrix36>(project),vel_1_0))),
                         dt_10);

  acc  = scalar_divide_(vel_1_0-vel,dt_10);


}

void compute_pose_derivatives(const Expression<Pose3> &x_t_2,
                              const Expression<Pose3> &x_t_1,
                              const Expression<Pose3> &x_t,
                              const Expression<double> &dt_21,
                              const Expression<double> &dt_10,
                              Expression<Vector6> &acc,
                              Expression<Vector3> &omega)
{

  //Expression<double> dt_21 = t_1 - t_2;
  //Expression<double> dt_10 = t_0 - t_1;

  Matrix36 project;
  project.setZero();
  project.block(0,0,3,3) = Matrix33::Identity();
  //Expression<Pose3> v_d = x_t_2.inverse() * x_t_1;
  Expression<Vector6>  vel = pose_vel_(x_t_2,x_t_1,dt_21);
  
  Expression<Vector6> vel_1_0 = pose_vel_(x_t_1,x_t,dt_10);

  // transform t_1 velocity to t_2 reference frame:
  
  omega = matmul_(Expression<Matrix36>(project), vel);

  Matrix63 retract_mat;
  retract_mat.setZero();
  retract_mat.block(0,0,3,3) = Matrix33::Identity();
  
  vel_1_0 += scalar_divide_(matmul_(Expression<Matrix63>(retract_mat),
                                 matmul_(Skew(omega),
                                         matmul_(Expression<Matrix36>(project),vel_1_0)))
                         ,dt_10);


  retract_mat.setZero();
  retract_mat.block(3,0,3,3) = Matrix33::Identity();
  project.setZero();
  project.block(0,3,3,3) = Matrix33::Identity();

  vel_1_0 += scalar_divide_(matmul_(Expression<Matrix63>(retract_mat),
                                 matmul_(Skew(omega),
                                   matmul_(Expression<Matrix36>(project),vel_1_0))),
                         dt_10);

  acc  = scalar_mul_(vel_1_0-vel,dt_10);


}


Vector6_ project_force_torque(const Vector3_ &f, const Vector3_ &tau)
{

  // constant variables:
  const static Matrix63 f_proj = (gtsam::Matrix63() <<
                           0, 0 ,0,
                           0, 0, 0,
                           0, 0, 0,
                           1, 0, 0,
                           0, 1, 0,
                           0, 0, 1).finished();

  const  static Matrix63 tau_proj = (gtsam::Matrix63() <<
                             1, 0 ,0,
                             0, 1, 0,
                             0, 0, 1,
                             0, 0, 0,
                             0, 0, 0,
                             0, 0, 0).finished();

  Vector6_ f_tau = matmul_(Expression<Matrix63>(f_proj),f) +
    matmul_(Expression<Matrix63>(tau_proj),tau);

  return f_tau;
}
Vector6_ create_dyn_bias(const Vector1_ &m, const Vector3_ &h_c,
                         const Expression<Matrix33> &H, const Vector3_ &omega, const Vector3_ &g_)
{
  Vector3_ force_bias = matmul_(g_,m) - matmul_(matmul_(Skew(omega),Skew(omega)),h_c);// + matmul_(Skew(matmul_(v,m)),omega);


  Vector3_ torque_bias = (matmul_( Skew(h_c) , g_ )
                          - matmul_(Skew(omega),matmul_(H,omega)));

  // TODO move projection constants 
  // constant variables:
  static const Matrix63 f_proj = (gtsam::Matrix63() <<
                                  0, 0 ,0,
                                  0, 0, 0,
                                  0, 0, 0,
                                  1, 0, 0,
                                  0, 1, 0,
                                  0, 0, 1).finished();

  static const Matrix63 tau_proj = (gtsam::Matrix63() <<
                                    1, 0 ,0,
                                    0, 1, 0,
                                    0, 0, 1,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0).finished();

  Vector6_ dyn_bias = matmul_(Expression<Matrix63>(f_proj),force_bias) + matmul_(Expression<Matrix63>(tau_proj),torque_bias);
  return dyn_bias;

}

Vector6_ create_dyn_bias(const Vector1_ &m, const Vector3_ &h_c,
                         const Expression<Matrix33> &H, const Vector3_ &omega,
                         const Vector3_ &omega_10, const Vector3_ &g_)
{
  Vector3_ force_bias = matmul_(g_,m) - matmul_(matmul_(Skew(omega),Skew(omega_10)),h_c);// + matmul_(Skew(matmul_(v,m)),omega);


  Vector3_ torque_bias = (matmul_( Skew(h_c) , g_ )
                          - matmul_(Skew(omega_10),matmul_(H,omega)));

  // TODO move projection constants 
  // constant variables:
  static const Matrix63 f_proj = (gtsam::Matrix63() <<
                                  0, 0 ,0,
                                  0, 0, 0,
                                  0, 0, 0,
                                  1, 0, 0,
                                  0, 1, 0,
                                  0, 0, 1).finished();

  static const Matrix63 tau_proj = (gtsam::Matrix63() <<
                                    1, 0 ,0,
                                    0, 1, 0,
                                    0, 0, 1,
                                    0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0).finished();

  Vector6_ dyn_bias = matmul_(Expression<Matrix63>(f_proj),force_bias) + matmul_(Expression<Matrix63>(tau_proj),torque_bias);
  return dyn_bias;

}

Vector6_ create_symbolic_manifold_dynamics(  const Expression<Matrix44> &P,
                                             const Expression<Pose3> &x_t_2,
                                             const Expression<Pose3> &x_t_1,
                                             const Expression<Pose3> &x_t,
                                             const Expression<double> &t_2,
                                             const Expression<double> &t_1,
                                             const Expression<double> &t_0,
                                             const Expression<Vector3> &w_g_,
                                             const Expression<Vector6> &f_tau)
{
  Vector6_ obj_acc = Vector6();
  Vector3_ omega = Vector3();
  //compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega);

  // A matrix
  Expression<Vector1> m = retract_inertia_mass_(P);
  Expression<Vector3> h_c = retract_inertia_com_(P);// this is actually h = m * r_c
  Expression<Matrix33> H = retract_inertia_h_(P);

  Expression<Matrix66> spatial_H = SpatialInertia(m,h_c,H);
  
  // B matrix
  Vector3_ obj_g_ = inv_proj_vec_(x_t_2,w_g_);
  Vector6_ dyn_bias = inv_proj_ft_(x_t_2,f_tau) + create_dyn_bias(m,h_c,H,omega,obj_g_);

  
  // TODO add dt_ * dt_ *  to dyn_bias
  Vector6_ err = matmul_(spatial_H,obj_acc) - dyn_bias;
  return err;

}
Vector6_ create_symbolic_dynamics(  const Expression<Vector7> &w,
                                    const Expression<Rot3> &R,
                                    const Expression<Pose3> &x_t_2,
                                    const Expression<Pose3> &x_t_1,
                                    const Expression<Pose3> &x_t,
                                    const Expression<double> &t_2,
                                    const Expression<double> &t_1,
                                    const Expression<double> &t_0,
                                    const Expression<Vector3> &w_g_,
                                    const Expression<Vector6> &f_tau)
{
  // TODO: currently an empty value is pass to expression constructor, change this to a pointer?
  Vector6_ obj_acc = Vector6().setConstant(0.0);
  Vector3_ omega = Vector3().setConstant(0.0);
  //compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega);
  
  Expression<Vector1> m = Mass(w);
  
  Expression<Vector3> h_c = mass_com_(w); // m*r_c
  Expression<Matrix33> H = Inertia(w,R);

  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,h_c,H);
  // B matrix
  Vector3_ obj_g_ = inv_proj_vec_(x_t_2,w_g_);
  Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,obj_g_);

  // TODO add dt_ * dt_ *  to dyn_bias
  Vector6_ err = matmul_(spatial_H,obj_acc) - dyn_bias;
  return err;

}

Vector6_ create_symbolic_dynamics_mfix(  const Expression<Vector7> &w,
                                         const Expression<Rot3> &R,
                                         const Expression<Pose3> &x_t_2,
                                         const Expression<Pose3> &x_t_1,
                                         const Expression<Pose3> &x_t,
                                         const Expression<double> &t_2,
                                         const Expression<double> &t_1,
                                         const Expression<double> &t_0,
                                         const Expression<Vector3> &w_g_,
                                         const Expression<Vector6> &f_tau,
                                         const Expression<Vector1> &fix_mass)
{
  // TODO: currently an empty value is pass to expression constructor, change this to a pointer?
  Vector6_ obj_acc = Vector6().setConstant(0.0);
  Vector3_ omega = Vector3().setConstant(0.0);
  Vector3_ omega_10 = Vector3().setConstant(0.0);

  compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega,omega_10);
  //compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega);
  
  Expression<Vector1> m = Mass(w);
  
  Expression<Vector3> h_c = matmul_(Com(w),m); // m*r_c
  Expression<Matrix33> H = Inertia(w,R);

  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,h_c,H);
  // B matrix
  Vector3_ obj_g_ = inv_proj_vec_(x_t_2,w_g_);
  Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,omega_10,obj_g_);
  //Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,obj_g_);

  // TODO add dt_ * dt_ *  to dyn_bias
  Vector6_ err = matmul_(spatial_H,obj_acc) - dyn_bias;
  return err;

}

Vector6_ create_rr_symbolic_dynamics(  const Expression<Vector7> &w,
                                       const Expression<Rot3> &R,
                                       const Expression<Pose3> &x_t_2,
                                       const Expression<Pose3> &x_t_1,
                                       const Expression<Pose3> &x_t,
                                       const Expression<double> &t_2,
                                       const Expression<double> &t_1,
                                       const Expression<double> &t_0,
                                       const Expression<Vector3> &w_g_,
                                       const Expression<Vector6> &f_tau)
{
  // TODO: currently an empty value is pass to expression constructor, change this to a pointer?
  Vector6_ obj_acc = Vector6().setConstant(0.0);
  Vector3_ omega = Vector3().setConstant(0.0);
  //compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega);
  
  Expression<Vector1> m = Mass(w);
  
  Expression<Vector3> h_c = mass_com_(w); // m*r_c
  Expression<Matrix33> H = Inertia(w,R);

  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,h_c,H);
  // B matrix
  Vector3_ obj_g_ = inv_proj_vec_(x_t_2,w_g_);
  Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,obj_g_);

  // TODO add dt_ * dt_ *  to dyn_bias
  Vector6_ err = matmul_(spatial_H,obj_acc) - dyn_bias;
  return err;

}

Vector6_ create_symbolic_linear_dynamics(const Expression<Vector7> &w,
                                         const Expression<Vector3> &r,
                                         const Expression<Pose3> &x_t_2,
                                         const Expression<Pose3> &x_t_1,
                                         const Expression<Pose3> &x_t,
                                         const Expression<double> &t_2,
                                         const Expression<double> &t_1,
                                         const Expression<double> &t_0,
                                         const Expression<Vector3> &w_g_,
                                         const Expression<Vector6> &f_tau)
{
  // t_2,t_1,( t_0 = t)
  Vector6_ obj_acc = Vector6().setConstant(0.0);
  Vector3_ omega = Vector3().setConstant(0.0);
  Vector3_ omega_10 = Vector3().setConstant(0.0);

  compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega,omega_10);

  
  
  Expression<Vector1> m = Mass(w);
  Expression<Vector3> h_c = matmul_(Com(w),m); // m*r_c

  
  //Expression<Vector3> h_c = Com(w);
  Expression<Matrix33> H = inertia_linear_(w,r);
  
  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,h_c,H);
  // B matrix

  // transform world gravity to object gravity
  Vector3_ obj_g_ = inv_proj_vec_(x_t_2,w_g_);
  Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,omega_10,obj_g_);

  //Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,obj_g_);
  
  // TODO add dt_ * dt_ *  to dyn_bias
  Vector6_ err = matmul_(spatial_H,obj_acc) - dyn_bias;

  return err;

}

Vector6_ create_forward_dynamics(const Expression<Vector1> &m,
                                 const Expression<Vector3> &r_c,
                                 const Expression<Matrix33> &H,
                                 const Expression<Pose3> &x_t_2,
                                 const Expression<Pose3> &x_t_1,
                                 const Expression<Pose3> &x_t,
                                 const Expression<double> &t_2,
                                 const Expression<double> &t_1,
                                 const Expression<double> &t_0,
                                 const Expression<Vector3> &w_g_,
                                 const Expression<Vector6> &f_tau)
{
  // t_2,t_1,( t_0 = t)

  Vector6_ obj_acc = Vector6();
  Vector3_ omega = Vector3();
  //compute_pose_derivatives(x_t_2,x_t_1,x_t,t_2,t_1,t_0,obj_acc,omega);

  Expression<Vector3> h_c = matmul_(r_c,m); // m*r_c

  
  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,h_c,H);
  // B matrix

  // transform world gravity to object gravity
  Vector3_ obj_g_ = inv_proj_vec_(x_t_2,w_g_);
  Vector6_ dyn_bias = f_tau + create_dyn_bias(m,h_c,H,omega,obj_g_);
  
  // TODO add dt_ * dt_ *  to dyn_bias
  Vector6_ err = matmul_(spatial_H,obj_acc) - dyn_bias;

  return err;

}

// Projection to manifold

// h_c = m * r_c
Matrix44 proj_inertia(const Vector1 &m, const Vector3 &h_c, const Matrix33 &H,
                      gtsam::OptionalJacobian<16,1> H1,
                      gtsam::OptionalJacobian<16,3> H2,
                      gtsam::OptionalJacobian<16,9> H3 )
{
  Matrix44 p;
  p.setZero();
  p(3,3) = m(0);
  //p.block(3,3,1,1) = m(0);
  p.block(3,0,1,3) = h_c.transpose();
  p.block(0,3,3,1) = h_c;
  p.block(0,0,3,3) = 0.5 * H.trace() * Matrix33::Identity() - H ;

  if(H1)
  {
    *H1 = Eigen::Matrix<double,16,1>().setZero();

    H1->coeffRef(4*3+3,0) = 1.0;
  }

  
  if(H2)
  {
    *H2 = Eigen::Matrix<double,16,3>().setZero();
    // column, row
    H2->coeffRef(4*3+0,0) = 1.0;

    H2->coeffRef(3*4+1,1) = 1.0;

    H2->coeffRef(3*4+2,2) = 1.0;
    
    
    H2->coeffRef(0*3+3,0) = 1.0;
    
    
    H2->coeffRef(1*4+3,1) = 1.0;
    
    
    H2->coeffRef(2*4+3,2) = 1.0;

  }
  
  if(H3)
  {
    *H3 = Eigen::Matrix<double,16,9>().setZero();

    //H3->coeffRef(4*3+3,0) = 1.0;
    // diagonal elements:
    
    Matrix44 grad_mat;
    

    for(int i = 0; i<3 ;++i)
    {
      grad_mat.setZero();

      grad_mat.block(0,0,3,3)= 0.5  * Matrix33::Identity() -
        grad_matrix<Eigen::Matrix<double,3,3>>(i,i);
    
      H3->block(0,i*3+i,16,1) = eigen_reshape(grad_mat, 16,1);
    }

    
    // non diagonal terms:
    for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3;++j)
      {
        if(i==j)
        {
          continue;
        }
        grad_mat.setZero();
        grad_mat.block(0,0,3,3) = -1.0 * grad_matrix<Eigen::Matrix<double,3,3>>(i,j);
        H3->block(0,j*3+i,16,1) = eigen_reshape(grad_mat,16,1);
        
      }
    }
    
  }
  
  return p;
}


Expression<Matrix44> proj_inertia_(const Expression<Vector1> &m, const Expression<Vector3> &h_c,
                                   const Expression<Matrix33> &H)
{
  return Expression<Matrix44>(&proj_inertia,m,h_c,H);
  
}

Vector1 retract_inertia_mass(const Matrix44 &P,
                             gtsam::OptionalJacobian<1,16> H1)
{
  Vector1 m;
  m(0) = P(3,3);
  if(H1)
  {
    *H1 = Eigen::Matrix<double,1,16>().setZero();

    H1->coeffRef(0,4*3+3) = 1.0;

  }
  return m;
}


Vector1_ retract_inertia_mass_(const Expression<Matrix44> &P)
{
  return Vector1_(&retract_inertia_mass,P);
}

Vector3 retract_inertia_com(const Matrix44 &P,
                             gtsam::OptionalJacobian<3,16> H1)
{
  Vector3 cm = 0.5 * (P.block(0,3,3,1) + P.block(3,0,1,3).transpose());
  if(H1)
  {
    *H1 = Eigen::Matrix<double,3,16>().setZero();
    H1->coeffRef(0,4*3+0) = 1.0;
    H1->coeffRef(0,0*3+3) = 1.0;
    
    H1->coeffRef(1,3*4+1) = 1.0;
    
    H1->coeffRef(1,1*4+3) = 1.0;
    
    H1->coeffRef(2,3*4+2) = 1.0;
    
    H1->coeffRef(2,2*4+3) = 1.0;
    *H1 = 0.5 * *H1;

  }
  return cm;
}

Vector3_ retract_inertia_com_(const Expression<Matrix44> &P)
{
  return Vector3_(&retract_inertia_com,P);
}


Matrix33 retract_inertia_h(const Matrix44 &P,
                           gtsam::OptionalJacobian<9,16> H1)
{
  Matrix33 h = P.block(0,0,3,3).trace() * Matrix33::Identity() - P.block(0,0,3,3);

  if(H1)
  {
     *H1 = Eigen::Matrix<double,9,16>().setZero();
     Matrix44 grad_mat;
    

    for(int i = 0; i<3 ;++i)
    {
      grad_mat.setZero();

      grad_mat.block(0,0,3,3)= 1.0 * Matrix33::Identity() -
        grad_matrix<Eigen::Matrix<double,3,3>>(i,i);
    
      H1->block(i*3+i,0,1,16) = eigen_reshape(grad_mat,1,16);
    }

    
    // non diagonal terms:
    for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3;++j)
      {
        if(i==j)
        {
          continue;
        }
        grad_mat.setZero();
        grad_mat.block(0,0,3,3) = -1.0 * grad_matrix<Eigen::Matrix<double,3,3>>(i,j);
        H1->block(j*3+i,0,1,16) = eigen_reshape(grad_mat,1,16);
        
      }
    }

  }
  return h;
}
Expression<Matrix33> retract_inertia_h_(const Expression<Matrix44> &P)
{
  return Expression<Matrix33>(&retract_inertia_h,P);
}



// Reimmannian distance metric for two sets of projection inertial parameters:
Vector1 inertia_distance(const Matrix44 &p_des, const Matrix44 &p_curr,
                         gtsam::OptionalJacobian<1,16> H1,
                         gtsam::OptionalJacobian<1,16> H2)
{
  Vector1 dist;

  Matrix44 p_inv = p_des.inverse();
  //std::cout<<p_curr.determinant()<<std::endl;
  //dist(0) = -log(p_curr.determinant()) + (p_inv * p_curr).trace();
  dist(0) = (p_inv * p_curr).trace();
  //dist(0) = -log(p_curr.determinant());// + (p_inv * p_curr).trace();


  // gradients:
  if(H1)
  {
    *H1 = Eigen::Matrix<double,1,16>().setZero();

    Matrix44 grad = p_des.inverse() * p_curr;

    for(int i=0;i<4;++i)
    {
      for(int j=0; j<4;++j)
        {
          H1->coeffRef(0,j*4+i)+= ( -1.0 * p_inv * grad_matrix<Eigen::Matrix<double,4,4>>(i,j) * grad).trace();
        }
    } 

  }
  if(H2) 
  {
    *H2 = Eigen::Matrix<double,1,16>().setZero();
    //*H2 = 0.0 *  -1.0 * eigen_reshape(p_curr.inverse().transpose(),1,16);


    for(int i=0;i<4;++i)
    {
      for(int j=0; j<4;++j)
        {
          H2->coeffRef(0,j*4+i)+=(p_inv * grad_matrix<Eigen::Matrix<double,4,4>>(i,j)).trace();
        }
    }
    
    
        
  }

  return dist;
}
Expression<Vector1> inertia_distance_(const Expression<Matrix44> &p1, const Expression<Matrix44> &p2)
{
  return Vector1_(&inertia_distance,p1,p2);
  
}


Vector1_ create_between_inertia(  const Expression<Vector7> &w_des,
                                  const Expression<Rot3> &R_des,
                                  const Expression<Vector7> &w,
                                  const Expression<Rot3> &R)
{
  //
  Expression<Vector1> m = Mass(w);
  
  //Expression<Vector3> r_c = Com(w);
  Expression<Vector3> h_c = matmul_(Com(w),m); // m*r_c

  //Expression<Vector3> h_c = mass_com_(w); // m*r_c

  Expression<Matrix33> H = Inertia(w,R);

  Expression<Vector1> m_d = Mass(w_des);
  //Expression<Vector3> r_c_d = Com(w_des);
  Expression<Vector3> h_c_d =  matmul_(Com(w_des),m_d);//mass_com_(w_des); // m*r_c

  Expression<Matrix33> H_d = Inertia(w_des, R_des);

  return inertia_distance_(proj_inertia_(m_d,h_c_d,H_d),proj_inertia_(m,h_c,H));
}

Vector1_ create_between_inertia_lin(  const Expression<Vector7> &w_des,
                                      const Expression<Vector3> &R_des,
                                      const Expression<Vector7> &w,
                                      const Expression<Rot3> &R)
{
  //
  Expression<Vector1> m = Mass(w);
  
  //Expression<Vector3> r_c = Com(w);
  Expression<Vector3> h_c = matmul_(Com(w),m); // m*r_c

  //Expression<Vector3> h_c = mass_com_(w); // m*r_c

  Expression<Matrix33> H = Inertia(w,R);

  Expression<Vector1> m_d = Mass(w_des);
  //Expression<Vector3> r_c_d = Com(w_des);
  Expression<Vector3> h_c_d =  matmul_(Com(w_des),m_d);//mass_com_(w_des); // m*r_c
  Expression<Matrix33> H_d = inertia_linear_(w_des,R_des);

  //Expression<Matrix33> H_d = Inertia(w_des, R_des);

  return inertia_distance_(proj_inertia_(m_d,h_c_d,H_d),proj_inertia_(m,h_c,H));
}

Vector1_ create_between_linear_inertia(  const Expression<Vector7> &w_des,
                                         const Expression<Vector3> &R_des,
                                         const Expression<Vector7> &w,
                                         const Expression<Vector3> &R)
{
  //
  Expression<Vector1> m = Mass(w);
  Expression<Vector3> h_c = mass_com_(w); // m*r_c

  //Expression<Vector3> r_c = Com(w);
  Expression<Matrix33> H = inertia_linear_(w,R);

  Expression<Vector1> m_d = Mass(w_des);
  Expression<Vector3> h_c_d = mass_com_(w_des); // m*r_c

  //Expression<Vector3> r_c_d = Com(w_des);
  Expression<Matrix33> H_d = inertia_linear_(w_des, R_des);

  return inertia_distance_(proj_inertia_(m_d,h_c_d,H_d),proj_inertia_(m,h_c,H));
}




// retract to linear opt parameters
Vector7_ retract_linear_opt_(const Expression<Matrix44> &P)
{
  // mass:
  const static Matrix71 m_proj = (Matrix71()<<
                                  1,0,0,0,0,0,0).finished();

  Vector7_ w_opt =  matmul_(Expression<Matrix71>(m_proj),retract_inertia_mass_(P));
  const static Matrix73 c_proj = (Matrix73()<<
                                  0,0,0,
                                  1,0,0,
                                  0,1,0,
                                  0,0,1,
                                  0,0,0,
                                  0,0,0,
                                  0,0,0).finished();

  w_opt += matmul_(Expression<Matrix73>(c_proj),retract_inertia_com_(P));

  Expression<Matrix33> H = retract_inertia_h_(P);

  // extract only diagonal elements
  const static Expression<Matrix33> h1_proj = (Matrix33()<<
                                               1,0,0,
                                               0,0,0,
                                               0,0,0).finished();

  const static Expression<Matrix33> h2_proj = (Matrix33()<<
                                               0,0,0,
                                               0,1,0,
                                               0,0,0).finished();
  const static Expression<Matrix33> h3_proj = (Matrix33()<<
                                               0,0,0,
                                               0,0,0,
                                               0,0,1).finished();

  const static Expression<Matrix31> h_proj = (Matrix31() << 1,1,1).finished();
  
  const static Matrix73 hc_proj = (Matrix73()<<
                                   0,0,0,
                                   0,0,0,
                                   0,0,0,
                                   0,0,0,
                                   1,0,0,
                                   0,1,0,
                                   0,0,1).finished();


  w_opt += matmul_(Expression<Matrix73>(hc_proj),
                          matmul_(matmul_(matmul_(h1_proj,H),h1_proj) +
                                  matmul_(matmul_(h2_proj,H),h2_proj) +
                                  matmul_(matmul_(h3_proj,H),h3_proj),h_proj));

  
  return w_opt;
}
// retract to linear inertial parameters
Vector7 retract_linear_inertial(const Vector7 &w_opt, gtsam::OptionalJacobian<7,7> H1)
{
  Vector7 w = w_opt;
  w(1) = w_opt(1)/w_opt(0);
  w(2) = w_opt(2)/w_opt(0);
  w(3) = w_opt(3)/w_opt(0);

  if(H1)
  {
    *H1 =  Eigen::Matrix<double,7,7>().setIdentity();
    H1->coeffRef(1,0) = -w_opt(1)/(w_opt(0)*w_opt(0));
    H1->coeffRef(2,0) = -w_opt(2)/(w_opt(0)*w_opt(0));
    H1->coeffRef(3,0) = -w_opt(3)/(w_opt(0)*w_opt(0));
    
    H1->coeffRef(1,1) = 1.0/w_opt(0);
    H1->coeffRef(2,2) = 1.0/w_opt(0);
    H1->coeffRef(3,3) = 1.0/w_opt(0);
    
  }
  return w;
}

Vector7_ retract_linear_inertial_(const Vector7_ &w_opt)
{
  return Vector7_(&retract_linear_inertial,w_opt);
}

// create physics inequality constraint

// linear
// w_opt is assumed to be m,r_c, H and not m, m*r_c, H
Vector7_ physics_constraint_linear_(const Vector7_ &w_opt)
{
  return physics_constraint_(w_opt);
}
// manifold
Vector7_ physics_constraint_manifold_(const Expression<Matrix44> &P)
{
  return physics_constraint_(retract_linear_inertial_(retract_linear_opt_(P)));
}


// Difference between X1 and X2 ( X1-X2)
Vector6 pose_diff(const Pose3 &X1,const Pose3 &X2,
                  gtsam::OptionalJacobian<6,6> H1,
                  gtsam::OptionalJacobian<6,6> H2) 
{
  Vector6 retVal = X1.Logmap(X1,H1) - X2.Logmap(X2,H2);

  if (H2)
  {
    *H2 = -1.0 * *H2;
  }
  return retVal;
}
Vector6_ pose_diff_(const Expression<Pose3> &X1, const Expression<Pose3> &X2)
{
  return Vector6_(&pose_diff, X1, X2);
}

Vector6_ variable_dt_acc_(const Expression<Pose3> &x_t_2,
                     const Expression<Pose3> &x_t_1,
                     const Expression<Pose3> &x_t_0,
                     const Expression<double> &t_2,
                     const Expression<double> &t_1,
                     const Expression<double> &t_0)
{
  Vector6_ vel_t_1 = scalar_divide_(pose_diff_(x_t_1,x_t_2),t_1-t_2);
  Vector6_ vel_t_0 = scalar_divide_(pose_diff_(x_t_0,x_t_1),t_0-t_1);

  Vector6_ acc = scalar_divide_(vel_t_0 - vel_t_1,t_0-t_1);
  return acc;
}

Vector6_ variable_dt_jerk_(const Expression<Pose3> &x_t_3,
                           const Expression<Pose3> &x_t_2,
                           const Expression<Pose3> &x_t_1,
                           const Expression<Pose3> &x_t_0,
                           const Expression<double> &t_3,
                           const Expression<double> &t_2,
                           const Expression<double> &t_1,
                           const Expression<double> &t_0)
{
  Vector6_ acc_t_0 = variable_dt_acc_(x_t_2,x_t_1,x_t_0,t_2,t_1,t_0);
  Vector6_ acc_t_1 = variable_dt_acc_(x_t_3,x_t_2,x_t_1,t_3,t_2,t_1);
  
  Vector6_ jerk = scalar_divide_(acc_t_0 - acc_t_1,t_0-t_1);
  return jerk;
}

}
