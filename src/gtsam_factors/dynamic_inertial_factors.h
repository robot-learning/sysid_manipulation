#include <gtsam/nonlinear/NonlinearFactor.h>

using namespace gtsam;

class SmoothBetweenFactor : public NoiseModelFactor2<Point3,Point3>
{
public:
  SmoothBetweenFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2) :
    NoiseModelFactor2<Point3,Point3>(noiseModel, j1,j2) {}
  
  Vector evaluateError(const Point3 &X1, const Point3 &X2,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none) const
  {
    if(H1) *H1 =(gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();

    if(H2) *H2 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    Vector3 err=X2-X1;
    
    return err;
  }
  
};

class SmoothBetweenFactorD : public NoiseModelFactor2<Vector1,Vector1>
{
public:
  SmoothBetweenFactorD(const SharedNoiseModel& noiseModel, Key j1, Key j2) :
    NoiseModelFactor2<Vector1,Vector1>(noiseModel, j1,j2) {}
  
  Vector evaluateError(const Vector1 &X1, const Vector1 &X2,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none) const
  {
    if(H1) *H1 =(gtsam::Matrix11() << -1).finished();

    if(H2) *H2 =(gtsam::Matrix11() << 1).finished();

    Vector1 err=X2-X1;
    
    return err;
  }
  
};

class TransformFactor : public NoiseModelFactor3<Point3,Point3,Point3>
{
  // x1=O_r_i, x2=O_r_j, x3=i_r_j
public:
  TransformFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3) :
    NoiseModelFactor3<Point3,Point3,Point3>(noiseModel, j1, j2, j3) {}
  
  Vector evaluateError(const Point3 &X1, const Point3 &X2, const Point3 &X3,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none) const
  {
    //Pose3 o_r_i(Rot3(),X1);
    //Pose3 o_r_j(Rot3(),X2);
    
    Vector3 transformed_pt=X2-X1;//(o_r_i.inverse()*o_r_j).translation();
    
    //Vector3 transformed_pt=(o_r_i.inverse()*o_r_j).translation();
    
    Vector3 err=transformed_pt-X3;
    
    if(H1) *H1 =(gtsam::Matrix33() << -1,0,0,
                 0,-1,0,
                 0,0,-1).finished();


    if(H2) *H2 =(gtsam::Matrix33() << 1,0,0,
                 0,1,0,
                 0,0,1).finished();
    

    if(H3) *H3 =(gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();

    
    return err;
  }

};


class TorqueForceFactor : public NoiseModelFactor3<Point3,Point3,Point3>
{
  // x1=r, x2=f, x3=tau
public:
  TorqueForceFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3) :
    NoiseModelFactor3<Point3,Point3,Point3>(noiseModel, j1, j2, j3) {}
  
  Vector evaluateError(const Point3 &X1, const Point3 &X2, const Point3 &X3,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none) const
  {
    if(H1) *H1 =(gtsam::Matrix33() << 0, X2.z(),-X2.y(),
                 -X2.z(),0,X2.x(),
                 X2.y(),-X2.x(),0).finished();

    if(H2) *H2 =(gtsam::Matrix33() << 0, -X1.z(), X1.y(),
                 X1.z(),0,-X1.x(),
                 -X1.y(),X1.x(),0).finished();

    if(H3) *H3 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();


    Vector3 torque(X1.y()*X2.z()-X1.z()*X2.y(),
                   X1.z()*X2.x()-X1.x()*X2.z(),
                   X1.x()*X2.y()-X1.y()*X2.x());
    Vector3 err=X3-torque;
    
    return err;
  }

};

/*
// transforms the point3 to pose3 frame
class ContactTransformFactor: public NoiseModelFactor2<Point3,Pose3>
{
public:
  ContactTransformFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2) :
    NoiseModelFactor2<Point3,Pose3>(noiseModel, j1,j2) {}
  Vector evaluateError(const Point3 &X1, const Pose3 &X2,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none) const
  {
    if(H1) *H1 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    if(H2) *H2 =X3[0]*(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    Vector3 err=X2-X1;
    
    return err;
  }

};
*/
// given object velocity at 1,2, netforce, gravity, mass
class linearAccFactor : public NoiseModelFactor5<Point3,Point3,Point3,Point3, Vector1 >
{
public:
  linearAccFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3, Key j4, Key j5) :
    NoiseModelFactor5<Point3,Point3,Point3,Point3,Vector1>(noiseModel, j1,j2,j3,j4,j5) {}

  Vector evaluateError(const Point3 &X1, const Point3 &X2, const Point3 &X3, const Point3 &X4, const Vector1 &X5,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none) const
  {

    if(H1) *H1 =(gtsam::Matrix33() << -1,0,0,
                 0,-1,0,
                 0,0,-1).finished();
    if(H2) *H2= (gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();

    if(H3) *H3= (gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();

    if(H4) *H4= (gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();
    
    if(H5) *H5= (gtsam::Matrix31() << 1, 0,0).finished();
    
    Vector3 err=X5[0]*((X2-X1)*DELTA_T-X4)-X3;
    
    return err;
  }
};

class SumFactor : public NoiseModelFactor5<Point3,Point3,Point3,Point3,Point3>
{
public:
  SumFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3, Key j4, Key j5) :
    NoiseModelFactor5<Point3,Point3,Point3,Point3,Point3>(noiseModel, j1,j2,j3,j4,j5) {}

  Vector evaluateError(const Point3 &X1, const Point3 &X2, const Point3 &X3, const Point3 &X4, const Point3 &X5,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none) const
  {

    if(H1) *H1 =(gtsam::Matrix33() << -1,0,0,
                 0,-1,0,
                 0,0,-1).finished();
    if(H2) *H2= (gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();

    if(H3) *H3= (gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();

    if(H4) *H4= (gtsam::Matrix33() << -1, 0,0,
                 0,-1,0,
                 0,0,-1).finished();
    if(H5) *H5= (gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    Vector3 err=X5-(X1+X2+X3+X4);
    
    return err;
  }
};

class MassFactor : public NoiseModelFactor3<Point3,Point3,Vector1>
{
public:
  MassFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3) :
    NoiseModelFactor3<Point3,Point3,Vector1>(noiseModel, j1,j2,j3) {}
  Vector evaluateError(const Point3 &X1, const Point3 &X2, const Vector1 &X3,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none) const
  {
    if(H1) *H1 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    if(H2) *H2 =X3[0]*(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();
    if(H3) *H3 = (gtsam::Matrix31()<< X2[0],X2[1],X2[2]).finished();

    Vector3 err=X3[0]*X2+X1;
    
    return err;
  }
};

class CoMFactor : public NoiseModelFactor4<Point3,Point3,Point3,Vector1>
{
  // com, gravity, sum torque, mass
public:
  CoMFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3, Key j4) :
    NoiseModelFactor4< Point3, Point3, Point3, Vector1>(noiseModel, j1,j2,j3,j4) {}
  
  Vector evaluateError(const Point3 &X1, const Point3 &X2,const Point3 &X3, const Vector1 &X4,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {

    if(H1) *H1 =X4[0]*(gtsam::Matrix33() << 0, X2.z(),-X2.y(),
                 -X2.z(),0,X2.x(),
                 X2.y(),-X2.x(),0).finished();

    if(H2) *H2 =X4[0]*(gtsam::Matrix33() << 0, -X1.z(), X1.y(),
                 X1.z(),0,-X1.x(),
                 -X1.y(),X1.x(),0).finished();

    
    
    if(H3) *H3 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();
    

    Vector3 torque_g(X1.y()*X2.z()-X1.z()*X2.y(),
                   X1.z()*X2.x()-X1.x()*X2.z(),
                   X1.x()*X2.y()-X1.y()*X2.x());

    if(H4) *H4 = (gtsam::Matrix31()<< torque_g[0],torque_g[1],torque_g[2]).finished();

    Vector3 err=X4[0]*torque_g+X3;
    
    return err;
  }

};

class CoMFullFactor : public NoiseModelFactor
{
  // com, gravity, torque 1-4, mass
protected:

  typedef NoiseModelFactor Base;
  typedef CoMFullFactor This;

  
public:
  typedef Point3 X1;
  typedef Point3 X2;
  typedef Point3 X3;
  typedef Point3 X4;
  typedef Point3 X5;
  typedef Point3 X6;
  typedef Vector1 X7;
  
  
  
  //Point3 X1,X2,X3,X4,X5,X6;
  //Vector1 X7;
  
  CoMFullFactor(){}

  virtual ~CoMFullFactor() {}

    /** methods to retrieve keys */
  inline Key key1() const { return keys_[0]; }
  inline Key key2() const { return keys_[1]; }
  inline Key key3() const { return keys_[2]; }
  inline Key key4() const { return keys_[3]; }
  inline Key key5() const { return keys_[4]; }
  inline Key key6() const { return keys_[5]; }
  inline Key key7() const { return keys_[6]; }


  CoMFullFactor(const SharedNoiseModel& noiseModel, Key j1, Key j2, Key j3, Key j4, Key j5, Key j6, Key j7) :
    Base(noiseModel,cref_list_of<7>(j1)(j2)(j3)(j4)(j5)(j6)(j7)) {}

  
  /** Calls the 6-key specific version of evaluateError, which is pure virtual
   * so must be implemented in the derived class. */
  virtual Vector unwhitenedError(const Values& x, boost::optional<std::vector<Matrix>&> H = boost::none) const
  {
    if(this->active(x))
    {
      if(H)
      {
        return evaluateError(x.at<X1>(keys_[0]), x.at<X2>(keys_[1]), x.at<X3>(keys_[2]),
                             x.at<X4>(keys_[3]), x.at<X5>(keys_[4]), x.at<X6>(keys_[5]),
                             x.at<X7>(keys_[6]),
                             (*H)[0], (*H)[1], (*H)[2], (*H)[3], (*H)[4],
                             (*H)[5],
                             (*H)[6]);
      }
      else
      {
        return Vector::Zero(this->dim());
        
        return evaluateError(x.at<X1>(keys_[0]), x.at<X2>(keys_[1]), x.at<X3>(keys_[2]), x.at<X4>(keys_[3]), x.at<X5>(keys_[4]), x.at<X6>(keys_[5]), x.at<X7>(keys_[6]));
        
      }
    }
    else
    {
      return Vector::Zero(this->dim());
    }
  }
  
  Vector evaluateError(const Point3 &X1, const Point3 &X2,const Point3 &X3, const Point3 &X4, const Point3 &X5,
                       const Point3 &X6, const Vector1 &X7,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none,
                       boost::optional<Matrix&> H6 = boost::none,
                       boost::optional<Matrix&> H7 = boost::none) const
  {
    // compute sum torque"
    //Vector3 sum_torque=X3+X4+X5+X6;
    
    if(H1) *H1 =X7[0]*(gtsam::Matrix33() << 0, X2.z(),-X2.y(),
                 -X2.z(),0,X2.x(),
                 X2.y(),-X2.x(),0).finished();

    if(H2) *H2 =X7[0]*(gtsam::Matrix33() << 0, -X1.z(), X1.y(),
                 X1.z(),0,-X1.x(),
                 -X1.y(),X1.x(),0).finished();

    
    
    if(H3) *H3 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    if(H4) *H4 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();
    if(H5) *H5 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();
    if(H6) *H6 =(gtsam::Matrix33() << 1, 0,0,
                 0,1,0,
                 0,0,1).finished();

    Vector3 torque_g(X1.y()*X2.z()-X1.z()*X2.y(),
                   X1.z()*X2.x()-X1.x()*X2.z(),
                   X1.x()*X2.y()-X1.y()*X2.x());

    
    if(H7) *H7 = (gtsam::Matrix31()<< torque_g[0],torque_g[1],torque_g[2]).finished();
    
    Vector3 err=X7[0]*torque_g+X3+X4+X5+X6;
    
    return err;
  }
private:

  /** Serialization function */
  friend class boost::serialization::access;
  template<class ARCHIVE>
  void serialize(ARCHIVE & ar, const unsigned int /*version*/) {
    ar & boost::serialization::make_nvp("NoiseModelFactor",
        boost::serialization::base_object<Base>(*this));
  }

};
