#pragma once
#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/geometry/Pose3.h>

#include <sysid_manipulation/supp_fns.hpp>
using namespace gtsam;
class BtFactor : public NoiseModelFactor4< Vector12, Vector12, Vector7, Rot3>
{
 private:
  Vector3 g_;
  Vector6 acc;
  Vector6 vel;
  
 public:
  
  BtFactor(Key j3, Key j4, Key j5, Key j6, const SharedNoiseModel& noiseModel, Vector3 g_vec, Vector6 acc_, Vector6 vel_):
    NoiseModelFactor4<Vector12, Vector12, Vector7, Rot3> (noiseModel, j3, j4, j5,j6),g_(g_vec),acc(acc_),vel(vel){}

  Vector gradientTester( const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                         const Rot3 &R_i )
  {

        // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) * skew_rc * skew_rc;
    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
                      - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)-g_)
                      - inertia[0] * skew_rc * acc.head(3)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    return retVal;
  }
  Vector evaluateError(const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none,
                       boost::optional<Matrix&> H6 = boost::none) const
  {
    Vector3 g_v = 1.0 * g_;

    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = supp_fns::skew(vel.segment(0,3));//(gtsam::Matrix33() <<
   
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
                      - skew_rc * ( inertia[0]*g_v))
                      - torque; // Torque error

    
    retVal.tail(3) = inertia[0]*(acc.tail(3)-g_v)
      - inertia[0] * skew_rc * acc.head(3)
      + inertia[0] * skew_vel * skew_vel * q_rc 
      - force; // X2 should be negative?  Force error

    /*
    cout<<"BT:"<<endl;
    cout<<inertia.transpose()<<endl;
    cout<<retVal.tail(3).transpose()<<endl;
    */
    // compute gradients: 6xn
    // force: 6 x 12
    if(H3)
    {
      
      *H3 = Eigen::Matrix<double,6,12>().setZero();

      for(int i=0;i<4;++i)
      {
        // linear acceleration:

        H3->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * -1.0;
        // angular acceleration:
        
        H3->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * -1.0;
      }
      
    }


    // contact points: 6 x 12
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,12>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<4;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H4->block(0,i*3,3,3) = -1.0 * H_rc;
      }
    }


    // inertia, com, mass 6x10
    if(H5)
    {

      *H5 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H5->block(0,0,3,1) = skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(0,0,3,1) = (skew_rc * acc.tail(3)
                            - skew_rc * skew_rc * acc.head(3)
                            - skew_vel * (skew_rc * skew_rc) * vel.head(3)
                            - skew_rc * g_v);

      //skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(3,0,3,1) = (acc.tail(3)-g_v) - skew_rc * acc.head(3) + skew_vel * skew_vel * q_rc;

      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(3,1,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(3,2,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(3,3,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;


      
      
      // gradient for body frame:
      
      H5->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();
      
      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(0,1,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * g_v;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(0,2,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * g_v;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(0,3,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,3,3,1) += -1.0 * skew_grad * inertia[0] * g_v;

      // gradient for inertia: ( )
      gtsam::Matrix63 H_inertia;
      //gtsam::Matrix33 Q = R_i.matrix() * R_i.matrix().transpose();
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);
      
      H_inertia.block(0,0,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *vel.head(3);

      H_inertia.block(0,1,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * vel.head(3);


      H_inertia.block(0,2,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * vel.head(3);
      H5->block(0,4,6,3) = H_inertia;
      


    }
    if(H6)
    {
      //Rot3 r_mat(gtsam::Rot3::Ypr(0.1,-0.1,1.1)); 
     
      *H6 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3))
        -1.0 * skew_vel * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * vel.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))
        + 1.0 * skew_vel * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * vel.head(3));
      
      H6->block(0,0,3,3) = first + last;
    }

    return retVal;
  }

};

class ParamBtFactor : public NoiseModelFactor2<Vector7, Rot3>
{
 private:
  Vector3 g_;
  Vector6 acc;
  Vector6 vel;
  Vector12 bt_pts;
  Vector12 bt_forces;
 public:
  
  ParamBtFactor(Key j5, Key j6, const SharedNoiseModel& noiseModel, Vector3 g_vec, Vector6 acc_, Vector6 vel_,Vector12 bt_pts_,Vector12 bt_forces_):
    NoiseModelFactor2<Vector7, Rot3> (noiseModel, j5,j6),g_(g_vec),acc(acc_),vel(vel),bt_pts(bt_pts_),bt_forces(bt_forces_){}


  
  Vector inertialError( const Vector10 &inertia)
  {
    // inertia = i_xx,i_yy,i_zz,i_xy,i_xz,i_yz
        // build inertia matrix:
    Matrix33 inertia_matrix = (gtsam::Matrix33() <<
                               inertia[4+0], inertia[4+3],inertia[4+4],
                               inertia[4+3],inertia[4+1],inertia[4+5],
                               inertia[4+4],inertia[4+5],inertia[4+2]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
                      - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)-g_)
                      - inertia[0] * skew_rc * acc.head(3)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    return retVal;
  }
  Vector gradientTester( const Vector7 &inertia,
                         const Rot3 &R_i )
  {

        // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) * skew_rc * skew_rc;
    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
        retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix ) * acc.head(3)
                      + skew_vel * (inertia_matrix) *vel.head(3) 
                      - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error
    
    retVal.tail(3) = inertia[0]*(acc.tail(3)-g_)
      - inertia[0] * skew_rc * acc.head(3)
      + inertia[0] * skew_vel * skew_vel * q_rc 
      - force; // X2 should be negative?  Force error
    return retVal;
  }
  Vector evaluateError(const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H5 = boost::none,
                       boost::optional<Matrix&> H6 = boost::none) const
  {
    Vector3 g_v = 1.0 * g_;

    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = supp_fns::skew(vel.segment(0,3));//(gtsam::Matrix33() <<
   
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.setZero();
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix ) * acc.head(3)
                      + skew_vel * (inertia_matrix) *vel.head(3) 
                      - skew_rc * ( inertia[0]*g_v))
                      - torque; // Torque error
    
    retVal.tail(3) = inertia[0]*(acc.tail(3)-g_v)
      - inertia[0] * skew_rc * acc.head(3)
      + inertia[0] * skew_vel * skew_vel * q_rc 
      - force; // X2 should be negative?  Force error
    // compute gradients: 6xn

    // inertia, com, mass 6x10
    if(H5)
    {

      *H5 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H5->block(0,0,3,1) = skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);
      H5->block(0,0,3,1) = (skew_rc * (acc.tail(3) -g_v));
      //skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(3,0,3,1) = (acc.tail(3)-g_v) - skew_rc * acc.head(3) + skew_vel * skew_vel * q_rc;

      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(3,1,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(3,2,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(3,3,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;


      
      
      // gradient for body frame:
      
      H5->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();
      
      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;

      H5->block(0,1,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      //H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      //H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      //H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      //H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * g_v;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(0,2,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      //H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      //H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      //H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      //H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * g_v;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(0,3,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      //H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      //H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      //H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      //H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,3,3,1) += -1.0 * skew_grad * inertia[0] * g_v;
      // gradient for inertia: ( )
      gtsam::Matrix63 H_inertia;
      //gtsam::Matrix33 Q = R_i.matrix() * R_i.matrix().transpose();
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);
      
      H_inertia.block(0,0,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *vel.head(3);

      H_inertia.block(0,1,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * vel.head(3);


      H_inertia.block(0,2,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * vel.head(3);
      H5->block(0,4,6,3) = H_inertia;
      

    }
    if(H6)
    {
      //Rot3 r_mat(gtsam::Rot3::Ypr(0.1,-0.1,1.1)); 
     
      *H6 = gtsam::Matrix63().setZero();
      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3))
        -1.0 * skew_vel * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * vel.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))
        + 1.0 * skew_vel * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * vel.head(3));
      H6->block(0,0,3,3) = first + last;
    }

    return retVal;
  }

};

class QS_BtFactor : public NoiseModelFactor3<Vector12, Vector12, Vector7>
{
 private:
  Vector3 g_;
  double w_;
  Vector6 vel;
 public:
  
  QS_BtFactor( Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Vector3 g_vec, Vector6 vel_,double w=1.0):
    NoiseModelFactor3<Vector12, Vector12, Vector7> (noiseModel, j2, j3, j4),g_(g_vec),vel(vel_),w_(w){}

  Vector gradientTester( const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia)
  {
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();


    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) =  - skew_rc * ( inertia[0]*g_)
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(g_)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    retVal = w_ * retVal;
    return retVal;
  }
  Vector evaluateError(const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();


    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) =  - skew_rc * ( inertia[0]*g_)
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(-1.0*g_)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    retVal = w_ * retVal;
    // compute gradients: 6xn

    // force: 6 x 12
    if(H2)
    {
      
      *H2 = Eigen::Matrix<double,6,12>().setZero();

      for(int i=0;i<4;++i)
      {
        // linear acceleration:

        H2->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * -1.0;
        // angular acceleration:
        
        H2->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * -1.0;
      }
      *H2 = w_ * *H2;

    }


    // contact points: 6 x 12
    if(H3)
    {
      *H3 = Eigen::Matrix<double,6,12>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<4;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H3->block(0,i*3,3,3) = -1.0 * H_rc;
      }
      *H3 = w_ * *H3;

    }


    // inertia, com, mass 6x10
    if(H4)
    {
            *H4 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H4->block(0,0,3,1) = skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H4->block(0,0,3,1) =  - skew_rc * g_;

      //skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H4->block(3,0,3,1) +=  skew_vel * skew_vel * q_rc;

      H4->block(3,0,3,1) += -1.0 * g_;
      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for body frame:
      
      H4->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();

      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H4->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;


      H4->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * g_;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H4->block(0,3,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      *H4 = w_ * *H4;


    }

    return retVal;
  }
  
};
class FtFactor : public NoiseModelFactor4<Vector3, Vector3, Vector7, Rot3>
{
private:
  Vector3 g_;
  gtsam::Pose3 Q_T_O_;  // transformation matrix
  Vector6 acc,vel;
  
public:

  FtFactor(Key j3, Key j4, Key j5, Key j6, const SharedNoiseModel& noiseModel, Vector3 g_vec, Vector6 obj_acc, Vector6 obj_vel, Pose3 Q_T_O = Pose3()):
    NoiseModelFactor4<Vector3, Vector3, Vector7, Rot3>(noiseModel,j3, j4,j5,j6),g_(g_vec), Q_T_O_(Q_T_O),acc(obj_acc),vel(obj_vel){}

  Vector gradientTester(const Vector6 &vel, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       const Rot3 &R_i) const
  {
            // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    // transform point:
    Point3 q_rc = Q_T_O_ * Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    
    Matrix33 inertia_matrix = Q_T_O_.rotation().transpose().matrix() * R_i.matrix() * inertia_diag * R_i.matrix().transpose() *  Q_T_O_.rotation().matrix();//  + inertia(0) * skew_rc * skew_rc;

    Vector6 retVal;
    
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc)
                      * vel.head(3) 
                      - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)-g_)
                      - inertia[0] * skew_rc * acc.head(3) 
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error


    return retVal;
  }

  Vector evaluateError(const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none,
                       boost::optional<Matrix&> H6 = boost::none) const
  {
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    // transform point:
    Point3 q_rc = Q_T_O_ * Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    
    Matrix33 inertia_matrix = Q_T_O_.rotation().transpose().matrix() * R_i.matrix() * inertia_diag * R_i.matrix().transpose() *  Q_T_O_.rotation().matrix();//  + inertia(0) * skew_rc * skew_rc;

    Vector6 retVal;
    Vector3 g_v = 1.0 * g_;
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
                      + skew_rc * ( inertia[0]*g_v))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)+g_v)
                      - inertia[0] * skew_rc * acc.head(3)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error


    // compute gradients: 3xn
    // force:
    if(H3)
    {
      *H3 = gtsam::Matrix63().setZero();
      H3->block(3,0,3,3) = gtsam::Matrix33().Identity() * -1.0;
    }


    if (H4) // 6x3 torque
    {
      *H4 = gtsam::Matrix63().setZero();
      H4->block(0,0,3,3) = gtsam::Matrix33().Identity() * -1.0;

    } 

    
    // inertia, com, mass 6x10
    if(H5)
    {
      *H5 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H5->block(0,0,3,1) = skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(0,0,3,1) = (skew_rc * acc.tail(3)
                            - skew_rc * skew_rc * acc.head(3)
                            - skew_vel * (skew_rc * skew_rc) * vel.head(3)
                            + skew_rc * g_v);

      //skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(3,0,3,1) = (acc.tail(3)+g_v) - skew_rc * acc.head(3) + skew_vel * skew_vel * q_rc;

      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(3,1,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(3,2,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(3,3,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;


      
      
      // gradient for body frame:
      
      H5->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();

      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(0,1,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,1,3,1) += 1.0 * skew_grad * inertia[0] * g_v;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(0,2,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,2,3,1) += 1.0 * skew_grad * inertia[0] * g_v;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(0,3,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,3,3,1) += 1.0 * skew_grad * inertia[0] * g_v;

      // gradient for inertia: ( )
      gtsam::Matrix63 H_inertia;
      //gtsam::Matrix33 Q = R_i.matrix() * R_i.matrix().transpose();
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);
      

      H_inertia.block(0,0,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *vel.head(3);

      H_inertia.block(0,1,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * vel.head(3);


      H_inertia.block(0,2,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * vel.head(3);

      H5->block(0,4,6,3) = H_inertia;


    }
    if(H6)
    {
      //Rot3 r_mat(gtsam::Rot3::Ypr(0.1,-0.1,1.1)); 
     
      *H6 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3))
         -1.0 * skew_vel * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * vel.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))
        + 1.0 * skew_vel * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * vel.head(3));
      
      H6->block(0,0,3,3) = first + last;
    }
    return retVal;
  }
};


