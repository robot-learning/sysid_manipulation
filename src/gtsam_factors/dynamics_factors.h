#pragma once
#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/geometry/Pose3.h>

#include <sysid_manipulation/supp_fns.hpp>
//typedef Eigen::Matrix<double, 10, 1> VectorTen;

using namespace gtsam;
using namespace std;
// object acceleration, force, torque, [mass, com, inertia]
class AccFtFactor : public NoiseModelFactor4<Vector6, Vector3, Vector3, Vector10>
{
private:
  Vector3 g_;
public:

  AccFtFactor(Key j1, Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Vector3 g_vec):
    NoiseModelFactor4<Vector6, Vector3, Vector3, Vector10>(noiseModel, j1, j2, j3, j4),g_(g_vec){}
  Vector evaluateError(const Vector6 &acc, const Vector3 &force, const Vector3 &torque, const Vector10 &inertia,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {
    
    // build inertia matrix:
    Matrix33 inertia_matrix = (gtsam::Matrix33() <<
                               inertia[4+0],inertia[4+3],inertia[4+4],
                               inertia[4+3],inertia[4+1],inertia[4+5],
                               inertia[4+4],inertia[4+5],inertia[4+2]).finished();
    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia[0]*g_) + torque;
    retVal.tail(3)=inertia[0]*(acc.tail(3)-g_)+force; // X2 should be negative..TODO


    // compute gradients: 3xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      H1->block(0,0,3,3)=inertia_matrix;
      H1->block(3,3,3,3)=gtsam::Matrix33().Identity() * inertia(0);

    }

    // force:
    if(H2)
    {
      *H2 = gtsam::Matrix63().setZero();
      H2->block(3,0,3,3) = gtsam::Matrix33().Identity() * 1.0;
    }


    if (H3) // 6x3 torque
    {
      *H3 = gtsam::Matrix63().setZero();
      H3->block(0,0,3,3) = gtsam::Matrix33().Identity() * 1.0;

    } 

    
    // inertia, com, mass 6x10
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,10>().setZero();
      // gradient for inertia:
      gtsam::Matrix66 H_inertia;
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = (gtsam::Matrix33()<<
                                  1,0,0,
                                  0,0,0,
                                  0,0,0).finished() * acc.head(3);

      H_inertia.block(0,1,3,1) = (gtsam::Matrix33()<<
                                  0,0,0,
                                  0,1,0,
                                  0,0,0).finished() * acc.head(3);


      H_inertia.block(0,2,3,1) = (gtsam::Matrix33()<<
                                  0,0,0,
                                  0,0,0,
                                  0,0,1).finished() * acc.head(3);
      H_inertia.block(0,3,3,1) = (gtsam::Matrix33()<<
                                  0,1,0,
                                  1,0,0,
                                  0,0,0).finished() * acc.head(3);
      
      H_inertia.block(0,4,3,1) = (gtsam::Matrix33()<<
                                  0,0,1,
                                  0,0,0,
                                  1,0,0).finished() * acc.head(3);
      H_inertia.block(0,5,3,1) = (gtsam::Matrix33()<<
                                  0,0,0,
                                  0,0,1,
                                  0,1,0).finished() * acc.head(3);

      H4->block(0,4,6,6) = H_inertia;

      // gradient for com:
      Matrix skew_H = gtsam::Matrix39().setZero();
      // add partial derivatives for skew vector:
      // for x:
      skew_H(2,1) = 1.0;
      skew_H(1,2) = -1.0;
      // for y:
      skew_H(0,2+3) = 1.0;
      skew_H(2,0+3) = -1.0;
      // for z:
      skew_H(0,1+6) = -1.0;
      skew_H(1,0+6) = 1.0;
      //cerr<<skew_H<<endl;
      Matrix constant_mg =  gtsam::Matrix93().setZero();
      constant_mg.block(0,0,3,1) = inertia[0] * g_;
      constant_mg.block(3,1,3,1) = inertia[0] * g_;
      constant_mg.block(6,2,3,1) = inertia[0] * g_;
      //cerr<<constant_mg<<endl;
      H4->block(0,1,3,3) = -1.0 * skew_H * constant_mg ;
      
      // gradient for mass:
      H4->block(0,0,3,1) = skew_rc * g_ * -1.0;
      H4->block(3,0,3,1) = (acc.tail(3)-g_);

    }
    return retVal;
  }
};

// object acceleration, force, torque, [mass, com, inertia]
class ManifoldAccFtFactor : public NoiseModelFactor5<Vector6, Vector3, Vector3, Vector7, Rot3>
{
private:
  Vector3 g_;
  gtsam::Pose3 Q_T_O_;  // transformation matrix
  
public:

  ManifoldAccFtFactor(Key j1, Key j2, Key j3, Key j4, Key j5, const SharedNoiseModel& noiseModel, Vector3 g_vec, Pose3 Q_T_O = Pose3()):
    NoiseModelFactor5<Vector6, Vector3, Vector3, Vector7, Rot3>(noiseModel, j1, j2, j3, j4,j5),g_(g_vec), Q_T_O_(Q_T_O){}

  Vector gradientTester(const Vector6 &acc, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       const Rot3 &R_i) const
  {
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();


    // transform point:
    Point3 q_rc = Q_T_O_ * Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    
    Matrix33 inertia_matrix = Q_T_O_.rotation().transpose().matrix() * R_i.matrix() * inertia_diag * R_i.matrix().transpose() *  Q_T_O_.rotation().matrix()  + inertia(0) * skew_rc * skew_rc;

    Vector6 retVal;
    retVal.setZero();
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia[0]*g_) + torque;
    
    retVal.tail(3)=inertia[0]*(acc.tail(3)-g_)+force; // X2 should be negative..TODO
    return retVal;
  }

  Vector evaluateError(const Vector6 &acc, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none) const
  {
        // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    // transform point:
    Point3 q_rc = Q_T_O_ * Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    
    Matrix33 inertia_matrix = Q_T_O_.rotation().transpose().matrix() * R_i.matrix() * inertia_diag * R_i.matrix().transpose() *  Q_T_O_.rotation().matrix()  + inertia(0) * skew_rc * skew_rc;

    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia[0]*g_) + torque;
    retVal.tail(3)=inertia[0]*(acc.tail(3)-g_)+force; // X2 should be negative..TODO


    // compute gradients: 3xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      H1->block(0,0,3,3)=inertia_matrix;
      H1->block(3,3,3,3)=gtsam::Matrix33().Identity() * inertia(0);

    }

    // force:
    if(H2)
    {
      *H2 = gtsam::Matrix63().setZero();
      H2->block(3,0,3,3) = gtsam::Matrix33().Identity() * 1.0;
    }


    if (H3) // 6x3 torque
    {
      *H3 = gtsam::Matrix63().setZero();
      H3->block(0,0,3,3) = gtsam::Matrix33().Identity() * 1.0;

    } 

    
    // inertia, com, mass 6x10
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      H4->block(0,0,3,1) = skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);
      H4->block(3,0,3,1) = (acc.tail(3)-g_);

      // gradient for com:
      Matrix skew_H = gtsam::Matrix39().setZero();
      // add partial derivatives for skew vector:
      // for x:
      skew_H(2,1) = 1.0;
      skew_H(1,2) = -1.0;
      // for y:
      skew_H(0,2+3) = 1.0;
      skew_H(2,0+3) = -1.0;
      // for z:
      skew_H(0,1+6) = -1.0;
      skew_H(1,0+6) = 1.0;
      //cerr<<skew_H<<endl;
      Matrix constant_mg =  gtsam::Matrix93().setZero();
      constant_mg.block(0,0,3,1) = inertia[0] * g_;
      constant_mg.block(3,1,3,1) = inertia[0] * g_;
      constant_mg.block(6,2,3,1) = inertia[0] * g_;
      //cerr<<constant_mg<<endl;
      H4->block(0,1,3,3) = -1.0 * skew_H * constant_mg ;

      // gradient for parallel axis theorem:
      Matrix33 parallel_grad;
      parallel_grad.setZero();
      
      Matrix33 skew_I = (gtsam::Matrix33() <<
                         0, -0, 0,
                         0, 0 , -1,
                        -0, 1, 0).finished();

      parallel_grad.block(0,0,3,1) =  inertia(0) * skew_rc  * skew_I * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -0, 1,
                         0, 0 , -0,
                        -1, 0, 0).finished();

      parallel_grad.block(0,1,3,1) =  inertia(0) * skew_rc * skew_I  * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -1, 0,
                         1, 0 , -0,
                        -0, 0, 0).finished();

      parallel_grad.block(0,2,3,1) =  inertia(0) * skew_rc * skew_I * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      
      //cerr<<parallel_grad<<endl
      H4->block(0,1,3,3) += parallel_grad;//.transpose();
      
      // gradient for inertia: ( )
      gtsam::Matrix63 H_inertia;
      //gtsam::Matrix33 Q = R_i.matrix() * R_i.matrix().transpose();
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);

      H4->block(0,4,6,3) = H_inertia;


    }
    if(H5)
    {
      //Rot3 r_mat(gtsam::Rot3::Ypr(0.1,-0.1,1.1)); 
     
      *H5 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))  ;
      H5->block(0,0,3,3) = first + last;
    }
    return retVal;
  }
};
class NewtonEulerAccFtFactor : public NoiseModelFactor6<Vector6, Vector6, Vector3, Vector3, Vector7, Rot3>
{
private:
  Vector3 g_;
  gtsam::Pose3 Q_T_O_;  // transformation matrix
  
public:

  NewtonEulerAccFtFactor(Key j1, Key j2, Key j3, Key j4, Key j5, Key j6, const SharedNoiseModel& noiseModel, Vector3 g_vec, Pose3 Q_T_O = Pose3()):
    NoiseModelFactor6<Vector6,Vector6, Vector3, Vector3, Vector7, Rot3>(noiseModel, j1, j2, j3, j4,j5,j6),g_(g_vec), Q_T_O_(Q_T_O){}

  Vector gradientTester(const Vector6 &vel, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       const Rot3 &R_i) const
  {
    Vector6 acc;
    acc.setConstant(1.0);
            // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    // transform point:
    Point3 q_rc = Q_T_O_ * Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    
    Matrix33 inertia_matrix = Q_T_O_.rotation().transpose().matrix() * R_i.matrix() * inertia_diag * R_i.matrix().transpose() *  Q_T_O_.rotation().matrix();//  + inertia(0) * skew_rc * skew_rc;

    Vector6 retVal;
    
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc)
                      * vel.head(3) 
                      - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)-g_)
                      - inertia[0] * skew_rc * acc.head(3) 
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error


    return retVal;
  }

  Vector evaluateError(const Vector6 &acc, const Vector6 &vel, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none,
                       boost::optional<Matrix&> H6 = boost::none) const
  {
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    // transform point:
    Point3 q_rc = Q_T_O_ * Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    
    Matrix33 inertia_matrix = Q_T_O_.rotation().transpose().matrix() * R_i.matrix() * inertia_diag * R_i.matrix().transpose() *  Q_T_O_.rotation().matrix();//  + inertia(0) * skew_rc * skew_rc;

    Vector3 g_v = -1.0 * g_;

    Vector6 retVal;
    
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
                      + skew_rc * ( inertia[0]*g_v))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)+g_v)
                      - inertia[0] * skew_rc * acc.head(3)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error


    // compute gradients: 3xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      // torque error gradient
      H1->block(0,0,3,3) += inertia_matrix - inertia(0) * skew_rc * skew_rc;
      H1->block(0,3,3,3) += inertia(0) * skew_rc;
      
      //H1->block(0,3,3,3)= - inertia(0) * skew_rc + 2.0 * inertia(0) * skew_vel *skew_rc ;

      // force error gradient:
      H1->block(3,0,3,3) += - inertia(0) * skew_rc ;

      /*
      Matrix skew_acc = gtsam::Matrix33().setZero();
      skew_acc(0,2) = -1.0;
      skew_acc(2,0) = 1.0;
      
      H1->block(3,0,3,3) +=  inertia(0) * skew_acc * skew_vel * q_rc + inertia(0) * skew_vel * skew_acc ;
      */
      H1->block(3,3,3,3) += gtsam::Matrix33().Identity() * inertia(0);

    }


    if(H2) // 6x6 velocity
    {
      *H2 = gtsam::Matrix66().setZero();

      H2->block(0,0,3,3) += skew_vel * inertia_matrix  * Matrix33().Identity();
      H2->block(0,0,3,3) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_rc  * Matrix33().Identity();

      Matrix33 skew_grad = gtsam::Matrix33().setZero();
      // x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      H2->block(3,0,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H2->block(3,0,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      H2->block(0,0,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      H2->block(0,0,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H2->block(3,1,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H2->block(3,1,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      H2->block(0,1,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      H2->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H2->block(3,2,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H2->block(3,2,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;
      H2->block(0,2,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      H2->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);


    }

    // force:
    if(H3)
    {
      *H3 = gtsam::Matrix63().setZero();
      H3->block(3,0,3,3) = gtsam::Matrix33().Identity() * -1.0;
    }


    if (H4) // 6x3 torque
    {
      *H4 = gtsam::Matrix63().setZero();
      H4->block(0,0,3,3) = gtsam::Matrix33().Identity() * -1.0;

    } 

    
    // inertia, com, mass 6x10
    if(H5)
    {
      *H5 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H5->block(0,0,3,1) = skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(0,0,3,1) = (skew_rc * acc.tail(3)
                            - skew_rc * skew_rc * acc.head(3)
                            - skew_vel * (skew_rc * skew_rc) * vel.head(3)
                            + skew_rc * g_v);

      //skew_rc * g_v * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(3,0,3,1) = (acc.tail(3)+g_v) - skew_rc * acc.head(3) + skew_vel * skew_vel * q_rc;

      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(3,1,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(3,2,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(3,3,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;


      
      
      // gradient for body frame:
      
      H5->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();

      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(0,1,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,1,3,1) += 1.0 * skew_grad * inertia[0] * g_v;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(0,2,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,2,3,1) += 1.0 * skew_grad * inertia[0] * g_v;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(0,3,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,3,3,1) += 1.0 * skew_grad * inertia[0] * g_v;

      // gradient for inertia: ( )
      gtsam::Matrix63 H_inertia;
      //gtsam::Matrix33 Q = R_i.matrix() * R_i.matrix().transpose();
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);
      

      H_inertia.block(0,0,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *vel.head(3);

      H_inertia.block(0,1,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * vel.head(3);


      H_inertia.block(0,2,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * vel.head(3);

      H5->block(0,4,6,3) = H_inertia;


    }
    if(H6)
    {
      //Rot3 r_mat(gtsam::Rot3::Ypr(0.1,-0.1,1.1)); 
     
      *H6 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3))
         -1.0 * skew_vel * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * vel.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))
        + 1.0 * skew_vel * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * vel.head(3));
      
      H6->block(0,0,3,3) = first + last;
    }
    return retVal;
  }
};

class QSFtFactor : public NoiseModelFactor4<Vector6, Vector3, Vector3, Vector7>
{
private:
  Vector3 g_;
  double w_;
  gtsam::Pose3 Q_T_O_;  // transformation matrix
  
public:

  QSFtFactor(Key j1, Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Vector3 g_vec, double w=1.0, Pose3 Q_T_O = Pose3()):
    NoiseModelFactor4<Vector6, Vector3, Vector3, Vector7>(noiseModel, j1, j2, j3, j4),g_(g_vec),w_(w), Q_T_O_(Q_T_O){}

  Vector gradientTester(const Vector6 &vel, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia)
  {
    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    

    Vector6 retVal;

        
    retVal.head(3) = ( - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = inertia[0]*(g_) + (inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error

    retVal = w_ * retVal;
    return retVal;
  }
  Vector evaluateError(const Vector6 &vel, const Vector3 &force, const Vector3 &torque, const Vector7 &inertia,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {
    // transform point:
    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -q_rc.z(), q_rc.y(),
                        q_rc.z(), 0 , -q_rc.x(),
                        -q_rc.y(), q_rc.x(), 0).finished();

    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    

    Vector6 retVal;
    
    retVal.head(3) = ( - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = inertia[0]*(g_) + (inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error

    retVal = w_ * retVal;

    if(H1) // 6x6 velocity
    {
      *H1 = gtsam::Matrix66().setZero();

      //H1->block(0,0,3,3) += skew_vel * inertia_matrix  * Matrix33().Identity();
      //H1->block(0,0,3,3) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_rc  * Matrix33().Identity();

      Matrix33 skew_grad = gtsam::Matrix33().setZero();
      // x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      H1->block(3,0,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H1->block(3,0,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      //H1->block(0,0,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      //H1->block(0,0,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H1->block(3,1,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H1->block(3,1,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      //H1->block(0,1,3,1) += skew_grad * inertia_matrix  * vel.head(3);
      
      //H1->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H1->block(3,2,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H1->block(3,2,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;
      
      *H1 = w_ * *H1;
      //H1->block(0,2,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      //H1->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);


    }

    // force:
    if(H2)
    {
      *H2 = gtsam::Matrix63().setZero();
      H2->block(3,0,3,3) = gtsam::Matrix33().Identity() * -1.0;
      *H2 = w_ * *H2;

    }


    if (H3) // 6x3 torque
    {
      *H3 = gtsam::Matrix63().setZero();
      H3->block(0,0,3,3) = gtsam::Matrix33().Identity() * -1.0;
      *H3 = w_ * *H3;

    } 

    
    // inertia, com, mass 6x10
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H4->block(0,0,3,1) = skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H4->block(0,0,3,1) =  - skew_rc * g_;

      //skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H4->block(3,0,3,1) +=  skew_vel * skew_vel * q_rc;

      H4->block(3,0,3,1) += 1.0 * g_;
      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for body frame:
      
      H4->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();

      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H4->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;


      H4->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * g_;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H4->block(0,3,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      *H4 = w_ * *H4;

    }
    return retVal;
  }
};

/*
// input: Q_w, Q_rot, O_w, O_rot
class TransformPhysicsFactor : public NoiseModelFactor4<Vector7, Rot3, Vector7, Rot3>
{
private:
  Pose3 o_T_Q_;
public:
  TransformPhysicsFactor(Key j1, Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Pose3 o_T_Q):
    NoiseModelFactor4< Vector7, Rot3, Vector7, Rot3 > (noiseModel, j1, j2, j3, j4), o_T_Q_(o_T_Q){}

  Vector evaluateError(const Vector7 &Q_W, const Rot3 &Q_H, const Vector7 &o_W, const Rot3 &o_H,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {

    Vector retVal;

    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               Q_W[4+1] + Q_W[4+2],0,0,
                               0,Q_W[4+0] + Q_W[4+2],0,
                               0,0,Q_W[4+0]+ Q_W[4+1]).finished();

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();

    return retVal;
  }
};
*/

// Add biotac force factor:
// object acceleration, 3*4bt_forces(1-4), bt_contact points(1-4), W(10 parameters)
class AccBtFactor : public NoiseModelFactor4<Vector6, Vector12, Vector12, Vector10>
{
 private:
  Vector3 g_;
 public:
  AccBtFactor(Key j1, Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Vector3 g_vec):
    NoiseModelFactor4<Vector6, Vector12, Vector12, Vector10> (noiseModel, j1, j2, j3, j4),g_(g_vec){}
  Vector evaluateError(const Vector6 &acc, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector10 &inertia,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {
    
    // build inertia matrix:
    Matrix33 inertia_matrix = (gtsam::Matrix33() <<
                               inertia(0),inertia(3),inertia(4),
                               inertia(3),inertia(1),inertia(5),
                               inertia(4),inertia(5),inertia(2)).finished();
    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia(6+2), inertia(6+1),
                        inertia(6+2), 0 , -inertia(6+0),
                        -inertia(6+1), inertia(6+0), 0).finished();

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia(6+3) * g_) + torque;
    
    retVal.tail(3)=inertia[6+3]*(acc.tail(3)-g_)+force; // X2 should be negative..TODO
    // compute gradients: 6xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      H1->block(0,0,3,3)=inertia_matrix;
      H1->block(3,3,3,3)=gtsam::Matrix33().Identity() * inertia(6+3);

    }

    // force: 6 x 12
    if(H2)
    {
      *H2 = Eigen::Matrix<double,6,12>().setZero();

      for(int i=0;i<4;++i)
      {
        // linear acceleration:

        H2->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * 1.0;
        // angular acceleration:
        
        H2->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * 1.0;
      }
      
    }


    // contact points: 6 x 12
    if(H3)
    {
      *H3 = Eigen::Matrix<double,6,12>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<4;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H3->block(0,i*3,3,3) = H_rc;
      }
    }


    // inertia, com, mass 6x10
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,10>().setZero();
      // gradient for inertia:
      gtsam::Matrix66 H_inertia;
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = (gtsam::Matrix33()<<
                                  1,0,0,
                                  0,0,0,
                                  0,0,0).finished() * acc.head(3);

      H_inertia.block(0,1,3,1) = (gtsam::Matrix33()<<
                                  0,0,0,
                                  0,1,0,
                                  0,0,0).finished() * acc.head(3);


      H_inertia.block(0,2,3,1) = (gtsam::Matrix33()<<
                                  0,0,0,
                                  0,0,0,
                                  0,0,1).finished() * acc.head(3);
      H_inertia.block(0,3,3,1) = (gtsam::Matrix33()<<
                                  0,1,0,
                                  1,0,0,
                                  0,0,0).finished() * acc.head(3);
      
      H_inertia.block(0,4,3,1) = (gtsam::Matrix33()<<
                                  0,0,1,
                                  0,0,0,
                                  1,0,0).finished() * acc.head(3);
      H_inertia.block(0,5,3,1) = (gtsam::Matrix33()<<
                                  0,0,0,
                                  0,0,1,
                                  0,1,0).finished() * acc.head(3);

      H4->block(0,0,6,6) = H_inertia;

      // gradient for com:
      Matrix skew_H = gtsam::Matrix39().setZero();
      // add partial derivatives for skew vector:
      // for x:
      skew_H(2,1) = 1.0;
      skew_H(1,2) = -1.0;
      // for y:
      skew_H(0,2+3) = 1.0;
      skew_H(2,0+3) = -1.0;
      // for z:
      skew_H(0,1+6) = -1.0;
      skew_H(1,0+6) = 1.0;
      //cerr<<skew_H<<endl;
      Matrix constant_mg =  gtsam::Matrix93().setZero();
      constant_mg.block(0,0,3,1) = inertia[6+3] * g_;
      constant_mg.block(3,1,3,1) = inertia[6+3] * g_;
      constant_mg.block(6,2,3,1) = inertia[6+3] * g_;
      //cerr<<constant_mg<<endl;
      H4->block(0,6,3,3) = -1.0 * skew_H * constant_mg ;
      
      // gradient for mass:
      H4->block(0,9,3,1) = skew_rc * g_ * -1.0;
      H4->block(3,9,3,1) = (acc.tail(3)-g_);

    }

    return retVal;
  }
  
};

// Add biotac force factor:
// object acceleration, 3*4bt_forces(1-4), bt_contact points(1-4), W(7 parameters), Inertia Rotation
class ManifoldAccBtFactor : public NoiseModelFactor5<Vector6, Vector12, Vector12, Vector7, Rot3>
{
 private:
  Vector3 g_;
 public:
  
  ManifoldAccBtFactor(Key j1, Key j2, Key j3, Key j4, Key j5, const SharedNoiseModel& noiseModel, Vector3 g_vec):
    NoiseModelFactor5<Vector6, Vector12, Vector12, Vector7, Rot3> (noiseModel, j1, j2, j3, j4, j5),g_(g_vec){}

  Vector gradientTester( const Vector6 &acc, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                         const Rot3 &R_i )
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose()  + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia(0) * g_) + torque;
    
    retVal.tail(3)=inertia(0)*(acc.tail(3)-g_)+force; // X2 should be negative..TODO
    return retVal;
  }
  Vector evaluateError(const Vector6 &acc, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none) const
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose() + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia(0) * g_) + torque;
    
    retVal.tail(3)=inertia(0)*(acc.tail(3)-g_)+force; // X2 should be negative..TODO
    // compute gradients: 6xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      H1->block(0,0,3,3)=inertia_matrix;
      H1->block(3,3,3,3)=gtsam::Matrix33().Identity() * inertia(0);

    }

    // force: 6 x 12
    if(H2)
    {
      
      *H2 = Eigen::Matrix<double,6,12>().setZero();

      for(int i=0;i<4;++i)
      {
        // linear acceleration:

        H2->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * 1.0;
        // angular acceleration:
        
        H2->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * 1.0;
      }
      
    }


    // contact points: 6 x 12
    if(H3)
    {
      *H3 = Eigen::Matrix<double,6,12>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<4;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H3->block(0,i*3,3,3) = H_rc;
      }
    }


    // inertia, com, mass 6x10
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,7>().setZero();
      
      // gradient for mass:
      H4->block(0,0,3,1) = skew_rc * g_ * -1.0  + (skew_rc * skew_rc) * acc.head(3);
      H4->block(3,0,3,1) = (acc.tail(3)-g_);

      // gradient for com:
      Matrix skew_H = gtsam::Matrix39().setZero();
      // add partial derivatives for skew vector:
      // for x:
      skew_H(2,1) = 1.0;
      skew_H(1,2) = -1.0;
      // for y:
      skew_H(0,2+3) = 1.0;
      skew_H(2,0+3) = -1.0;
      // for z:
      skew_H(0,1+6) = -1.0;
      skew_H(1,0+6) = 1.0;
      //cerr<<skew_H<<endl;
      Matrix constant_mg =  gtsam::Matrix93().setZero();
      constant_mg.block(0,0,3,1) = inertia(0) * g_;
      constant_mg.block(3,1,3,1) = inertia(0) * g_;
      constant_mg.block(6,2,3,1) = inertia(0) * g_;
      //cerr<<constant_mg<<endl;
      H4->block(0,1,3,3) = -1.0 * skew_H * constant_mg ;
      
      // gradient for parallel axis theorem:
      Matrix33 parallel_grad;
      parallel_grad.setZero();
      
      Matrix33 skew_I = (gtsam::Matrix33() <<
                         0, -0, 0,
                         0, 0 , -1,
                        -0, 1, 0).finished();

      parallel_grad.block(0,0,3,1) =  inertia(0) * skew_rc  * skew_I * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -0, 1,
                         0, 0 , -0,
                        -1, 0, 0).finished();

      parallel_grad.block(0,1,3,1) =  inertia(0) * skew_rc * skew_I  * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -1, 0,
                         1, 0 , -0,
                        -0, 0, 0).finished();

      parallel_grad.block(0,2,3,1) =  inertia(0) * skew_rc * skew_I * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);


      H4->block(0,1,3,3) += parallel_grad;


      // gradient for diagonal inertia:
      gtsam::Matrix63 H_inertia;
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix(). transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);

      H4->block(0,4,6,3) = H_inertia;


    }
    if(H5)
    {
      *H5 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))  ;
      H5->block(0,0,3,3) = first + last;

    }

    return retVal;
  }
  
};

// Add biotac force factor:
// object acceleration, 3*4bt_forces(1-4), bt_contact points(1-4), W(7 parameters), Inertia Rotation
class ManifoldAccBtLocalFactor : public NoiseModelFactor5<Vector6, Vector9, Vector9, Vector7, Rot3>
{
 private:
  Vector3 g_;
  double w_=1e-1;
 public:
  
  ManifoldAccBtLocalFactor(Key j1, Key j2, Key j3, Key j4, Key j5, const SharedNoiseModel& noiseModel, Vector3 g_vec):
    NoiseModelFactor5<Vector6, Vector9, Vector9, Vector7, Rot3> (noiseModel, j1, j2, j3, j4, j5),g_(g_vec){}

  Vector gradientTester( const Vector6 &acc, const Vector9 &bt_forces, const Vector9 &bt_pts,const Vector7 &inertia,
                         const Rot3 &R_i )
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose()  + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3);// + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<3;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = w_ * (inertia_matrix * acc.head(3) - skew_rc * ( inertia(0) * g_) + torque);
    
    retVal.tail(3)= w_ *( inertia(0)*(acc.tail(3)-g_)+force); // X2 should be negative..TODO
    return retVal;
  }
  Vector evaluateError(const Vector6 &acc, const Vector9 &bt_forces, const Vector9 &bt_pts,const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none) const
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose() + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3);// + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<3;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = w_ *( inertia_matrix * acc.head(3) - skew_rc * ( inertia(0) * g_) + torque);
    
    retVal.tail(3)= w_ * (inertia(0)*(acc.tail(3)-g_)+force); // X2 should be negative..TODO
    // compute gradients: 6xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      H1->block(0,0,3,3)=  inertia_matrix;
      H1->block(3,3,3,3)= gtsam::Matrix33().Identity() * inertia(0);
      *H1 = *H1 * w_; 
    }

    // force: 6 x 12
    if(H2)
    {
      
      *H2 = Eigen::Matrix<double,6,9>().setZero();

      for(int i=0;i<3;++i)
      {
        // linear acceleration:

        H2->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * 1.0;
        // angular acceleration:
        
        H2->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * 1.0;
      }

      *H2 = *H2 * w_; 

    }


    // contact points: 6 x 12
    if(H3)
    {
      *H3 = Eigen::Matrix<double,6,9>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<3;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H3->block(0,i*3,3,3) = H_rc;
      }
      *H3 = *H3 * w_; 

    }


    // inertia, com, mass 6x10
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,7>().setZero();
      
      // gradient for mass:
      H4->block(0,0,3,1) = skew_rc * g_ * -1.0  + (skew_rc * skew_rc) * acc.head(3);
      H4->block(3,0,3,1) = (acc.tail(3)-g_);

      // gradient for com:
      Matrix skew_H = gtsam::Matrix39().setZero();
      // add partial derivatives for skew vector:
      // for x:
      skew_H(2,1) = 1.0;
      skew_H(1,2) = -1.0;
      // for y:
      skew_H(0,2+3) = 1.0;
      skew_H(2,0+3) = -1.0;
      // for z:
      skew_H(0,1+6) = -1.0;
      skew_H(1,0+6) = 1.0;
      //cerr<<skew_H<<endl;
      Matrix constant_mg =  gtsam::Matrix93().setZero();
      constant_mg.block(0,0,3,1) = inertia(0) * g_;
      constant_mg.block(3,1,3,1) = inertia(0) * g_;
      constant_mg.block(6,2,3,1) = inertia(0) * g_;
      //cerr<<constant_mg<<endl;
      H4->block(0,1,3,3) = -1.0 * skew_H * constant_mg ;
      
      // gradient for parallel axis theorem:
      Matrix33 parallel_grad;
      parallel_grad.setZero();
      
      Matrix33 skew_I = (gtsam::Matrix33() <<
                         0, -0, 0,
                         0, 0 , -1,
                        -0, 1, 0).finished();

      parallel_grad.block(0,0,3,1) =  inertia(0) * skew_rc  * skew_I * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -0, 1,
                         0, 0 , -0,
                        -1, 0, 0).finished();

      parallel_grad.block(0,1,3,1) =  inertia(0) * skew_rc * skew_I  * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -1, 0,
                         1, 0 , -0,
                        -0, 0, 0).finished();

      parallel_grad.block(0,2,3,1) =  inertia(0) * skew_rc * skew_I * acc.head(3) + inertia(0)  * skew_I * skew_rc * acc.head(3);


      H4->block(0,1,3,3) += parallel_grad;


      // gradient for diagonal inertia:
      gtsam::Matrix63 H_inertia;
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix(). transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);

      H4->block(0,4,6,3) = H_inertia;

      *H4 = *H4 * w_; 

    }
    if(H5)
    {
      *H5 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))  ;
      H5->block(0,0,3,3) = first + last;

      *H5 = *H5 * w_; 

    }

    return retVal;
  }
  
};

class ManifoldFixedAccBtFactor : public NoiseModelFactor4<Vector9, Vector9, Vector7, Rot3>
{
 private:
  Vector3 g_;
  Vector6 acc_;
 public:
  
  ManifoldFixedAccBtFactor(Key j1,Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Vector3 g_vec,Vector6 acc):
    NoiseModelFactor4<Vector9, Vector9, Vector7, Rot3> (noiseModel, j1, j2, j3, j4),g_(g_vec),acc_(acc){}

  
  Vector gradientTester( const Vector9 &bt_forces, const Vector9 &bt_pts,const Vector7 &inertia,
                         const Rot3 &R_i )
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose()  + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3);// + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<3;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc_.head(3) - skew_rc * ( inertia(0) * g_) + torque;
    
    retVal.tail(3)=inertia(0)*(acc_.tail(3)-g_)+force; // X2 should be negative..TODO
    return retVal;
  }
  Vector evaluateError(const Vector9 &bt_forces, const Vector9 &bt_pts,const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose() + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3);// + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<3;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc_.head(3) - skew_rc * ( inertia(0) * g_) + torque;
    
    retVal.tail(3)=inertia(0)*(acc_.tail(3)-g_)+force; // X2 should be negative..TODO
    // compute gradients: 6xn
    /*
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      H1->block(0,0,3,3)=inertia_matrix;
      H1->block(3,3,3,3)=gtsam::Matrix33().Identity() * inertia(0);

    }
    */
    // force: 6 x 9
    if(H1)
    {
      
      *H1 = Eigen::Matrix<double,6,9>().setZero();

      for(int i=0;i<3;++i)
      {
        // linear acceleration:

        H1->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * 1.0;
        // angular acceleration:
        
        H1->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * 1.0;
      }
      
    }


    // contact points: 6 x 9
    if(H2)
    {
      *H2 = Eigen::Matrix<double,6,9>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<3;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H2->block(0,i*3,3,3) = H_rc;
      }
    }


    // inertia, com, mass 6x10
    if(H3)
    {
      *H3 = Eigen::Matrix<double,6,7>().setZero();
      
      // gradient for mass:
      H3->block(0,0,3,1) = skew_rc * g_ * -1.0  + (skew_rc * skew_rc) * acc_.head(3);
      H3->block(3,0,3,1) = (acc_.tail(3)-g_);

      // gradient for com:
      Matrix skew_H = gtsam::Matrix39().setZero();
      // add partial derivatives for skew vector:
      // for x:
      skew_H(2,1) = 1.0;
      skew_H(1,2) = -1.0;
      // for y:
      skew_H(0,2+3) = 1.0;
      skew_H(2,0+3) = -1.0;
      // for z:
      skew_H(0,1+6) = -1.0;
      skew_H(1,0+6) = 1.0;
      //cerr<<skew_H<<endl;
      Matrix constant_mg =  gtsam::Matrix93().setZero();
      constant_mg.block(0,0,3,1) = inertia(0) * g_;
      constant_mg.block(3,1,3,1) = inertia(0) * g_;
      constant_mg.block(6,2,3,1) = inertia(0) * g_;
      //cerr<<constant_mg<<endl;
      H3->block(0,1,3,3) = -1.0 * skew_H * constant_mg ;
      
      // gradient for parallel axis theorem:
      Matrix33 parallel_grad;
      parallel_grad.setZero();
      
      Matrix33 skew_I = (gtsam::Matrix33() <<
                         0, -0, 0,
                         0, 0 , -1,
                        -0, 1, 0).finished();

      parallel_grad.block(0,0,3,1) =  inertia(0) * skew_rc  * skew_I * acc_.head(3) + inertia(0)  * skew_I * skew_rc * acc_.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -0, 1,
                         0, 0 , -0,
                        -1, 0, 0).finished();

      parallel_grad.block(0,1,3,1) =  inertia(0) * skew_rc * skew_I  * acc_.head(3) + inertia(0)  * skew_I * skew_rc * acc_.head(3);

      skew_I = (gtsam::Matrix33() <<
                         0, -1, 0,
                         1, 0 , -0,
                        -0, 0, 0).finished();

      parallel_grad.block(0,2,3,1) =  inertia(0) * skew_rc * skew_I * acc_.head(3) + inertia(0)  * skew_I * skew_rc * acc_.head(3);


      H3->block(0,1,3,3) += parallel_grad;


      // gradient for diagonal inertia:
      gtsam::Matrix63 H_inertia;
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix(). transpose() *acc_.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc_.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc_.head(3);

      H3->block(0,4,6,3) = H_inertia;


    }
    if(H4)
    {
      *H4 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc_.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc_.head(3))  ;
      H4->block(0,0,3,3) = first + last;

    }

    return retVal;
  }
  
};

class NewtonEulerBtFactor : public NoiseModelFactor6<Vector6, Vector6, Vector12, Vector12, Vector7, Rot3>
{
 private:
  Vector3 g_;
 public:
  
  NewtonEulerBtFactor(Key j1, Key j2, Key j3, Key j4, Key j5, Key j6, const SharedNoiseModel& noiseModel, Vector3 g_vec):
    NoiseModelFactor6<Vector6, Vector6, Vector12, Vector12, Vector7, Rot3> (noiseModel, j1, j2, j3, j4, j5,j6),g_(g_vec){}

  Vector gradientTester( const Vector6 &acc, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                         const Rot3 &R_i )
  {
    
    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();
    

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose()  + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    
    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = inertia_matrix * acc.head(3) - skew_rc * ( inertia(0) * g_) - torque;
    
    retVal.tail(3)=inertia(0)*(acc.tail(3)+g_)-force; // X2 should be negative..TODO
    return retVal;
  }
  Vector evaluateError(const Vector6 &acc, const Vector6 &vel, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                       const Rot3 &R_i,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none,
                       boost::optional<Matrix&> H5 = boost::none,
                       boost::optional<Matrix&> H6 = boost::none) const
  {

    // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();

    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) * skew_rc * skew_rc;

    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
                      + (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
                      + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
                      - skew_rc * ( inertia[0]*g_))
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(acc.tail(3)+g_)
                      - inertia[0] * skew_rc * acc.head(3)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    // compute gradients: 6xn
    if(H1) // 6x6 acceleration
    {
      *H1 = gtsam::Matrix66().setZero();
      // torque error gradient
      H1->block(0,0,3,3) += inertia_matrix - inertia(0) * skew_rc * skew_rc;
      H1->block(0,3,3,3) += inertia(0) * skew_rc;
      
      // force error gradient:
      H1->block(3,0,3,3) += - inertia(0) * skew_rc ;

      H1->block(3,3,3,3) += gtsam::Matrix33().Identity() * inertia(0);


    }

    if(H2) // 6x6 velocity
    {
      *H2 = gtsam::Matrix66().setZero();

      H2->block(0,0,3,3) += skew_vel * inertia_matrix  * Matrix33().Identity();
      H2->block(0,0,3,3) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_rc  * Matrix33().Identity();

      Matrix33 skew_grad;
      skew_grad.setZero();
      // x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      H2->block(3,0,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H2->block(3,0,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      H2->block(0,0,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      H2->block(0,0,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H2->block(3,1,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H2->block(3,1,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      H2->block(0,1,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      H2->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H2->block(3,2,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H2->block(3,2,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;
      H2->block(0,2,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      H2->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);


    }

    // force: 6 x 12
    if(H3)
    {
      
      *H3 = Eigen::Matrix<double,6,12>().setZero();

      for(int i=0;i<4;++i)
      {
        // linear acceleration:

        H3->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * -1.0;
        // angular acceleration:
        
        H3->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * -1.0;
      }
      
    }


    // contact points: 6 x 12
    if(H4)
    {
      *H4 = Eigen::Matrix<double,6,12>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<4;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H4->block(0,i*3,3,3) = -1.0 * H_rc;
      }
    }


    // inertia, com, mass 6x10
    if(H5)
    {

      *H5 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H5->block(0,0,3,1) = skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(0,0,3,1) = (skew_rc * acc.tail(3)
                            - skew_rc * skew_rc * acc.head(3)
                            - skew_vel * (skew_rc * skew_rc) * vel.head(3)
                            - skew_rc * g_);

      //skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H5->block(3,0,3,1) = (acc.tail(3)+g_) - skew_rc * acc.head(3) + skew_vel * skew_vel * q_rc;

      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(3,1,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(3,2,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;

      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(3,3,3,1) += -1.0 * inertia[0] * skew_grad * acc.head(3) ;


      
      
      // gradient for body frame:
      
      H5->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();

      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H5->block(0,1,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,1,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H5->block(0,2,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,2,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * g_;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;
      H5->block(0,3,3,1) += 1.0 * inertia[0] * skew_grad * acc.tail(3) ;

      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_grad * skew_rc * acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * inertia[0] * skew_rc * skew_grad *  acc.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_rc * skew_grad *  vel.head(3) ;
      H5->block(0,3,3,1) += -1.0 * skew_vel * inertia[0] * skew_grad * skew_rc *  vel.head(3) ;

      H5->block(0,3,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      // gradient for inertia: ( )
      gtsam::Matrix63 H_inertia;
      //gtsam::Matrix33 Q = R_i.matrix() * R_i.matrix().transpose();
      H_inertia.setZero();
      H_inertia.block(0,0,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *acc.head(3);

      H_inertia.block(0,1,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * acc.head(3);


      H_inertia.block(0,2,3,1) = R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * acc.head(3);
      

      H_inertia.block(0,0,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 0,0,0,
                                                 0,1,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() *vel.head(3);

      H_inertia.block(0,1,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,0,0,
                                                 0,0,1).finished() * R_i.matrix().transpose() * vel.head(3);


      H_inertia.block(0,2,3,1) += skew_vel * R_i.matrix() * (gtsam::Matrix33()<<
                                                 1,0,0,
                                                 0,1,0,
                                                 0,0,0).finished() * R_i.matrix().transpose() * vel.head(3);

      H5->block(0,4,6,3) = H_inertia;



    }
    if(H6)
    {
      //Rot3 r_mat(gtsam::Rot3::Ypr(0.1,-0.1,1.1)); 
     
      *H6 = gtsam::Matrix63().setZero();

      Matrix3 first = -1.0 * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * acc.head(3))
         -1.0 * skew_vel * R_i.matrix() *  supp_fns::skew(inertia_diag * R_i.matrix().transpose() * vel.head(3));

      Matrix3 last = 1.0 * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * acc.head(3))
        + 1.0 * skew_vel * R_i.matrix() * inertia_diag * supp_fns::skew(  R_i.matrix().transpose() * vel.head(3));
      
      H6->block(0,0,3,3) = first + last;
    }

    return retVal;
  }
  
};


class QSBtFactor : public NoiseModelFactor4<Vector6, Vector12, Vector12, Vector7>
{
 private:
  Vector3 g_;
  double w_;
 public:
  
  QSBtFactor(Key j1, Key j2, Key j3, Key j4, const SharedNoiseModel& noiseModel, Vector3 g_vec, double w=1.0):
    NoiseModelFactor4<Vector6, Vector12, Vector12, Vector7> (noiseModel, j1, j2, j3, j4),g_(g_vec),w_(w){}

  Vector gradientTester( const Vector6 &vel, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia)
  {
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();


    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) =  - skew_rc * ( inertia[0]*g_)
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(g_)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    retVal = w_ * retVal;
    return retVal;
  }
  Vector evaluateError(const Vector6 &vel, const Vector12 &bt_forces, const Vector12 &bt_pts,const Vector7 &inertia,
                       boost::optional<Matrix&> H1 = boost::none,
                       boost::optional<Matrix&> H2 = boost::none,
                       boost::optional<Matrix&> H3 = boost::none,
                       boost::optional<Matrix&> H4 = boost::none) const
  {

    
    Matrix33 skew_rc = (gtsam::Matrix33() <<
                        0, -inertia[1+2], inertia[1+1],
                        inertia[1+2], 0 , -inertia[1+0],
                        -inertia[1+1], inertia[1+0], 0).finished();

    Point3 q_rc = Point3(inertia(1), inertia(2), inertia(3)); 
    
    Matrix33 skew_vel = (gtsam::Matrix33() <<
                         0, -vel(2), vel(1),
                         vel(2), 0 , -vel(0),
                        -vel(1), vel(0), 0).finished();


    // compute net force:
    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=supp_fns::skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }

    // compute net torque:
    
    Vector6 retVal;
    retVal.head(3) =  - skew_rc * ( inertia[0]*g_)
                      - torque; // Torque error

    
    retVal.tail(3) = (inertia[0]*(-1.0*g_)
                      + inertia[0] * skew_vel * skew_vel * q_rc )
                      - force; // X2 should be negative?  Force error
    retVal = w_ * retVal;
    // compute gradients: 6xn


    if(H1) // 6x6 velocity
    {
      *H1 = gtsam::Matrix66().setZero();


      Matrix33 skew_grad = gtsam::Matrix33().setZero();
      // x
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      H1->block(3,0,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H1->block(3,0,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      //H1->block(0,0,3,1) += skew_grad * inertia_matrix  * vel.head(3);

      //H1->block(0,0,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;

      H1->block(3,1,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H1->block(3,1,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;

      //H1->block(0,1,3,1) += skew_grad * inertia_matrix  * vel.head(3);
      
      //H1->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * skew_rc * skew_rc  * vel.head(3);

      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H1->block(3,2,3,1)+=inertia[0] * skew_grad * skew_vel * q_rc;
      H1->block(3,2,3,1)+=inertia[0] * skew_vel * skew_grad * q_rc;
      
      *H1 = w_ * *H1;

    }

    // force: 6 x 12
    if(H2)
    {
      
      *H2 = Eigen::Matrix<double,6,12>().setZero();

      for(int i=0;i<4;++i)
      {
        // linear acceleration:

        H2->block(3,i*3,3,3) = gtsam::Matrix33().Identity() * -1.0;
        // angular acceleration:
        
        H2->block(0,i*3,3,3) = supp_fns::skew(bt_pts.segment(i*3,3)) * gtsam::Matrix33().Identity() * -1.0;
      }
      *H2 = w_ * *H2;

    }


    // contact points: 6 x 12
    if(H3)
    {
      *H3 = Eigen::Matrix<double,6,12>().setZero();
      gtsam::Matrix33 H_rc;

      for(int i=0;i<4;++i)
      {
        // contact point only affects angular acceleration:
        H_rc.setZero();
        H_rc.block(0,0,3,1) = (gtsam::Matrix33()<<
                               0,0,0,
                               0,0,-1.0,
                               0,1.0,0).finished() * bt_forces.segment(i*3,3) ;
        H_rc.block(0,1,3,1) = (gtsam::Matrix33()<<
                               0,0,1.0,
                               0,0,0.0,
                               -1.0,0.0,0).finished() * bt_forces.segment(i*3,3);
        
        H_rc.block(0,2,3,1) = (gtsam::Matrix33()<<
                               0,-1.0,0,
                               1.0,0,0.0,
                               0,0.0,0).finished() * bt_forces.segment(i*3,3);
        

        H3->block(0,i*3,3,3) = -1.0 * H_rc;
      }
      *H3 = w_ * *H3;

    }


    // inertia, com, mass 6x10
    if(H4)
    {
            *H4 = Eigen::Matrix<double,6,7>().setZero();

      // gradient for mass:
      //H4->block(0,0,3,1) = skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H4->block(0,0,3,1) =  - skew_rc * g_;

      //skew_rc * g_ * -1.0 + (skew_rc * skew_rc) * acc.head(3);

      H4->block(3,0,3,1) +=  skew_vel * skew_vel * q_rc;

      H4->block(3,0,3,1) += -1.0 * g_;
      // gradient for com:
      
      Matrix33 skew_grad = gtsam::Matrix33().setZero();

      // gradient for body frame:
      
      H4->block(3,1,3,3) += inertia[0] * skew_vel * skew_vel * Matrix33().Identity();

      // gradient for torque error:

      // gradient for x
      skew_grad.setZero();
      skew_grad(2,1) = 1.0;
      skew_grad(1,2) = -1.0;
      
      H4->block(0,1,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      // y
      skew_grad.setZero();
      skew_grad(0,2) = 1.0;
      skew_grad(2,0) = -1.0;


      H4->block(0,2,3,1) += -1.0 * skew_grad * inertia[0] * g_;


      // z
      skew_grad.setZero();
      skew_grad(0,1) = -1.0;
      skew_grad(1,0) = 1.0;

      H4->block(0,3,3,1) += -1.0 * skew_grad * inertia[0] * g_;

      *H4 = w_ * *H4;


    }

    return retVal;
  }
  
};
