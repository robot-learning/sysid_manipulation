#include <pagmo/pagmo.hpp>


// setup optimization problem


namespace pagmo
{
  /*
    Force Torque function:
    min F(X)

  */
  struct force_torque_function{
    force_torque_function(vector_double force_=vector_double(),vector_double torque_=vector_double(),vector_double::size_type dim=3,vector_double::size_type nic=0,vector_double::size_type nec=0) : force(force_),torque(torque_),m_dim(dim), m_nic(nic), m_nec(nec)
    {
    };

    // Fitness computation
    // This is the objective function+constraints giving the total cost
    vector_double fitness(const vector_double &x) const
    {
      //
      double retval=0.0;

      // x is the contact point
      retval = pow(((pow(torque[0] + (x[2] * force[1] - x[1] * force[2]),2)
                            + pow(torque[1] + (x[0] * force[2] - x[2] * force[0]),2) 
                            + pow(torque[2] + (x[1] * force[0] - x[0] * force[1]),2))),1);
      
      return {retval};// return {obj_fn,equality_constraints,inequality_constraints}
    
    }

    // Bounds(variable bounds)
    // This function encodes the constraints
    std::pair<vector_double,vector_double> get_bounds() const
    {
      vector_double lb(m_dim,-50.0);
      vector_double ub(m_dim,50.0);
      return {lb,ub};
    }

    // returns the number of inequality constraints
    vector_double::size_type get_nic() const
    {
      return m_nic;
    }

    // returns the number of equality constraints
    vector_double::size_type get_nec() const
    {
      return m_nec;
    }

      
    // Problem name

    std::string get_name() const
    {
      return "Force Torque Optimization Problem";
    }

    vector_double gradient ( const vector_double &x) const
    {
      vector_double retval(m_dim,0.0);

      double cost_scale = 1.0;//pow(((pow(torque[0] - (x[2] * force[1] - x[1] * force[2]),2)
      //+ pow(torque[1] - (x[0] * force[2] - x[2] * force[0]),2) 
      //+ pow(torque[2] - (x[1] * force[0] - x[0] * force[1]),2))),2);

      // compute gradient for cost function
      retval[0]= cost_scale * (2.0 * (torque[1] + (x[0] * force[2] - x[2] * force[0])) * (1.0 * force[2]) +
                               2.0 *(torque[2] + (x[1] * force[0] - x[0] * force[1])) * (-1.0*force[1]));
      
      retval[1]= cost_scale * (2.0 * (torque[0] + (x[2] * force[1] - x[1] * force[2])) * (-1.0*force[2])
                           + 2.0 * (torque[2] + (x[1] * force[0] - x[0] * force[1])) * (1.0 * force[0])) ;

      
      retval[2]= cost_scale * (2.0 * (torque[0] + (x[2] * force[1] - x[1] * force[2])) * (1.0 * force[1]) +
                           2.0 * (torque[1] + (x[0] * force[2] - x[2] * force[0])) * (-1.0 * force[0]));
      return retval;
    }
    
    // optimal solution:
    /*
    vector_double best_known() const
    {
      return vector_double(m_dim,1.);// Maybe initialize with current state?
    }
    */

    // problem dimension
    vector_double::size_type m_dim;
    vector_double::size_type m_nec;
    vector_double::size_type m_nic;
    vector_double force,torque;
  };
}


Eigen::Vector3d opt_cpt(const Eigen::Vector3d &force, const Eigen::Vector3d &torque)
{
  using namespace pagmo;

  vector_double f,tau;
  f.resize(3);
  f[0] = force[0];
  f[1] = force[1];
  f[2] = force[2];
  tau.resize(3);
  tau[0] = torque[0];
  tau[1] = torque[1];
  tau[2] = torque[2];
  problem prob{force_torque_function{f,tau}};
  //problem prob{rosenbrock{2}};
  //problem prob{cec2006{7u}};
  population pop{prob, 500u};
  // call nlopt slsqp
  nlopt nl_solver("slsqp");
  //nl_solver.set_xtol_abs(1e-16);
  //nl_solver.set_xtol_abs(0);

  algorithm algo{nl_solver};
  
  algo.set_verbosity(1);


  pop = algo.evolve(pop);
  std::vector<double> f_best,x_best;
  f_best=pop.champion_f();
  x_best=pop.champion_x();
  //std::cout<<"optimized force cost: "<<f_best[0]<<std::endl;
  Eigen::Vector3d contact;
  contact[0] = x_best[0];
  contact[1] = x_best[1];
  contact[2] = x_best[2];
  return contact;
  
}
