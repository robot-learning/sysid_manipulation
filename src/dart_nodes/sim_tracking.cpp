// This file tests your dart installation
/*
 * Copyright (c) 2015-2016, Graphics Lab, Georgia Tech Research Corporation
 * Copyright (c) 2015-2016, Humanoid Lab, Georgia Tech Research Corporation
 * Copyright (c) 2016, Personal Robotics Lab, Carnegie Mellon University
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 *   Redistribution and use in source and binary forms, with or
 *   without modification, are permitted provided that the following
 *   conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 *   CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
 *   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 *   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *   AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 */

//#include "gtsam_factors/sysid_factors.h"
#include <dart/dart.hpp>
#include <dart/gui/gui.hpp>
#include <dart/utils/urdf/urdf.hpp>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <Eigen/QR>
#include <visualization_msgs/Marker.h>

//#include <gtsam/geometry/Pose3.h>
#include <vector>
//#include <gtsam/slam/BetweenFactor.h>
//#include "force_opt.hpp"
//#include "gtsam_factors/sysid_expressions.h"

#define DART_DATA_PATH_ "/home/bala/dart/data/"
typedef Eigen::Matrix3d Matrix33;
typedef Eigen::Vector3d Vector3;
typedef Eigen::Matrix< double, 6, 1 > 	Vector6d;

using namespace std;




double default_push_force = 9.8;  // N
const int default_force_duration = 200; // # iterations
const int default_push_duration = 1000;  // # iterations

const double default_endeffector_offset = 0.05;

using namespace dart::common;
using namespace dart::dynamics;
using namespace dart::simulation;
using namespace dart::math;

const Eigen::Vector3d com(0.2,0.5,0.1);
const double dt_ = 1.0/100.0;///5.0;



class dartSim
{
public:
  int force_count=0;
  int t_step = 0;
  std::vector<Eigen::Vector3d> forces;
  vector<Eigen::VectorXd> bt_f_t,bt_p_t;
  dartSim()
  {
    forces.resize(2);
    forces[0].setZero();
    forces[1].setZero();
    
    ros::NodeHandle n;
    force1_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force1",1);
    force2_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force2",1);
    force3_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force3",1);
    force4_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force4",1);

    m_pub = n.advertise<visualization_msgs::Marker>("sim/object", 1);
    pose_pub=n.advertise<geometry_msgs::PoseStamped>("sim/obj_pose",1);
    uint32_t shape = visualization_msgs::Marker::CUBE;

    // Set the frame ID and timestamp.  See the TF tutorials for information on these.

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "basic_shapes";
    marker.id = 0;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker.type = shape;

    // Set the marker action.  Options are ADD and DELETE
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.14;
    marker.scale.y = 0.14;
    marker.scale.z = 0.3;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 1.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();
    
  }
  ros::Publisher force1_pub,force2_pub,force3_pub,force4_pub,m_pub,pose_pub;
  tf::TransformBroadcaster br_;

  visualization_msgs::Marker marker;

  bool simulate_object(WorldPtr world,const string obj_name)
  {
    
    //SkeletonPtr mFirstDomino = world->getSkeleton(obj_name);
    
    //cout<<obj_name;
    
    //cout<<mFirstDomino->isMobile()<<endl;
    //cout<<mFirstDomino->getBodyNode(0)->getLocalCOM()<<endl;
    Eigen::Isometry3d w_T_o=world->getSkeleton(obj_name)->getBodyNode(0)->getWorldTransform();
    //cout<<mFirstDomino->getBodyNode(0)->getGravityMode()<<endl;
    //cout<<"D Inertia:"<<endl;
    Eigen::Vector3d force = Eigen::Vector3d(0.01,0.01,0.01)*default_push_force;

    //double low = 0.5;
    //force = Eigen::Vector3d(low+0.01*(rand() % 100),low+0.01*(rand() % 100),force(2));
    //cout<<force.transpose()<<endl;
    Eigen::Vector3d location =  Eigen::Vector3d(0.0,0,0);
    
    

    world->getSkeleton(obj_name)->getBodyNode(0)->addExtForce(force, location,true,true);
    
    //mFirstDomino->getBodyNode(0)->addExtTorque(force,true);

    //location = Eigen::Vector3d(0.1,0.1,0.1);
    //force = Eigen::Vector3d(0.05,-0.01,0.05)*default_push_force;
    //mFirstDomino->getBodyNode(0)->addExtForce(force, location,false,true);
 
    //Eigen::Vector6d w_obj_acc=mFirstDomino->getBodyNode(0)->getSpatialAcceleration();
    //cout<<w_obj_acc.transpose()<<endl;
    //cout<<w_T_o.rotation()<<endl;
    //Eigen::Vector3d net_force = mFirstDomino->getBodyNode(0)->getBodyForce().tail(3);
    //cout<<net_force.transpose()<<endl;

    publish_force_poses(force,location,w_T_o,
    obj_name);
    //delete mFirstDomino;
    return true;
  }

  bool publish_object(WorldPtr world,const string obj_name)
  {

    Eigen::Vector3d force = Eigen::Vector3d(0.01,0.01,0.1)*default_push_force;
    Eigen::Vector3d location =  Eigen::Vector3d(0.0,0,0);

    Eigen::Isometry3d w_T_o=world->getSkeleton(obj_name)->getBodyNode(0)->getWorldTransform();

    publish_force_poses(force,location,w_T_o,
    obj_name);
    return true;
  }
  
  bool timeStepping(WorldPtr world)
  {

    simulate_object(world,"object_gt");
    simulate_object(world,"object_est");

    //publish_object(world,"object_gt");

    world->step();
    t_step++;
    return 1;
  }

  void publish_force_poses(Eigen::Vector3d local_force,
                           Eigen::Vector3d force_location,
                           Eigen::Isometry3d w_T_o,
                           string obj_name)
    
  {

    // send data to ROS
    // convert eigen isometry to transform
    Eigen::Vector3d trans=w_T_o.translation();
    Eigen::Quaterniond q_(w_T_o.rotation());

    /*
    geometry_msgs::PoseStamped obj_pose;
    obj_pose.header.frame_id="world";
    obj_pose.pose.position.x=trans[0];
    obj_pose.pose.position.y=trans[1];
    obj_pose.pose.position.z=trans[2];

    obj_pose.pose.orientation.x=q_.x();
    obj_pose.pose.orientation.y=q_.y();
    obj_pose.pose.orientation.z=q_.z();
    obj_pose.pose.orientation.w=q_.w();
    obj_pose.header.stamp = ros::Time::now();
        
    pose_pub.publish(obj_pose);
    */
    //trans=w_T_o.translation();
    //q_=Eigen::Quaterniond(w_T_o.rotation());

    
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(trans[0],trans[1], trans[2]) );
    tf::Quaternion q(q_.x(),q_.y(),q_.z(),q_.w());
    transform.setRotation(q);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world",obj_name));

    // forces:
    
    geometry_msgs::WrenchStamped f1;
    f1.header.frame_id="f1";
    f1.wrench.force.x=local_force[0];
    f1.wrench.force.y=local_force[1];
    f1.wrench.force.z=local_force[2];
    f1.header.stamp= ros::Time::now();
    transform.setOrigin( tf::Vector3(force_location[0],
                                     force_location[1],
                                     force_location[2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force1_pub.publish(f1);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), obj_name,"f1"));

    //m_pub.publish(marker);
    

  }
};


bool  createObject(const Eigen::Isometry3d& _T,const Eigen::VectorXd w,
                   const string obj_name, WorldPtr world)
{
  dart::dynamics::SkeletonPtr newSkeleton = dart::dynamics::Skeleton::create(obj_name);

  dart::dynamics::ShapePtr newShape(new dart::dynamics::BoxShape(Eigen::Vector3d(1.0,1,1)));
  //newShape-
  dart::dynamics::BodyNode::Properties bodyProp;
  string obj_link=obj_name;
  obj_link.append("_link");
  bodyProp.mName = obj_link;
  
  //bodyProp.mInertia.setMass(_mass);


  // Set up inertia for the domino
  

  dart::dynamics::Inertia inertia(w(0),w(1),w(2),w(3),w(4),w(5),w(6),w(7),w(8),w(9));

  //inertia.setMass(_mass);
  //Eigen::Matrix3d H_mat;
  //H_mat.setIdentity();
  std::cerr<<"valid moment: "<<bool(inertia.verifyMoment(inertia.getMoment()))<<std::endl;

  std::cerr<<inertia.getMoment()<<std::endl;

  bool valid_dynamics = inertia.verifySpatialTensor(inertia.getSpatialTensor());
  std::cerr<<"valid spatial tensor: "<<valid_dynamics<<std::endl;
  if(valid_dynamics ==false)
  {
    std::cerr<<"ERROR: invalid inertial parameters"<<endl;
    exit(0);
  }

  std::cerr<<inertia.getSpatialTensor()<<std::endl;
  //H_mat=0.1*H_mat;
  //inertia.setMoment(H_mat);
  //inertia.setLocalCOM(com);
  bodyProp.mInertia=inertia;
  bodyProp.mIsCollidable = false;
  bodyProp.mGravityMode = false;

  dart::dynamics::FreeJoint::Properties jointProp;

  jointProp.mT_ParentBodyToJoint = _T;
  string j_name=obj_name;
  j_name.append("_joint");

  jointProp.mName = j_name;

  
  auto pair = newSkeleton->createJointAndBodyNodePair<dart::dynamics::FreeJoint>(nullptr, jointProp, bodyProp);

  cerr<<newSkeleton->getJoint(0)->getActuatorType()<<endl;

  //dart::dynamics::CollisionAspect c_ap(dart::dynamics::CollisionAspect::Properties(false));
  //pair.second->createShapeNode(newShape);

  /*
  auto shapeNode = pair.second->createShapeNodeWith<
                                                      dart::dynamics::VisualAspect,c_ap,
    dart::dynamics::DynamicsAspect>(newShape);
  */
  world->addSkeleton(newSkeleton);
  return true;
}

int main(int argc, char* argv[])
{

  // create a logging file and open it.
  ros::init(argc,argv,"dartsim_node");
  dartSim sim;
  //sim.update_force_vec();
  Eigen::Isometry3d T = Eigen::Isometry3d::Identity();
  //cout<<T.linear()<<endl;

  T.translation() =  Eigen::Vector3d(0.0,0.0,0);
  //  cout<<T.affine()<<endl;
  T.rotate(Eigen::Quaterniond(1.0,0.0,0,0));

  // gt properties:
  Eigen::VectorXd w_gt;
  w_gt.resize(10);
  w_gt<<
    0.131,0.09862,-0.036092,0.033414,0.00042,0.001829,0.001844,-0.000458,0.000446,-0.000155;
  
  Eigen::VectorXd w_est;
  w_est.resize(10);
  w_est<<0.0333802,0.240829,-0.0159907,0.0138405,1.49349e-05,0.00179554,0.00179059,9.4286e-05,-0.000133199,7.03345e-06
;
  //w_est<<0.156313,0.302184,0.527664,0.0570328,0.0454666,0.0169347,0.0982181,-0.0266526,-0.0102765,0.00469279;

  default_push_force = 5.0;  // N

  // est properties:
  
  WorldPtr world = std::make_shared<World>();
  world->setTimeStep(dt_);

  Eigen::Vector3d g(0.0,0.0,-9.8);
  world->setGravity(g);

  createObject(T,w_gt,"object_gt",world);
  
  //T.translation() =  Eigen::Vector3d(0.0,0.2,0);

  createObject(T,w_est,"object_est",world);


  //SkeletonPtr floor = createFloor();
  //world->addSkeleton(cyl_gt);
  //world->addSkeleton(cyl_est);

  cout<<"Running simulation:"<<endl;
  //cout<<1.0/dt<<endl;
  ros::Rate loop_rate(1.0/(10.0*dt_));
  int i = 0;
  while(ros::ok())
  {
    if(i>40)
    {
      break;
    }
    if(sim.timeStepping(world)==1)
    {
    }
    i++;
    loop_rate.sleep();
  }
  // write to file
}
