// This file tests your dart installation
/*
 * Copyright (c) 2015-2016, Graphics Lab, Georgia Tech Research Corporation
 * Copyright (c) 2015-2016, Humanoid Lab, Georgia Tech Research Corporation
 * Copyright (c) 2016, Personal Robotics Lab, Carnegie Mellon University
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 *   Redistribution and use in source and binary forms, with or
 *   without modification, are permitted provided that the following
 *   conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 *   CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
 *   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 *   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *   AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 */

//#include "gtsam_factors/sysid_factors.h"
#include <dart/dart.hpp>
#include <dart/gui/gui.hpp>
#include <dart/utils/urdf/urdf.hpp>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <Eigen/QR>
#include <visualization_msgs/Marker.h>

//#include <gtsam/geometry/Pose3.h>
#include <vector>
//#include <gtsam/slam/BetweenFactor.h>
//#include "force_opt.hpp"
//#include "gtsam_factors/sysid_expressions.h"

#define DART_DATA_PATH_ "/home/bala/dart/data/"
typedef Eigen::Matrix3d Matrix33;
typedef Eigen::Vector3d Vector3;
typedef Eigen::Matrix< double, 6, 1 > 	Vector6d;

using namespace std;
const  double mass=1.297;

const double default_push_force = mass*9.8;  // N
const int default_force_duration = 200; // # iterations
const int default_push_duration = 1000;  // # iterations

const double default_endeffector_offset = 0.05;

using namespace dart::common;
using namespace dart::dynamics;
using namespace dart::simulation;
using namespace dart::math;

const Eigen::Vector3d com(0.2,0.5,0.1);
const double dt_ = 1.0/50.0;///5.0;



class dartSim
{
public:
  int force_count=0;
  int t_step = 0;
  std::vector<Eigen::Vector3d> forces;
  vector<Eigen::VectorXd> bt_f_t,bt_p_t;

  void update_force_vec()
  {
        float low=-0.5;
        //forces[1]=Eigen::Vector3d(0,0,0);
        forces[0]=1.0*Eigen::Vector3d(low+0.01*(rand() % 100),low+0.01*(rand() % 100),low+0.01*(rand() % 100));
        //forces[0]=0.0*Eigen::Vector3d(0,0,0.1);
        
        forces[1]=0.0*Eigen::Vector3d(low+0.01*(rand() % 100),low+0.01*(rand() % 100),low+0.01*(rand() % 100));
    
  }
  dartSim()
  {
    forces.resize(2);
    forces[0].setZero();
    forces[1].setZero();
    
    ros::NodeHandle n;
    force1_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force1",1);
    force2_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force2",1);
    force3_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force3",1);
    force4_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force4",1);

    m_pub = n.advertise<visualization_msgs::Marker>("sim/object", 1);
    pose_pub=n.advertise<geometry_msgs::PoseStamped>("sim/obj_pose",1);
    uint32_t shape = visualization_msgs::Marker::CUBE;

    // Set the frame ID and timestamp.  See the TF tutorials for information on these.

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "basic_shapes";
    marker.id = 0;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker.type = shape;

    // Set the marker action.  Options are ADD and DELETE
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.14;
    marker.scale.y = 0.14;
    marker.scale.z = 0.3;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 1.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();
    
  }
  ros::Publisher force1_pub,force2_pub,force3_pub,force4_pub,m_pub,pose_pub;
  tf::TransformBroadcaster br_;

  visualization_msgs::Marker marker;

  bool timeStepping(WorldPtr world)
  {

    SkeletonPtr mFirstDomino = world->getSkeleton("object");
    //cout<<"D Inertia:"<<endl;
    Eigen::Isometry3d relative_transform = mFirstDomino->getBodyNode(0)->getSkeleton()->getJoint(0)->getRelativeTransform();
    std::vector<Eigen::Vector3d> local_forces;

    
    std::vector<Eigen::Vector3d> force_locations,o_f_loc;
    local_forces.resize(4);
    force_locations.resize(4);
    o_f_loc.resize(4);
  
    Eigen::Isometry3d w_T_o=mFirstDomino->getBodyNode(0)->getWorldTransform();
    
    // apply a force to make object float: 
    // direction

    // f_1
    Eigen::Vector3d force =-0.1 * forces[0] ;
    //location
    Eigen::Vector3d location = Eigen::Vector3d(0.2,0.2,0.2);
    /*
    Eigen::Isometry3d o_T_f1;
    o_T_f1.setIdentity();
    o_T_f1.translate(location);
    
    Eigen::Isometry3d w_T_f1=w_T_o*o_T_f1;
    */
    local_forces[2]=force;
    //force_locations[2]=o_T_f1.inverse()*location;
    o_f_loc[2]=location;

    mFirstDomino->getBodyNode(0)->addExtForce(force, location,true,true);

    force =1.01* default_push_force * Eigen::Vector3d::UnitZ() * 1.0;
    //force = -0.1 * forces[0];
    //location
    location = com;// * 0.01;// Eigen::Vector3d(0,0,0);
    
    // get force vec and location
    Eigen::Vector3d local_force= w_T_o.linear().transpose() * force;
    ///Eigen::Vector3d local_force=force;

    //Eigen::Vector3d local_location=w_T_o.inverse()*location;
    local_forces[0]=local_force;//local_force;
    //force_locations[0]=o_T_f1.inverse()*location;
    o_f_loc[0]=location;

    //    o_f_loc[0]=w_T_o.linear()*location;
    mFirstDomino->getBodyNode(0)->addExtForce(local_force, location,true,true);
    
    // f_3
    
    
    
    // f_2
    // find force to balance f_1 and mg
    //Eigen::Vector3d des_force(0.0,0,0);
    location =Eigen::Vector3d(-0.2,-0.2,-0.2);
    //o_f_loc[1]=location;
    // get net torque:
    Eigen::Vector3d tau_sum(0,0,0);
    tau_sum=o_f_loc[0].cross(local_forces[0])+ o_f_loc[2].cross(local_forces[2])+com.cross(-1.0*default_push_force*local_forces[0].normalized());
    // skew matrix of location:
    Eigen::Matrix3d p2_skew;
    p2_skew.setZero();
    p2_skew(0,1)=-location[2];
    p2_skew(0,2)=location[1];
    p2_skew(1,0)=location[2];
    p2_skew(1,2)=-location[0];
    p2_skew(2,0)=-location[1];
    p2_skew(2,1)=location[0];
    
    //p2_skew=p2_skew.inverse();
    tau_sum=-1.0*tau_sum;
    //cerr<<(p2_skew.transpose()*p2_skew).inverse()<<endl;
    //Eigen::CompleteOrthogonalDecomposition<Eigen::Matrix3d> cqr(p2_skew);
    //Eigen::CompleteOrthogonalDecomposition<Eigen::Matrix3d> cqr2(p2_skew);
    
    // force=cqr.solve(tau_sum);
    force=0.1*forces[0];
    //force = p2_skew.transpose()*tau_sum;
    //std::cerr<<(p2_skew*force-tau_sum).transpose()<<std::endl;
    //cerr<<(p2_skew*(cqr2.pseudoInverse()*tau_sum)-tau_sum).transpose()<<endl;
    //location
    local_forces[1]=force;
    o_f_loc[1] = location;
    //force_locations[1]=o_T_f1.inverse()*location;

      
    mFirstDomino->getBodyNode(0)->addExtForce(force, location,true,true);
    // add f4 force:
    force =0.01 * default_push_force * Eigen::Vector3d::UnitX() * 0.5;
    //force[2] = 1.0;
    //location
    location = com * 0.01;// * 0.01;// * 0.001;// Eigen::Vector3d(0,0,0);
    //location[1]=1.1;
    //location[1]=1.1; 

    //cout<<location.transpose()<<endl;
    // get force vec and location
    //cerr<<force<<endl;
    local_force=force;
    ///Eigen::Vector3d local_force=force;

    //Eigen::Vector3d local_location=w_T_o.inverse()*location;
    local_forces[3]= local_force;
    //force_locations[3]=o_T_f1.inverse()*location;
    o_f_loc[3]=location; 
    mFirstDomino->getBodyNode(0)->addExtForce(force, location,true,true);
    //cerr<<"ext force sum: "<<endl;
    //cerr<<mFirstDomino->getBodyNode(0)->getExternalForceGlobal().tail(3).transpose()<<endl;
 
    //Eigen::Vector3d net_force = mFirstDomino->getBodyNode(0)->getExternalForceGlobal().tail(3);
    Eigen::Vector3d net_torque = mFirstDomino->getBodyNode(0)->getExternalForceLocal().head(3);
    Eigen::Vector3d net_force = mFirstDomino->getBodyNode(0)->getExternalForceLocal().tail(3);
    //cout<<"f: "<<endl;
    //cout<<local_forces[0].transpose()<<endl;
    //cout<<"df,t: "<<net_force.transpose()<< " "<< net_torque.transpose()<<endl;
    Eigen::Vector6d w_obj_vel=mFirstDomino->getBodyNode(0)->getSpatialVelocity();
    //cout<<" rv: "<<mFirstDomino->getBodyNode(0)->getRelativeSpatialVelocity().transpose()<<endl;

    //w_obj_vel = dart::math::AdInvT(relative_transform,w_obj_vel);

    string base_frame="object";
    
    
    //cout<<mFirstDomino->getBodyNode(0)->getSkeleton()->getJoint(0)->getRelativeJacobian()<<endl;
        Eigen::Vector3d w_g;
    w_g.setZero();
    w_g(2) = -9.8;

    // difference between linear and rotation?
    w_g  = w_T_o.linear().transpose() * w_g;

    publish_force_poses(local_forces,o_f_loc,w_T_o,
                        base_frame);
    
    w_T_o=mFirstDomino->getBodyNode(0)->getWorldTransform();


    world->step();

    // get acceleration,velocity
    Eigen::Vector6d w_obj_acc=mFirstDomino->getBodyNode(0)->getSpatialAcceleration();

    relative_transform = mFirstDomino->getBodyNode(0)->getSkeleton()->getJoint(0)->getRelativeTransform();

    //cout<<dart::math::AdInvT(relative_transform,w_obj_acc).transpose()<<endl;
    w_obj_acc = w_obj_acc ;

    //w_obj_acc.head(3) = relative_transform.linear()* w_obj_acc.head(3);
    //w_obj_acc.tail(3) = relative_transform.linear().transpose() *

    //cout<<w_obj_acc<<endl;
    /*
    Eigen::Vector6d o_obj_acc=mFirstDomino->getBodyNode(0)->getSpatialAcceleration();
    Eigen::Vector6d o_obj_vel=mFirstDomino->getBodyNode(0)->getSpatialVelocity();
    */
    // transform angular:
    //cout<<w_T_o.linear()<<endl;
    //cout<<w_T_o.rotation()<<endl;
    //w_obj_acc.segment(0,3) = w_T_o.linear().transpose() * w_obj_acc.segment(0,3);
   
    // transform linear:
    //w_obj_acc.segment(3,3) = w_T_o.linear().transpose() * Eigen::Vector3d(w_obj_acc.segment(3,3));
    //cout<<w_obj_acc.transpose()<<endl;

    //cout<<w_obj_acc.transpose()<<endl;
    //gtsam::Vector6 w_obj_vel=mFirstDomino->getBodyNode(0)->getSpatialVelocity();

    // get force and contact points
    Eigen::VectorXd bt_forces,bt_pts;
    bt_forces.resize(12);
    bt_pts.resize(12);
    // convert force & contact points to world frame:
    for(int i=0;i<4;++i)      
    {
      bt_forces.segment(i*3,3) =  local_forces[i];
      //bt_forces.segment(i*3,3) =  Eigen::Vector3d(0,0,0);
      bt_pts.segment(i*3,3) = o_f_loc[i];

    }
    //cout<<bt_forces.transpose()<<endl;
    bt_f_t.emplace_back(bt_forces);
    bt_p_t.emplace_back(bt_pts);
    //bt_forces.segment(0,3) = net_force;
    // get inertial parameters in world frame:
    Eigen::VectorXd w_param;
    w_param.resize(10);
    //const Eigen::Vector3d com(0.0,0.0,0.0);
    double _mass = mass;

    // Set up inertia for the domino
    double _radius=0.4;
    double _height=0.339;


    w_param[4]=(1.0/12.0)*_mass*(3.0*_radius*_radius+_height*_height);
    w_param[5]=(1.0/12.0)*_mass*(3.0*_radius*_radius+_height*_height);
    w_param[6]=0.5*_mass*_radius*_radius;
    w_param[7]=0.0;
    w_param[8]=0.0;
    w_param[9]=0.0;

    w_param[0] = _mass;
    w_param.segment(1,3) = com;
    //cout<<w_param.transpose()<<endl;

    //cout<<w_T_o.translation()<<endl;
    //cout<<w_T_o.rotation()<<endl;

    //cout<<w_T_o.rotation()<<endl;
    //obj_poses.emplace_back(gtsam::Pose3(gtsam::Rot3(w_T_o.rotation()),w_T_o.translation()));
    // get value of factor
    
    //if(t_step>2)
    t_step++;
    //cerr<<w_obj_acc.transpose()<<endl; 

  dart::dynamics::Inertia current_inertia =  mFirstDomino->getBodyNode(0)->getArticulatedInertiaImplicit();
  //cerr<<current_inertia.getSpatialTensor()<<endl;
    force_count++;
   
    
    if(force_count>0)
    {
      update_force_vec();
      force_count=0;
    }
    return 1;
  }

  void publish_force_poses(vector<Eigen::Vector3d> local_forces,vector<Eigen::Vector3d> force_locations,     Eigen::Isometry3d w_T_o, string base_frame)
    
  {

    // send data to ROS
    // convert eigen isometry to transform
    Eigen::Vector3d trans=w_T_o.translation();
    Eigen::Quaterniond q_(w_T_o.rotation());

    geometry_msgs::PoseStamped obj_pose;
    obj_pose.header.frame_id="world";
    obj_pose.pose.position.x=trans[0];
    obj_pose.pose.position.y=trans[1];
    obj_pose.pose.position.z=trans[2];

    obj_pose.pose.orientation.x=q_.x();
    obj_pose.pose.orientation.y=q_.y();
    obj_pose.pose.orientation.z=q_.z();
    obj_pose.pose.orientation.w=q_.w();
    obj_pose.header.stamp = ros::Time::now();
        
    pose_pub.publish(obj_pose);
    trans=w_T_o.translation();
    q_=Eigen::Quaterniond(w_T_o.rotation());

    
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(trans[0],trans[1], trans[2]) );
    tf::Quaternion q(q_.x(),q_.y(),q_.z(),q_.w());
    transform.setRotation(q);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world","object"));

    // forces:
    
    geometry_msgs::WrenchStamped f1;
    f1.header.frame_id="f1";
    f1.wrench.force.x=local_forces[0][0];
    f1.wrench.force.y=local_forces[0][1];
    f1.wrench.force.z=local_forces[0][2];
    f1.wrench.torque.x=force_locations[0][0];
    f1.wrench.torque.y=force_locations[0][1];
    f1.wrench.torque.z=force_locations[0][2];
    f1.header.stamp=obj_pose.header.stamp;
    transform.setOrigin( tf::Vector3(force_locations[0][0],force_locations[0][1],force_locations[0][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force1_pub.publish(f1);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f1"));

    geometry_msgs::WrenchStamped f2;
    f2.header.frame_id="f2";
    f2.wrench.force.x=local_forces[1][0];
    f2.wrench.force.y=local_forces[1][1];
    f2.wrench.force.z=local_forces[1][2];
    f2.wrench.torque.x=force_locations[1][0];
    f2.wrench.torque.y=force_locations[1][1];
    f2.wrench.torque.z=force_locations[1][2];
    f2.header.stamp=obj_pose.header.stamp;

    transform.setOrigin( tf::Vector3(force_locations[1][0],force_locations[1][1],force_locations[1][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force2_pub.publish(f2);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f2"));
    
    geometry_msgs::WrenchStamped f3;
    f3.header.frame_id="f3";
    f3.wrench.force.x=local_forces[2][0];
    f3.wrench.force.y=local_forces[2][1];
    f3.wrench.force.z=local_forces[2][2];
    f3.wrench.torque.x=force_locations[2][0];
    f3.wrench.torque.y=force_locations[2][1];
    f3.wrench.torque.z=force_locations[2][2];
    f3.header.stamp=obj_pose.header.stamp;

    transform.setOrigin( tf::Vector3(force_locations[2][0],force_locations[2][1],force_locations[2][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force3_pub.publish(f3);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f3"));
    marker.header.frame_id = "/object";
    marker.header.stamp = ros::Time::now();

    geometry_msgs::WrenchStamped f4;
    f4.header.frame_id="f4";
    f4.wrench.force.x=local_forces[3][0];
    f4.wrench.force.y=local_forces[3][1];
    f4.wrench.force.z=local_forces[3][2];
    f4.wrench.torque.x=force_locations[3][0];
    f4.wrench.torque.y=force_locations[3][1];
    f4.wrench.torque.z=force_locations[3][2];
    f4.header.stamp=obj_pose.header.stamp;
    
    transform.setOrigin( tf::Vector3(force_locations[0][0],
                                     force_locations[0][1],
                                     force_locations[0][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
  
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f4"));
    force4_pub.publish(f4);

    m_pub.publish(marker);
    

  }
};


SkeletonPtr createCylinder(const Eigen::Isometry3d& _T,
                             double _radius,
                             double _height,
                             double _mass)
{
  dart::dynamics::SkeletonPtr newSkeleton = dart::dynamics::Skeleton::create("object");

  dart::dynamics::ShapePtr newShape(new dart::dynamics::CylinderShape(_radius, _height));

  dart::dynamics::BodyNode::Properties bodyProp;
  bodyProp.mName = "cylinder_link";
  bodyProp.mInertia.setMass(_mass);


  // Set up inertia for the domino
  

  double i_xx=(1.0/12.0)*_mass*(3.0*_radius*_radius+_height*_height);
  double i_yy=(1.0/12.0)*_mass*(3.0*_radius*_radius+_height*_height);
  double i_zz=0.5*_mass*_radius*_radius;
  double i_xy=0.0;
  double i_xz=0.0;
  double i_yz=0.0;
  dart::dynamics::Inertia inertia(_mass,com[0],com[1],com[2],i_xx,i_yy,i_zz,i_xy,i_xz,i_yz);

  //inertia.setMass(_mass);
  //Eigen::Matrix3d H_mat;
  //H_mat.setIdentity();
  std::cerr<<"valid moment: "<<bool(inertia.verifyMoment(inertia.getMoment()))<<std::endl;

  std::cerr<<inertia.getMoment()<<std::endl;
  
  std::cerr<<"valid spatial tensor: "<<bool(inertia.verifySpatialTensor(inertia.getSpatialTensor()))<<std::endl;
  std::cerr<<inertia.getSpatialTensor()<<std::endl;
  //H_mat=0.1*H_mat;
  //inertia.setMoment(H_mat);
  //inertia.setLocalCOM(com);
  bodyProp.mInertia=inertia;
  cout<<"creating joint"<<endl;
  //dart::dynamics::FreeJoint free_joint;
  //cout<<"creating joint"<<endl;

  //dart::dynamics::detail::GenericJointUniqueProperties<dart::math::SE3Space> t;
  
  //dart::dynamics::FreeJoint::Properties jointProp;
  //dart::dynamics::detail::GenericJointProperties<dart::math::SE3Space> jointProp1;
  
  dart::dynamics::FreeJoint::Properties jointProp;

  cout<<"created joint"<<endl;
  ///jointProp.mName = "cylinder_joint";
  jointProp.mT_ParentBodyToJoint = _T;
  

  
  auto pair = newSkeleton->createJointAndBodyNodePair<dart::dynamics::FreeJoint>(nullptr, jointProp, bodyProp);

  cerr<<newSkeleton->getJoint(0)->getActuatorType()<<endl;

  auto shapeNode = pair.second->createShapeNodeWith<
    dart::dynamics::VisualAspect,
    dart::dynamics::CollisionAspect,
    dart::dynamics::DynamicsAspect>(newShape);


  return newSkeleton;
}

int main(int argc, char* argv[])
{

  // create a logging file and open it.
  ros::init(argc,argv,"dartsim_node");
  dartSim sim;
  sim.update_force_vec();
  Eigen::Isometry3d T = Eigen::Isometry3d::Identity();
  //cout<<T.linear()<<endl;
  double rad=0.4;
  double ht=0.339;
  const Eigen::Vector3d position = Eigen::Vector3d(0.0,0.0,0);

  T.translation() = position;
  //  cout<<T.affine()<<endl;
  T.rotate(Eigen::Quaterniond(0.707,0.707,0,0));
  SkeletonPtr cyl = createCylinder(T,rad,ht,mass);
  
  //SkeletonPtr floor = createFloor();

  WorldPtr world = std::make_shared<World>();
  world->setTimeStep(dt_);
  Eigen::Vector3d g(0.0,0.0,-9.8);
  world->setGravity(g);
  world->addSkeleton(cyl);
  cout<<"Running simulation:"<<endl;
  //cout<<1.0/dt<<endl;
  ros::Rate loop_rate(1.0/dt_);
  int i = 0;
  while(ros::ok())
  {
    /*
    if(i>10)
    {
      break;
    }
    */
    if(sim.timeStepping(world)==1)
    {
    }
    i++;
    loop_rate.sleep();
  }
  // write to file
}
