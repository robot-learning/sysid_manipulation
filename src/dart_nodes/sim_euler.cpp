// setup simulator
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
//#include <Eigen/Dense>

#include <gtsam/geometry/Pose3.h>
#include <vector>
#include "force_opt.hpp"
#include "gtsam_factors/sysid_expressions.h"
using namespace std;
typedef Eigen::Matrix3d Matrix33;
typedef Eigen::Vector3d Vector3;
typedef Eigen::Matrix< double, 6, 1 > 	Vector6d;
inline  Matrix33 skew(const Eigen::Vector3d &v) 
{

  return (Matrix33() <<
                      0, -v[2], v[1],
                      v[2], 0 , -v[0],
                      -v[1], v[0], 0).finished();
}

//typedef Vector6d Eigen::Matrix<double, 6, 1>;

bool computeSensor( const Eigen::VectorXd &inertia, const gtsam::Rot3 &R_i, const Eigen::Vector3d &g_, const Eigen::VectorXd &acc, const Eigen::VectorXd &vel, Eigen::VectorXd &bt_forces, Eigen::VectorXd &bt_pts, Eigen::Vector3d &net_torque)
{
    // inertia = i_xx,i_yy,i_zz,i_xy,i_xz,i_yz
    // build inertia matrix:

          // build inertia matrix:
    Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) *
    /*
    Matrix33 inertia_matrix = (Matrix33() <<
                               inertia[4+0], inertia[4+3],inertia[4+4],
                               inertia[4+3],inertia[4+1],inertia[4+5],
                               inertia[4+4],inertia[4+5],inertia[4+2]).finished();
    */
    
    Matrix33 skew_rc = skew(inertia.segment(1,3));

    Vector3 q_rc = (Vector3()<<inertia(1), inertia(2), inertia(3)).finished(); 
    
    Matrix33 skew_vel = skew(vel.head(3));

    
    
    Eigen::Vector3d net_force;
    //retVal.setZero();
    //cerr<<g_.transpose()<<endl;
    net_torque = (inertia_matrix * acc.head(3) 
                + skew_vel * (inertia_matrix) * vel.head(3))
                + inertia[0] * skew_rc * (acc.tail(3) - g_);

    //- inertia[0]*skew_rc * ( acc.tail(3) -g_);


    //cerr<<((inertia_matrix.inverse() * (net_torque - skew_vel * inertia_matrix * vel.head(3))) - acc.head(3)).transpose()<<endl;

                  //;

    //retVal.head(3) = (inertia[0] * skew_rc * acc.tail(3)
    //+ (inertia_matrix - (inertia(0) * skew_rc * skew_rc)) * acc.head(3)
    //  + skew_vel * (inertia_matrix - inertia[0] * skew_rc * skew_rc) *vel.head(3) 
    //  - skew_rc * ( inertia[0]*g_))
    //  - torque; // Torque error


    net_force = (inertia[0]*(acc.tail(3)-g_)
                 + skew(acc.head(3)) * q_rc * inertia[0] 
                 + inertia[0] * skew_vel * skew_vel * q_rc );


    // generate sensor readings

    bt_forces.resize(4*3);
    bt_forces.setZero();

    bt_forces.segment(0,3) = net_force;

    bt_pts.resize(4*3);
    bt_pts.setZero();
    
    //Eigen::MatrixXd f_mat = skew(net_force);
    //Eigen::MatrixXd r_inv = f_mat.transpose() * (f_mat * f_mat.transpose()).inverse() ;
    //Eigen::MatrixXd l_inv =  (f_mat.transpose() * f_mat).inverse()  * f_mat.transpose();

    //cerr<<f_mat * r_inv<<endl;
    //cerr<<l_inv * f_mat<<endl;
        
    //cerr<< skew(net_force).inverse() * skew(net_force)<<endl;
    Eigen::Vector3d cpt;// =  -skew(net_force).inverse() * net_torque;
    cpt.setZero();

    cpt = opt_cpt(net_force,net_torque);
    
    //cerr<< net_torque.transpose() << (skew(cpt) * net_force).transpose() <<endl;
    bt_pts.segment(0,3) = cpt;
    
    
    return true;
}


Eigen::VectorXd inertialError( const Eigen::VectorXd &inertia, const gtsam::Rot3 &R_i, const Eigen::Vector3d g_, const gtsam::Pose3 &x_t_2, const gtsam::Pose3 &x_t_1,const gtsam::Pose3 &x_t, const Eigen::VectorXd &bt_forces, const Eigen::VectorXd &bt_pts , const double &dt,const Eigen::Vector3d net_torque, const Vector6d &obj_vel = Vector6d(), const Vector6d &obj_acc = Vector6d())
{
    // inertia = i_xx,i_yy,i_zz,i_xy,i_xz,i_yz
    // build inertia matrix:
      Matrix33 inertia_diag = (gtsam::Matrix33() <<
                               inertia[4+1] + inertia[4+2],0,0,
                               0,inertia[4+0] + inertia[4+2],0,
                               0,0,inertia[4+0]+ inertia[4+1]).finished();
    Matrix33 inertia_matrix = R_i.matrix() * inertia_diag * R_i.matrix().transpose();// + inertia(0) *
    
    Matrix33 skew_rc = skew(inertia.segment(1,3));
    
    Vector3 q_rc = (Vector3()<<inertia(1), inertia(2), inertia(3)).finished(); 

    Vector6d vel = (gtsam::Pose3::Logmap(x_t) - gtsam::Pose3::Logmap(x_t_2))/(2*dt);

    //cerr<<obj_vel.transpose()<<endl;
    //cerr<<vel.transpose()<<endl;
    Matrix33 skew_vel = skew(vel.head(3));

    Vector3 force = bt_forces.segment(0,3) + bt_forces.segment(3,3) + bt_forces.segment(6,3) + bt_forces.segment(9,3);
    //cerr<<force.transpose()<<endl;
    Vector3 torque;
    torque.setZero();
    for(int i=0;i<4;++i)
    {
      torque+=skew(bt_pts.segment(i*3,3)) * bt_forces.segment(i*3,3) ;
    }
    // compute acceleration:
    
    
    Vector6d acc = obj_acc;
    //acc.setZero();

    // compute acceleration:

    // build acceleration product tensor:
    gtsam::Matrix66 acc_prod;
    acc_prod.setZero();
    acc_prod.block(3,3,3,3) = inertia[0] * gtsam::Matrix33().Identity();
    acc_prod.block(3,0,3,3) = -inertia[0] * skew_rc;
    acc_prod.block(0,3,3,3) = inertia[0] * skew_rc;
    acc_prod.block(0,0,3,3) = inertia_matrix;

    //cerr<<acc_prod.inverse() * acc_prod<<endl;


    Eigen::Vector3d deb_force = (inertia[0]*(obj_acc.tail(3)-g_)
                                 + skew(acc.head(3)) * q_rc * inertia[0] 
                                 + inertia[0] * skew_vel * skew_vel * q_rc );

    Eigen::Vector3d deb_torque = (inertia_matrix * acc.head(3) 
                      + skew_vel * (inertia_matrix) * vel.head(3))
                + inertia[0] * skew_rc * (acc.tail(3) - g_);

    //cout<<"deb, sim force: "<<endl;
    //cerr<<deb_torque.transpose()<<endl;
    //cerr<<net_torque.transpose()<<endl;
    
    gtsam::Matrix61 acc_bias;
    acc_bias.setZero();

    acc_bias.block(0,0,3,1) = force + inertia[0] * g_ - skew_vel * skew_vel * q_rc *inertia[0];
    acc_bias.block(3,0,3,1) = torque - skew_vel * inertia_matrix * vel.head(3)
                            + inertia[0] * skew_rc * g_;

    Vector6d acc_temp = acc_prod.inverse() * acc_bias;

   
    // compute error in Lie algebra:
    gtsam::Matrix66 A = acc_prod;
    //A.block(0,0,3,6) = acc_prod.block(3,0,3,6);
    //A.block(3,0,3,6) = acc_prod.block(0,0,3,6);
    
    gtsam::Matrix61 B;

    B.block(0,0,3,1) = acc_bias.block(3,0,3,1);
    B.block(3,0,3,1) = acc_bias.block(0,0,3,1);

    //Vector6d acc_t = obj_acc * dt * dt;
    Vector6d acc_log = gtsam::Pose3::Logmap(x_t) - 2.0 * gtsam::Pose3::Logmap(x_t_1) + gtsam::Pose3::Logmap(x_t_2);

    //cout<<A<<endl;
    //cout<<acc_t.transpose()<<endl;
    //cout<<acc_log.transpose()<<endl;
    //cout<<B<<endl;
    //cout<<(A*acc_t).transpose()<<endl;
    //cout<<B.transpose()*dt*dt<<endl;


    Vector6d retVal_log = A*(acc_log) - B * dt * dt;
    //    cerr<<retVal_log.transpose()<<endl;
    //cerr<< retVal.squaredNorm()<< " "<<retVal_log.squaredNorm()<<endl;
    
    
    return retVal_log;
}

class worldSim
{
public:
  ros::Publisher force1_pub,force2_pub,force3_pub,force4_pub,m_pub,pose_pub;
  tf::TransformBroadcaster br_;
  visualization_msgs::Marker marker;
  double dt;
  Vector6d obj_acc_;
  Vector6d current_obj_vel_;
  gtsam::Pose3 current_obj_pose_;
  gtsam::Vector7 w_;
  gtsam::Rot3 R_i_;
  Eigen::Vector3d g_;
  int t =0;
  vector<gtsam::Pose3> obj_poses_;
  //gtsam::Pose3 init_obj_pose_;
  //
  worldSim(const double time_step=10)
  {
    
    ros::NodeHandle n;
    dt = 1.0/time_step;
    //ros::Rate loop_rate(time_step);
    //sim_rate_ = loop_rate;

    force1_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force1",1);
    force2_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force2",1);
    force3_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force3",1);
    force4_pub=n.advertise<geometry_msgs::WrenchStamped>("/sim/force4",1);

    m_pub = n.advertise<visualization_msgs::Marker>("sim/object", 1);
    pose_pub=n.advertise<geometry_msgs::PoseStamped>("sim/obj_pose",1);
    uint32_t shape = visualization_msgs::Marker::CUBE;

    // Set the frame ID and timestamp.  See the TF tutorials for information on these.

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "basic_shapes";
    marker.id = 0;

    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    marker.type = shape;

    // Set the marker action.  Options are ADD and DELETE
    marker.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.14;
    marker.scale.y = 0.14;
    marker.scale.z = 0.3;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 1.0f;
    marker.color.g = 0.0f;
    marker.color.b = 1.0f;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();

  }
  void publish_force_poses(vector<Eigen::Vector3d> local_forces,vector<Eigen::Vector3d> force_locations,     Eigen::Isometry3d w_T_o, string base_frame)
    
  {

    // send data to ROS
    // convert eigen isometry to transform
    Eigen::Vector3d trans=w_T_o.translation();
    Eigen::Quaterniond q_(w_T_o.rotation());

    geometry_msgs::PoseStamped obj_pose;
    obj_pose.header.frame_id="world";
    obj_pose.pose.position.x=trans[0];
    obj_pose.pose.position.y=trans[1];
    obj_pose.pose.position.z=trans[2];

    obj_pose.pose.orientation.x=q_.x();
    obj_pose.pose.orientation.y=q_.y();
    obj_pose.pose.orientation.z=q_.z();
    obj_pose.pose.orientation.w=q_.w();
    obj_pose.header.stamp = ros::Time::now();
        
    pose_pub.publish(obj_pose);
    trans=w_T_o.translation();
    q_=Eigen::Quaterniond(w_T_o.rotation());

    
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(trans[0],trans[1], trans[2]) );
    tf::Quaternion q(q_.x(),q_.y(),q_.z(),q_.w());
    transform.setRotation(q);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world","object"));

    // forces:
    
    geometry_msgs::WrenchStamped f1;
    f1.header.frame_id="f1";
    f1.wrench.force.x=local_forces[0][0];
    f1.wrench.force.y=local_forces[0][1];
    f1.wrench.force.z=local_forces[0][2];
    f1.wrench.torque.x=force_locations[0][0];
    f1.wrench.torque.y=force_locations[0][1];
    f1.wrench.torque.z=force_locations[0][2];
    f1.header.stamp=obj_pose.header.stamp;
    transform.setOrigin( tf::Vector3(force_locations[0][0],force_locations[0][1],force_locations[0][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force1_pub.publish(f1);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f1"));

    geometry_msgs::WrenchStamped f2;
    f2.header.frame_id="f2";
    f2.wrench.force.x=local_forces[1][0];
    f2.wrench.force.y=local_forces[1][1];
    f2.wrench.force.z=local_forces[1][2];
    f2.wrench.torque.x=force_locations[1][0];
    f2.wrench.torque.y=force_locations[1][1];
    f2.wrench.torque.z=force_locations[1][2];
    f2.header.stamp=obj_pose.header.stamp;

    transform.setOrigin( tf::Vector3(force_locations[1][0],force_locations[1][1],force_locations[1][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force2_pub.publish(f2);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f2"));
    
    geometry_msgs::WrenchStamped f3;
    f3.header.frame_id="f3";
    f3.wrench.force.x=local_forces[2][0];
    f3.wrench.force.y=local_forces[2][1];
    f3.wrench.force.z=local_forces[2][2];
    f3.wrench.torque.x=force_locations[2][0];
    f3.wrench.torque.y=force_locations[2][1];
    f3.wrench.torque.z=force_locations[2][2];
    f3.header.stamp=obj_pose.header.stamp;

    transform.setOrigin( tf::Vector3(force_locations[2][0],force_locations[2][1],force_locations[2][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
    force3_pub.publish(f3);
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f3"));
    marker.header.frame_id = "/object";
    marker.header.stamp = ros::Time::now();

    geometry_msgs::WrenchStamped f4;
    f4.header.frame_id="f4";
    f4.wrench.force.x=local_forces[3][0];
    f4.wrench.force.y=local_forces[3][1];
    f4.wrench.force.z=local_forces[3][2];
    f4.wrench.torque.x=force_locations[3][0];
    f4.wrench.torque.y=force_locations[3][1];
    f4.wrench.torque.z=force_locations[3][2];
    f4.header.stamp=obj_pose.header.stamp;
    
    transform.setOrigin( tf::Vector3(force_locations[0][0],
                                     force_locations[0][1],
                                     force_locations[0][2]) );

    q=tf::Quaternion(0,0,0,1);
    transform.setRotation(q);
  
    br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "object","f4"));
    force4_pub.publish(f4);
    m_pub.publish(marker);
    
    
  }

  bool init_simulation(const Vector6d &obj_acc, const Vector6d &obj_pose)
  {
    obj_acc_ = Vector6d().setConstant(0.001);//std::rand() * obj_acc;
    current_obj_vel_.setZero();
    current_obj_pose_ = gtsam::Pose3(gtsam::Rot3(gtsam::Quaternion(1,0,0,0)),                         gtsam::Point3(obj_pose[3],obj_pose[4],obj_pose[5]));

    // w_params:
    g_<< 0,0,-9.8;

    w_.resize(7);
    w_<< 1.3, 0.1,0.1,0.1, 0.25,0.1,0.1;

    t = 0;
    //obj_vel_ = obj_vel;
    //init_obj_pose ;
    obj_poses_.clear();
    return true;
  }
  bool simulate()
  {
    // obj_acc_ = Vector6d::Random()*0.1;//std::rand() * obj_acc;
    //obj_acc_[5] = obj_acc_[5] * 2.0;
    //cerr<<obj_acc_.transpose()<<endl;
    //
    // Update current pose
    current_obj_pose_ = current_obj_pose_ * gtsam::Pose3::Expmap(current_obj_vel_*dt + 0.5 * obj_acc_ * dt * dt);

    obj_poses_.push_back(current_obj_pose_);
    //cerr<<current_obj_vel_.transpose()<<endl;
    vector<Eigen::Vector3d> local_forces;
    local_forces.resize(4);
    local_forces[0].setZero();
    local_forces[1].setZero();
    local_forces[2].setZero();
    local_forces[3].setZero();
    
    vector<Eigen::Vector3d> local_cpts = local_forces;
    string base_frame = "world";
    Eigen::Isometry3d w_T_o(current_obj_pose_.matrix());

    Eigen::VectorXd bt_forces,bt_pts;
    Eigen::Vector3d net_torque;


    // compute sensor readings:
    Eigen::VectorXd acc_inter,vel_inter;
    vel_inter = current_obj_vel_;
    acc_inter = obj_acc_;

    
    computeSensor(w_,R_i_,g_,acc_inter,vel_inter,bt_forces,bt_pts,net_torque);

    // compute error:
    if(t>2)
    {
      vel_inter = (gtsam::Pose3::Logmap(obj_poses_[t]) - gtsam::Pose3::Logmap(obj_poses_[t-2]))/(2.0*dt);
      
      acc_inter = (gtsam::Pose3::Logmap(obj_poses_[t]) - 2.0 * gtsam::Pose3::Logmap(obj_poses_[t-1]) + gtsam::Pose3::Logmap(obj_poses_[t-2]))/(dt*dt);
      
      computeSensor(w_,R_i_,g_,acc_inter,vel_inter,bt_forces,bt_pts,net_torque);

      Eigen::VectorXd err = inertialError(w_,R_i_,g_,obj_poses_[t-2],
                                          obj_poses_[t-1], obj_poses_[t],
                                          bt_forces,bt_pts,dt,net_torque,
                                          vel_inter, acc_inter);
      //cout<<err.transpose()<<endl;

      // compute symbolic error:
      gtsam::Vector6 f_tau = sysid_expr::net_force_torque(bt_forces,bt_pts);

      gtsam::Vector7 w_sub = w_;
      w_sub.segment(1,3) =  w_(0) * w_sub.segment(1,3);
      //cout<<w_sub.transpose()<<endl;
      gtsam::Vector6_ err_sym_ = sysid_expr::create_symbolic_dynamics(w_sub,R_i_,obj_poses_[t-2],
                                                              obj_poses_[t-1],
                                                              obj_poses_[t],
                                                              dt,
                                                              g_,f_tau);

      gtsam::Vector6 err_sym = err_sym_.value(gtsam::Values());
      
      cout<<err_sym.lpNorm<1>()<<" "<<err.lpNorm<1>()<<endl;
    } 
    
    //
    //cerr<<inertialError(w_,R_i_,g_,obj_acc_,current_obj_vel_,bt_forces,bt_pts).transpose()<<endl;
    //cerr<<bt_forces<<endl;
    local_forces[0] = bt_forces.segment(0,3);
    local_cpts[0] = bt_pts.segment(0,3);
    publish_force_poses(local_forces, local_cpts, w_T_o, base_frame);
    current_obj_vel_ = current_obj_vel_ + obj_acc_ * dt;

    t++;
  }

};

int main(int argc, char* argv[])
{

  // create a logging file and open it.
  
  ros::init(argc,argv,"sensor_sim_node");

  worldSim sim;
  ros::Rate sim_rate_(10);

  Vector6d obj_acc;
  obj_acc << 0.01,0.001,0,0,0,0.1;
  //Vector6d obj_vel << 0,0,0,0,0,0;
  Vector6d obj_pose;
  obj_pose<< 0,0,0,0,0,0;
  sim.init_simulation(obj_acc,obj_pose);

  while(ros::ok())
  {
    sim.simulate();
    sim_rate_.sleep();
  }
  
  
  return 0;
}
