#include <sysid_manipulation/ft_problem.hpp>
#define DELTA_T 0.2 //seconds
//#include "gtsam_factors/dynamic_inertial_factors.h"
#include "gtsam_factors/dynamics_factors.h"
#include "gtsam_factors/sysid_factors.h"

#include "gtsam_factors/pose_factors.h"
#include <gtsam_unstable/linear/LinearInequality.h>
#include <boost/optional/optional_io.hpp>
// create custom factor:
using namespace gtsam;

poseSmooth::poseSmooth()
{

  
}

bool poseSmooth::initialize_graph()
{
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  ISAM2Params parameters;

  
  //ISAM2DoglegParams dog_params;  
  // parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.1;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.evaluateNonlinearError = true;
  //parameters.factorization = gtsam::ISAM2Params::Factorization::QR; 
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  t_steps_ = 0;
  l_step = 0;
  l_delta = 5000;

  return true;
}
bool poseSmooth::update_pose_graph(const gtsam::Pose3 &obj_pose, const double &time_step)
{
  int t=t_steps_;
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  
  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // acceleration


  // prior on object pose:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose,poseMeasureNoiseModel));


  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    // add zero velocity, accleration:
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    
    
  }

  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;

    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    //graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),smoothAccModel,1e-1));
    
  }
    
  old_time_step=time_step;

  t_steps_++;
}

bool poseSmooth::update_dynamics_graph(const gtsam::Pose3 &obj_pose, const gtsam::Vector6 &obj_vel, const gtsam::Vector6 &obj_acc, const vector<gtsam::Vector> &ft_data,  const gtsam::Vector3 &g_vec)
{
  int t=t_steps_;

 // at first timestep, make velocity and accel zero
  if(t==0)
  {
    initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(1)); // mass
    initialEstimate.insert(Symbol('R',l_step),Rot3()); // mass

    
    
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),Vector7().setConstant(0.01),priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));

    graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));


    initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
    initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
    initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force
    
    graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],ftPointNM));
    graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
    graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));
    
  }
  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }


    initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
    initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
    initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force

    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(1.0)); // ft force
      graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
      graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),
                                       Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));
    }
    
    graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],ftPointNM));
    graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
    graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));

    
    // compute velocity, accel by central difference
    
    // add dynamics factor for FT:
    //graph.add(AccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',0),z6Model, g_vec));
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),                            Symbol('R',l_step), physicsErrModel, g_vec));

    
    graph.add(FtFactor(Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),                            Symbol('R',l_step), physicsErrModel, obj_pose.rotation().inverse() * g_vec,obj_acc,obj_vel));

    //graph.add(QSFtFactor(Symbol('v',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step), physicsErrModel, g_vec,1));


    

    
  }
    
  //old_time_step=time_step;

  t_steps_++;
}
bool poseSmooth::update_graph(const Eigen::VectorXd &obj_pose, const vector<gtsam::Vector> &ft_data, const double &time_step, const gtsam::Vector3 &g_vec)
{
  int t=t_steps_;

  // Prior noise models:
  
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  gtsam::Pose3 obj_pose3(gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
                        gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));


  
  //

  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
  
  
  initialEstimate.insert(Symbol('a',t),Vector6()); // acceleration

  // add measurement of pose as a prior:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose3,poseMeasureNoiseModel));

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(1)); // mass
    initialEstimate.insert(Symbol('R',l_step),Rot3()); // mass

    
    
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),Vector7().setConstant(0.01),priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));

    graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));

    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
    initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
    initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force
    
    graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],ftPointNM));
    graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
    graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));
    
  }
  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }


    initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
    initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
    initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force

    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(1.0)); // ft force
      graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
      graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),
                                       Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));
    }
    
    graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],ftPointNM));
    graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
    graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;
    
    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    // add dynamics factor for FT:
    //graph.add(AccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',0),z6Model, g_vec));
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),                            Symbol('R',l_step), physicsErrModel, g_vec));

    graph.add(NewtonEulerAccFtFactor(Symbol('a',t), Symbol('v',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),                            Symbol('R',l_step), physicsErrModel, g_vec));

    //graph.add(QSFtFactor(Symbol('v',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step), physicsErrModel, g_vec,1));


    
    graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),smoothAccModel,1e-1));

    
  }
    
  old_time_step=time_step;

  t_steps_++;
}

bool poseSmooth::optimize(Values &results,double &error_after, double &error_before)
//bool poseSmooth::optimize(Values &results,double* error_after, double* error_before)
{

  /*
  ISAM2Result result = isam.update(graph,initialEstimate);

  for(int i=0;i<5;++i)
  {
    isam.update();
  }
  result = isam.update();
  results = isam.calculateEstimate();
  error_after=*result.errorAfter;
  */
  
  gtsam::LevenbergMarquardtParams parametersLev;
  parametersLev.maxIterations = 200;
  parametersLev.setVerbosity("ERROR");
  gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);

  results = optimizer.optimize();
  //gtsam::LevenbergMarquardtParams parametersLev;
  //parametersLev.maxIterations = 200;
  //parametersLev.setVerbosity("ERROR");
  //gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);
  
  //GaussNewtonParams parameters;
  //parameters.setVerbosity("ERROR");
  //GaussNewtonOptimizer optimizer(graph, initialEstimate, parameters);
  //results = optimizer.optimize();

  initialEstimate.clear();
  graph.resize(0);

  return true;
}
bool poseSmooth::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "x1 covariance:\n" << marginals.marginalCovariance(Symbol('x', 1)) << endl;
  cout << "x2 covariance:\n" << marginals.marginalCovariance(Symbol('x', 2)) << endl;
  cout << "x3 covariance:\n" << marginals.marginalCovariance(Symbol('x', 3)) << endl;
  cout << "x4 covariance:\n" << marginals.marginalCovariance(Symbol('x', 4)) << endl;
  cout << "x5 covariance:\n" << marginals.marginalCovariance(Symbol('x', 5)) << endl;
  return true;
}
