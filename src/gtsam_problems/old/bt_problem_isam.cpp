#include <sysid_manipulation/bt_problem.hpp>
#define DELTA_T 0.2 //seconds
//#include "gtsam_factors/dynamic_inertial_factors.h"
#include "gtsam_factors/dynamics_factors.h"
#include "gtsam_factors/pose_factors.h"
#include <gtsam_unstable/linear/LinearInequality.h>
#include <boost/optional/optional_io.hpp>
// create custom factor:
using namespace gtsam;

poseSmooth::poseSmooth()
{
  
}

bool poseSmooth::initialize_graph()
{
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  ISAM2Params parameters;

  
  ISAM2DoglegParams dog_params;  
  parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.1;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.evaluateNonlinearError = true;
  //parameters.factorization = gtsam::ISAM2Params::Factorization::;
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  t_steps_=1;
  
  return true;
}
bool poseSmooth::update_graph(const Eigen::VectorXd &obj_pose, const vector<gtsam::Vector> &ft_data, const vector<gtsam::Vector> &bt_data, const double &time_step, const gtsam::Vector3 &g_vec)
{
  int t=t_steps_;
  /*
  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.1));
  noiseModel::Diagonal::shared_ptr betwennModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));
  */
  noiseModel::Diagonal::shared_ptr comBoundsModel=noiseModel::Diagonal::Sigmas(Vector3(0000.0001,0000.0001,0000.0001));
  noiseModel::Diagonal::shared_ptr mBoundModel = noiseModel::Diagonal::Sigmas(Vector1(00000.1));
  noiseModel::Diagonal::shared_ptr zrotModel=noiseModel::Diagonal::Sigmas(Vector3().setConstant(1e-1));
  noiseModel::Diagonal::shared_ptr z7Model=noiseModel::Diagonal::Sigmas(Vector7().setConstant(1e-3));

  
  noiseModel::Diagonal::shared_ptr poseMeasureNoiseModel=noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  noiseModel::Diagonal::shared_ptr z10Model=noiseModel::Diagonal::Sigmas(Vector10().setConstant(1e-1));
  
  noiseModel::Diagonal::shared_ptr z6Model=noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  noiseModel::Diagonal::shared_ptr n6Model=noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  noiseModel::Diagonal::shared_ptr ftNM=noiseModel::Diagonal::Sigmas(Vector3(1e-3,1e-3,1e-3));

  noiseModel::Diagonal::shared_ptr bt_cpt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));
  
  noiseModel::Diagonal::shared_ptr bt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));
  noiseModel::Diagonal::shared_ptr l7Model=noiseModel::Diagonal::Sigmas(Vector7().setConstant(1e-8));

  /*
  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr ftPriorModel = noiseModel::Diagonal::Sigmas(Vector3(1.4, 1.4, 1.4));
  noiseModel::Diagonal::shared_ptr cptPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.001, 0.001, 0.001));
    
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.0));
  noiseModel::Diagonal::shared_ptr betweenModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));
  */
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  gtsam::Pose3 obj_pose3(gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
                        gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));
  
  //Vector6 vd=Vector6::Zero();

  
  //

  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6()); // acceleration

  // ft measurements:
  initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
  initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
  initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force
    

  // ft priors:
  
  graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],ftNM));
  graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftNM));
  graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftNM));

  // bt:
  initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // ft location w.r.t object
  initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // ft force


  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
  }
  graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_cpt_NM));
  
  graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
  
  // add measurement of pose as a prior:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose3,poseMeasureNoiseModel));

  // at first timestep, make velocity and accel zero
  if(t==1)
  {
    initialEstimate.insert(Symbol('W',0),Vector7().setConstant(1)); // mass
    initialEstimate.insert(Symbol('R',0),Rot3()); // mass
    // add bounds on mass:
    //graph.add(LinearInequality(Symbol('M',0),gtsam::RowVector(1).setIdentity(),0.0
    // insert com
    //initialEstimate.insert(Symbol('C',0),Vector3().setConstant(1));
    // insert inertia
    //initialEstimate.insert(Symbol('I',0),Vector6().setConstant(1));
    
    
    //graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),z6Model));
    //graph.add(PriorFactor<Vector10>(Symbol('W',0),Vector10().setConstant(0.01),z10Model));
    graph.add(PriorFactor<Vector7>(Symbol('W',0),Vector7().setConstant(0.01),z7Model));
    graph.add(PriorFactor<Rot3>(Symbol('R',0),Rot3(),zrotModel));

    // add inequality constraint on factors
    //graph.add(PriorFactor<Vector3>(Symbol('C',0),Vector3().setConstant(0.01),CmassModel));
    //graph.add(PriorFactor<Vector6>(Symbol('I',0),Vector6().setConstant(0.01),IModel));

    // A row vector
    //Vector10 mass_pve;
    //mass_pve.setZero();
    //mass_pve(9)=-1.0;
    graph.add(PhysicsFactor(Symbol('W',0),l7Model));
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),z6Model));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),z6Model));
    
  }

  // add velocity factors from second timestep:  
  if(t>1)
  {
    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;
    
    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),z6Model,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),z6Model,dt));

    // add dynamics factor for FT:
    //graph.add(AccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',0),z6Model, g_vec));
    // add dynamics factor for BT:
    
    //graph.add(AccBtFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1), Symbol('W',0),z6Model,g_vec));
    graph.add(ManifoldAccBtFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1),
                                  Symbol('W',0),Symbol('R',0),z6Model,g_vec));
    
    // add inertia factor w.r.t. force, ft distance
    // add minimum jerk factor
    // This will make the error larger!!
    //graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),z6Model,1e-8));

    
  }
    
  old_time_step=time_step;

  t_steps_++;
}

bool poseSmooth::optimize(Values &results,double &error_after, double &error_before)
//bool poseSmooth::optimize(Values &results,double* error_after, double* error_before)
{

  ISAM2Result result = isam.update(graph,initialEstimate);
  for(int i=0;i<10;++i)
  {
    isam.update();
  }
  result = isam.update();

  results = isam.calculateEstimate();
  error_after=*result.errorAfter;
  
  //gtsam::LevenbergMarquardtParams parametersLev;
  //parametersLev.maxIterations = 200;
  //parametersLev.setVerbosity("ERROR");
  //gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);
  
  //GaussNewtonParams parameters;
  //parameters.setVerbosity("ERROR");
  //GaussNewtonOptimizer optimizer(graph, initialEstimate, parameters);
  //results = optimizer.optimize();

  initialEstimate.clear();
  graph.resize(0);

  return true;
}
bool poseSmooth::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "x1 covariance:\n" << marginals.marginalCovariance(Symbol('x', 1)) << endl;
  cout << "x2 covariance:\n" << marginals.marginalCovariance(Symbol('x', 2)) << endl;
  cout << "x3 covariance:\n" << marginals.marginalCovariance(Symbol('x', 3)) << endl;
  cout << "x4 covariance:\n" << marginals.marginalCovariance(Symbol('x', 4)) << endl;
  cout << "x5 covariance:\n" << marginals.marginalCovariance(Symbol('x', 5)) << endl;
  return true;
}
