#include <sysid_manipulation/inertial_problem_isam_ft.hpp>
#define DELTA_T 0.2 //seconds
#include "gtsam_factors/dynamic_inertial_factors.h"
// create custom factor:
using namespace gtsam;

inertialEst::inertialEst()
{
  
}

bool inertialEst::initialize_graph()
{
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  ISAM2Params parameters;
  ISAM2DoglegParams dog_params;
  parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.01;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  t_steps_=1;
  
  
  return true;
}
bool inertialEst::update_graph(const Eigen::Vector3d &g, const vector<Eigen::Vector3d> &ft_data, const Eigen::Vector3d &obj_vel)
{
  int t=t_steps_;
  /*
  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.1));
  noiseModel::Diagonal::shared_ptr betwennModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));
  */
  noiseModel::Diagonal::shared_ptr comBoundsModel=noiseModel::Diagonal::Sigmas(Vector3(0000.1,0000.1,0000.1));
  noiseModel::Diagonal::shared_ptr mBoundModel = noiseModel::Diagonal::Sigmas(Vector1(00000.1));

  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr ftPriorModel = noiseModel::Diagonal::Sigmas(Vector3(1.4, 1.4, 1.4));
  noiseModel::Diagonal::shared_ptr cptPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.001, 0.001, 0.001));
    
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.0));
  noiseModel::Diagonal::shared_ptr betweenModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));

  
  if(ft_data.size()==3)
  {
    initialEstimate.insert(Symbol('a', t), Point3(0.0, 0.0, 0.0)); // force
    initialEstimate.insert(Symbol('b', t), Point3(0.0, 0.0, 0.0));// torque
    initialEstimate.insert(Symbol('d', t), Point3(0.0, 0.0, 0.0)); // pose of ft w.r.t object
    initialEstimate.insert(Symbol('j', t), Point3(0.0, 0.0, 0.0)); // j is the com
    
  }
  initialEstimate.insert(Symbol('v',t),Point3(0,0,0));// object velocity
  initialEstimate.insert(Symbol('G', t), Point3(0.0, -9.8, 0.0));

  if(t==1)
  {
   
    //initialEstimate.insert(Symbol('M', 0), Vector1(0.1));
    initialEstimate.insert(Symbol('m', 0), Vector1(0.1));
    //initialEstimate.insert(Symbol('v',0),Point3(0,0,0));// object velocity

    //initialEstimate.insert(Symbol('C', 0), Point3(0.1,0.1,0.1));
    //graph.add(PriorFactor<Point3>(Symbol('C',0),Point3(0,0,0),comBoundsModel));
    graph.add(PriorFactor<Vector1>(Symbol('m',0),Vector1(0.01),mBoundModel));
    //graph.add(SmoothBetweenFactorD(vec1PriorModel,Symbol('M',0),Symbol('m',0)));


  }

  // update graph
  graph.add(PriorFactor<Point3>(Symbol('G',t),Point3(g),zeroModel));
  
  graph.add(PriorFactor<Point3>(Symbol('v',t),Point3(obj_vel),zeroModel));
  
            
    // add factor for force torque sensor readings:
  if(ft_data.size()==3)
  {
    graph.add(PriorFactor<Point3>(Symbol('a',t),Point3(ft_data[0]),ftPriorModel));
    graph.add(PriorFactor<Point3>(Symbol('b',t),Point3(ft_data[1]),vecPriorModel));

    //graph.add(MassFactor(zeroModel,Symbol('a',t),Symbol('G',t),Symbol('M',0)));
    if(t>1)
    {
      
      graph.add(linearAccFactor(zeroModel,Symbol('v',t-1),Symbol('v',t),Symbol('a',t-1),Symbol('G',t-1),Symbol('m',0)));
    }
    // add torque factor:

    // d is the distance of ft sensor from object
    //graph.add(PriorFactor<Point3>(Symbol('d',t),Point3(ft_data[2]),vecPriorModel));

    // add factor between center of mass and estimated center of mass

    //graph.add(TransformFactor(zeroModel,Symbol('d',t),Symbol('j',t),Symbol('C',0)));
    
    //graph.add(CoMFactor(zeroModel,Symbol('j',t),Symbol('G',t),Symbol('b',t),Symbol('M',0)));
    
  }
        
  

  t_steps_++;
}

bool inertialEst::optimize(Values &results)
{ 
  ISAM2Result result =isam.update(graph,initialEstimate);
  
  result.print();
  /*
  cout << "Detailed results:" << endl;
  for (auto keyedStatus : result.detail->variableStatus) {
      const auto& status = keyedStatus.second;
      PrintKey(keyedStatus.first);
      cout << " {" << endl;
      cout << "reeliminated: " << status.isReeliminated << endl;
      cout << "relinearized above thresh: " << status.isAboveRelinThreshold
           << endl;
      cout << "relinearized involved: " << status.isRelinearizeInvolved << endl;
      cout << "relinearized: " << status.isRelinearized << endl;
      cout << "observed: " << status.isObserved << endl;
      cout << "new: " << status.isNew << endl;
      cout << "in the root clique: " << status.inRootClique << endl;
      cout << "}" << endl;
  }
  */
  results = isam.calculateEstimate();
  graph.resize(0);
  initialEstimate.clear();
  /*
  
  gtsam::LevenbergMarquardtParams parametersLev;
  parametersLev.maxIterations = 100;
  parametersLev.setVerbosity("ERROR");

  gtsam::LevenbergMarquardtOptimizer optimizer(graph, data, parametersLev);
  GaussNewtonParams parameters;
  
  
  GaussNewtonOptimizer optimizer(graph, data, parameters);
  results = optimizer.optimize();

  */
 
  return true;
}
bool inertialEst::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "x1 covariance:\n" << marginals.marginalCovariance(Symbol('x', 1)) << endl;
  cout << "x2 covariance:\n" << marginals.marginalCovariance(Symbol('x', 2)) << endl;
  cout << "x3 covariance:\n" << marginals.marginalCovariance(Symbol('x', 3)) << endl;
  cout << "x4 covariance:\n" << marginals.marginalCovariance(Symbol('x', 4)) << endl;
  cout << "x5 covariance:\n" << marginals.marginalCovariance(Symbol('x', 5)) << endl;
  return true;
}
