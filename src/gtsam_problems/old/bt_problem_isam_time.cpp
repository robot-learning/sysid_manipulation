#include <sysid_manipulation/bt_problem.hpp>
#define DELTA_T 0.2 //seconds
//#include "gtsam_factors/dynamic_inertial_factors.h"

#include <sysid_manipulation/sysid_factors_lib.hpp>

#include <gtsam_unstable/linear/LinearInequality.h>
#include <boost/optional/optional_io.hpp>
// create custom factor:
using namespace gtsam;
using namespace sysid_expr;
poseSmooth::poseSmooth()
{
  
}

// create expression factor : input: x_t_2,x_t_1,x_t, f,contact_points, r_c
Vector6_ poseSmooth::create_dynamics_expression(const Key &w, const Key &R, const Pose3 &x_t_2, const Pose3 &x_t_1, const Pose3 &x_t,
                                    const double &dt, const Vector3 &g, const Vector12 &f, const Vector12 &r_f)
{
  Vector6 f_tau = net_force_torque(f,r_f);
  return create_symbolic_dynamics(w,R,x_t_2,x_t_1,x_t,dt,g,f_tau);
  //Vector6_ err;
  
  //Expression<Vector7> w(w_);
  Expression<Vector3> omega = pose_vel_(x_t_2,x_t_1,dt);
  Expression<Vector1> m = Mass(w);
  Expression<Vector3> r_c = Com(w);
  Expression<Matrix33> H = Inertia(w,R);
  Expression<Matrix33> skew_rc = Skew(r_c);
  Expression<Matrix33> skew_omega = Skew(omega);

  Expression<Vector6> x_diff = pose_acc_(x_t_2,x_t_1,x_t);
  // A matrix
  Expression<Matrix66> spatial_H = SpatialInertia(m,r_c,H);
  // B matrix
  Vector6_ b1a = b1_a_(m,g);
  Vector6_ b1b = b1_b_(m,r_c,skew_omega);
  Vector6_ b2a = b2_a_(H,omega,skew_omega);
  Vector6_ b2b = b2_b_(m,g,skew_rc);
  Vector6_ net_f_tau = f_tau;
  Vector6_ dyn_bias = net_f_tau + b1a+b1b+b2a+b2b;
  Vector6_ dyn_bias_rate = AccRate(dyn_bias,dt);
  Vector6_ err = dynamics_error_(spatial_H,dyn_bias_rate,x_diff); 

  return err;
}


bool poseSmooth::initialize_graph()
{
  init_noise_models();
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  ISAM2Params parameters;

  
  //ISAM2DoglegParams dog_params;  
  //parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.1;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.evaluateNonlinearError = true;
  //parameters.factorization = gtsam::ISAM2Params::Factorization::CHOLESKY; 
    
  //parameters.factorization = gtsam::ISAM2Params::Factorization::;
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  t_steps_ = 0;
  l_step = 0;
  l_delta = 1000;
  
  return true;
}

bool poseSmooth::update_dynamics_graph(const gtsam::Pose3 &obj_pose, const gtsam::Vector6 &obj_vel, const gtsam::Vector6 &obj_acc, const vector<gtsam::Vector> &ft_data,  const vector<gtsam::Vector> &bt_data,  const gtsam::Vector3 &g_vec)
{
  int t = t_steps_;
  
  noiseModel::Diagonal::shared_ptr bt_cpt_NM=noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(0));

    // bt:

  initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // bt location w.r.t object
  initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // bt force


  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  }
  
  graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_cpt_NM));

  noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(0.0));
  //noiseModel::Diagonal::shared_ptr bt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));

  
  graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
  
  Vector7 w_i;
  w_i <<1.297,0.01,0.03,0.05,1,1,1;
  //w_i <<0.171,0.0902,-0.033399,0.03145,1,1,1;

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    
    initialEstimate.insert(Symbol('W',t),w_i); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_i,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    

    //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));

  }

  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }
    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),w_i); // ft force
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    } 

    // add dynamics factor for FT:
    // add dynamics factor for BT:
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),
    //                              Symbol('R',l_step), physicsErrModel, g_vec));
                                  
    //graph.add(ManifoldAccBtFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    //graph.add(NewtonEulerBtFactor(Symbol('a',t), Symbol('v',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    graph.add(BtFactor(Symbol('b',t-1), Symbol('c',t-1),
    Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,obj_pose.rotation().inverse()*g_vec,obj_acc,obj_vel));

    //graph.add(QS_BtFactor(Symbol('b',t-1), Symbol('c',t-1),
    //                      Symbol('W',l_step),physicsErrModel,g_vec,obj_vel));

    

  }
  t_steps_++;
    
  return true;
}
bool poseSmooth::update_param_graph(const gtsam::Pose3 &obj_pose, const gtsam::Vector6 &obj_vel, const gtsam::Vector6 &obj_acc, const vector<gtsam::Vector> &ft_data,  const vector<gtsam::Vector> &bt_data,  const gtsam::Vector3 &g_vec)
{
  int t = t_steps_;
  

  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  }
  
  Vector7 w_i;
  w_i <<1.297,0.1,0.1,0.1,0.25,0.25,0.25;
  //w_i <<0.3,0.1,0.1,0.1,0.25,0.25,0.25;

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    
    initialEstimate.insert(Symbol('W',t),w_i); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_i,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    

    //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));

  }

  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }
    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),w_i); // ft force
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    } 

    // add dynamics factor for FT:
    // add dynamics factor for BT:
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),
    //                              Symbol('R',l_step), physicsErrModel, g_vec));
                                  
    //graph.add(ManifoldAccBtFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    //graph.add(NewtonEulerBtFactor(Symbol('a',t), Symbol('v',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    graph.add(ParamBtFactor(Symbol('W',l_step), Symbol('R',l_step),physicsErrModel, g_vec, obj_acc, obj_vel, bt_cpts, bt_forces));

    //graph.add(QS_BtFactor(Symbol('b',t-1), Symbol('c',t-1),
    //                      Symbol('W',l_step),physicsErrModel,g_vec,obj_vel));

    

  }
  t_steps_++;
    
  return true;
}

bool poseSmooth::update_graph(const gtsam::Pose3 &obj_pose, const vector<gtsam::Vector> &ft_data, const vector<gtsam::Vector> &bt_data, const double &time_step, const gtsam::Vector3 &g_vec)
{
  int t=t_steps_;
  

  noiseModel::Diagonal::shared_ptr bt_cpt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));
  
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  //gtsam::Pose3 obj_pose3(gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
  //gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));
  
  //Vector6 vd=Vector6::Zero();

  
  //

  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // acceleration


  // prior on object pose:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose,poseMeasureNoiseModel));

  // ft measurements:
  //initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
  //initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
  //initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force
    

  // ft priors:

  //graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],allegroFTPointNM));    
  //graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
  //graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));

  // bt:

  initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // bt location w.r.t object
  initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // bt force


  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  }
  
  graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_cpt_NM));

  noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(bt_acc);
  //noiseModel::Diagonal::shared_ptr bt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));

  
  graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    Vector7 w_init;
    w_init.setConstant(1.0);
    w_init(4) = 0.0;
    w_init(5) = 0.0;
    w_init(6) = 0.0;
    
    initialEstimate.insert(Symbol('W',t),w_init); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass

    // add zero velocity, accleration:
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    
    
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_init,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    

    graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
    
  }

  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;

    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(0.1)); // ft force
      graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
        graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    } 

    // add dynamics factor for FT:
    // add dynamics factor for BT:
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),
    //                              Symbol('R',l_step), physicsErrModel, g_vec));
                                  
    //graph.add(ManifoldAccBtFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    graph.add(NewtonEulerBtFactor(Symbol('a',t), Symbol('v',t), Symbol('b',t-1), Symbol('c',t-1),
                                  Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    


    //graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),smoothAccModel,10.0));
    
  }
    
  old_time_step=time_step;

  t_steps_++;
}


bool poseSmooth::update_expression_graph(const gtsam::Pose3 &x_t_2,
                                         const gtsam::Pose3 &x_t_1,
                                         const gtsam::Pose3 &x_t,
                                         const vector<gtsam::Vector> &bt_data,  const
                                         gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;
  

  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 
  
  Vector7 w_i;
  //w_i <<1.3,0.1,0.1,0.1,0.25,0.25,0.25;
  w_i <<0.1,0.001,0.001,0.001,0.25,0.25,0.25;

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    
    initialEstimate.insert(Symbol('W',t),w_i); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_i,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    

    //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));

  }

  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }
    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),w_i); // ft force
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    } 
    double dt=time_step-old_time_step;
    Vector6 net_f_tau= net_force_torque(bt_forces,bt_cpts);
    Vector6_ err_expression =
      create_symbolic_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                                 x_t_2,x_t_1,x_t,dt,g_vec,net_f_tau);
    graph.addExpressionFactor(physicsErrModel,Vector6().setZero(),err_expression);


    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}

bool poseSmooth::update_pose_dynamics_graph(const gtsam::Pose3 &x_t,
                                            const vector<gtsam::Vector> &bt_data,
                                            const gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;
  

  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 
  
  Vector7 w_i;
  //w_i <<1.3,0.1,0.1,0.1,0.25,0.25,0.25;
  w_i <<0.001,0.001,0.001,0.001,0.25,0.25,0.25;

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    
    initialEstimate.insert(Symbol('W',t),w_i); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_i,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    
    
    graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));

  }

  // add pose factors:
  // prior on object pose:
  initialEstimate.insert(Symbol('x',t),x_t); // pose


  graph.add(PriorFactor<Pose3>(Symbol('x',t),x_t,poseMeasureNoiseModel));
  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }
    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),w_i); // ft force
      graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    }

    double dt=time_step-old_time_step;
    if(t>1)
    {
      initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // bt location w.r.t object
      initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // bt force

      noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(1e-5));

      graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
      graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_NM));
      
      Vector6_ net_f_tau= net_force_torque_(Symbol('b',t),Symbol('c',t));
      //Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);

      Vector6_ err_expression =
        create_symbolic_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                                 Symbol('x',t-2),
                                 Symbol('x',t-1),
                                 Symbol('x',t),
                                 dt,g_vec,net_f_tau);
      auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));

      graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
    }

    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}

bool poseSmooth::update_qs_graph(const gtsam::Pose3 &obj_pose, const vector<gtsam::Vector> &ft_data, const vector<gtsam::Vector> &bt_data, const double &time_step, const gtsam::Vector3 &g_vec)
{
  int t=t_steps_;
  

  noiseModel::Diagonal::shared_ptr bt_cpt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));
  
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  //Vector6 vd=Vector6::Zero();

  
  //

  initialEstimate.insert(Symbol('x',t),obj_pose); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // acceleration


  // prior on object pose:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose,poseMeasureNoiseModel));

  // ft measurements:
  //initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
  //initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
  //initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force
    

  // ft priors:

  //graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],allegroFTPointNM));    
  //graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
  //graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));

  // bt:

  initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // bt location w.r.t object
  initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // bt force


  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  }
  
  graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_cpt_NM));

  noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(bt_acc);
  //noiseModel::Diagonal::shared_ptr bt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));

  
  graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    Vector7 w_init;
    w_init.setConstant(1.0);
    
    initialEstimate.insert(Symbol('W',t),w_init); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass

    // add zero velocity, accleration:
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    
    
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_init,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    

    graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
    
  }

  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;

    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(0.1)); // ft force
      graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
        graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    } 

    // add dynamics factor for FT:
    // add dynamics factor for BT:
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),
    //                              Symbol('R',l_step), physicsErrModel, g_vec));
                                  
    //graph.add(ManifoldAccBtFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    //graph.add(NewtonEulerBtFactor(Symbol('a',t), Symbol('v',t), Symbol('b',t-1), Symbol('c',t-1),
    //                              Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    graph.add(QSBtFactor(Symbol('v',t), Symbol('b',t-1), Symbol('c',t-1),
                                  Symbol('W',l_step),physicsErrModel,g_vec));

    


    graph.add(ConstantFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),smoothAccModel,10.0));
    
  }
    
  old_time_step=time_step;

  t_steps_++;
}

bool poseSmooth::update_pose_graph(const gtsam::Pose3 &obj_pose, const double &time_step)
{
  int t=t_steps_;
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  
  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // acceleration


  // prior on object pose:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose,poseMeasureNoiseModel));


  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    // add zero velocity, accleration:
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    
    
  }

  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;
    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));
    if(t>1)
    {
      graph.add(PoseAccFactor(Symbol('x',t-2),Symbol('x',t-1),Symbol('x',t),Symbol('a',t),fdModel,dt));
      //graph.add(PoseSmoothFactor(Symbol('x',t-2),Symbol('x',t-1),Symbol('x',t),smoothAccModel));
    }
    else
    {
      graph.add(BetweenFactor<Vector6>(Symbol('a',t),Symbol('a',t-1),Vector6().setZero(),priorFDModel));

      //graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    }
    //graph.add(LocalPoseAccFactor(Symbol('x',t-1),Symbol('x',t),Symbol('a',t),fdModel,dt));

    //graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    //graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),smoothAccModel,1e-3));

    
  }
  if(t>1)
  {
    //graph.add(SmoothFactor<Vector6>(Symbol('a',t-2),Symbol('a',t-1),Symbol('a',t),smoothAccModel));

  }
    
  old_time_step=time_step;

  t_steps_++;
}

bool poseSmooth::update_graph_3ftips(const Eigen::VectorXd &obj_pose, const vector<gtsam::Vector> &ft_data, const vector<gtsam::Vector> &bt_data, const double &time_step, const gtsam::Vector3 &g_vec)
{
  int t=t_steps_;
  

  noiseModel::Diagonal::shared_ptr bt_cpt_NM=noiseModel::Diagonal::Sigmas(Vector9().setConstant(1e-6));
  
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  gtsam::Pose3 obj_pose3(gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
                        gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));
  
  //Vector6 vd=Vector6::Zero();

  
  //

  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // acceleration


  // prior on object pose:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose3,poseMeasureNoiseModel));

  // ft measurements:
  //initialEstimate.insert(Symbol('r',t),Vector3().setZero()); // ft location w.r.t object
  //initialEstimate.insert(Symbol('f',t),Vector3().setZero()); // ft force
  //initialEstimate.insert(Symbol('n',t),Vector3().setZero()); // ft force
    

  // ft priors:

  //graph.add(PriorFactor<Vector3>(Symbol('r',t),ft_data[0],allegroFTPointNM));    
  //graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
  //graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));

  // bt:

  initialEstimate.insert(Symbol('c',t),Vector9().setZero()); // bt location w.r.t object
  initialEstimate.insert(Symbol('b',t),Vector9().setZero()); // bt force


  // bt priors:
  Vector9 bt_forces;
  Vector9 bt_cpts;
  Vector9 bt_acc;

  for(int i=0;i<3;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  }

  //cerr<<bt_forces<<endl;
  graph.add(PriorFactor<Vector9>(Symbol('c',t),bt_cpts,bt_cpt_NM));

  noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(bt_acc);
  //noiseModel::Diagonal::shared_ptr bt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));

  
  graph.add(PriorFactor<Vector9>(Symbol('b',t),bt_forces,bt_NM));
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    Vector7 w_init;
    w_init.setConstant(1.0);
    w_init(4) = 0.0;
    w_init(5) = 0.0;
    w_init(6) = 0.0;
    
    initialEstimate.insert(Symbol('W',t),w_init); // mass
    initialEstimate.insert(Symbol('R',t),Rot3()); // mass

    // add zero velocity, accleration:
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    
    
    graph.add(PriorFactor<Vector7>(Symbol('W',l_step),w_init,priorIModel));
    graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    

    graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
    
  }

  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;
    //cerr<<dt<<endl;
    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),Vector7().setConstant(0.1)); // ft force
      graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
        graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    } 

    // add dynamics factor for FT:
    // add dynamics factor for BT:
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),
    //                              Symbol('R',l_step), physicsErrModel, g_vec));
                                  
    //graph.add(ManifoldFixedAccBtFactor(Symbol('b',t-1), Symbol('c',t-1),                                       Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec,Symbol('a',t)));

    /*
    graph.add(ManifoldAccBtLocalFactor(Symbol('a',t), Symbol('b',t-1), Symbol('c',t-1),
                                  Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec));

    */
    graph.add(ManifoldFixedAccBtFactor(Symbol('b',t-1), Symbol('c',t-1),
                                  Symbol('W',l_step),Symbol('R',l_step),physicsErrModel,g_vec,Vector6().setZero()));

    //graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),smoothAccModel,1e-5));
    
  }
    
  old_time_step=time_step;

  t_steps_++;
}
/*
bool poseSmooth::update_graph_pose(const Eigen::VectorXd &obj_pose,const double &time_step)
{
  int t=t_steps_;
  
  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  gtsam::Pose3 obj_pose3(gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
                        gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));
  
  //Vector6 vd=Vector6::Zero();

  
  //

  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
    
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // acceleration


  // prior on object pose:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose3,poseMeasureNoiseModel));

  //noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(bt_acc);
  //noiseModel::Diagonal::shared_ptr bt_NM=noiseModel::Diagonal::Sigmas(Vector12().setConstant(1e-6));

  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    // add zero velocity, accleration:
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),priorFDModel));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),priorFDModel));

    
    
    
  }

  // add velocity factors from second timestep:  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }

    
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;
    //cerr<<dt<<endl;
    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),fdModel,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),fdModel,dt));

    // add dynamics factor for FT:
    // add dynamics factor for BT:
    //graph.add(ManifoldAccFtFactor(Symbol('a',t), Symbol('f',t-1), Symbol('n',t-1),Symbol('W',l_step),
    //                              Symbol('R',l_step), physicsErrModel, g_vec));
                                  

    graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),smoothAccModel,1e-8));
    
  }
    
  old_time_step=time_step;

  t_steps_++;
}
*/
bool poseSmooth::debug_print(Values &estimates)
{
  graph.printErrors(estimates,"Factor errors");
  return true;
}
bool poseSmooth::optimize(Values &results,double &error_after, double &error_before)
//bool poseSmooth::optimize(Values &results,double* error_after, double* error_before)
{

  /*
  ISAM2Result result = isam.update(graph,initialEstimate);
  for(int i=0;i<2;++i)
  {
    isam.update();
  }
  result = isam.update();
  results = isam.calculateEstimate();
  error_after=*result.errorAfter;
  //initialEstimate.clear();
  //graph.resize(0);


  */
  //*/

  //initialEstimate.print("\nInitial Values:\n");
  gtsam::LevenbergMarquardtParams parametersLev;
  parametersLev.setlambdaUpperBound(1e32);
    
  parametersLev.setlambdaInitial(1e6);
  parametersLev.maxIterations = 1000;
  parametersLev.setVerbosity("ERROR");
  gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);

  results = optimizer.optimize();
  initialEstimate = results;

  //graph.printErrors(results);
  /*
  GaussNewtonParams parameters;
  parameters.setVerbosity("ERROR");
  GaussNewtonOptimizer optimizer(graph, initialEstimate, parameters);
  results = optimizer.optimize();
  */
  //std::ofstream out("out.txt"); cout.rdbuf(out.rdbuf());
  //debug_print(results);

  //results.print("Final Result:\n");


  return true; 
}
bool poseSmooth::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "x1 covariance:\n" << marginals.marginalCovariance(Symbol('x', 1)) << endl;
  cout << "x2 covariance:\n" << marginals.marginalCovariance(Symbol('x', 2)) << endl;
  cout << "x3 covariance:\n" << marginals.marginalCovariance(Symbol('x', 3)) << endl;
  cout << "x4 covariance:\n" << marginals.marginalCovariance(Symbol('x', 4)) << endl;
  cout << "x5 covariance:\n" << marginals.marginalCovariance(Symbol('x', 5)) << endl;
  return true;
}
