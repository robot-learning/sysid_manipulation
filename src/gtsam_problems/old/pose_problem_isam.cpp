#include <sysid_manipulation/pose_problem_isam.hpp>
#define DELTA_T 0.2 //seconds
//#include "gtsam_factors/dynamic_inertial_factors.h"
#include "gtsam_factors/pose_factors.h"
#include <boost/optional/optional_io.hpp>
// create custom factor:
using namespace gtsam;

poseSmooth::poseSmooth()
{
  
}

bool poseSmooth::initialize_graph()
{
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  ISAM2Params parameters;

  
  //ISAM2DoglegParams dog_params;  
  //parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.1;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.evaluateNonlinearError = true;
  parameters.factorization = gtsam::ISAM2Params::Factorization::QR;
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  t_steps_=1;
  
  return true;
}
bool poseSmooth::update_graph(const Eigen::VectorXd &obj_pose, const double &time_step)
{
  int t=t_steps_;
  /*
  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.1));
  noiseModel::Diagonal::shared_ptr betwennModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));
  */
  noiseModel::Diagonal::shared_ptr comBoundsModel=noiseModel::Diagonal::Sigmas(Vector3(0000.0001,0000.0001,0000.0001));
  noiseModel::Diagonal::shared_ptr mBoundModel = noiseModel::Diagonal::Sigmas(Vector1(00000.1));

  
  noiseModel::Diagonal::shared_ptr poseMeasureNoiseModel=noiseModel::Diagonal::Sigmas(1e-6*Vector6().setIdentity());
  
  noiseModel::Diagonal::shared_ptr z6Model=noiseModel::Diagonal::Sigmas(Vector6().setZero());
  noiseModel::Diagonal::shared_ptr n6Model=noiseModel::Diagonal::Sigmas(1e-8*Vector6().setIdentity());
 
  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr ftPriorModel = noiseModel::Diagonal::Sigmas(Vector3(1.4, 1.4, 1.4));
  noiseModel::Diagonal::shared_ptr cptPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.001, 0.001, 0.001));
    
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.0));
  noiseModel::Diagonal::shared_ptr betweenModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));

  // add factor between previous previous and current time step:
  
  // convert pose to pose 3
  gtsam::Pose3 obj_pose3(gtsam::Rot3(gtsam::Quaternion(obj_pose[6],obj_pose[3],obj_pose[4],obj_pose[5])),
                        gtsam::Point3(obj_pose[0],obj_pose[1],obj_pose[2]));
  
  //Vector6 vd=Vector6::Zero();
  
  //
  initialEstimate.insert(Symbol('x',t),Pose3()); // pose

  initialEstimate.insert(Symbol('v',t),Vector6().setZero()); // velocity
  initialEstimate.insert(Symbol('a',t),Vector6().setZero()); // velocity
    
  //initialEstimate.insert(Symbol('x',t),Vector3(0,0,0)); // pose

  //initialEstimate.insert(Symbol('a',t),Vector6()); // acceleration

  // add measurement of pose as a prior:
  graph.add(PriorFactor<Pose3>(Symbol('x',t),obj_pose3,poseMeasureNoiseModel));

  // at first timestep, make velocity and accel zero
  if(t==1)
  {
    
    graph.add(PriorFactor<Vector6>(Symbol('v',t),Vector6().setZero(),z6Model));
    graph.add(PriorFactor<Vector6>(Symbol('a',t),Vector6().setZero(),z6Model));
    
  }
  
  // add velocity factors from second timestep:  
  if(t>1)
  {
    // compute velocity, accel by central difference
    double dt=time_step-old_time_step;
    
    graph.add(PoseCDFactor(Symbol('x',t-1),Symbol('x',t),Symbol('v',t),z6Model,dt));    
    graph.add(CDFactor<Vector6>(Symbol('v',t-1),Symbol('v',t),Symbol('a',t),z6Model,dt));
    
    // add minimum jerk factor
    // This will make the error larger!!
    //graph.add(ConstantFactor<Vector6>(Symbol('a',t-1),Symbol('a',t),z6Model,1e-8));

  }
    
  old_time_step=time_step;

  t_steps_++;
}

bool poseSmooth::optimize(Values &results,double &error_after, double &error_before)
//bool poseSmooth::optimize(Values &results,double* error_after, double* error_before)
{

  ISAM2Result result = isam.update(graph,initialEstimate);
  for(int i=0;i<10;++i)
  {
    isam.update();
  }
  result = isam.update();

  results = isam.calculateEstimate();
  error_after=*result.errorAfter;
  
  //gtsam::LevenbergMarquardtParams parametersLev;
  //parametersLev.maxIterations = 200;
  //parametersLev.setVerbosity("ERROR");
  //gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);
  
  //GaussNewtonParams parameters;
  //parameters.setVerbosity("ERROR");
  //GaussNewtonOptimizer optimizer(graph, initialEstimate, parameters);
  //results = optimizer.optimize();

  initialEstimate.clear();
  graph.resize(0);

  return true;
}
bool poseSmooth::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "x1 covariance:\n" << marginals.marginalCovariance(Symbol('x', 1)) << endl;
  cout << "x2 covariance:\n" << marginals.marginalCovariance(Symbol('x', 2)) << endl;
  cout << "x3 covariance:\n" << marginals.marginalCovariance(Symbol('x', 3)) << endl;
  cout << "x4 covariance:\n" << marginals.marginalCovariance(Symbol('x', 4)) << endl;
  cout << "x5 covariance:\n" << marginals.marginalCovariance(Symbol('x', 5)) << endl;
  return true;
}
