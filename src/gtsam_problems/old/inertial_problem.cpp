#include <sysid_manipulation/inertial_problem.hpp>
#include "gtsam_factors/inertial_factors.h"
// create custom factor:

inertialEst::inertialEst()
{
  
}
bool inertialEst::update_measurements(vector<Eigen::Vector3d> forces, Eigen::Vector3d g, vector<Eigen::Vector3d> cpts,const int &t_step)
{
  forces_.resize(t_step+1);
  forces_[t_step].resize(forces.size());
  for(int i=0; i<forces.size();++i)
  {
    forces_[t_step][i]=forces[i];
  }

  cpts_.resize(t_step+1);
  cpts_[t_step].resize(cpts.size());
  for(int i=0; i<cpts.size();++i)
  {
    cpts_[t_step][i]=cpts[i];
  }
    
  g_vecs_.resize(t_step+1);
  g_vecs_[t_step]=g;
  return true;
}
Values inertialEst::initialize_values(const int &t_steps)
{
  Values initials;
  for(int t=0;t<t_steps;++t)
  {

    for(int i=0;i<forces_[t].size();++i)
    {
    initials.insert(Symbol('f', t*4+i), Point3(0.0, 0.0, 0.0));
    initials.insert(Symbol('r', t*4+i), Point3(0.0, 0.0, 0.0));
      
    }
    initials.insert(Symbol('F', t), Point3(0.0, 0.0, 0.0));
    for(int j=0;j<cpts_[t].size();++j)
    {
      for(int i=0;i<cpts_[t].size();++i)
      {
        initials.insert(Symbol('q', t*4*4+j*4+i), Point3(0.0, 0.0, 0.0));
        initials.insert(Symbol('s', t*4*4+j*4+i), Point3(0.0, 0.0, 0.0));
        
      }
      //initials.insert(Symbol('Q', t*4+j), Point3(0.0, 0.0, 0.0));
      initials.insert(Symbol('c', t*4+j), Point3(0.0, 0.0, 0.0));

    }

    initials.insert(Symbol('G', t), Point3(0.0, -9.8, 0.0));
    //initials.insert(Symbol('C', t), Point3(0.1,0.1,0.1));
    //initials.insert(Symbol('M', t), Vector1(0.1));
  }
  initials.insert(Symbol('M', 0), Vector1(0.0));
  initials.insert(Symbol('m', 0), Vector1(0.1));
  initials.insert(Symbol('C', 0), Point3(0.0,0.0,0.0));

  return initials;
}
bool inertialEst::build_graph(const int &t_step)
{


  noiseModel::Diagonal::shared_ptr zeroModel = noiseModel::Diagonal::Sigmas(Vector3(0.0, 0.0, 0.0));
  noiseModel::Diagonal::shared_ptr vecPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.1, 0.1, 0.1));
  noiseModel::Diagonal::shared_ptr cptPriorModel = noiseModel::Diagonal::Sigmas(Vector3(0.001, 0.001, 0.001));
    
  noiseModel::Diagonal::shared_ptr vec1PriorModel = noiseModel::Diagonal::Sigmas(Vector1(0.00001));
  noiseModel::Diagonal::shared_ptr betweenModel=noiseModel::Diagonal::Sigmas(Vector3(0.1,0.1,0.1));

  
  noiseModel::Diagonal::shared_ptr comBoundsModel=noiseModel::Diagonal::Sigmas(Vector3(00.1,00000.1,000.1));
  noiseModel::Diagonal::shared_ptr mBoundModel = noiseModel::Diagonal::Sigmas(Vector1(00.1));
  
  // variables:
  // f = force
  // r = contact points
  // q = torque computed by r X f
  // F = sum force, comp. by \sum f_i
  // Q = sum torque
  // M = mass
  // C = center of mass w.r.t. object frame
  // G = gravity vector
  // s = transformed contact points
  // c = transformed center of mass
  for(int t=0;t<t_step;++t)
  {

    graph.add(PriorFactor<Point3>(Symbol('G',t),Point3(g_vecs_[t]),vecPriorModel));

    for(int i=0;i<forces_[t].size();++i)
    {
      graph.add(PriorFactor<Point3>(Symbol('f',t*4+i),Point3(forces_[t][i]),vecPriorModel));
    }
    for(int i=0;i<cpts_[t].size();++i)
    {
      // add contact points w.r.t object frame:
      graph.add(PriorFactor<Point3>(Symbol('r',t*4+i),Point3(cpts_[t][i]),cptPriorModel));
      
    }
    graph.add(SumFactor(vecPriorModel,Symbol('f',t*4+0), Symbol('f',t*4+1), Symbol('f',t*4+2), Symbol('f',t*4+3),Symbol('F',t)));
    graph.add(MassFactor(zeroModel,Symbol('F',t),Symbol('G',t),Symbol('M',0)));
    if(t==0)
    {
      graph.add(PriorFactor<Point3>(Symbol('C',0),Point3(0,0,0),comBoundsModel));
      graph.add(PriorFactor<Vector1>(Symbol('m',0),Vector1(0.01),mBoundModel));
      graph.add(SmoothBetweenFactorD(vec1PriorModel,Symbol('M',0),Symbol('m',0)));
    }
    //continue;
    // add reference frames for fingertips
    for(int j=0;j<cpts_[t].size();++j)
    {
      for(int i=0;i<cpts_[t].size();++i)
      {
        graph.add(TransformFactor(zeroModel,Symbol('r',t*4+j),Symbol('r',t*4+i),Symbol('s',t*(4*4)+j*4+i)));
        graph.add(TorqueForceFactor(zeroModel,
                                    Symbol('s',t*(4*4)+j*4+i),Symbol('f',t*4+i),
                                    Symbol('q',t*(4*4)+j*4+i)));

      }
      graph.add(TransformFactor(zeroModel,Symbol('r',t*4+j),Symbol('C',0),Symbol('c',t*4+j)));
      graph.add(CoMFullFactor(zeroModel,Symbol('c',t*4+j),Symbol('G',t),
                              Symbol('q',t*(4*4)+j*4+0),
                              Symbol('q',t*(4*4)+j*4+1),
                              Symbol('q',t*(4*4)+j*4+2),
                              Symbol('q',t*(4*4)+j*4+3),
                              Symbol('m',0)));
    }
    

  }
  
  //graph.print("\nFactor Graph:\n"); 
  return true;

  }
bool inertialEst::optimize(Values data, Values &results)
{
  /*
  gtsam::LevenbergMarquardtParams parametersLev;
  parametersLev.maxIterations = 100;
  parametersLev.setVerbosity("ERROR");

  gtsam::LevenbergMarquardtOptimizer optimizer(graph, data, parametersLev);
  */
  GaussNewtonParams parameters;
  parameters.setVerbosity("ERROR");  
  GaussNewtonOptimizer optimizer(graph, data, parameters);
  results = optimizer.optimize();
 
  return true;
}
bool inertialEst::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "x1 covariance:\n" << marginals.marginalCovariance(Symbol('x', 1)) << endl;
  cout << "x2 covariance:\n" << marginals.marginalCovariance(Symbol('x', 2)) << endl;
  cout << "x3 covariance:\n" << marginals.marginalCovariance(Symbol('x', 3)) << endl;
  cout << "x4 covariance:\n" << marginals.marginalCovariance(Symbol('x', 4)) << endl;
  cout << "x5 covariance:\n" << marginals.marginalCovariance(Symbol('x', 5)) << endl;
  return true;
}
