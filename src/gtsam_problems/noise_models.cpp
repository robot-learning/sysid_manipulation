#include <sysid_manipulation/noise_models.hpp>

using namespace gtsam;
bool noiseModels::init_noise_models()
{

  priorGeodesicModel = noiseModel::Constrained::MixedSigmas(Vector1().setConstant(1000),Vector1().setConstant(1e-2));
  // 0.01
 
  // Physics error noise model:
  Vector6 physics_weights;
  physics_weights(0) = 1e-3;// 1e-3;
  physics_weights(1) = 1e-3;// 1e-3;
  physics_weights(2) = 1e-3; // 1e-3;
  physics_weights(3) = 1.0; 
  physics_weights(4) = 1.0;
  physics_weights(5) = 1.0;

  Vector6 physics_var;
  physics_var << 1e-2,1e-2,1e-2,1e-3,1e-3,1e-3;// 5*1e-1 or 1e-1
  //physics_var.setConstant(1e-3); // other methods
  physics_var.setConstant(0.0); // baseline

  physics_weights.setConstant(1.0);

  physicsErrModel = noiseModel::Constrained::MixedSigmas(physics_weights,physics_var);


  // inequality constraint nosie model:
  Vector7 ineq_mean;
  ineq_mean(0) = 1000.0;// * 0.0;
  ineq_mean(1) = 1000.0 ;// * 0.0;
  ineq_mean(2) = 1000.0 ;// * 0.0;
  ineq_mean(3) = 1000.0 ;// * 0.0;
  ineq_mean(4) = 1000.0;// * 0.0; 
  ineq_mean(5) = 1000.0;// * 0.0;
  ineq_mean(6) = 1000.0;// * 0.0;

  l7Model = noiseModel::Constrained::MixedSigmas(ineq_mean,Vector7().setConstant(0.0));
  

  Vector6 pose_noise;
  // orignal: 1.0, 1e-2
  pose_noise.setConstant(1000.0); 
  //pose_noise.head(3) = pose_noise.head(3);
  poseMeasureNoiseModel= noiseModel::Constrained::MixedSigmas(pose_noise,Vector6().setConstant(1e-2));

  // Sensor noise models:
  
  ftForceNM= noiseModel::Constrained::MixedSigmas(Vector3().setConstant(100.0),
                                                  Vector3().setConstant(1e-1));


  ftTorqueNM = noiseModel::Constrained::MixedSigmas(Vector3().setConstant(100.0),
                                                    Vector3().setConstant(1e-1));

    bt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(1e-1));

    // bt:
    //  cpt_NM = noiseModel::Constrained::MixedSigmas(Vector6().setConstant(100.0),
    //Vector6().setConstant(1e-2));

  cpt_NM = noiseModel::Constrained::MixedSigmas(Vector6().setConstant(1000.0),
                                                    Vector6().setConstant(1e-3));



  // not used:
  priorRotModel=noiseModel::Constrained::MixedSigmas(Vector3().setConstant(1),Vector3().setConstant(0.5));
  priorIModel=noiseModel::Constrained::MixedSigmas(Vector7().setConstant(1),Vector7().setConstant(0.5));
  priorQSModel=noiseModel::Constrained::MixedSigmas(Vector4().setConstant(1e-1), Vector4().setConstant(0.0));

  priorFDModel = noiseModel::Constrained::MixedSigmas(Vector6().setConstant(1e-6),Vector6().setConstant(0.0));
    
  // Between noise models: (close to zero)

  betweenRotModel=noiseModel::Constrained::MixedSigmas(Vector3().setConstant(1.0),Vector3().setConstant(0.0));

  betweenIModel= noiseModel::Constrained::MixedSigmas(Vector7().setConstant(1), Vector7().setConstant(0.0));

  // Velocity, acceleration finite differencing variance:
  
  fdModel=noiseModel::Constrained::MixedSigmas(Vector6().setConstant(1e5), Vector6().setConstant(0.0));
  smoothAccModel=noiseModel::Constrained::MixedSigmas(Vector6().setConstant(1e-7), Vector6().setConstant(0.5));
  
  
  


  ftPointNM=noiseModel::Constrained::MixedSigmas(Vector3(1e-4,1e-4,1e-4));
  allegroFTPointNM=noiseModel::Constrained::MixedSigmas(Vector3(1e-2,1e-2,1e-2));
  

  T_NM = noiseModel::Constrained::MixedSigmas(Vector1().setConstant(1.0),Vector1().setConstant(1e-1));

  return true;
}

bool noiseModels::smoothing()
{

  priorGeodesicModel = noiseModel::Constrained::MixedSigmas(Vector1().setConstant(1000),Vector1().setConstant(1e-2));
  // 0.01
 
  Vector6 physics_weights;
  physics_weights(0) = 1;// * 0.0;
  physics_weights(1) = 1;// *0.0;
  physics_weights(2) = 1; // *0.0;
  physics_weights(3) = 1.0; 
  physics_weights(4) = 1.0;
  physics_weights(5) = 1.0;

  physics_weights.setConstant(1000.0);
  Vector6 physics_var;
  //physics_var << 5*1e-1,5*1e-1,5*1e-1,5*1e-2,5*1e-2,5*1e-2;
  physics_var.setConstant(0.0);
  physicsErrModel = noiseModel::Constrained::MixedSigmas(physics_weights,physics_var);


  Vector6 pose_noise;
  // orignal: 1.0, 1e-2
  pose_noise.setConstant(1000.0);
  //pose_noise.head(3) = pose_noise.head(3);
  poseMeasureNoiseModel= noiseModel::Constrained::MixedSigmas(pose_noise,Vector6().setConstant(1e-5));

  bt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1e-3),Vector12().setConstant(5.0*1e-1));
  
  cpt_NM = noiseModel::Constrained::MixedSigmas(Vector6().setConstant(1000.0),
                                                    Vector6().setConstant(1e-3));

}
