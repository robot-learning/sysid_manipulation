#include <sysid_manipulation/bt_problem_expressions.hpp>
#include "gtsam_factors/inequality_factors.h"

#define DELTA_T 0.2 //seconds
//#include "gtsam_factors/dynamic_inertial_factors.h"
//#include "gtsam_factors/inequality_factors.h"

//#include <sysid_manipulation/sysid_factors_lib.hpp>

// create custom factor:
using namespace gtsam;
using namespace sysid_expr;
poseSmooth::poseSmooth()
{ 
  //w_i <<1.1,0.1,0.1,0.2,0.1,0.10,0.2;
  //w_i <<0.05,0.07,-0.03,0.03,0.001,0.001,0.001; 
 
  w_i <<0.05,0.001,-0.001,0.001,0.0001,0.0001,0.0002;

  r_prior = gtsam::Rot3::Ypr(0,0,0);
  r_p.setZero();

}

bool poseSmooth::update_prior(const gtsam::Vector7 &w, const gtsam::Vector3 inertia_r, const gtsam::Rot3 i_rot)
{
  //w_i = w;
  w_prior = w;
  r_p = inertia_r;
  r_prior = i_rot;
  return true;
}

bool poseSmooth::update_seed(const gtsam::Vector7 &w, const gtsam::Vector3 inertia_r, const gtsam::Rot3 i_rot)
{
  //w_i = w;
  w_i = w;
  r_p = inertia_r;
  r_i = i_rot;
  return true;
}

/*
// create expression factor : input: x_t_2,x_t_1,x_t, f,contact_points, r_c
Vector6_ poseSmooth::create_dynamics_expression(const Key &w, const Key &R, const Pose3 &x_t_2, const Pose3 &x_t_1, const Pose3 &x_t,
                                    const double &dt, const Vector3 &g, const Vector12 &f, const Vector12 &r_f)
{
  Vector6 f_tau = net_force_torque(f,r_f);
  return create_symbolic_dynamics(w,R,x_t_2,x_t_1,x_t,dt,g,f_tau);
}
*/

bool poseSmooth::initialize_graph()
{
  init_noise_models();
  
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  /*
  ISAM2Params parameters;

  
  //ISAM2DoglegParams dog_params;  
  //parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.1;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.evaluateNonlinearError = true;
  //parameters.factorization = gtsam::ISAM2Params::Factorization::CHOLESKY; 
    
  //parameters.factorization = gtsam::ISAM2Params::Factorization::;
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  */
  t_steps_ = 0;
  l_step = 0;
  l_delta = 1000;
  
  return true;
}


bool poseSmooth::update_forward_sim(const gtsam::Pose3 &x_t_2,
                                    const gtsam::Pose3 &x_t_1,
                                    const double &t_2, const double &t_1, const double &t_0,
                                    const vector<gtsam::Vector> &bt_data,
                                    const gtsam::Vector3 &g_vec,
                                    const double &opt_mass, const gtsam::Vector3 &opt_com,
                                    const gtsam::Matrix33 &opt_inertia_matrix)
{
  
  //initialEstimate.clear();
  initialEstimate.insert(Symbol('x',t_steps_),Pose3()); // mass
  //graph.resize(0);
  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;
  
  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 
  
  Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);
  
  Vector6_ err_expression =create_forward_dynamics(Vector1().setConstant(opt_mass),
                                                   opt_com,opt_inertia_matrix,
                                                   x_t_2,
                                                   x_t_1,
                                                   Symbol('x',t_steps_),
                                                   t_2,t_1,t_0,
                                                   g_vec,net_f_tau);
  
  auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  
  graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
  t_steps_++;
  return true;
}
bool poseSmooth::update_forward_sim_integral(const gtsam::Pose3 &x_t_2,
                                             const gtsam::Pose3 &x_t_1,
                                             const double &t_2, const double &t_1,
                                             const double &t_0,
                                             const vector<gtsam::Vector> &bt_data,
                                             const vector<gtsam::Pose3> &w_cpts,
                                             const gtsam::Vector3 &g_vec,
                                             const double &opt_mass,
                                             const gtsam::Vector3 &opt_com,
                                             const gtsam::Matrix33 &opt_inertia_matrix)
{
  
  //initialEstimate.clear();
  initialEstimate.insert(Symbol('x',t_steps_),Pose3()); // mass
  if(t_steps_==0)
  {

    Vector6 pose_noise;
    // orignal: 1.0, 1e-2
    pose_noise.setConstant(1.0);
    //pose_noise.head(3) = pose_noise.head(3);
    poseMeasureNoiseModel = noiseModel::Constrained::MixedSigmas(pose_noise,pose_noise*0.0);

    graph.add(PriorFactor<Pose3>(Symbol('x',t_steps_),x_t_2,poseMeasureNoiseModel));
    initialEstimate.insert(Symbol('x',t_steps_+1),Pose3()); // mass

    graph.add(PriorFactor<Pose3>(Symbol('x',t_steps_+1),x_t_1,poseMeasureNoiseModel));
    t_steps_+=2;
    
    return true;
  }
  
  //graph.resize(0);
  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;
  
  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 

  Expression<Vector12> o_f = proj_force_obj_frame_(Symbol('x',t_steps_-2),
                                                   w_cpts[0],w_cpts[1],w_cpts[2],w_cpts[3],
                                                   bt_forces);
                                                 
  // get contact point from contact frame
  Expression<Vector12> o_cpt = proj_cpt_obj_frame_(Symbol('x',t_steps_-2),
                                                   w_cpts[0],w_cpts[1],w_cpts[2],w_cpts[3]);
  //Expression<Vector12> o_cpt = o_f;
  Vector6_ net_f_tau= net_force_torque_(o_f,o_cpt);
  
  //Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);
  
  Vector6_ err_expression =create_forward_dynamics(Vector1().setConstant(opt_mass),
                                                   opt_com,opt_inertia_matrix,
                                                   Symbol('x',t_steps_-2),
                                                   Symbol('x',t_steps_-1),
                                                   Symbol('x',t_steps_),
                                                   t_2,t_1,t_0,
                                                   g_vec,net_f_tau);
  
  auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  
  graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
  t_steps_++;
  return true;
}




bool poseSmooth::add_bt_mm(const vector<gtsam::Vector> &bt_data, const int &t)
{
  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];
 
  }
    // add pose factors:
  initialEstimate.insert(Symbol('c',t),bt_cpts); // bt location w.r.t object
  initialEstimate.insert(Symbol('b',t),bt_forces); // bt force 

  //noiseModel::Constrained::shared_ptr bt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(1e-1));
  
  //bt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),bt_acc); 

  noiseModel::Constrained::shared_ptr pt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(1e-2));
  graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,pt_NM));
  graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
  
  
    
  return true;
}
bool poseSmooth::add_ft_mm(const vector<gtsam::Vector> &ft_data, const int &t)
{
  initialEstimate.insert(Symbol('f',t),ft_data[1]); // pose
  initialEstimate.insert(Symbol('n',t),ft_data[2]); // pose
  
  graph.add(PriorFactor<Vector3>(Symbol('f',t),ft_data[1],ftForceNM));
  graph.add(PriorFactor<Vector3>(Symbol('n',t),ft_data[2],ftTorqueNM));
  return true;
}

bool poseSmooth::add_pose_mm(const gtsam::Pose3 &x_t, const int &t)
{
  // prior on object pose:
  initialEstimate.insert(Symbol('x',t),x_t); // pose


  graph.add(PriorFactor<Pose3>(Symbol('x',t),x_t,poseMeasureNoiseModel));

  return true;
}

bool poseSmooth::add_cpt_mm(const vector<gtsam::Pose3> &w_cpts, const int &t)
{
  // prior on object pose:
  Vector6 pose_noise;
  // orignal: 1.0, 1e-2
  pose_noise.setConstant(1.0);
  //pose_noise.head(3) = pose_noise.head(3);
  //auto cpt_NM= noiseModel::Constrained::MixedSigmas(Vector6().setConstant(100.0),
  //                                                  Vector6().setConstant(1e-2));

  for(int i=0;i<4;++i)
  {

    initialEstimate.insert(Symbol('c',t*4+i),w_cpts[i]); // pose


    graph.add(PriorFactor<Pose3>(Symbol('c',t*4+i),w_cpts[i],cpt_NM));
  }
  return true;
}

bool poseSmooth::update_manifold_graph(const gtsam::Pose3 &x_t, 
                                       const vector<gtsam::Vector> &bt_data,  
                                       const gtsam::Vector3 &g_vec,
                                       const double &time_step)
{
  int t = t_steps_;

  initialEstimate.insert(Symbol('T',t),time_step); // time_step

  graph.add(PriorFactor<double>(Symbol('T',t),time_step,T_NM));

  
  // add bt priors:
  add_bt_mm(bt_data,t);
  // add object priors:
  add_pose_mm(x_t,t);
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  { 
    Matrix44 P_prior = sysid_expr::proj_inertia(sysid_expr::mass(w_i),
                                                sysid_expr::com(w_i),
                                                sysid_expr::inertia(w_i,r_prior));
    //P_prior.setIdentity();
    // create global w
    initialEstimate.insert(Symbol('P',0),P_prior); 
    //initialEstimate.insert(Symbol('R',0),r_p); 

    //initialEstimate.insert(Symbol('W',0),w_i); 

  auto model_1 = noiseModel::Constrained::MixedSigmas(Vector1().setConstant(1),Vector1().setConstant(1e-1));


  Vector7_ phys_expression = physics_constraint_manifold_(Symbol('P',0));

  graph.add(ExpressionFactor<Vector7>(l7Model,Vector7().setZero(),phys_expression));

  //graph.add(ExpressionFactor<Vector1>(model_1,
  //                                      Vector1().setConstant(4),
  //inertia_distance_(P_prior,Symbol('P',0))));
    //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
  
    //graph.add(PhysicsFactor(Symbol('W',0),l7Model));
     
    //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),r_prior,priorRotModel));
    //graph.print();
    //cout<<l_step<<endl;

  } 
  //t_steps_++;

  //return true;

  if(t>1)
  {
    if(t%l_delta==0)
    {
      /*
      l_step+=l_delta;

      //cout<<l_step<<endl;

      initialEstimate.insert(Symbol('R',l_step),Rot3()); 

      initialEstimate.insert(Symbol('W',l_step),w_i); 

      // add between global w and w_t
            
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      
      graph.add(ExpressionFactor<Vector1>(model_1,
                                          Vector1().setConstant(4),
                                          create_between_inertia(w_i,
                                                                 Rot3(),
                                                                 Symbol('W',l_step),
                                                                 Symbol('R',l_step))));
      */
      /*
      graph.add( BetweenFactor<Rot3>(Symbol('R',l_step),
                                     Symbol('R',0),Rot3(),
                                     betweenRotModel));
      
      graph.add( BetweenFactor<Vector7>(Symbol('W',l_step),
                                        Symbol('W',0),Vector7().setConstant(0.0),
                                        betweenIModel));

      */
    }

    double dt=time_step-old_time_step;
    
    Vector6_ net_f_tau= net_force_torque_(Symbol('b',t-2),Symbol('c',t-2));
    
    Vector6_ err_expression =
      create_symbolic_manifold_dynamics(Symbol('P',l_step),
                                        Symbol('x',t-2),
                                        Symbol('x',t-1),
                                        Symbol('x',t),
                                        Symbol('T',t-2),Symbol('T',t-1),Symbol('T',t),
                                        g_vec,net_f_tau);

    
    graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));
    
    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}

bool poseSmooth::update_rr_so3_manifold_graph(const gtsam::Pose3 &x_t, 
                                              const vector<gtsam::Vector> &bt_data,  
                                              const vector<gtsam::Pose3> &w_cpts,
                                              const gtsam::Vector3 &g_vec,
                                              const double &time_step)
{
  int t = t_steps_;

  time_data.push_back(time_step);

  //initialEstimate.insert(Symbol('T',t),time_step); // time_step

  //graph.add(PriorFactor<double>(Symbol('T',t),time_step,T_NM));

  
  // add bt priors:
  add_bt_mm(bt_data,t);

  // add contact frames priors
  add_cpt_mm(w_cpts,t);
  // add object priors:
  //add_pose_mm(x_t,t);
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {

    initialEstimate.insert(Symbol('R',0),Rot3()); 
        
    initialEstimate.insert(Symbol('W',0),w_i);
    /*
    Vector7_ phys_expression = physics_constraint_linear_(Symbol('W',0));
  
    graph.add(ExpressionFactor<Vector7>(l7Model,Vector7().setZero(),phys_expression));
    

    // get manifo
    graph.add(ExpressionFactor<Vector1>(priorGeodesicModel,
                                        Vector1().setConstant(4.0),
                                        create_between_inertia(w_i,r_prior,
                                                               Symbol('W',0),
                                                               Symbol('R',0))));
    */
  //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
    //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),r_prior,priorRotModel));
    //graph.print();
    //cout<<l_step<<endl;

  } 

  if(t>1)
  {

    // force/torque in robot frame

    // convert force to object frame
    //Expression<Vector12> o_f = inv_proj_force_(x_t,Symbol('b',t-2));

    Expression<Vector12> o_f = proj_force_obj_frame_(Symbol('c',(t-2)*4+3),
                                                     Symbol('c',(t-2)*4+0),
                                                     Symbol('c',(t-2)*4+1),
                                                     Symbol('c',(t-2)*4+2),
                                                     Symbol('c',(t-2)*4+3),
                                                    //w_cpts[0],w_cpts[1],w_cpts[2],w_cpts[3],
                                                    Symbol('b',t-2));
    // get contact point from contact frame
    Expression<Vector12> o_cpt = proj_cpt_obj_frame_(Symbol('c',(t-2)*4+3),
                                                     Symbol('c',(t-2)*4+0),
                                                     Symbol('c',(t-2)*4+1),
                                                     Symbol('c',(t-2)*4+2),
                                                     Symbol('c',(t-2)*4+3));
    //Expression<Vector12> o_f = 
    //Expression<Vector12> o_cpt = o_f;
    Vector6_ net_f_tau= net_force_torque_(o_f,o_cpt);
    //Vector6_ net_f_tau= net_force_torque_(Symbol('b',t-2),Symbol('c',t-2));

    
    Vector6_ err_expression =
      create_symbolic_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                               Symbol('c',(t-2)*4+3),
                               Symbol('c',(t-1)*4+3),
                               Symbol('c',(t)*4+3),
                               time_data[t-2],time_data[t-1],time_data[t],
                               g_vec,net_f_tau);
    
    graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

    if(t>2)
    {
      /*
      Expression<Vector6> obj_acc  = variable_dt_jerk_(Symbol('x',t-3),
                                                       Symbol('x',t-2),
                                                       Symbol('x',t-1),Symbol('x',t),
                                                       time_data[t-3],
                                                       time_data[t-2],
                                                       time_data[t-1],time_data[t]);

    //
    graph.add(ExpressionFactor<Vector6>(smoothAccModel,Vector6().setZero(),obj_acc));
      */
      /*
    gtsam::noiseModel::Constrained::shared_ptr smoothForceModel =
      noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1e-7), Vector12().setConstant(0.0));

    Expression<Vector12> bt_force_acc  = variable_dt_jerk_vector_<Vector12>
      (
       Symbol('b',t-3),
       Symbol('b',t-2),
       Symbol('b',t-1),
       Symbol('b',t),
       time_data[t-3],
       time_data[t-2],
       time_data[t-1],
       time_data[t]); 
       graph.add(ExpressionFactor<Vector12>(smoothForceModel,Vector12().setZero(),bt_force_acc));
    }
      */
    }
  }
  t_steps_++;
  //old_time_step=time_step; 

  return true;
}

bool poseSmooth::update_hybrid_so3_manifold_graph(const gtsam::Pose3 &x_t, 
                                                  const vector<gtsam::Vector> &bt_data,  
                                                  const vector<gtsam::Pose3> &w_cpts,
                                                  const gtsam::Vector3 &g_vec,
                                                  const double &time_step,
                                                  const bool inequality,
                                                  const bool geodesic,
                                                  const bool linear_parameters,
                                                  const bool fix_inertia)
{
  int t = t_steps_;

  time_data.push_back(time_step);

  //initialEstimate.insert(Symbol('T',t),time_step); // time_step

  //graph.add(PriorFactor<double>(Symbol('T',t),time_step,T_NM));

  
  // add bt priors:
  add_bt_mm(bt_data,t);

  // add contact frames priors
  //  add_cpt_mm(w_cpts,t);
  // add object priors:
  add_pose_mm(x_t,t);
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {

    if(!fix_inertia && !linear_parameters)
    {
      initialEstimate.insert(Symbol('R',0),Rot3()); 
      
      initialEstimate.insert(Symbol('W',0),w_i); 
      
      if(inequality == true)
      {
        
        Vector7_ phys_expression = soft_physics_constraint_(Symbol('W',0));
        
        graph.add(ExpressionFactor<Vector7>(l7Model,Vector7().setZero(),phys_expression));
        // add mass prior factor:
        
        //graph.add(softMassPriorFactor(Symbol('W',0),l7Model,(w_i-w_i*0.1)));
      }
    // get manifo
      if(geodesic == true)
      {
        graph.add(ExpressionFactor<Vector1>(priorGeodesicModel,
                                            Vector1().setConstant(4.0),
                                            create_between_inertia(w_i,r_prior,
                                                               Symbol('W',0),
                                                                   Symbol('R',0))));
      }
      //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
      //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),r_prior,priorRotModel));
      //graph.print();
      //cout<<l_step<<endl;
      
    }
    else if(linear_parameters)
    {
      initialEstimate.insert(Symbol('R',0),Vector3()); 
      
      initialEstimate.insert(Symbol('W',0),w_i); 
      
    }

  }

  if(t>1)
  {

    // force/torque in robot frame

    // convert force to object frame
    //Expression<Vector12> o_f = inv_proj_force_(x_t,Symbol('b',t-2));
    /*
    Expression<Vector12> o_f = proj_force_obj_frame_(Symbol('x',t-2),
                                                    Symbol('c',(t-2)*4+0),
                                                    Symbol('c',(t-2)*4+1),
                                                    Symbol('c',(t-2)*4+2),
                                                    Symbol('c',(t-2)*4+3),
                                                    //w_cpts[0],w_cpts[1],w_cpts[2],w_cpts[3],
                                                    Symbol('b',t-2));
    // get contact point from contact frame
    Expression<Vector12> o_cpt = proj_cpt_obj_frame_(Symbol('x',t-2),
                                                     Symbol('c',(t-2)*4+0),
                                                     Symbol('c',(t-2)*4+1),
                                                     Symbol('c',(t-2)*4+2),
                                                     Symbol('c',(t-2)*4+3));
    //Expression<Vector12> o_f = 
    //Expression<Vector12> o_cpt = o_f;
    Vector6_ net_f_tau= net_force_torque_(o_f,o_cpt);
    */
    Vector6_ net_f_tau= net_force_torque_(Symbol('b',t-2),Symbol('c',t-2));

    if(!fix_inertia && !linear_parameters)
    {
      Vector6_ err_expression =
        create_symbolic_dynamics_mfix(Symbol('W',l_step),Symbol('R',l_step),
                                      Symbol('x',t-2),
                                      Symbol('x',t-1),
                                      Symbol('x',t),
                                      time_data[t-2],time_data[t-1],time_data[t],
                                      g_vec,net_f_tau,Vector1().setConstant(0.155));
      graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

    }
    else if(fix_inertia)

    {
      Vector6_ err_expression =
        create_symbolic_linear_dynamics(w_i,r_p,
                                        Symbol('x',t-2),
                                        Symbol('x',t-1),
                                        Symbol('x',t),
                                        time_data[t-2],time_data[t-1],time_data[t],
                                        g_vec,net_f_tau);

      graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

      
    }
    else if(linear_parameters)
    {
      Vector6_ err_expression =
        create_symbolic_linear_dynamics(Symbol('W',0),Symbol('R',0),
                                        Symbol('x',t-2),
                                        Symbol('x',t-1),
                                        Symbol('x',t),
                                        time_data[t-2],time_data[t-1],time_data[t],
                                        g_vec,net_f_tau);

      graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

    }
    

    
    if(t>2 and t%1==0 and false)
    {
      /*
      Expression<Vector6> obj_acc  = variable_dt_jerk_(Symbol('x',t-3),
                                                       Symbol('x',t-2),
                                                       Symbol('x',t-1),Symbol('x',t),
                                                       time_data[t-3],
                                                       time_data[t-2],
                                                       time_data[t-1],time_data[t]);

      //
       graph.add(ExpressionFactor<Vector6>(smoothAccModel,Vector6().setZero(),obj_acc));
      */
       /*
    gtsam::noiseModel::Constrained::shared_ptr smoothForceModel =
      noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1e-3), Vector12().setConstant(5*1e-1));

    Expression<Vector12> bt_force_acc  = variable_dt_jerk_vector_<Vector12>
      (
       Symbol('b',t-3),
       Symbol('b',t-2),
       Symbol('b',t-1),
       Symbol('b',t),
       time_data[t-3],
       time_data[t-2],
       time_data[t-1],
       time_data[t]); 
       graph.add(ExpressionFactor<Vector12>(smoothForceModel,Vector12().setZero(),bt_force_acc));
    }
       */
    }
  }
  t_steps_++;
  //old_time_step=time_step; 

  return true;
}

bool poseSmooth::update_so3_graph(const gtsam::Pose3 &x_t, 
                                            const vector<gtsam::Vector> &bt_data,  
                                            const gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;


    // add bt priors:
  add_bt_mm(bt_data,t);
  // add object priors:
  add_pose_mm(x_t,t);

  
  time_data.push_back(time_step);
  obj_poses_.emplace_back(x_t);

  //Vector3 r_p;
  //r_p.setZero();
  //;
  //w_i <<1.3,0.1,0.1,0.1,0.25,0.25,0.25;
  //w_i <<0.8,0.01,0.01,0.01,0.025,0.025,0.025;

  // at first timestep, make velocity and accel zero
  if(t==0)
  { 

    // create global w
    initialEstimate.insert(Symbol('R',0),Rot3()); 

    initialEstimate.insert(Symbol('W',0),w_i); 


    Vector7_ phys_expression = physics_constraint_linear_(Symbol('W',0));
    
    graph.add(ExpressionFactor<Vector7>(l7Model,Vector7().setZero(),phys_expression));


  } 


  else if(t>1)
  {

    
    Vector6_ net_f_tau= net_force_torque_(Symbol('b',t-2),Symbol('c',t-2));
    Vector6_ err_expression =
      create_symbolic_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                               Symbol('x',t-2),
                               Symbol('x',t-1),
                               Symbol('x',t),
                               time_data[t-2],time_data[t-1],time_data[t],
                               //Symbol('T',t-1),Symbol('T',t),
                               g_vec,net_f_tau);
    
    graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));
    // add pose smoothing:
    /*
    Expression<Vector6> obj_acc  = variable_dt_acc_(Symbol('x',t-2),
                                                    Symbol('x',t-1),Symbol('x',t),
                                                    time_data[t-2],
                                                    time_data[t-1],time_data[t]);

    //
    graph.add(ExpressionFactor<Vector6>(smoothAccModel,Vector6().setZero(),obj_acc));
    */
    // add force smoothing:
    
    gtsam::noiseModel::Constrained::shared_ptr smoothForceModel =
      noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0), Vector12().setConstant(0.0));

    Expression<Vector12> bt_force_acc  = variable_dt_vector_<Vector12>(Symbol('b',t-2),
                                                             Symbol('b',t-1),Symbol('b',t),
                                                             time_data[t-2],
                                                             time_data[t-1],
                                                             time_data[t]);
    graph.add(ExpressionFactor<Vector12>(smoothForceModel,Vector12().setZero(),bt_force_acc));

    gtsam::noiseModel::Constrained::shared_ptr smoothPtModel =
      noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0), Vector12().setConstant(0.0));

    Expression<Vector12> bt_pt_acc  = variable_dt_vector_<Vector12>(Symbol('c',t-2),
                                                             Symbol('c',t-1),Symbol('c',t),
                                                             time_data[t-2],
                                                             time_data[t-1],
                                                             time_data[t]);
    graph.add(ExpressionFactor<Vector12>(smoothPtModel,Vector12().setZero(),bt_pt_acc));

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}

bool poseSmooth::update_linear_graph(const gtsam::Pose3 &x_t, 
                                     const vector<gtsam::Vector> &bt_data,  
                                     const gtsam::Vector3 &g_vec,
                                     const double &time_step)
{
  int t = t_steps_;
    // add bt priors:
  add_bt_mm(bt_data,t);
  // add object priors:
  add_pose_mm(x_t,t);

  // add timestep as a factor
  time_data.push_back(time_step);

  //initialEstimate.insert(Symbol('T',t),time_step); // time_step

  //graph.add(PriorFactor<double>(Symbol('T',t),time_step,T_NM));


  auto model_1 = noiseModel::Constrained::MixedSigmas(Vector3().setConstant(1),Vector3().setConstant(0.5));

  obj_poses_.emplace_back(x_t);
  // at first timestep, make velocity and accel zero
  if(t==0)
  { 

    // create global w
    //initialEstimate.insert(Symbol('R',0),r_prior); 
    initialEstimate.insert(Symbol('R',0),r_p); 

    initialEstimate.insert(Symbol('W',0),w_i); 

        
    //Symbol('P',0) proj_inertia_
    // add prior to global w

    //auto model_2 = noiseModel::Constrained::MixedSigmas(Vector1().setConstant(100.0),Vector1().setConstant(0.1));
    //graph.add(ExpressionFactor<Vector1>(model_2,
    //                                   Vector1().setConstant(4),
    //                                    create_between_linear_inertia(w_i,r_p,
    //                                                                  Symbol('W',0),
    //                                                                  Symbol('R',0))));
    // 
    //initialEstimate.insert(Symbol('W',0),w_i); // mass
    //initialEstimate.insert(Symbol('R',0),Rot3()); // mass
    
    //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));

    Vector7_ phys_expression = physics_constraint_linear_(Symbol('W',0));

    graph.add(ExpressionFactor<Vector7>(l7Model,Vector7().setZero(),phys_expression));

    //graph.add(PhysicsFactor(Symbol('W',0),l7Model));
     
    //graph.add(PriorFactor<Vector3>(Symbol('R',0),r_p,model_1));
    
    //graph.print();
    //cout<<l_step<<endl;

  }
  
  if(t>1)
  {

    //double dt=time_step-old_time_step;
    //cout<<g_vec.transpose()<<endl;
    //Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);
    Vector6_ net_f_tau= net_force_torque_(Symbol('b',t-2),Symbol('c',t-2));

    
    Vector6_ err_expression =
      create_symbolic_linear_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                                      //obj_poses_[t-2], obj_poses_[t-1],obj_poses_[t],
                                      Symbol('x',t-2),Symbol('x',t-1),Symbol('x',t),
                                      //Symbol('T',t-2),Symbol('T',t-1),Symbol('T',t),
                                      time_data[t-2], time_data[t-1],time_data[t],
                                      g_vec,net_f_tau);
   
    graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

         // add pose smoothing:
    Expression<Vector6> obj_acc  = variable_dt_acc_(Symbol('x',t-2),Symbol('x',t-1),Symbol('x',t),
                                                    time_data[t-2], time_data[t-1],time_data[t]);

    //
    graph.add(ExpressionFactor<Vector6>(smoothAccModel,Vector6().setZero(),obj_acc));

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}


bool poseSmooth::update_graph_ft(const gtsam::Pose3 &x_t, 
                                 const vector<gtsam::Vector> &ft_data,  
                                 const gtsam::Vector3 &g_vec,
                                 const double &time_step,
                                 const bool inequality,
                                 const bool geodesic,
                                 const bool linear_parameters,
                                 const bool fix_inertia)
{
  int t = t_steps_;

  time_data.push_back(time_step);

  //initialEstimate.insert(Symbol('T',t),time_step); // time_step

  //graph.add(PriorFactor<double>(Symbol('T',t),time_step,T_NM));

  add_ft_mm(ft_data,t);
  
  // add bt priors:
  //add_bt_mm(bt_data,t);

  // add contact frames priors
  //add_cpt_mm(w_cpts,t);
  // add object priors:
  add_pose_mm(x_t,t);
  

  // at first timestep, make velocity and accel zero
  if(t==0)
  {

    if(!fix_inertia && !linear_parameters)
    {
      initialEstimate.insert(Symbol('R',0),r_i); 
      
      initialEstimate.insert(Symbol('W',0),w_i); 
      
      if(inequality == true)
      {
        
        Vector7_ phys_expression = soft_physics_constraint_(Symbol('W',0));
        
        graph.add(ExpressionFactor<Vector7>(l7Model,Vector7().setZero(),phys_expression));
        // add mass prior factor:
        
        //graph.add(softMassPriorFactor(Symbol('W',0),l7Model,(w_i-w_i*0.1)));
      }
    // get manifo
      if(geodesic == true)
      {
        graph.add(ExpressionFactor<Vector1>(priorGeodesicModel,
                                            Vector1().setConstant(4.0),
                                            create_between_inertia(w_prior,r_prior,
                                                               Symbol('W',0),
                                                                   Symbol('R',0))));
      }
      //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
      //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),r_prior,priorRotModel));
      //graph.print();
      //cout<<l_step<<endl;
      
    }
    else if(linear_parameters)
    {
      cerr<<"Linear parameters..."<<endl;
      initialEstimate.insert(Symbol('R',0),Vector3()); 
      
      initialEstimate.insert(Symbol('W',0),w_i); 
      
    }
  }

  if(t>1)
  {

    // force/torque in robot frame
    Vector6_ net_f_tau= project_force_torque(Symbol('f',t-2),Symbol('n',t-2));


    if(!fix_inertia && !linear_parameters)
    {
      Vector6_ err_expression =
        create_symbolic_dynamics_mfix(Symbol('W',l_step),Symbol('R',l_step),
                                      Symbol('x',t-2),
                                      Symbol('x',t-1),
                                      Symbol('x',t),
                                      time_data[t-2],time_data[t-1],time_data[t],
                                      g_vec,net_f_tau,Vector1().setConstant(0.155));
      graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

    }
    else if(fix_inertia)
    {
      Vector6_ err_expression =
        create_symbolic_linear_dynamics(w_i,r_p,
                                        Symbol('x',t-2),
                                        Symbol('x',t-1),
                                        Symbol('x',t),
                                        time_data[t-2],time_data[t-1],time_data[t],
                                        g_vec,net_f_tau);

      graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

      
    }
    else if(linear_parameters)
    {
      Vector6_ err_expression =
        create_symbolic_linear_dynamics(Symbol('W',0),Symbol('R',0),
                                        Symbol('x',t-2),
                                        Symbol('x',t-1),
                                        Symbol('x',t),
                                        time_data[t-2],time_data[t-1],time_data[t],
                                        g_vec,net_f_tau);

      graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));

    }
    

    if(t>2 and t%1==0 and false)
    {
      //add_pose_smoothing(
      /*
        Expression<Vector6> obj_acc  = variable_dt_jerk_(Symbol('x',t-3),
                                                       Symbol('x',t-2),
                                                       Symbol('x',t-1),Symbol('x',t),
                                                       time_data[t-3],
                                                       time_data[t-2],
                                                       time_data[t-1],time_data[t]);

      //
       graph.add(ExpressionFactor<Vector6>(smoothAccModel,Vector6().setZero(),obj_acc));
      */
       /*
    gtsam::noiseModel::Constrained::shared_ptr smoothForceModel =
      noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1e-3), Vector12().setConstant(5*1e-1));

    Expression<Vector12> bt_force_acc  = variable_dt_jerk_vector_<Vector12>
      (
       Symbol('b',t-3),
       Symbol('b',t-2),
       Symbol('b',t-1),
       Symbol('b',t),
       time_data[t-3],
       time_data[t-2],
       time_data[t-1],
       time_data[t]); 
       graph.add(ExpressionFactor<Vector12>(smoothForceModel,Vector12().setZero(),bt_force_acc));
    }
       */
    }
  }
  t_steps_++;
  //old_time_step=time_step; 

  return true;
}

bool poseSmooth::update_linear_graph_ft(const gtsam::Pose3 &x_t, 
                                     const vector<gtsam::Vector> &ft_data,  
                                     const gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;
  // add bt priors:
  //add_bt_mm(bt_data,t);
  add_ft_mm(ft_data,t);
  // add object priors:
  add_pose_mm(x_t,t);
  

  auto model_1 = noiseModel::Constrained::MixedSigmas(Vector3().setConstant(1),Vector3().setConstant(0.5));

  obj_poses_.emplace_back(x_t);
  // at first timestep, make velocity and accel zero
  if(t==0)
  { 

    initialEstimate.insert(Symbol('R',0),r_p); 

    initialEstimate.insert(Symbol('W',0),w_i); 


  }
  
  if(t>1)
  {

    double dt=time_step-old_time_step;


    Vector6_ net_f_tau= project_force_torque(Symbol('f',t-2),Symbol('n',t-2));
    
    /*
    Vector6_ err_expression =
      create_symbolic_linear_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                                      //obj_poses_[t-2], obj_poses_[t-1],obj_poses_[t],
                                      Symbol('x',t-2),Symbol('x',t-1),Symbol('x',t),
                                      dt,g_vec,net_f_tau);
   
    
    graph.add(ExpressionFactor<Vector6>(physicsErrModel,Vector6().setZero(),err_expression));
    */
    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}


bool poseSmooth::debug_print(Values &estimates)
{
  graph.printErrors(estimates,"Factor errors");
  return true;
}

bool poseSmooth::debug_print()
{
  graph.printErrors(initialEstimate,"Factor errors");
  return true;
}

bool poseSmooth::debug_jacobian()
{
  //debug_print(initialEstimate);
  
  GaussianFactorGraph g_fg = *graph.linearize(initialEstimate);
  std::pair<Matrix,Matrix> d;
  d= g_fg.jacobian();
  Matrix jacobian = d.first;
  //cout<<jacobian<<endl;
  auto factor = g_fg.at(0);
  //factor->print();
  return true; 
}
bool poseSmooth::optimize(Values &results,double &error_after, double &error_before)
{
  //graph.print();
  //graph.printErrors(initialEstimate);
  //graph.print();
  /*
  ISAM2Result result = isam.update(graph,initialEstimate);
  for(int i=0;i<2;++i)
  {
    isam.update();
  }
  result = isam.update();
  results = isam.calculateEstimate();
  error_after=*result.errorAfter;
  initialEstimate.clear();
  graph.resize(0);
  return true;
  */
  //initialEstimate.print("\nInitial Values:\n");
  gtsam::LevenbergMarquardtParams parametersLev;
  parametersLev.setlambdaUpperBound(1e32);
  //parametersLev.lambdaLowerBound = 1e-16;

  parametersLev.setlambdaInitial(1e16);
  parametersLev.maxIterations = 500;
  parametersLev.setVerbosity("ERROR");
  parametersLev.absoluteErrorTol = 1e-6; 
  parametersLev.relativeErrorTol = 0.0;//1e-4;
  parametersLev.useFixedLambdaFactor = false;
  //  parametersLev.diagonalDamping = false;
  parametersLev.lambdaFactor = 2.0;
  
  //parametersLev.setVerbosity("TERMINATION");
  gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);

  results = optimizer.optimize();
  initialEstimate = results;
  error_after = graph.error(results);
  //graph.printErrors(results);

  return true;
  //*/
  //cout<<"Final error: "<<<<endl;
  
  
  //gtsam::DoglegParams par;
  //par.setVerbosity("ERROR");
  //results = DoglegOptimizer(graph, initialEstimate,par).optimize();
  //return true;
  /*
  GaussNewtonParams parameters;
  parameters.setVerbosity("ERROR");
  //parameters.setLinearSolverType("MULTIFRONTAL_QR");
  GaussNewtonOptimizer g_optimizer(graph, initialEstimate, parameters);
  results = g_optimizer.optimize();
  */
  //graph.printErrors(results);

  //std::ofstream out("out.txt"); cout.rdbuf(out.rdbuf());
  //debug_print(results);
  
  //results.print("Final Result:\n");


  return true; 
}
bool poseSmooth::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "w covariance:\n" << marginals.marginalCovariance(Symbol('W', l_step)) << endl;
  return true;
}
bool poseSmooth::update_dynamics_param_graph(const gtsam::Pose3 &x_t,
                                             const gtsam::Pose3 &x_t_1,
                                             const gtsam::Pose3 &x_t_2,
                                             const vector<gtsam::Vector> &bt_data,
                                             const gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;
  

  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 
  
  Vector7 w_i;
  //w_i <<1.3,0.1,0.1,0.1,0.25,0.25,0.25;
  //w_i <<1.0,0.0,0.0,0.0,0.25,0.25,0.25;

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    
    initialEstimate.insert(Symbol('W',0),w_i); // mass
    initialEstimate.insert(Symbol('R',0),Rot3()); // mass
    //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
    //graph.add(PhysicsFactor(Symbol('W',0),l7Model));
    
    //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    //graph.print();
    //cout<<l_step<<endl;

  }

  // add pose factors:
  // prior on object pose:
  //initialEstimate.insert(Symbol('x',t),x_t); // pose


  //graph.add(PriorFactor<Pose3>(Symbol('x',t),x_t,poseMeasureNoiseModel));
  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }
    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),w_i); // ft force
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    }

    double dt=time_step-old_time_step;
    if(t>1)
    {
      /*
      initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // bt location w.r.t object
      initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // bt force

      noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(1e-5));

      graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
      graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_NM));
      
      Vector6_ net_f_tau= net_force_torque_(Symbol('b',t),Symbol('c',t));
      */
      //cout<<g_vec.transpose()<<endl;
      Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);
      /*
      Vector6_ err_expression =
        create_symbolic_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                                 x_t_2,
                                 x_t_1,
                                 x_t,
                                 dt,g_vec,net_f_tau);
      auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));

      graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
      */
    }

    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}

