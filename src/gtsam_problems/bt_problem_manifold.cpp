#include <sysid_manipulation/bt_problem_expressions.hpp>
#define DELTA_T 0.2 //seconds
//#include "gtsam_factors/dynamic_inertial_factors.h"
#include "gtsam_factors/sysid_expressions.h"
#include "gtsam_factors/inequality_factors.h"

//#include <sysid_manipulation/sysid_factors_lib.hpp>

#include <gtsam_unstable/linear/LinearInequality.h>
#include <boost/optional/optional_io.hpp>
// create custom factor:
using namespace gtsam;
using namespace sysid_expr;
poseSmooth::poseSmooth()
{
  
}

// create expression factor : input: x_t_2,x_t_1,x_t, f,contact_points, r_c
Vector6_ poseSmooth::create_dynamics_expression(const Key &w, const Key &R, const Pose3 &x_t_2, const Pose3 &x_t_1, const Pose3 &x_t,
                                    const double &dt, const Vector3 &g, const Vector12 &f, const Vector12 &r_f)
{
  Vector6 f_tau = net_force_torque(f,r_f);
  return create_symbolic_dynamics(w,R,x_t_2,x_t_1,x_t,dt,g,f_tau);
}


bool poseSmooth::initialize_graph()
{
  init_noise_models();
  NonlinearFactorGraph new_graph;
  Values initial_estimate;
  graph =  new_graph;
  initialEstimate=initial_estimate;
  ISAM2Params parameters;

  
  //ISAM2DoglegParams dog_params;  
  //parameters.optimizationParams=dog_params;
  parameters.relinearizeThreshold = 0.1;
  parameters.relinearizeSkip = 1;
  parameters.cacheLinearizedFactors = true;
  parameters.enableDetailedResults = false;
  parameters.evaluateNonlinearError = true;
  //parameters.factorization = gtsam::ISAM2Params::Factorization::CHOLESKY; 
    
  //parameters.factorization = gtsam::ISAM2Params::Factorization::;
  parameters.print();
  ISAM2 new_isam(parameters);
  isam=new_isam;
  t_steps_ = 0;
  l_step = 0;
  l_delta = 1000;
  
  return true;
}

gtsam::Pose3 poseSmooth::forward_sim_pose(const gtsam::Pose3 &x_t_2,
                              const gtsam::Pose3 &x_t_1,
                              const vector<gtsam::Vector> &bt_data,
                              const gtsam::Vector3 &g_vec, const double &dt,
                              const gtsam::Vector7 &w, const gtsam::Rot3 &R)
{
  
  initialEstimate.clear();
  initialEstimate.insert(Symbol('x',0),x_t_1); // mass
  graph.resize(0);
    // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 

  Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);
  
  Vector6_ err_expression =create_symbolic_dynamics(w,R,
                                                    x_t_2,
                                                    x_t_1,
                                                    Symbol('x',0),
                                                    dt,g_vec,net_f_tau);
  
  auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  
  graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
  
  Values results;
  double e1,e2;
  optimize(results,e1,e2);

  
  gtsam::Pose3 x_t = results.at<gtsam::Pose3>(Symbol('x',0));
  
  
  return x_t;
}

gtsam::Pose3 poseSmooth::forward_sim_pose(const gtsam::Pose3 &x_t_2,
                                          const gtsam::Pose3 &x_t_1,
                                          const vector<gtsam::Vector> &bt_data,
                                          const gtsam::Vector3 &g_vec, const double &dt,
                                          const gtsam::Vector7 &w, const gtsam::Vector3 &R)
{
  
  initialEstimate.clear();
  initialEstimate.insert(Symbol('x',0),x_t_1); // mass
  graph.resize(0);
    // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 

  Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);
  
  Vector6_ err_expression =create_symbolic_linear_dynamics(w,R,
                                                    x_t_2,
                                                    x_t_1,
                                                    Symbol('x',0),
                                                    dt,g_vec,net_f_tau);
  
  auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));
  
  graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
  
  Values results;
  double e1,e2;
  optimize(results,e1,e2);

  
  gtsam::Pose3 x_t = results.at<gtsam::Pose3>(Symbol('x',0));
  
  
  return x_t;
}


bool poseSmooth::update_pose_dynamics_graph(const gtsam::Pose3 &x_t, 
                                            const vector<gtsam::Vector> &bt_data,  
                                            const gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;
  

  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];
 
  } 
  
  Vector7 w_i;
  w_i <<1.1,0.001,0.003,0.002,0.015,0.010,0.005;
  Rot3 r_prior(gtsam::Rot3::Ypr(0,0,0));
  //Vector3 r_p;
  //r_p.setZero();

  //;
  //w_i <<1.3,0.1,0.1,0.1,0.25,0.25,0.25;
  //w_i <<0.8,0.01,0.01,0.01,0.025,0.025,0.025;

  // at first timestep, make velocity and accel zero
  if(t==0)
  { 
    Matrix44 P_prior = sysid_expr::proj_inertia(sysid_expr::mass(w_i),
                                                sysid_expr::com(w_i),
                                                sysid_expr::inertia(w_i,r_prior));
    //P_prior.setIdentity();
    // create global w
    initialEstimate.insert(Symbol('P',0),P_prior); 
    //initialEstimate.insert(Symbol('R',0),r_p); 

    //initialEstimate.insert(Symbol('W',0),w_i); 

  auto model_1 = noiseModel::Constrained::MixedSigmas(Vector1().setConstant(1),Vector1().setConstant(1e-1));
        
  //graph.add(ExpressionFactor<Vector1>(model_1,
  //                                      Vector1().setConstant(4),
  //inertia_distance_(P_prior,Symbol('P',0))));
    //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
    //graph.add(PhysicsFactor(Symbol('W',0),l7Model));
     
    //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),r_prior,priorRotModel));
    //graph.print();
    //cout<<l_step<<endl;

  } 
  //t_steps_++;

  //return true;
  // add pose factors:
  // prior on object pose:
  initialEstimate.insert(Symbol('x',t),x_t); // pose


  graph.add(PriorFactor<Pose3>(Symbol('x',t),x_t,poseMeasureNoiseModel));

      initialEstimate.insert(Symbol('c',t),bt_cpts); // bt location w.r.t object
    initialEstimate.insert(Symbol('b',t),bt_forces); // bt force
      
    noiseModel::Constrained::shared_ptr bt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1.0),Vector12().setConstant(1e-4));
      
    noiseModel::Constrained::shared_ptr pt_NM = noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1.0),Vector12().setConstant(1e-4));
      
    graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
    graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,pt_NM));
    

  if(t>1)
  {
    if(t%l_delta==0)
    {
      /*
      l_step+=l_delta;

      //cout<<l_step<<endl;

      initialEstimate.insert(Symbol('R',l_step),Rot3()); 

      initialEstimate.insert(Symbol('W',l_step),w_i); 

      // add between global w and w_t
            
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      
      graph.add(ExpressionFactor<Vector1>(model_1,
                                          Vector1().setConstant(4),
                                          create_between_inertia(w_i,
                                                                 Rot3(),
                                                                 Symbol('W',l_step),
                                                                 Symbol('R',l_step))));
      */
      /*
      graph.add( BetweenFactor<Rot3>(Symbol('R',l_step),
                                     Symbol('R',0),Rot3(),
                                     betweenRotModel));
      
      graph.add( BetweenFactor<Vector7>(Symbol('W',l_step),
                                        Symbol('W',0),Vector7().setConstant(0.0),
                                        betweenIModel));

      */
    }

    double dt=time_step-old_time_step;
    
    Vector6_ net_f_tau= net_force_torque_(Symbol('b',t-2),Symbol('c',t-2));
    
    Vector6_ err_expression =
      create_symbolic_manifold_dynamics(Symbol('P',l_step),
                                        Symbol('x',t-2),
                                        Symbol('x',t-1),
                                        Symbol('x',t),
                                        dt,g_vec,net_f_tau);
    auto model = noiseModel::Constrained::MixedSigmas(Vector6().setConstant(1000.0),Vector6().setConstant(1e-6));
    
    graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
    
    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}


bool poseSmooth::debug_print(Values &estimates)
{
  graph.printErrors(estimates,"Factor errors");
  return true;
}

bool poseSmooth::debug_jacobian()
{
  GaussianFactorGraph g_fg = *graph.linearize(initialEstimate);
  std::pair<Matrix,Matrix> d;
  d= g_fg.jacobian();
  Matrix jacobian = d.first;
  cout<<jacobian<<endl;
  auto factor = g_fg.at(0);
  factor->print();
  return true; 
}
bool poseSmooth::optimize(Values &results,double &error_after, double &error_before)
{
  //graph.print();
  //graph.printErrors(initialEstimate);
  //graph.print();
  /*
  ISAM2Result result = isam.update(graph,initialEstimate);
  for(int i=0;i<2;++i)
  {
    isam.update();
  }
  result = isam.update();
  results = isam.calculateEstimate();
  error_after=*result.errorAfter;
  initialEstimate.clear();
  graph.resize(0);
  return true;
  */
  //initialEstimate.print("\nInitial Values:\n");
  gtsam::LevenbergMarquardtParams parametersLev;
  parametersLev.setlambdaUpperBound(1e32);
  //parametersLev.lambdaLowerBound = 1e-16;

  parametersLev.setlambdaInitial(1e6);
  parametersLev.maxIterations = 1500;
  parametersLev.setVerbosity("ERROR");
  parametersLev.absoluteErrorTol = 1e-6; 
  parametersLev.relativeErrorTol = 1e-6;//1e-4;
  parametersLev.useFixedLambdaFactor = false;
  //parametersLev.diagonalDamping = false;
  parametersLev.lambdaFactor = 2.0;
  
  //parametersLev.setVerbosity("TERMINATION");
  gtsam::LevenbergMarquardtOptimizer optimizer(graph, initialEstimate, parametersLev);

  results = optimizer.optimize();
  initialEstimate = results;
  error_after = graph.error(results);
  return true;
  //*/
  //graph.printErrors(results);
  //cout<<"Final error: "<<<<endl;
  
  
  //gtsam::DoglegParams par;
  //par.setVerbosity("ERROR");
  //results = DoglegOptimizer(graph, initialEstimate,par).optimize();
  //return true;
  /*
  GaussNewtonParams parameters;
  parameters.setVerbosity("ERROR");
  //parameters.setLinearSolverType("MULTIFRONTAL_QR");
  GaussNewtonOptimizer g_optimizer(graph, initialEstimate, parameters);
  results = g_optimizer.optimize();
  */
  //graph.printErrors(results);

  //std::ofstream out("out.txt"); cout.rdbuf(out.rdbuf());
  //debug_print(results);
  
  //results.print("Final Result:\n");


  return true; 
}
bool poseSmooth::get_marginals(Values results)
{
  // Calculate marginal covariances for all poses
  Marginals marginals(graph, results);
  
  // print marginal covariances
  cout << "w covariance:\n" << marginals.marginalCovariance(Symbol('W', l_step)) << endl;
  return true;
}
bool poseSmooth::update_dynamics_param_graph(const gtsam::Pose3 &x_t,
                                             const gtsam::Pose3 &x_t_1,
                                             const gtsam::Pose3 &x_t_2,
                                             const vector<gtsam::Vector> &bt_data,
                                             const gtsam::Vector3 &g_vec, const double &time_step)
{
  int t = t_steps_;
  

  // bt priors:
  Vector12 bt_forces;
  Vector12 bt_cpts;
  Vector12 bt_acc;

  for(int i=0;i<4;++i)
  {
    bt_cpts.segment(3*i,3) = bt_data[i*3];
    bt_forces.segment(3*i,3) = bt_data[i*3+1];
    bt_acc.segment(3*i,3) = bt_data[i*3+2];

  } 
  
  Vector7 w_i;
  //w_i <<1.3,0.1,0.1,0.1,0.25,0.25,0.25;
  w_i <<1.0,0.0,0.0,0.0,0.25,0.25,0.25;

  // at first timestep, make velocity and accel zero
  if(t==0)
  {
    
    initialEstimate.insert(Symbol('W',0),w_i); // mass
    initialEstimate.insert(Symbol('R',0),Rot3()); // mass
    //graph.add(PriorFactor<Vector7>(Symbol('W',0),w_i,priorIModel));
    //graph.add(PhysicsFactor(Symbol('W',0),l7Model));
    
    //graph.add(PriorFactor<Rot3>(Symbol('R',l_step),Rot3(),priorRotModel));
    //graph.print();
    //cout<<l_step<<endl;

  }

  // add pose factors:
  // prior on object pose:
  //initialEstimate.insert(Symbol('x',t),x_t); // pose


  //graph.add(PriorFactor<Pose3>(Symbol('x',t),x_t,poseMeasureNoiseModel));
  
  if(t>0)
  {
    
    if(t%l_delta==0)
    {
      l_step+=l_delta;
    }
    if(t == l_step)
    {
      initialEstimate.insert(Symbol('R',l_step),Rot3()); // ft force
      initialEstimate.insert(Symbol('W',l_step),w_i); // ft force
      //graph.add(PhysicsFactor(Symbol('W',l_step),l7Model));
      graph.add(BetweenFactor<Rot3>(Symbol('R',l_step),Symbol('R',l_step-l_delta),Rot3(),betweenRotModel));
        graph.add(BetweenFactor<Vector7>(Symbol('W',l_step),Symbol('W',l_step-l_delta),Vector7().setConstant(0.0),betweenIModel));

    }

    double dt=time_step-old_time_step;
    if(t>1)
    {
      /*
      initialEstimate.insert(Symbol('c',t),Vector12().setZero()); // bt location w.r.t object
      initialEstimate.insert(Symbol('b',t),Vector12().setZero()); // bt force

      noiseModel::Constrained::shared_ptr bt_NM=noiseModel::Constrained::MixedSigmas(Vector12().setConstant(1000.0),Vector12().setConstant(1e-5));

      graph.add(PriorFactor<Vector12>(Symbol('b',t),bt_forces,bt_NM));
      graph.add(PriorFactor<Vector12>(Symbol('c',t),bt_cpts,bt_NM));
      
      Vector6_ net_f_tau= net_force_torque_(Symbol('b',t),Symbol('c',t));
      */
      //cout<<g_vec.transpose()<<endl;
      Vector6_ net_f_tau= net_force_torque_(bt_forces,bt_cpts);

      Vector6_ err_expression =
        create_symbolic_dynamics(Symbol('W',l_step),Symbol('R',l_step),
                                 x_t_2,
                                 x_t_1,
                                 x_t,
                                 dt,g_vec,net_f_tau);
      auto model = noiseModel::Diagonal::Sigmas(Vector6().setConstant(1e-6));

      graph.add(ExpressionFactor<Vector6>(model,Vector6().setZero(),err_expression));
    }

    

  }
  t_steps_++;
  old_time_step=time_step;

  return true;
}

