// detect biotac blob in camera frame and get contact point of biotac
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <geometry_msgs/PointStamped.h>
static const std::string OPENCV_WINDOW = "Image window";
using namespace std;
class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Subscriber pcd_sub_;
  ros::Publisher contact_pub_;
  cv::Mat inp_image;
  int pix_x=-1,pix_y=-1;
  //sensor_msgs::PointCloud2 
  
public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/camera/rgb/image_raw", 1, &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/biotac_detector/output_video", 1);
    contact_pub_ = nh_.advertise<geometry_msgs::PointStamped>("/biotac_detector/point", 1);
    // subscribe to pointcloud:
    pcd_sub_=nh_.subscribe("/camera/depth_registered/points", 1 , &ImageConverter::pcdCb,this);
    //cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    //cv::destroyWindow(OPENCV_WINDOW);
  }

  bool pixelTo3DPoint(const sensor_msgs::PointCloud2 pCloud, const int u, const int v, geometry_msgs::Point &p)
  {
      // get width and height of 2D point cloud data
      int width = pCloud.width;
      int height = pCloud.height;

      // Convert from u (column / width), v (row/height) to position in array
      // where X,Y,Z data starts
      int arrayPosition = v*pCloud.row_step + u*pCloud.point_step;

      // compute position in array where x,y,z data start
      int arrayPosX = arrayPosition + pCloud.fields[0].offset; // X has an offset of 0
      int arrayPosY = arrayPosition + pCloud.fields[1].offset; // Y has an offset of 4
      int arrayPosZ = arrayPosition + pCloud.fields[2].offset; // Z has an offset of 8

      float X = 0.0;
      float Y = 0.0;
      float Z = 0.0;
      float X_mean = 0.0;
      float Y_mean = 0.0;
      float Z_mean = 0.0;


      
      memcpy(&X, &pCloud.data[arrayPosX], sizeof(float));
      memcpy(&Y, &pCloud.data[arrayPosY], sizeof(float));
      memcpy(&Z, &pCloud.data[arrayPosZ], sizeof(float));
      if(isnan(X) || isnan(Y) || isnan(Z))
      {
        return false;
      }
          
     // put data into the point p
      p.x = X;
      p.y = Y;
      p.z = Z;
      return true;
  }
  void pcdCb(const sensor_msgs::PointCloud2 &msg)
  {
    if(pix_x>-1 && pix_y>-1)
    {
      geometry_msgs::PointStamped bt_contact;

      if(pixelTo3DPoint(msg,pix_x,pix_y,bt_contact.point))
      {
        bt_contact.header.frame_id=msg.header.frame_id;
        
        // get average contact point:
        contact_pub_.publish(bt_contact);
      }
    }
  }
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    // detect biotac:
    inp_image=cv_ptr->image;
    
  }
  void getContact()
  {
        using namespace cv;

    if (inp_image.rows > 60 && inp_image.cols > 60)
    {

      // change to hsv space:
      Mat  frame_HSV, frame_threshold;
      cvtColor(inp_image, frame_HSV, COLOR_BGR2HSV);

      Scalar low_hsv=Scalar(60,100,50);
      Scalar high_hsv=Scalar(95,255,255);
      
      // Detect the object based on HSV Range Values
      inRange(frame_HSV, low_hsv,high_hsv, frame_threshold);
      // erode
      int erosion_elem = 0;
      int erosion_size = 1;
      
      int dilation_elem = 2;
      int dilation_size = 4;
      int const max_elem = 2;
      int const max_kernel_size = 21;
      int erosion_type = 0;
      if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
      else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
      else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }
      Mat element = getStructuringElement( erosion_type,
                                           Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                           Point( erosion_size, erosion_size ) );
      erode( frame_threshold, frame_threshold, element );

      int dilation_type = 0;
      if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
      else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
      else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }
      Mat element_d = getStructuringElement( dilation_type,
                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                       Point( dilation_size, dilation_size ) );
      dilate( frame_threshold, frame_threshold, element_d );

      Moments m = moments(frame_threshold,true);
      Point p(m.m10/m.m00, m.m01/m.m00);
      pix_x=p.x;
      pix_y=p.y;
      //cerr<<pix_x<<" "<<pix_y<<endl;
      // get contact point from point cloud:
      //circle(frame_threshold, p, 5, Scalar(128,0,0), -1);
      cv::SimpleBlobDetector detector;
      Ptr<SimpleBlobDetector> d = SimpleBlobDetector::create();

      // Detect blobs.
      std::vector<cv::KeyPoint> keypoints;
      d->detect(  frame_threshold, keypoints);
        
      // Draw detected blobs as red circles.
      //DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
      cv::Mat im_with_keypoints;
      cv::drawKeypoints(  inp_image , keypoints, im_with_keypoints, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
      
    
      // Update GUI Window
      cv::imshow(OPENCV_WINDOW,frame_threshold);//im_with_keypoints);
      cv::waitKey(3);
    }
    // Output modified video stream
    //image_pub_.publish(cv_ptr->toImageMsg());
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "biotac_detector_node");
  ImageConverter ic;
  ros::Rate loop_rate(100);
  while(ros::ok())
  {
    loop_rate.sleep();
    ros::spinOnce();
    ic.getContact();
  }
  return 0;
}

