#include "PushFactors.h"
#include "PushFactorsSquare.h"
#include "PushFactorsCircle.h"
#include "utilities.h"
#include "io.h"
#include "config_param.h"
#include "noise_generator.h"

#include <gtsam/geometry/Pose2.h>
#include <gtsam/nonlinear/ISAM2.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>

#include <iostream>
#include <fstream>
#include <string>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/filesystem.hpp>

using namespace std;
using namespace gtsam;
using namespace pushsam;


int main(int argc, char** argv) {

    string root_dir = "/home/gtrll/pushGraph-GTSAM/";

    string problem_name;
    string dataset;

    if (argc>=2) {
        try {
            problem_name = argv[1];
            dataset = argv[2];
        } catch (const std::exception& e) {};
    } else {
        problem_name = "wam_test";
    }

    string data_dir = root_dir + "data/wam_probe/" + dataset+"/";

    vector<string> fileList;
    readListFile(&fileList, root_dir+"config/data_lists/dataList_wam_"+dataset+".conf");

    string filename = problem_name+".xml";

    printf("\nOperating on dataset: %s\n", dataset.c_str());

    printf("\nGraph defined in : %s\n\n", filename.c_str());

    string config_file = root_dir + "config/" + filename;
 
    /*============================================================================================*/
    /*===============================  Parameters Set =============================== */
    /*============================================================================================*/

    configParam config;
    config.loadConfigFile(config_file);
    config.printFactors();

    // Experiment param
    expParam * ex = &config.param.exp;

    // GTSAM parameters
    // std::unique_ptr<gtsamParams> gtsam_param_ptr (& config.param.gtsam);
    gtsamParams * gtsam_p = & config.param.gtsam;

    // List of factors (pushsam::Factor type)
    // std::unique_ptr<factorList> factor_list_ptr (& config.param.factor_list);
    factorList * factor_list_ptr = & config.param.factor_list;



    /*============================================================================================*/
    /*===============================   Initialize Distributions =============================== */
    /*============================================================================================*/   
    
    printf("Initializing Distributions... \n\n");


    // if add_noise is true
    default_random_engine rand;

    // zero-mean normal dist.
    noiseGenerator noise_obj_pose(ex->obj_pose_sigmas);
    noiseGenerator noise_ee_pose(ex->ee_pose_sigmas);
    noiseGenerator noise_c_pt(ex->c_pt_sigmas);
    noiseGenerator noise_f_vec(ex->f_vec_sigmas);

    for(auto f = factor_list_ptr->begin(); f != factor_list_ptr->end(); ++f) {

        try {
                
               if (f->dim == 1) {

                    Vector sigma = (Vector(1) << f->sigma).finished();
                    f->cov_ptr = noiseModel::Diagonal::Sigmas(sigma);

                } else if (f->dim == 2) {

                    Vector2 sigmas (f->sigma_x, f->sigma_y);
                    f->cov_ptr = noiseModel::Diagonal::Sigmas(sigmas);

                } else if (f->dim == 3) {

                    Vector3 sigmas (f->sigma_x, f->sigma_y, f->sigma_theta);
                    f->cov_ptr = noiseModel::Diagonal::Sigmas(sigmas);

                } else {
                    printf( "ERROR: Invalid Factor Dimension.: %d \n", f->dim);
                    throw std::exception();
                }
        }
        catch (const std::exception& e) {}

    }

    /*============================================================================================*/
    /*===============================   Iterate Over Experiments =============================== */
    /*============================================================================================*/

    for (size_t d_ind=0; d_ind < fileList.size(); d_ind++){

        string dataName = fileList[d_ind];

        // Output directory
        string output_dir = data_dir+dataName+"/results_isam2/"+problem_name+"/";
        if (!boost::filesystem::exists(output_dir)){
          boost::filesystem::create_directory(output_dir);
        }

        printf("Loading Exp.: %s \n\n",dataName.c_str());

        vector<vector<float>> data;
        string data_file = data_dir + dataName+"/pushsam_data/"+dataName + ".txt"; 
        readDataFile(&data, data_file);

        cout << data_file << endl;

        int i_start = config.param.exp.start_index;
        int i_end = data.size()-1;

        // size_t traj_len = i_end-i_start+1;

        /*============================================================================================*/
        /*===============================   Graph Construction  =============================== */
        /*============================================================================================*/

        /** 
          Graph Variables:
          * X : Oject pose
          * F : End-Eff. pose
          * p : Contact point
          * f : Force vector
          */

        Values init_values, opt_values;    

        NonlinearFactorGraph graph;

        ISAM2 isam;

        bool initialized = false;
        uint current_batch_size = 0;

        printf("Constructing graph. \n\n");


        for (size_t i=i_start; i <= (uint) i_end; i++) {


            /*********************** Variables *******************************************/
            
            // Object pose

            Vector3 xo_m;

            if (config.use_obj_pose) {
                
                xo_m = Vector3(data[i][4],data[i][5],data[i][6]);

                // Initialize
                if (ex->add_noise) {
                    auto xo_n= noise_obj_pose.gen_noise(rand);
                    Vector3 xo_init (xo_m[0] + xo_n[0], xo_m[1] + xo_n[1], xo_m[2] + xo_n[2]);
                    init_values.insert(Symbol('X',i), xo_init);

                } else {
                    init_values.insert(Symbol('X',i), xo_m);
                }
            }

            // End-Eff. pose

            Vector3 xf_m;

            if (config.use_ee_pose) {
                
                xf_m = Vector3(data[i][7],data[i][8],data[i][9]);

                // Initialize
                if (ex->add_noise) {
                    auto xf_n = noise_ee_pose.gen_noise(rand);
                    Vector3 xf_init (xf_m[0] + xf_n[0], xf_m[1] + xf_n[1], xf_m[2] + xf_n[2]);
                    init_values.insert(Symbol('F',i), xf_init);

                } else {
                    init_values.insert(Symbol('F',i), xf_m);
                }
            }

            // Contact point

            Point2 pc_m;

            if (config.use_c_pt) {
                
                pc_m = Point2(data[i][0],data[i][1]);

                // Initialize
                if (ex->add_noise) {
                    auto pc_n = noise_c_pt.gen_noise(rand);
                    Point2 pc_init (pc_m.x() + pc_n[0], pc_m.y() + pc_n[1]);
                    init_values.insert(Symbol('p',i), pc_init);

                } else {
                    init_values.insert(Symbol('p',i), pc_m);
                }
            }

            // Force vector
            
            Point2 f_m;

            if (config.use_f_vec) {
                
                f_m = Point2(data[i][2],data[i][3]);

                // Initialize
                if (ex->add_noise) {
                    auto f_n = noise_f_vec.gen_noise(rand);
                    Point2 f_init (f_m.x() + f_n[0], f_m.y() + f_n[1]);
                    init_values.insert(Symbol('f',i), f_init);

                } else {
                    init_values.insert(Symbol('f',i), f_m);
                }
            }



            /*********************** Factors *******************************************/

            for(auto f = factor_list_ptr->begin(); f != factor_list_ptr->end(); ++f) {


                const char * f_type = f->type.c_str();
                const char * f_name = f->name.c_str();

                /*********************** Priors *******************************************/

                if ( strcmp(f_type, "prior") == 0 ) {

                    if (i == (uint) i_start) {

                        if ( strcmp(f_name, "obj_pose") == 0 ) {
                            graph.add(PriorFactor<Vector3>(Symbol('X',i), xo_m, f->cov_ptr));
                        }

                        else if ( strcmp(f_name, "ee_pose") == 0 ) 
                            graph.add(PriorFactor<Vector3>(Symbol('F',i), xf_m, f->cov_ptr));

                        else {
                            printf("Error: prior factor of unknown name: \"%s\" \n", f_name);
                            return 0;
                        }
                    }

                } 

                /*********************** Measurements Factors  *******************************************/

                else if ( strcmp(f_type, "measurement") == 0 ) {

                    if ( strcmp(f_name, "obj_pose") == 0 ) 
                        graph.add(PriorFactor<Vector3>(Symbol('X',i), xo_m, f->cov_ptr));
                    
                    else if ( strcmp(f_name, "ee_pose") == 0 ) 
                        graph.add(PriorFactor<Vector3>(Symbol('F',i), xf_m, f->cov_ptr));
                    
                    else if ( strcmp(f_name, "c_pt") == 0 ) 
                        graph.add(PriorFactor<Point2>(Symbol('p',i), pc_m, f->cov_ptr));
                    
                    else if ( strcmp(f_name, "f_vec") == 0 ) 
                        graph.add(PriorFactor<Point2>(Symbol('f',i), f_m, f->cov_ptr));

                    else {
                        printf("Error: measurement factor of unknown name: \"%s\" \n", f_name);
                        return 0;
                    }
                } 

                /*********************** Geometric Factors  *******************************************/

                else if ( strcmp(f_type, "sdf") == 0 ) {

                    if ( strcmp(f_name, "sdf") == 0 )
                        graph.add(RectCircleSDFFactor(Symbol('X',i), Symbol('F',i), f->cov_ptr, ex->obj_length, ex->obj_width, ex->ee_radius,ex->eps) );
                                
                    else {
                        printf("Error: sdf factor of unknown name: \"%s\" \n", f_name);
                        return 0;
                    }
                }
                else if ( strcmp(f_type, "contact") == 0 ) {

                    if ( strcmp(f_name, "obj_pose") == 0 ) {
                        graph.add(RectSurfaceContactFactor(Symbol('X',i),Symbol('p',i), f->cov_ptr, ex->obj_length, ex->obj_width));
                    }
                    else if ( strcmp(f_name, "ee_pose") == 0 ) 
                        graph.add(SurfaceContactCircleFactor(Symbol('F',i),Symbol('p',i), f->cov_ptr, ex->ee_radius));

                    else {
                        printf("Error: contact factor of unknown name: \"%s\" \n", f_name);
                        return 0;
                    }
                } 

                /*********************** Transition Factors  *******************************************/


                else if ( strcmp(f_type, "zero_velocity") == 0 ) {

                    if (i > (uint) i_start ) {

                        if ( strcmp(f_name, "obj_pose") == 0 ) 
                            graph.add(ZeroVelFactorVector(Symbol('X',i-1), Symbol('X',i), f->cov_ptr));    
                            // graph.add(BetweenFactor<Vector3>(Symbol('X',i), Symbol('X',i+1), Vector3(0,0,0), f->cov_ptr));    

                        else if ( strcmp(f_name, "ee_pose") == 0 ) 
                            graph.add(ZeroVelFactorVector(Symbol('F',i-1), Symbol('F',i), f->cov_ptr));    

                        else if ( strcmp(f_name, "c_pt") == 0 ) 
                            graph.add(ZeroVelFactorPoint(Symbol('p',i-1), Symbol('p',i), f->cov_ptr));    

                        else if ( strcmp(f_name, "f_vec") == 0 ) 
                            graph.add(ZeroVelFactorPoint(Symbol('f',i-1), Symbol('f',i), f->cov_ptr));    

                        else {
                            printf("Error: zero-velocity factor of unknown name: \"%s\" \n", f_name);
                            return 0;
                        }
                    }
                }
                else if ( strcmp(f_type, "constant_velocity") == 0 )  {

                    if (i > (uint) i_start + 1 ) {

                        if ( strcmp(f_name, "obj_pose") == 0 ) 
                            graph.add(ConstVelFactorVector(Symbol('X',i-2), Symbol('X',i-1), Symbol('X',i), f->cov_ptr, ex->dt));

                        else if ( strcmp(f_name, "ee_pose") == 0 ) 
                            graph.add(ConstVelFactorVector(Symbol('F',i-2), Symbol('F',i-1), Symbol('F',i), f->cov_ptr, ex->dt));
                        
                        else if ( strcmp(f_name, "c_pt") == 0 ) 
                            graph.add(ConstVelFactorPoint(Symbol('p',i-2), Symbol('p',i-1), Symbol('p',i), f->cov_ptr, ex->dt));

                        else if ( strcmp(f_name, "f_vec") == 0 ) 
                            graph.add(ConstVelFactorPoint(Symbol('f',i-2), Symbol('f',i-1), Symbol('f',i), f->cov_ptr, ex->dt));

                        else {
                            printf("Error: constant-velocity factor of unknown name: \"%s\" \n", f_name);
                            return 0;
                        }
                    }
                }

                else if ( strcmp(f_type, "quasi_static") == 0 ) {
                    
                    if (i > (uint) i_start && i < (uint) i_end )  {

                        if ( strcmp(f_name, "quasi_static") == 0 ) 
                            graph.add(DynamicsQuasiStaticFactor(Symbol('X',i-1), Symbol('X',i), Symbol('p',i), Symbol('f',i), f->cov_ptr, ex->c_param, ex->dt));

                        else {
                            printf("Error: constant-velocity factor of unknown name: \"%s\" \n", f_name);
                            return 0;
                        }
                    }
                } 
                else {
                    printf("Error: factor of unknown type \"%s\" \n", f_type);
                    return 0;
                }

            } // factor loop


            /*============================================================================================*/
            /*=================================  Incremental Optimization ============================================ */
            /*============================================================================================*/

            if (((i == (uint) gtsam_p->start_len) || (current_batch_size == (uint) gtsam_p->batch_size))
                || (i == (uint) i_end) ){
              

                if (!initialized && i!=0) {
                    LevenbergMarquardtParams parameters;
                    parameters.setVerbosityLM(gtsam_p->verbosityLM);
                    parameters.setAbsoluteErrorTol(gtsam_p->absoluteErrorTol);
                    parameters.setRelativeErrorTol(gtsam_p->relativeErrorTol);
                    parameters.setlambdaInitial(gtsam_p->lambdaInitial);
                    parameters.setlambdaFactor(gtsam_p->lambdaFactor);
                    parameters.setlambdaUpperBound(gtsam_p->lambdaUpperBound);
                    parameters.setMaxIterations(gtsam_p->maxIter);

                    LevenbergMarquardtOptimizer optimizer(graph, init_values, parameters);
                    init_values = optimizer.optimize();
                    initialized = true;
                }

                isam.update(graph, init_values);
                for (size_t j=1; j < (uint) gtsam_p->iter_per_step; j++ ) {
                    isam.update();
                }

                opt_values = isam.calculateEstimate();

                init_values.clear();
                graph.resize(0);
                current_batch_size = 0;

                // Save values
                char buff[500];
                sprintf(buff, "mean_values_i%04d.csv", (int) i);
                std::string out_csv = buff;
                writeValues (opt_values, output_dir+out_csv, i_start, i, true);

                sprintf(buff, "object_cov_i%04d.csv", (int) i);
                out_csv = buff;
                saveCovFile_iSAM2<Vector3>( opt_values, isam, 'X', output_dir+out_csv, i_start, i);
                
                sprintf(buff, "tip_cov_i%04d.csv", (int) i);
                out_csv = buff;
                saveCovFile_iSAM2<Vector3>( opt_values, isam, 'F', output_dir+out_csv, i_start, i);
  
                sprintf(buff, "force_cov_i%04d.csv", (int) i);
                out_csv = buff;
                saveCovFile_iSAM2<Point2>(  opt_values, isam, 'f', output_dir+out_csv, i_start, i);

                sprintf(buff, "contact_cov_i%04d.csv", (int) i);
                out_csv = buff;
                saveCovFile_iSAM2<Point2>(  opt_values, isam, 'p', output_dir+out_csv, i_start, i);

            }            

            current_batch_size += 1;

        } // traj loop


        /*============================================================================================*/
        /*==================================== Output =========================================== */
        /*============================================================================================*/
        // Copy config to exp directory
        config.copyConfigFile(config_file, output_dir+"/config.xml");
    } // fileList loop
}
