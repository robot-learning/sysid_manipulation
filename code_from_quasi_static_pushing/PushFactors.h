  #pragma once

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/base/Vector.h>
#include <gtsam/geometry/Pose2.h>
#include <math.h>

using namespace std;
// using namespace gtsam;

namespace pushsam {

class PushPoseMeasFactor: public gtsam::NoiseModelFactor1<gtsam::Pose2> {

private:
  // measurement
  double mx_, my_, mtheta_;

public:

  PushPoseMeasFactor(gtsam::Key poseKey, const gtsam::Pose2 m, gtsam::SharedNoiseModel model) :
      gtsam::NoiseModelFactor1<gtsam::Pose2>(model, poseKey), mx_(m.x()), my_(m.y()), mtheta_(m.theta()) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Pose2& p, boost::optional<gtsam::Matrix&> H = boost::none) const {
  
    if (H) *H = gtsam::eye(3,3);
    
    // return error vector
    return (gtsam::Vector3() << p.x() - mx_, 
                                p.y() - my_, 
                                p.theta() - mtheta_ ).finished();
  }

};

class PushPointMeasFactor: public gtsam::NoiseModelFactor1<gtsam::Point2> {

private:
  // measurement
  gtsam::Point2 m_;

public:

  PushPointMeasFactor(gtsam::Key pointKey, const gtsam::Point2 m, gtsam::SharedNoiseModel model) :
      gtsam::NoiseModelFactor1<gtsam::Point2>(model, pointKey), m_(m) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Point2& p, boost::optional<gtsam::Matrix&> H = boost::none) const {
  
    if (H) *H = gtsam::eye(2,2);
    
    // return error vector
    return (gtsam::Vector2() << p.x() - m_.x(),
                                p.y() - m_.y()).finished();
  }

};

class ForceVectMeasFactor: public gtsam::NoiseModelFactor2<gtsam::Point2, gtsam::Vector3> {
/** 
  * Inputs: 
  *   Point2 f: force vec. in global frame 
  *   Vector3 x : ref. frame (ex. end-effector pose)
  *
  * Definition:
  *   Measure force of contact and convert to world reference frame
  */

private:
  // measurement (in end eff. frame)
  const gtsam::Vector2 f_m_;

public:

  ForceVectMeasFactor(gtsam::Key forceKey, gtsam::Key poseKey, gtsam::SharedNoiseModel model, const gtsam::Point2 f_m) :
      gtsam::NoiseModelFactor2<gtsam::Point2, gtsam::Vector3>(model, forceKey, poseKey), f_m_(f_m) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Point2& f, const gtsam::Vector3& x, 
                                  boost::optional<gtsam::Matrix&> Hf = boost::none,
                                  boost::optional<gtsam::Matrix&> Hx = boost::none) const {
  
  gtsam::Point2 f_m_w;

  if (Hf || Hx) {

    gtsam::Matrix dfmw_df, dfmw_dx;

    f_m_w = transform2D(f_m_, x, dfmw_df, dfmw_dx);

    if (Hf) *Hf = gtsam::eye(2,2) - dfmw_df;

    if (Hx) *Hx = -1.*dfmw_dx;
    
  } else {

    f_m_w = transform2D(f_m_, x);

  }

    // return error vector
    return (gtsam::Vector2() << f.x() - f_m_w.x(),
                                f.y() - f_m_w.y()).finished();
  }


  //  Transform force to world reference frame
  gtsam::Point2 transform2D(const gtsam::Point2& fp, const gtsam::Vector3& x, 
                          gtsam::OptionalJacobian<2,2> Hf = boost::none, 
                          gtsam::OptionalJacobian<2,3> Hx = boost::none)  const {

    gtsam::Vector2 f = fp.vector();
    gtsam::Vector2 x_tr (x[0],x[1]);
    gtsam::Vector2 f_w;

    // Rotation matrix
    gtsam::Matrix22 R ;
    R = (gtsam::Matrix22() << cos(x[2]), -1.*sin(x[2]), sin(x[2]), cos(x[2])).finished();

    f_w = R * f;

    if (Hf) *Hf = R;

    if (Hx) {
      gtsam::Matrix22 dR_dth;
      dR_dth = (gtsam::Matrix22() << -1.*sin(x[2]), -1.*cos(x[2]), cos(x[2]), -1.*sin(x[2])).finished();

      *Hx = (gtsam::Matrix23() << gtsam::zeros(2,2), dR_dth * f).finished();
    }

    return gtsam::Point2(f_w[0], f_w[1]);
  }


};


class DynamicsQuasiStaticFactor: public gtsam::NoiseModelFactor4<gtsam::Vector3, gtsam::Vector3, gtsam::Point2, gtsam::Point2> {
/** 
  * Inputs: 
  *   Vector3 x_t1 : object pose at time t1. 
  *   Vector3 x_t2 : object pose at time t2.
  *   Point2 pc  : contact point.
  *   Point2 f  : force vector.
  *
  * Definition:
  *   Error on No-Slip dynamics constraint. 
  */

private:

  double cParam_ ;
  double dt_ ;

public:

  DynamicsQuasiStaticFactor(gtsam::Key objKey_1, gtsam::Key objKey_2, gtsam::Key contKey, gtsam::Key forceKey, gtsam::SharedNoiseModel model,
                         const double cParam, const double dt) :
      gtsam::NoiseModelFactor4<gtsam::Vector3, gtsam::Vector3, gtsam::Point2, gtsam::Point2>(model, objKey_1, objKey_2, contKey, forceKey), 
                              cParam_(cParam), dt_(dt) {}


  virtual ~DynamicsQuasiStaticFactor() {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Vector3& x_t1,
                              const gtsam::Vector3& x_t2,
                              const gtsam::Point2& pc,
                              const gtsam::Point2& f,
                              boost::optional<gtsam::Matrix&> Hx_t1 = boost::none,
                              boost::optional<gtsam::Matrix&> Hx_t2 = boost::none,
                              boost::optional<gtsam::Matrix&> Hpc = boost::none,
                              boost::optional<gtsam::Matrix&> Hf = boost::none) const {

    // Moment on object
    double m;
 
    gtsam::Vector3 dx =  (x_t2 - x_t1) / dt_;
   // Finite-difference velocity
    gtsam::Vector2 v = (gtsam::Vector2() << dx[0], dx[1]).finished();

    // Angular velocity
    double omega = dx[2];

    // Moment: cross(po,f)

    m = (pc.x() - x_t2[0])*f[1] - (pc.y() - x_t2[1])*f[0];


    if (Hx_t1 || Hx_t2 || Hpc || Hf) {

      // Intermediate derivatives
      gtsam::Matrix dh_dm, dm_dp_t2, dm_dpc, dm_df;

      // moment derivatives
      dh_dm = v; 
      dm_dp_t2 = (gtsam::Matrix12() << -f[1], f[0]).finished();
      dm_dpc = (gtsam::Matrix12() << f[1], -f[0]).finished();
      dm_df = (gtsam::Matrix12() << -pc.y() + x_t2[1], pc.x() - x_t2[0]).finished(); 

      if (Hx_t1) {

        gtsam::Matrix Hp_t1 = -1.0 * m / dt_ * gtsam::eye(2,2)  ;         
         
        gtsam::Matrix Htheta_t1 = pow(cParam_,2) / dt_ * f;
        
        // Concat. matrices
        *Hx_t1 = (gtsam::Matrix23() << Hp_t1, Htheta_t1).finished();
      }      

      if (Hx_t2) {

        // gtsam::Matrix Hp_t2 = m / dt_ * gtsam::eye(2,2) ;

        gtsam::Matrix Hp_t2 = m / dt_ * gtsam::eye(2,2) + dh_dm * dm_dp_t2 ;    

        gtsam::Matrix Htheta_t2 = -1.0 * pow(cParam_,2) / dt_ * f;

        *Hx_t2 = (gtsam::Matrix23() << Hp_t2, Htheta_t2).finished();
      }

      if (Hpc) *Hpc = dh_dm * dm_dpc;
    
      if (Hf) *Hf = -1.0*pow(cParam_,2) * omega * gtsam::eye(2,2) + dh_dm * dm_df; 


    } 

    /* Print Err values for tuning */
    gtsam::Vector2 err;
    err = (gtsam::Vector2() << v * m - pow(cParam_,2) * f * omega).finished();
    // cout << err[0] << endl;
    // cout << err[1] << endl;

    return v * m - pow(cParam_,2) * f * omega; 

  }

};

class DynamicsLinearFactor: public gtsam::NoiseModelFactor4<gtsam::Pose2, gtsam::Pose2, gtsam::Pose2, gtsam::Point2> {
/** 
  * Inputs: 
  *   Pose2 x1 : object pose at time t-1. 
  *   Pose2 x2 : object pose at time t.
  *   Pose2 x3 : object pose at time t+1.
  *   Point2 f  : force vector.
  *
  * Definition:
  *   Error on No-Slip dynamics constraint. 
  */

private:

  double dt_ ; 
  double m_ ; // mass
  double mu_ ; // coeff. of friction
  double g_ = 9.81; 

public:

  DynamicsLinearFactor(gtsam::Key objKey_1, gtsam::Key objKey_2, gtsam::Key objKey_3, gtsam::Key forceKey, gtsam::SharedNoiseModel model,
                       const double dt, const double m, const double mu) :
                      gtsam::NoiseModelFactor4<gtsam::Pose2, gtsam::Pose2, gtsam::Pose2, gtsam::Point2>(model, objKey_1, objKey_2, objKey_3, forceKey), 
                            dt_(dt), m_(m), mu_(mu)  {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Pose2& x1,
                              const gtsam::Pose2& x2,
                              const gtsam::Pose2& x3,
                              const gtsam::Point2& f,
                              boost::optional<gtsam::Matrix&> Hx1 = boost::none,
                              boost::optional<gtsam::Matrix&> Hx2 = boost::none,
                              boost::optional<gtsam::Matrix&> Hx3 = boost::none,
                              boost::optional<gtsam::Matrix&> Hf = boost::none) const {

    // disp
    gtsam::Point2 dx12 = x2.t() - x1.t();

    // accel.
    gtsam::Point2 a = 1./ m_ * f - mu_ * g_ * dx12 / dx12.norm();

    if (Hx1 || Hx2 || Hx3 || Hf) {

        // Intermediate derivatives
        gtsam::Matrix da_dx1, da_dx2, da_dx3, da_df;

        da_dx1 = mu_ * g_ * (  1.0 / dx12.norm() * gtsam::eye(2,2) - 1.0 / pow(dx12.norm(),3) * dx12 * dx12.transpose() );
        da_dx2 = mu_ * g_ * ( -1.0 / dx12.norm() * gtsam::eye(2,2) + 1.0 / pow(dx12.norm(),3) * dx12 * dx12.transpose() );
        da_df = 1./ mu_ * gtsam::eye(2,2);

        if (Hx1) *Hx1 = gtsam::eye(2,2) - dt_ * gtsam::eye(2,2) * da_dx1;
        if (Hx2) *Hx2 = -2.0 * gtsam::eye(2,2) - dt_ * gtsam::eye(2,2) * da_dx2;
        if (Hx3) *Hx3 = gtsam::eye(2,2);
        if (Hf) *Hf = -1.0 * dt_ * gtsam::eye(2,2) * da_df;
    }

    return x3.t() - 2*x2.t() + x1.t() - a * dt_ * dt_ ;

  }

};
class ConstVelFactorVector: public gtsam::NoiseModelFactor3<gtsam::Vector3, gtsam::Vector3, gtsam::Vector3> {
/** 
  * Inputs: 
  *   Pose2 x_t1 : object pose at time t1. 
  *   Pose2 x_t2 : object pose at time t2.
  *   Pose2 x_t3 : object pose at time t3.
  *
  * Definition:
  *   Constant velocity constraint with finite-difference on poses. 
  */
private:

  double dt_ ; 

public:

  ConstVelFactorVector(gtsam::Key poseKey_1, gtsam::Key poseKey_2, gtsam::Key poseKey_3, gtsam::SharedNoiseModel model,
                const double dt) : 
                gtsam::NoiseModelFactor3<gtsam::Vector3, gtsam::Vector3, gtsam::Vector3>(model, poseKey_1, poseKey_2, poseKey_3) ,
                dt_(dt) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Vector3& x_t1,
                              const gtsam::Vector3& x_t2,
                              const gtsam::Vector3& x_t3,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const {
    
    if (H1) *H1 =  1.0*gtsam::eye(3,3) * (1.0/dt_);
    if (H2) *H2 = -2.0*gtsam::eye(3,3) * (1.0/dt_);
    if (H3) *H3 =  1.0*gtsam::eye(3,3) * (1.0/dt_);

    /* Print Err values for tuning */
    // double x = -x_t1.x() + 2*x_t2.x() - x_t3.x();
    // cout << x << endl;
    // double y = -x_t1.y() + 2*x_t2.y() - x_t3.y();
    // cout << y << endl;
    // double theta = -x_t1.theta() + 2*x_t2.theta() - x_t3.theta();
    // cout << theta << endl;

    // return error vector
    return  (1.0/dt_) * ( x_t1 - 2*x_t2 + x_t3) ;
  }

};

class ConstVelFactorPose: public gtsam::NoiseModelFactor3<gtsam::Pose2, gtsam::Pose2, gtsam::Pose2> {
/** 
  * Inputs: 
  *   Pose2 x_t1 : object pose at time t1. 
  *   Pose2 x_t2 : object pose at time t2.
  *   Pose2 x_t3 : object pose at time t3.
  *
  * Definition:
  *   Constant velocity constraint with finite-difference on poses. 
  */
private:

  double dt_ ; 

public:

  ConstVelFactorPose(gtsam::Key poseKey_1, gtsam::Key poseKey_2, gtsam::Key poseKey_3, gtsam::SharedNoiseModel model,
                const double dt) : 
                gtsam::NoiseModelFactor3<gtsam::Pose2, gtsam::Pose2, gtsam::Pose2>(model, poseKey_1, poseKey_2, poseKey_3) ,
                dt_(dt) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Pose2& x_t1,
                              const gtsam::Pose2& x_t2,
                              const gtsam::Pose2& x_t3,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const {
    
    if (H1) *H1 =  1.0*gtsam::eye(3,3) * (1.0/dt_);
    if (H2) *H2 = -2.0*gtsam::eye(3,3) * (1.0/dt_);
    if (H3) *H3 =  1.0*gtsam::eye(3,3) * (1.0/dt_);

    /* Print Err values for tuning */
    // double x = -x_t1.x() + 2*x_t2.x() - x_t3.x();
    // cout << x << endl;
    // double y = -x_t1.y() + 2*x_t2.y() - x_t3.y();
    // cout << y << endl;
    // double theta = -x_t1.theta() + 2*x_t2.theta() - x_t3.theta();
    // cout << theta << endl;

    // return error vector
    return  (1.0/dt_) * (gtsam::Vector3() << x_t1.x() - 2*x_t2.x() + x_t3.x(),
                                             x_t1.y() - 2*x_t2.y() + x_t3.y(),
                                             x_t1.theta() - 2*x_t2.theta() + x_t3.theta()).finished();

  }

};

class ConstVelFactorPoint: public gtsam::NoiseModelFactor3<gtsam::Point2, gtsam::Point2, gtsam::Point2> {
/** 
  * Inputs: 
  *   Point2 x_t1 : point at time t1. 
  *   Point2 x_t2 : point at time t2.
  *   Point2 x_t3 : point at time t3.
  *
  * Definition:
  *   Constant velocity constraint with finite-difference on point locations. 
  */
private:
  double dt_;

public:

  ConstVelFactorPoint(gtsam::Key pointKey_1,  gtsam::Key pointKey_2, gtsam::Key pointKey_3, gtsam::SharedNoiseModel model,
                      const double dt) :
                    gtsam::NoiseModelFactor3<gtsam::Point2, gtsam::Point2, gtsam::Point2>(model, pointKey_1, pointKey_2, pointKey_3),
                    dt_(dt) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Point2& x_t1,
                              const gtsam::Point2& x_t2,
                              const gtsam::Point2& x_t3,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none,
                              boost::optional<gtsam::Matrix&> H3 = boost::none) const {
    
    if (H1) *H1 = -1.0*gtsam::eye(2,2) * (1.0/dt_);
    if (H2) *H2 =  2.0*gtsam::eye(2,2) * (1.0/dt_);
    if (H3) *H3 = -1.0*gtsam::eye(2,2) * (1.0/dt_);


    /* Print Err values for tuning */
    // double x = -x_t1.x() + 2*x_t2.x() - x_t3.x();
    // cout << x << endl;
    // double y = -x_t1.y() + 2*x_t2.y() - x_t3.y();
    // cout << y << endl;


    // return error vector
    return (1.0/dt_) * (gtsam::Vector2() << -x_t1.x() + 2*x_t2.x() - x_t3.x(),
                                            -x_t1.y() + 2*x_t2.y() - x_t3.y()).finished();

  }

};

class ZeroVelFactorVector: public gtsam::NoiseModelFactor2<gtsam::Vector3, gtsam::Vector3> {
/** 
  * Inputs: 
  *   Pose2 x_t1 : object pose at time t1. 
  *   Pose2 x_t2 : object pose at time t2.
  *
  * Definition:
  *   Zero-velocity constraint between consecutive poses. 
  */

public:

  ZeroVelFactorVector(gtsam::Key poseKey_1,  gtsam::Key poseKey_2, gtsam::SharedNoiseModel model) :
      gtsam::NoiseModelFactor2<gtsam::Vector3, gtsam::Vector3>(model, poseKey_1, poseKey_2) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Vector3& x_t1,
                              const gtsam::Vector3& x_t2,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none) const {
    
    if (H1) *H1 = -1.0*gtsam::eye(3,3);
    if (H2) *H2 =  gtsam::eye(3,3);

    // return error vector
    return  x_t2 - x_t1 ;
  }

};

/* TODO: Make templated version of this class */
class ZeroVelFactorPose: public gtsam::NoiseModelFactor2<gtsam::Pose2, gtsam::Pose2> {
/** 
  * Inputs: 
  *   Pose2 x_t1 : object pose at time t1. 
  *   Pose2 x_t2 : object pose at time t2.
  *
  * Definition:
  *   Zero-velocity constraint between consecutive poses. 
  */

public:

  ZeroVelFactorPose(gtsam::Key poseKey_1,  gtsam::Key poseKey_2, gtsam::SharedNoiseModel model) :
      gtsam::NoiseModelFactor2<gtsam::Pose2, gtsam::Pose2>(model, poseKey_1, poseKey_2) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Pose2& x_t1,
                              const gtsam::Pose2& x_t2,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none) const {
    
    if (H1) *H1 = -1.0*gtsam::eye(3,3);
    if (H2) *H2 =  gtsam::eye(3,3);

    // return error vector
    return (gtsam::Vector3() << x_t2.x()- x_t1.x(),
                                x_t2.y()- x_t1.y(),
                                x_t2.theta() - x_t1.theta()).finished();
  }

};

class ZeroVelFactorPoint: public gtsam::NoiseModelFactor2<gtsam::Point2, gtsam::Point2> {
/** 
  * Inputs: 
  *   Point2 x_t1 : object pose at time t1. 
  *   Point2 x_t2 : object pose at time t2.
  *
  * Definition:
  *   Zero-velocity constraint between consecutive positions. 
  */
public:

  ZeroVelFactorPoint(gtsam::Key pointKey_1,  gtsam::Key pointKey_2, gtsam::SharedNoiseModel model) :
      gtsam::NoiseModelFactor2<gtsam::Point2, gtsam::Point2>(model, pointKey_1, pointKey_2) {}

  // error function
  gtsam::Vector evaluateError(const gtsam::Point2& x_t1,
                              const gtsam::Point2& x_t2,
                              boost::optional<gtsam::Matrix&> H1 = boost::none,
                              boost::optional<gtsam::Matrix&> H2 = boost::none) const {
    
    if (H1) *H1 = -1.0*gtsam::eye(2,2);
    if (H2) *H2 =  gtsam::eye(2,2);

    // return error vector
    return (gtsam::Vector2() << x_t2.x()- x_t1.x(),
                                x_t2.y()- x_t1.y()).finished();
  }

};


} // namespace 


 