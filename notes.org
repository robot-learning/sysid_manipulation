
* TODO
- Setup lie group
- Setup visualization of forces and object

* Lie group for inertial parameters:
- Create manifold in gtsam
- Add functions to get linear parameters and also to project them to manifold
- Test if the manifold remains stable
